import React, { useState, useEffect, useRef } from "react";
import swal from "sweetalert";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";
import { useSubheader } from "../../../../../_metronic/layout";
import CosignDjpFooter from "./CosignDjpFooter";
import CosignDjpForm from "./CosignDjpForm";
import {
  getPenyusunanById,
  saveCosignUnit,
  updateCosignUnit,
  uploadFile,
  getCosignUnitByIdCosign,
  uploadFileNew
} from "../../../Evaluation/Api";

function CosignDjpEdit({
  history,
  match: {
    params: { id_cosign, id }
  }
}) {
  const initValues = {
    nama_unit: "",
    no_surat: "",
    tgl_surat: "",
    file: ""
  };

  const [title, setTitle] = useState("");
  const [content, setContent] = useState();
  const [val, setVal] = useState([]);

  const suhbeader = useSubheader();

  useEffect(() => {
    let _title = id_cosign
      ? "Edit Jawaban Cosign Unit DJP"
      : "Tambah Jawaban Cosign Unit DJP";

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id_cosign, suhbeader]);

  useEffect(() => {
    getPenyusunanById(id).then(({ data }) => {
      setVal(data);
    });
  }, [id]);

  useEffect(()=> {
if(id_cosign){
  getCosignUnitByIdCosign(id_cosign).then(({ data }) => {
    setContent({
      nama_unit: data.nama_unit,
      no_surat: data.no_surat,
      tgl_surat: data.tgl_surat,
      file_upload: data.file_permintaan,
      id_minta_jawab: data.id_minta_jawab
    })
  })
}
  },[id_cosign])
  const btnRef = useRef();

  const handleBack = () => {
    history.push(`/compose/process/detils/${id}/cosign-djp`);
  };

  const saveProposal = values => {
    if (!id_cosign) {
      const formData = new FormData();
      formData.append("file", values.file);
      uploadFileNew(formData).then(({ data }) => {
        saveCosignUnit(
          data.message,
          values.id_minta_jawab,
          id,
          values.nama_unit,
          values.no_surat,
          values.tgl_surat
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push(`/compose/process/detils/${id}/cosign-djp`);
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push(`/compose/process/detils/${id}/cosign-djp/add`);
            });
          }
        });
      });
    } else {
      if (values.file.name) {
        const formData = new FormData();
        formData.append("file", values.file);
        uploadFile(formData).then(({ data }) => {
          updateCosignUnit(
            id_cosign,
            data.message,
            values.id_minta_jawab,
            id,
            values.nama_unit,
            values.no_surat,
            values.tgl_surat
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
                history.push(`/compose/process/detils/${id}/cosign-djp`);
              });
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                history.push(`/compose/process/detils/${id}/cosign-djp/add`);
              });
            }
          });
        });
      } else {
        updateCosignUnit(
          id_cosign,
          values.file_upload,
          values.id_minta_jawab,
          id,
          values.nama_unit,
          values.no_surat,
          values.tgl_surat
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push(`/compose/process/detils/${id}/cosign-djp`);
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push(`/compose/process/detils/${id}/cosign-djp/add`);
            });
          }
        });
      }
    }
  };

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="form-group row">
            <label className="col-xl-3 col-lg-3 col-form-label">
              No Penyusunan
            </label>
            <div className="col-lg-9 col-xl-6" style={{ marginTop: "10px" }}>
              {val.no_penyusunan}
            </div>
          </div>
        </>
        <div className="mt-5">
          <CosignDjpForm
            initValues={content || initValues}
            btnRef={btnRef}
            id={id}
            saveProposal={saveProposal}
          />
        </div>
      </CardBody>

      <CardFooter style={{ borderTop: "none" }}>
        <CosignDjpFooter backAction={handleBack} btnRef={btnRef} />
      </CardFooter>
    </Card>
  );
}

export default CosignDjpEdit;
