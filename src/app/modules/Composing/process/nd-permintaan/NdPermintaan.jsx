import React from "react";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../../_metronic/_partials/controls";
import NdPermintaanTable from "./NdPermintaanTable";

function NdPermintaan({
    history,
    match: {
      params: { id }
    }
  }) {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Surat Permintaan Jawaban"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <NdPermintaanTable id={id} />
        </CardBody>
      </Card>
    </>
  );
}

export default NdPermintaan;
