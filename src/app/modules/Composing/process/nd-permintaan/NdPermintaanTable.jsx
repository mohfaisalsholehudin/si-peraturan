/* Library */
import React, { useEffect, useState } from "react";
import swal from "sweetalert";
import { useSelector } from "react-redux";
import { useHistory, Route } from "react-router-dom";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";

/* Helpers */
import { Pagination } from "../../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses
} from "../../../../../_metronic/_helpers";
import { toAbsoluteUrl } from "../../../../../_metronic/_helpers";
import * as columnFormatters from "../../../../helpers/column-formatters";
import { deleteMintaJawab, getMintaJawab, getPenyusunanById } from "../../../Evaluation/Api";
import NdPermintaanOpen from "./NdPermintaanOpen";
// import ResearchOpen from "./ResearchOpen";

/* Component */
// import ProposalOpen from "./ProposalOpen";
// import ProposalReject from "./ProposalReject";

/* Utility */
function NdPermintaanTable({ id }) {
  const history = useHistory();
  const [content, setContent] = useState([]);
  const { status } = useSelector(state => state.auth);

  const add = () =>
    history.push(`/compose/process/detils/${id}/nd-permintaan/add`);
  const editAction = id_mintajawab =>
    history.push(
      `/compose/process/detils/${id}/nd-permintaan/${id_mintajawab}/edit`
    );
  const showDialog = id_minta_jawab =>
    history.push(
      `/compose/process/detils/${id}/nd-permintaan/${id_minta_jawab}/open`
    );
  const deleteAction = id_lhr => {
    swal({
      title: "Apakah Anda Yakin?",
      text: "Klik OK untuk melanjutkan",
      icon: "warning",
      buttons: true,
      dangerMode: true
    }).then(willDelete => {
      if (willDelete) {
        deleteMintaJawab(id_lhr).then(({ status }) => {
          if (status === 200) {
            swal("Berhasil", "Data berhasil dihapus", "success").then(() => {
              history.push("/dashboard");
              history.replace(`/compose/process/detils/${id}/nd-permintaan`);
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push("/dashboard");
              history.replace(`/compose/process/detils/${id}/nd-permintaan`);
            });
          }
        });
      }
      // else {
      //   swal("Your imaginary file is safe!");
      // }
    });
  };
  useEffect(() => {
    getMintaJawab(id).then(({ data }) => {
      setContent(data);
    });
  }, [id]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "nomor_surat",
      text: "No Surat",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "tgl_surat",
      text: "Tgl Surat",
      sort: true,
      formatter: columnFormatters.DateFormatterComposeProcessNdPermintaan,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "perihal",
      text: "Perihal",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "tujuan",
      text: "Tujuan Surat",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "file",
      text: "File",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.FileColumnFormatterComposeProcessNdPermintaan,
      headerSortingClasses
      // classes: "text-center pr-0",
      // headerClasses: "text-center pr-0",
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
        status === "Proses Penyusunan"
          ? columnFormatters.ActionsColumnFormatterComposeProcessNdPermintaan
          : columnFormatters.ActionsColumnFormatterComposeProcessNdPermintaanJustView,
      formatExtraData: {
        //   apply: applyAction,
        openEditDialog: editAction,
        openDeleteDialog: deleteAction,
        showDetil: showDialog
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "nomor_surat",
    pageNumber: 1,
    pageSize: 5
  };
  const defaultSorted = [{ dataField: "nomor_surat", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_minta_jawab"
                  data={content}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div
                          className="col-lg-6 col-xl-6 mb-3"
                          style={{ textAlign: "right" }}
                        >
                          {status === "Proses Penyusunan" ||
                            status === "Penelitian Cosign Unit/KL Es4" ||
                            status === "Cosign Jawaban" ||
                            status === "Cosign Sahli" ||
                            status === "Cosign Dirjen" ? (
                            <button
                              type="button"
                              className="btn btn-primary mx-3"
                              style={{
                                float: "right"
                              }}
                              onClick={add}
                            >
                              <i className="fa fa-plus"></i>
                              Tambah
                            </button>
                          ) : null}
                          <button
                            type="button"
                            className="btn btn-light mx3"
                            style={{
                              float: "right"
                            }}
                            onClick={() =>
                              history.push(`/compose/process/${id}/detil`)
                            }
                          >
                            <i className="fa fa-arrow-left"></i>
                            Kembali
                          </button>
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>

      <Route path="/compose/process/detils/:id/nd-permintaan/:id_minta_jawab/open">
        {({ history, match }) => (
          <NdPermintaanOpen
            show={match != null}
            id={match && match.params.id}
            id_minta_jawab={match && match.params.id_minta_jawab}
            after={false}
            onHide={() => {
              history.push(`/compose/process/detils/${id}/nd-permintaan`);
            }}
            onRef={() => {
              history.push(`/compose/process/detils/${id}/nd-permintaan`);
            }}
          />
        )}
      </Route>
    </>
  );
}

export default NdPermintaanTable;
