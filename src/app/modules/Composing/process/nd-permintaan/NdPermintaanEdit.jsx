import React, { useState, useEffect, useRef } from "react";
import swal from "sweetalert";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";
import { useSubheader } from "../../../../../_metronic/layout";
import NdPermintaanFooter from "./NdPermintaanFooter";
import NdPermintaanForm from "./NdPermintaanForm";
import {
  getMintaJawabByIdMintaJawab,
  getPenyusunanById,
  saveMintaJawab,
  updateMintaJawab,
  uploadFile,
  uploadFileNew
} from "../../../Evaluation/Api";
import { DateFormat } from "../../../../helpers/DateFormat";

function NdPermintaanEdit({
  history,
  match: {
    params: { id_mintajawab, id }
  }
}) {
  const [title, setTitle] = useState("");
  const [content, setContent] = useState();
  const [val, setVal] = useState([]);

  const suhbeader = useSubheader();
  useEffect(() => {
    let _title = id_mintajawab
      ? "Edit ND Permintaan Jawaban"
      : "Tambah ND Permintaan Jawaban";

    setTitle(_title);
    suhbeader.setTitle(_title);
    if(id_mintajawab){
      getMintaJawabByIdMintaJawab(id_mintajawab).then(({data})=> {
        setContent({
          perihal: data.perihal,
          nomor_surat: data.nomor_surat,
          tgl_surat: data.tgl_surat,
          tujuan: data.tujuan,
          jenis_instansi: data.jenis_instansi,
          file_upload: data.file_permintaan
        })
      })
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id_mintajawab, suhbeader]);


  useEffect(() => {
    getPenyusunanById(id).then(({ data }) => {
      setVal(data);
    });
  }, [id]);

  const initValues = {
    perihal: "",
    nomor_surat: "",
    tgl_surat: "",
    tujuan: "",
    jenis: "cosign unit",
    jenis_instansi: "",
    file: ""
  };

  const btnRef = useRef();

  const saveProposal = values => {
    if (!id_mintajawab) {
      const formData = new FormData();
      formData.append("file", values.file);
      uploadFileNew(formData).then(({ data }) => {
        saveMintaJawab(
          data.message,
          id,
          values.jenis,
          values.nomor_surat,
          values.perihal,
          values.tgl_surat,
          values.tujuan,
          values.jenis_instansi
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push(`/compose/process/detils/${id}/nd-permintaan`);
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push(`/compose/process/detils/${id}/nd-permintaan/add`);
            });
          }
        });
      });
    } else {
      if (values.file.name) {
        const formData = new FormData();
        formData.append("file", values.file);
        uploadFile(formData).then(({ data }) => {
          updateMintaJawab(
            id_mintajawab,
            data.message,
            id,
            values.jenis,
            values.nomor_surat,
            values.perihal,
            values.tgl_surat,
            values.tujuan,
            values.jenis_instansi
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
                history.push(`/compose/process/detils/${id}/nd-permintaan`);
              });
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                history.push(`/compose/process/detils/${id}/nd-permintaan/add`);
              });
            }
          });
        });
      } else {
        updateMintaJawab(
          id_mintajawab,
          values.file_upload,
          id,
          values.jenis,
          values.nomor_surat,
          values.perihal,
          values.tgl_surat,
          values.tujuan,
          values.jenis_instansi
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push(`/compose/process/detils/${id}/nd-permintaan`);
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push(`/compose/process/detils/${id}/nd-permintaan/add`);
            });
          }
        });
      }
    }
  };

  const handleBack = i => {
    // if(id_surat){
    history.push(`/compose/process/detils/${i}/nd-permintaan`);
    // }
    // setShow(false)
  };

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="form-group row">
            <label className="col-xl-3 col-lg-3 col-form-label">
              No Penyusunan
            </label>
            <div className="col-lg-9 col-xl-6" style={{ marginTop: "10px" }}>
              {val.no_penyusunan}
            </div>
          </div>
          <div className="form-group row">
            <label className="col-xl-3 col-lg-3 col-form-label">
              Tgl Penyusunan
            </label>
            <div className="col-lg-9 col-xl-6" style={{ marginTop: "10px" }}>
              {DateFormat(val.tgl_penyusunan)}
            </div>
          </div>
        </>
        <div className="mt-5">
          <NdPermintaanForm
            initValues={content || initValues}
            btnRef={btnRef}
            saveProposal={saveProposal}
          />
        </div>
      </CardBody>

      <CardFooter style={{ borderTop: "none" }}>
        <NdPermintaanFooter backAction={handleBack} btnRef={btnRef} id={id} />
      </CardFooter>
    </Card>
  );
}

export default NdPermintaanEdit;
