import React, { useState, useEffect } from "react";
import { Field, Formik, Form } from "formik";
import Select from "react-select";
import * as Yup from "yup";
import {
  DatePickerField,
  Input,
  Textarea,
  Select as Sel,
} from "../../../../helpers/";
import { unitKerja } from "../../../../references/UnitKerja";
import { getJenisSurat } from "../../../Evaluation/Api";
import CustomFileInput from "../../../../helpers/form/CustomFileInput";

function SuratUndanganForm({ initValues, btnRef, saveSurat }) {
  const [kantor, setKantor] = useState([]);
  const [jnsSurat, setJnsSurat] = useState([]);

  const SuratEditSchema = Yup.object().shape({
    nomor_surat: Yup.string()
      .min(2, "Minimum 2 symbols")
      // .max(50, "Maximum 50 symbols")
      .required("No Surat is required"),
    tgl_surat: Yup.mixed()
      .nullable(false)
      .required("Tanggal Surat is required"),
    perihal: Yup.string()
      .min(2, "Minimum 2 symbols")
      // .max(50, "Maximum 50 symbols")
      .required("Perihal is required"),
    unit_penerbit: Yup.string()
      .min(2, "Minimum 2 symbols")
      // .max(50, "Maximum 50 symbols")
      .required("Unit Penerbit is required"),
    unit_penerima: Yup.string()
      .min(2, "Minimum 2 symbols")
      // .max(50, "Maximum 50 symbols")
      .required("Unit Penerima is required"),
    jenis_instansi: Yup.number().required("Jenis Instansi is required"),
    jenis_instansi_2: Yup.number().required("Jenis Instansi is required"),
    id_jenis: Yup.string().required("Jenis Surat Is Required"),
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        (value) => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        // (value) => value && SUPPORTED_FORMATS.includes(value.type)
        value => value && SUPPORTED_FORMATS.some(a=> value.type.includes(a))
      ),
  });

  const FILE_SIZE = 500000000;
  const SUPPORTED_FORMATS = [
    "application/pdf",
    "application/x-rar-compressed",
    "application/octet-stream",
    "application/zip",
    "application/octet-stream",
    "application/x-zip-compressed",
    "multipart/x-zip",
    "application/vnd.rar",
    "application/rar",
    "application/x-rar"
  ];

  useEffect(() => {
    getJenisSurat(7).then(({ data }) => {
      setJnsSurat(data);
    });
    unitKerja.map((data) => {
      setKantor((kantor) => [
        ...kantor,
        {
          label: data.nm_UNIT_KERJA,
          value: data.kd_unit_kerja,
          alamat: data.alamat,
        },
      ]);
    });
  }, []);
  const asosiasi = () => {
    return (
      <>
        <div className="form-group row">
          <Field
            name="unit_penerbit"
            component={Input}
            placeholder="Unit Penerbit"
            label="Unit Penerbit"
          />
        </div>
      </>
    );
  };

  const asosiasiPenerima = () => {
    return (
      <>
        <div className="form-group row">
          <Field
            name="unit_penerima"
            component={Input}
            placeholder="Unit Penerima"
            label="Unit Penerima"
          />
        </div>
      </>
    );
  };

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={initValues}
        validationSchema={SuratEditSchema}
        onSubmit={(values) => {
          // console.log(values);
          saveSurat(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
          handleBlur,
          handleChange,
          errors,
          touched,
          values,
          isValid,
        }) => {
          const handleChangeUnitKerja = (val) => {
            setFieldValue("unit_penerbit", val.label);
          };
          const handleChangeUnitKerjaPenerima = (val) => {
            setFieldValue("unit_penerima", val.label);
          };
          return (
            <>
              <Form className="form form-label-right">
                {/* FIELD NO SURAT */}
                <div className="form-group row">
                  <Field
                    name="nomor_surat"
                    component={Input}
                    placeholder="No Surat"
                    label="No Surat"
                  />
                </div>
                {/* FIELD TANGGAL SURAT */}
                <div className="form-group row">
                  <DatePickerField name="tgl_surat" label="Tanggal Surat" />
                </div>
                {/* FIELD JENIS SURAT */}
                <div className="form-group row">
                  <Sel name="id_jenis" label="Jenis Surat">
                    <option>Pilih Jenis Surat</option>
                    {jnsSurat.map((data) => (
                      <option key={data.id} value={data.id}>
                        {data.nama}
                      </option>
                    ))}
                  </Sel>
                </div>
                {/* FIELD PERIHAL */}
                <div className="form-group row">
                  <Field
                    name="perihal"
                    component={Textarea}
                    placeholder="Perihal"
                    label="Perihal"
                  />
                </div>
                {/* FIELD Instansi Penerbit */}
                <div className="form-group row">
                  <Sel name="jenis_instansi" label="Jenis Instansi Penerbit">
                    <option>Pilih Jenis Instansi Penerbit</option>
                    <option value="1">Asosiasi / KL</option>
                    <option value="2">Unit DJP</option>
                  </Sel>
                </div>
                {values.jenis_instansi ? (
                  <>
                    {values.jenis_instansi === "1" ? (
                      asosiasi()
                    ) : (
                      <>
                        <div className="form-group row">
                          <label className="col-xl-3 col-lg-3 col-form-label">
                            Unit Penerbit
                          </label>
                          <div className="col-lg-9 col-xl-6">
                            <Select
                              options={kantor}
                              onChange={(value) => handleChangeUnitKerja(value)}
                              value={kantor.filter(
                                (data) => data.label === values.unit_penerbit
                              )}
                            />
                          </div>
                        </div>
                      </>
                    )}
                  </>
                ) : null}
                <div className="form-group row">
                  <Sel name="jenis_instansi_2" label="Jenis Instansi Penerima">
                    <option>Pilih Jenis Instansi Penerima</option>
                    <option value="1">Asosiasi / KL</option>
                    <option value="2">Unit DJP</option>
                  </Sel>
                </div>
                {values.jenis_instansi_2 ? (
                  <>
                    {values.jenis_instansi_2 === "1" ? (
                      asosiasiPenerima()
                    ) : (
                      <>
                        <div className="form-group row">
                          <label className="col-xl-3 col-lg-3 col-form-label">
                            Unit Penerima
                          </label>
                          <div className="col-lg-9 col-xl-6">
                            <Select
                              options={kantor}
                              onChange={(value) =>
                                handleChangeUnitKerjaPenerima(value)
                              }
                              value={kantor.filter(
                                (data) => data.label === values.unit_penerima
                              )}
                            />
                          </div>
                        </div>
                      </>
                    )}
                  </>
                ) : null}
                {values.nomor_surat ? (
                  <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Upload File
                    </label>
                    <div className="col-lg-9 col-xl-6">
                      <Field
                        name="file"
                        component={CustomFileInput}
                        title="Select a file"
                        label="File"
                        style={{ display: "flex" }}
                      />
                    </div>
                  </div>
                ) : null}
                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default SuratUndanganForm;
