import React, { useEffect, useState, useRef } from "react";
import axios from "axios";
import EmailEditor from "react-email-editor";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import swal from "sweetalert";
import CustomFileInput from "../../../../helpers/form/CustomFileInput";
import CustomLampiranInput from "../../../../helpers/form/CustomLampiranInput";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../../_metronic/_partials/controls";
import {
  saveDraftComposingCosign,
  uploadFile,
  getDraft,
  updateDraftComposingCosign,
  getPenyusunanById,
  updateStatusPenyusunan,
  uploadFileNew
} from "../../../Evaluation/Api";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import Editor from "ckeditor5-custom-build/build/ckeditor";
import DocumentEditor from "../../../../helpers/editor/DocumentEditor";
// import "../../proposal/process/drafting/decoupled.css";
const {BACKEND_URL} = window.ENV;

function TindakLanjutDirektoratDraftEdit({
  history,
  match: {
    params: { id, id_draft, name }
  }
}) {
  const [draft, setDraft] = useState("");
  const [show, setShow] = useState(false);
  const [content, setContent] = useState();
  // const [content, setContent] = useState({
  //   file: "",
  //   lampiran: ""
  // });
  const initialValues = {
    file: "",
    lamp: "",
  };

  const validationSchema = Yup.object().shape({
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        value => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        // value => value && SUPPORTED_FORMATS.includes(value.type)
        value => value && SUPPORTED_FORMATS.some(a=> value.type.includes(a))
      )
  });
  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  useEffect(() => {

    if(id_draft){
      getDraft(id_draft).then(({ data }) => {
        setContent({
          body_draft: data.body_cosign ? data.body_cosign : data.body_draft,
          jns_draft: data.jns_draft,
          file_upload: data.fileupload_cosign ? data.fileupload_cosign : data.fileupload,
          lampiran: data.filelampiran_cosign ? data.filelampiran_cosign : data.filelampiranr
        });
      });
    }
  }, [id_draft]);

  const saveDraft = values => {
    if (!id_draft) {
      if(!values.lampiran){
        const formData = new FormData();
        formData.append("file", values.file);
        uploadFileNew(formData).then((data_1) => {
          saveDraftComposingCosign(draft, name, id, data_1.data.message).then(
            ({ status }) => {
              if (status === 201 || status === 200) {
                swal("Berhasil", "Data berhasil disimpan", "success").then(
                  () => {
                    history.push(
                      `/compose/process/detils/${id}/tindak-lanjut-direktorat`
                    );
                  }
                );
              } else {
                swal("Gagal", "Data gagal disimpan", "error").then(() => {
                  history.push(
                    `/compose/process/detils/${id}/tindak-lanjut-direktorat`
                  );
                });
              }
            }
          );
          // const formLampiran = new FormData();
          // formLampiran.append("file", values.lampiran);
          // uploadFile(formLampiran).then(data_2 => {

          // });
        });
      } else {
        const formData = new FormData();
        formData.append("file", values.file);
        uploadFileNew(formData).then(data_1 => {
          const formLampiran = new FormData();
          formLampiran.append("file", values.lampiran);
          uploadFileNew(formLampiran).then(data_2 => {
              saveDraftComposingCosign(
                draft,
                name,
                id,
                data_1.data.message,
                data_2.data.message
              ).then(({ status }) => {
                if (status === 201 || status === 200) {
                  swal("Berhasil", "Data berhasil disimpan", "success").then(
                    () => {
                      history.push(
                        `/compose/process/detils/${id}/tindak-lanjut-direktorat`
                      );
                    }
                  );
                } else {
                  swal("Gagal", "Data gagal disimpan", "error").then(() => {
                    history.push(
                      `/compose/process/detils/${id}/tindak-lanjut-direktorat`
                    );
                  });
                }
              });
          }).catch((e)=> {
              console.log(e)
          })
        });

      }
    } else {
      if (values.file.name) {
        const formData = new FormData();
        formData.append("file", values.file);
        uploadFileNew(formData).then(data_1 => {
          if (!values.lampiran.name) {
              updateDraftComposingCosign(
                draft,
                id_draft,
                data_1.data.message
              ).then(({ status }) => {
                if (status === 201 || status === 200) {
                  swal("Berhasil", "Data berhasil disimpan", "success").then(
                    () => {
                      history.push(
                        `/compose/process/detils/${id}/tindak-lanjut-direktorat`
                      );
                    }
                  );
                } else {
                  swal("Gagal", "Data gagal disimpan", "error").then(() => {
                    history.push(
                      `/compose/process/detils/${id}/tindak-lanjut-direktorat`
                    );
                  });
                }
              });
          } else {
            const formLampiran = new FormData();
            formLampiran.append("file", values.lampiran);
            uploadFileNew(formLampiran).then(data_2 => {
                updateDraftComposingCosign(
                  draft,
                  id_draft,
                  data_1.data.message,
                  data_2.data.message
                ).then(({ status }) => {
                  if (status === 201 || status === 200) {
                    swal("Berhasil", "Data berhasil disimpan", "success").then(
                      () => {
                        history.push(
                          `/compose/process/detils/${id}/tindak-lanjut-direktorat`
                        );
                      }
                    );
                  } else {
                    swal("Gagal", "Data gagal disimpan", "error").then(() => {
                      history.push(
                        `/compose/process/detils/${id}/tindak-lanjut-direktorat`
                      );
                    });
                  }
                });
            });
          }
        });
      } else if (values.lampiran.name) {
        const formLampiran = new FormData();
        formLampiran.append("file", values.lampiran);
        uploadFileNew(formLampiran).then(data_1 => {
          if (!values.file.name) {
              updateDraftComposingCosign(
                draft,
                id_draft,
                values.file_upload,
                data_1.data.message
              ).then(({ status }) => {
                if (status === 201 || status === 200) {
                  swal("Berhasil", "Data berhasil disimpan", "success").then(
                    () => {
                      history.push(
                        `/compose/process/detils/${id}/tindak-lanjut-direktorat`
                      );
                    }
                  );
                } else {
                  swal("Gagal", "Data gagal disimpan", "error").then(() => {
                    history.push(
                      `/compose/process/detils/${id}/tindak-lanjut-direktorat`
                    );
                  });
                }
              });
          } else {
            const formData = new FormData();
            formData.append("file", values.file);
            uploadFileNew(formData).then(data_2 => {
                updateDraftComposingCosign(
                  draft,
                  id_draft,
                  data_2.data.message,
                  data_1.data.message
                ).then(({ status }) => {
                  if (status === 201 || status === 200) {
                    swal("Berhasil", "Data berhasil disimpan", "success").then(
                      () => {
                        history.push(
                          `/compose/process/detils/${id}/tindak-lanjut-direktorat`
                        );
                      }
                    );
                  } else {
                    swal("Gagal", "Data gagal disimpan", "error").then(() => {
                      history.push(
                        `/compose/process/detils/${id}/tindak-lanjut-direktorat`
                      );
                    });
                  }
                });
            });
          }
        }).catch((error) => {
            alert(error)
        })
      } else {
          updateDraftComposingCosign(
            draft,
            id_draft,
            values.file_upload,
            values.lampiran
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
                history.push(
                  `/compose/process/detils/${id}/tindak-lanjut-direktorat`
                );
              });
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                history.push(
                  `/compose/process/detils/${id}/tindak-lanjut-direktorat`
                );
              });
            }
          });
      }
    }
  };

  const updateStatus = () => {
    getPenyusunanById(id).then(({ data }) => {
      updateStatusPenyusunan(id, 1, data.kd_kantor).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push("/dashboard");
            history.replace(
              `/compose/process/detils/${id}/tindak-lanjut-direktorat`
            );
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/dashboard");
            history.replace(
              `/compose/process/detils/${id}/tindak-lanjut-direktorat`
            );
          });
        }
      });
    });
  };

  return (
    <Card>
      <CardHeader
        title="Edit Draft Peraturan"
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
        <DocumentEditor
        content={content}
        setDraft={setDraft}
        />
          <div className="mt-4">
            <Formik
              enableReinitialize={true}
              initialValues={content || initialValues}
              validationSchema={validationSchema}
              onSubmit={values => {
                saveDraft(values);
                // console.log(values);
              }}
            >
              {({ handleSubmit, values, setFieldValue, setFieldTouched, errors }) => {
                     const handleClick = () => {
                      if (!values.file) {
                        if (values.file_upload) {
                          setFieldValue("file", {
                            file: values.file_upload,
                            size: 5000000,
                            type: [
                              "application/pdf",
                              "application/x-rar-compressed",
                              "application/octet-stream",
                              "application/zip",
                              "application/octet-stream",
                              "application/x-zip-compressed",
                              "multipart/x-zip",
                              "application/vnd.rar",
                            ],
                          });
                        } else if (errors) {
                          setFieldTouched("file", true);
                        }
                      }
                    };
                return (
                  <Form className="form form-label-right">
                      <>
                        <div className="form-group row">
                          <label className="col-xl-3 col-lg-3 col-form-label">
                            Upload File
                          </label>
                          <div className="col-lg-9 col-xl-6">
                            <Field
                              name="file"
                              component={CustomFileInput}
                              title="Select a file"
                              label="File"
                              style={{ display: "flex" }}
                            />
                          </div>
                        </div>
                      </>
                      {draft ?
                    <div className="form-group row">
                      <label className="col-xl-3 col-lg-3 col-form-label">
                        Upload Lampiran
                      </label>
                      <div className="col-lg-9 col-xl-6">
                        <Field
                          name="lampiran"
                          component={CustomLampiranInput}
                          title="Select a file"
                          label="Lampiran"
                          style={{ display: "flex" }}
                        />
                      </div>
                    </div>
                     : null}
                    <div className="col-lg-12" style={{ textAlign: "right" }}>
                      <button
                        type="button"
                        className="btn btn-light ml-2"
                        // onSubmit={() => handleSubmit()}
                        onClick={() =>
                          history.push(
                            `/compose/process/detils/${id}/tindak-lanjut-direktorat`
                          )
                        }
                        style={{
                          boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                        }}
                      >
                        <i className="fa fa-arrow-left"></i>
                        Kembali
                      </button>
                      <button
                        type="submit"
                        className="btn btn-success ml-2"
                        onSubmit={() => handleSubmit()}
                        onClick={() => handleClick()}
                        style={{
                          boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                        }}
                      >
                        <i className="fas fa-save"></i>
                        Simpan
                      </button>
                    </div>
                  </Form>
                );
              }}
            </Formik>
          </div>
        </>
      </CardBody>
    </Card>
  );
}

export default TindakLanjutDirektoratDraftEdit;
