import React, { useEffect, useState, useRef } from "react";
import Select from "react-select";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";
import { useSubheader } from "../../../../../_metronic/layout";
import {
  getPeraturanTerkaitByIdPeraturan,
  getRegulasiPerpajakanTerima,
  savePeraturanTerkait,
  updatePeraturanTerkait
} from "../../../Evaluation/Api";

import swal from "sweetalert";
import PerekamanPeraturanFooter from "./PerekamanPeraturanFooter";
import PerekamanPeraturanForm from "./PerekamanPeraturanForm";

function PerekamanPeraturanEdit({
  history,
  match: {
    params: { id_perter, id }
  }
}) {
  const [content, setContent] = useState();
  const [peraturan, setPeraturan] = useState([]);
  const [show, setShow] = useState(false);
  const [val, setVal] = useState([]);
  const [tgl, setTgl] = useState([])


  const initValues = {
    nomor_peraturan: val,
    tgl_ditetapkan: tgl,
    tgl_diundangkan: "",
    perihal: "",
    status: "penyusunan"
  };

  const [title, setTitle] = useState("");
  const suhbeader = useSubheader();

  useEffect(() => {
    let _title = id_perter ? "Edit Peraturan Terkait" : "Kaitkan Peraturan";

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps

    if (id_perter) {
      getPeraturanTerkaitByIdPeraturan(id_perter).then(({ data }) => {
        setContent({
          nomor_peraturan: data.nomor_peraturan,
          tgl_ditetapkan: data.tgl_ditetapkan,
          tgl_diundangkan: data.tgl_diundangkan,
          perihal: data.perihal,
          status: data.status
        });
        setShow(true);
      });
    }
  }, [id_perter, suhbeader]);

  useEffect(() => {
    getRegulasiPerpajakanTerima().then(({ data }) => {
      data.content.map(data => {
        if (data.no_regulasi) {
          setPeraturan(peraturan => [
            ...peraturan,
            {
              label: data.no_regulasi,
              value: data.id_peraturan,
              tgl: data.tgl_regulasi
            }
          ]);
        }
      });
    });
  }, []);

  const handleChangePeraturan = val => {
    setVal(val.label);
    setTgl(val.tgl)
    setShow(true);
  };

  const handleBack = () => {
    if (id_perter) {
      history.push(`/compose/process/detils/${id}/perekaman-peraturan`);
    } else {
      setShow(false);
    }
  };

  const btnRef = useRef();
  const savePeraturan = values => {
    if (!id_perter) {
      savePeraturanTerkait(
        id,
        values.nomor_peraturan,
        values.perihal,
        values.tgl_ditetapkan,
        values.tgl_diundangkan,
        values.status
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push(`/compose/process/detils/${id}/perekaman-peraturan`);
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push(
              `/compose/process/detils/${id}/perekaman-peraturan/add`
            );
          });
        }
      });
    } else {
      updatePeraturanTerkait(
        id_perter,
        id,
        values.nomor_peraturan,
        values.perihal,
        values.tgl_ditetapkan,
        values.tgl_diundangkan,
        values.status
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push(`/compose/process/detils/${id}/perekaman-peraturan`);
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push(
              `/compose/process/detils/${id}/perekaman-peraturan/add`
            );
          });
        }
      });
    }
  };

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          {show ? (
            <PerekamanPeraturanForm
              initValues={content || initValues}
              btnRef={btnRef}
              savePeraturan={savePeraturan}
            />
          ) : (
            <div className="form-group row">
              <label className="col-xl-3 col-lg-3 col-form-label">
                No Peraturan
              </label>
              <div className="col-lg-9 col-xl-6">
                <Select
                  options={peraturan}
                  onChange={value => handleChangePeraturan(value)}
                />
              </div>
            </div>
          )}
        </>
      </CardBody>
      {show ? (
        <CardFooter style={{ borderTop: "none" }}>
          <PerekamanPeraturanFooter btnRef={btnRef} backAction={handleBack} />
        </CardFooter>
      ) : null}
    </Card>
  );
}

export default PerekamanPeraturanEdit;
