import React, { useState, useEffect } from "react";
import { useHistory, Route } from "react-router-dom";
import { useSelector } from "react-redux";
import BootstrapTable from "react-bootstrap-table-next";
import * as columnFormatters from "../../../../helpers/column-formatters";
import { deleteCosignSahli, getCosignSahli } from "../../../Evaluation/Api";
import CosignSahliDraftOpen from "./CosignSahliDraftOpen ";
import swal from "sweetalert";

function CosignSahliDraft({ id }) {
  const history = useHistory();
  const [content, setContent] = useState([]);
  const { status } = useSelector(state => state.auth);

  const editAction = id_cosign_sahli => {
    history.push(
      `/compose/process/detils/${id}/cosign-sahli/${id_cosign_sahli}/edit`
    );
  };

  const deleteAction = (id_cosign_sahli) => {
    swal({
      title: "Apakah Anda Yakin Untuk Menghapus?",
      text: "Klik OK untuk melanjutkan",
      icon: "warning",
      buttons: true,
      dangerMode: true
    }).then(willDelete => {
      if (willDelete) {
        deleteCosignSahli(id_cosign_sahli).then(({ status }) => {
          if (status === 200) {
            swal("Berhasil", "Data berhasil dihapus", "success").then(() => {
              history.push("/dashboard");
              history.replace(`/compose/process/detils/${id}/cosign-sahli`);
            });
          } else {
            swal("Gagal", "Data gagal dihapus", "error").then(() => {
              history.push("/dashboard");
              history.replace(`/compose/process/detils/${id}/cosign-sahli`);
            });
          }
        });
      }
      // else {
      //   swal("Your imaginary file is safe!");
      // }
    });
  }

  const addCosign = () => {
    history.push(`/compose/process/detils/${id}/cosign-sahli/add`);
  };

  const addNotes = () => {
    history.push(`/compose/process/detils/${id}/cosign-sahli/addnotes`)
  }

  const editNotes = (id_cosign_sahli) => {
    history.push(`/compose/process/detils/${id}/cosign-sahli/${id_cosign_sahli}/addnotes`)
  }

  const showDetil = id_cosign => {
    history.push(
      `/compose/process/detils/${id}/cosign-sahli/${id_cosign}/draft/open`
    );
  };

  useEffect(() => {
    getCosignSahli(id).then(({ data }) => {
      setContent(data);
    });
  }, [id]);


  const columns = [
    {
      dataField: "nama_unit",
      text: "Nama Unit",
      sort: true
    },
    {
      dataField: "tgl_surat",
      text: "Tanggal Surat",
      formatter: columnFormatters.DateFormatterComposeProcessCosignDjp,
      sort: true
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
        status === "Cosign Sahli"
          ? columnFormatters.ActionsColumnFormatterComposeProcessCosignSahli
          : columnFormatters.ActionsColumnFormatterComposeProcessCosignSahliJustView,
      formatExtraData: {
        editAction: content[0] ? content[0].no_surat ? editAction : editNotes : null ,
        showDetil: showDetil,
        deleteAction: deleteAction
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];
  const columnsNotes = [
    {
      dataField: "nama_unit",
      text: "Nama Unit",
      sort: true
    },
    {
      dataField: "catatan",
      text: "Catatan",
      sort: true
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
        status === "Cosign Sahli"
          ? columnFormatters.ActionsColumnFormatterComposeProcessCosignSahli
          : columnFormatters.ActionsColumnFormatterComposeProcessCosignSahliJustView,
      formatExtraData: {
        editAction: content[0] ? content[0].no_surat ? editAction : editNotes : null ,
        showDetil: showDetil,
        deleteAction: deleteAction
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];




  const emptyDataMessage = () => {
    return (
      <>
      <div className="row">
      <div className="col-lg-12 col-xl-12 mb-3 ml-2 text-center">
        <button
          type="button"
          className="btn btn-light-primary"
          onClick={addCosign}
        >
          Tambah Cosign
        </button>
        <button
          type="button"
          className="btn btn-light-warning ml-3"
          // style={{ float: "right" }}
          onClick={addNotes}
        >
          Tambah Catatan
        </button>
      </div>
      {/* <div className="col-lg-6 col-xl-6 mb-3 ml-2">
        <button
          type="button"
          className="btn btn-light-warning"
          style={{ float: "right" }}
          onClick={addNotes}
        >
          Tambah Catatan
        </button>
      </div> */}

      </div>
      
      </>
    );
  };

  return (
    <>
      <>
        <div className="row">
          <div className="col-lg-12 col-xl-12 mb-3 mt-3">
            <h4> Cosign Sahli</h4>
          </div>
        </div>
        <BootstrapTable
          wrapperClasses="table-responsive"
          bordered={false}
          headerWrapperClasses="thead-light"
          classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
          keyField="id_cosign_sahli"
          data={content}
          columns={content[0] ? content[0].tgl_surat ? columns : columnsNotes : columns}
          bootstrap4
          noDataIndication={emptyDataMessage}
        ></BootstrapTable>
      </>
      <Route path="/compose/process/detils/:id/cosign-sahli/:id_cosign/draft/open">
        {({ history, match }) => (
          <CosignSahliDraftOpen
            show={match != null}
            id={match && match.params.id}
            id_cosign={match && match.params.id_cosign}
            after={false}
            onHide={() => {
              history.push(`/compose/process/detils/${id}/cosign-sahli`);
            }}
            onRef={() => {
              history.push(`/compose/process/detils/${id}/cosign-sahli`);
            }}
          />
        )}
      </Route>
    </>
  );
}

export default CosignSahliDraft;
