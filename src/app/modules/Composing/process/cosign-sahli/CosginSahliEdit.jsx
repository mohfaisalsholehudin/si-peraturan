import React, { useState, useEffect, useRef } from "react";
import swal from "sweetalert";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";
import { useSubheader } from "../../../../../_metronic/layout";
import CosignSahliForm from "./CosignSahliForm";
import CosignSahliFooter from "./CosignSahliFooter";
import {
  getCosignSahliByIdCosign,
  getPenyusunanById,
  saveCosignSahli,
  updateCosignSahli,
  uploadFile,
  uploadFileNew
} from "../../../Evaluation/Api";

function CosginSahliEdit({
  history,
  match: {
    params: { id_cosign_sahli, id }
  }
}) {
  const initValues = {
    no_surat: "",
    tgl_surat: "",
    id_minta_jawab: "",
    file: "",
    nama_unit: "Staff Ahli Menteri"
  };

  const [title, setTitle] = useState("");
  const [content, setContent] = useState();
  const [val, setVal] = useState([]);

  const suhbeader = useSubheader();
  useEffect(() => {
    let _title = id_cosign_sahli ? "Edit Cosign Sahli" : "Tambah Cosign Sahli";

    setTitle(_title);
    suhbeader.setTitle(_title);
    if (id_cosign_sahli) {
      getCosignSahliByIdCosign(id_cosign_sahli).then(({ data }) => {
        setContent({
          nama_unit: data.nama_unit,
          no_surat: data.no_surat,
          tgl_surat: data.tgl_surat,
          file_upload: data.file_permintaan,
          id_minta_jawab: data.id_minta_jawab
        });
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id_cosign_sahli, suhbeader]);

  useEffect(() => {
    getPenyusunanById(id).then(({ data }) => {
      setVal(data);
    });
  }, [id]);
  const btnRef = useRef();

  const handleBack = i => {
    history.push(`/compose/process/detils/${i}/cosign-sahli`);
  };

  const saveCosign = values => {
    if (!id_cosign_sahli) {
      const formData = new FormData();
      formData.append("file", values.file);
      uploadFileNew(formData).then(({ data }) => {
        saveCosignSahli(
          id,
          values.id_minta_jawab,
          values.no_surat,
          values.tgl_surat,
          values.nama_unit,
          data.message
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push(`/compose/process/detils/${id}/cosign-sahli`);
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push(`/compose/process/detils/${id}/cosign-sahli/add`);
            });
          }
        });
      });
    } else {
      if (values.file.name) {
        const formData = new FormData();
        formData.append("file", values.file);
        uploadFile(formData).then(({ data }) => {
          updateCosignSahli(
            id,
            id_cosign_sahli,
            values.id_minta_jawab,
            values.no_surat,
            values.tgl_surat,
            values.nama_unit,
            data.message
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
                history.push(`/compose/process/detils/${id}/cosign-sahli`);
              });
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                history.push(`/compose/process/detils/${id}/cosign-sahli/add`);
              });
            }
          });
        });
      } else {
        updateCosignSahli(
          id,
          id_cosign_sahli,
          values.id_minta_jawab,
          values.no_surat,
          values.tgl_surat,
          values.nama_unit,
          values.file_upload,
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push(`/compose/process/detils/${id}/cosign-sahli`);
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push(`/compose/process/detils/${id}/cosign-sahli/add`);
            });
          }
        });
      }
    }
  };

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="form-group row">
            <label className="col-xl-3 col-lg-3 col-form-label">
              No Penyusunan
            </label>
            <div className="col-lg-9 col-xl-6" style={{ marginTop: "10px" }}>
              {val.no_penyusunan}
            </div>
          </div>
          <div className="mt-5">
            <CosignSahliForm
              initValues={content || initValues}
              btnRef={btnRef}
              saveCosign={saveCosign}
              id={id}
            />
          </div>
        </>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <CosignSahliFooter backAction={handleBack} btnRef={btnRef} id={id} />
      </CardFooter>
    </Card>
  );
}

export default CosginSahliEdit;
