import React from "react";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { Textarea } from "../../../../helpers";

function CosignSahliFormNotes({ initValues, btnRef, saveCosign, id }) {
  const CosignEditSchema = Yup.object().shape({
    catatan: Yup.string().required("Catatan is required")
  });

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={initValues}
        validationSchema={CosignEditSchema}
        onSubmit={values => {
          saveCosign(values);
          // console.log(values)
        }}
      >
        {({
          handleSubmit,
        }) => {
          return (
            <>
              <Form className="form form-label-right">
                {/* FIELD CATATAN */}
                <div className="form-group row">
                  <Field
                    name="catatan"
                    component={Textarea}
                    placeholder="Catatan"
                    label="Catatan"
                  />
                </div>

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default CosignSahliFormNotes;
