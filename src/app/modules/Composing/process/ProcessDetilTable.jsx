import React from "react";
import { useHistory } from "react-router-dom";

import BootstrapTable from "react-bootstrap-table-next";
import * as columnFormatters from "../../../helpers/column-formatters";

function ProcessDetilTable({ id, status }) {
  const history = useHistory();

  const detils = [
    {
      id_detil: "surat-undangan",
      nama_proses: "Dokumentasi Administrasi Persuratan"
    },
    {
      id_detil: "input-lhr",
      nama_proses: "LHR Pembahasan"
    },
    {
      id_detil: "perekaman-peraturan",
      nama_proses: "Peraturan Terkait"
    },
    {
      id_detil: "nd-permintaan",
      nama_proses: "ND Surat Permintaan Pendapat"
    },
    {
      id_detil: "cosign_djp",
      nama_proses: "Cosign Jawaban Unit DJP"
    },
    {
      id_detil: "cosign-kementerian",
      nama_proses: "Cosign Jawaban KL"
    },
    {
      id_detil: "tindak-lanjut-direktorat",
      nama_proses:
        "Tindak Lanjut Jawaban dari Direktorat Lain / Kanwil / KPP / KL"
    },
    {
      id_detil: "cosign-sahli",
      nama_proses: "Rekam / Review Jawaban Staff Ahli Menteri",
      status: status
    },
    {
      id_detil: "tindak-lanjut-sahli",
      nama_proses: "Tindak Lanjut Jawaban dari Staff Ahli Menteri",
      status: status
    },
    {
      id_detil: "pengajuan-rancangan",
      nama_proses: "Penyampaian Rancangan ke Direktur Jenderal Pajak",
      status: status
    },
    {
      id_detil: "penetapan-peraturan",
      nama_proses: "Penetapan Peraturan",
      status: status === "Penetapan" ? status : "hide"
    }
  ];
  const process = id_detil => {
    switch (id_detil) {
      case "nd-permintaan":
        history.push(`/compose/process/detils/${id}/nd-permintaan`);
        break;
      case "input-lhr":
        history.push(`/compose/process/detils/${id}/input-lhr`);
        break;
      case "cosign_djp":
        history.push(`/compose/process/detils/${id}/cosign-djp`);
        break;
      case "cosign-kementerian":
        history.push(`/compose/process/detils/${id}/cosign-kementerian`);
        break;
      case "tindak-lanjut-direktorat":
        history.push(`/compose/process/detils/${id}/tindak-lanjut-direktorat`);
        break;
      case "cosign-sahli":
        history.push(`/compose/process/detils/${id}/cosign-sahli`);
        break;
      case "tindak-lanjut-sahli":
        history.push(`/compose/process/detils/${id}/tindak-lanjut-sahli`);
        break;
      case "pengajuan-rancangan":
        history.push(`/compose/process/detils/${id}/pengajuan-rancangan`);
        break;
      case "surat-undangan":
        history.push(`/compose/process/detils/${id}/surat-undangan`);
        break;
      case "perekaman-peraturan":
        history.push(`/compose/process/detils/${id}/perekaman-peraturan`);
        break;
      case "penetapan-peraturan":
        history.push(`/compose/process/detils/${id}/penetapan-peraturan`);
        break;
      default:
        break;
    }
  };

  const columns = [
    {
      dataField: "nama_proses",
      text: "Detil Proses",
      sort: true
    },

    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterComposeProcess,
      formatExtraData: {
        openProcess: process
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];

  return (
    <>
      <div className="row"></div>
      <BootstrapTable
        wrapperClasses="table-responsive"
        bordered={false}
        headerWrapperClasses="thead-light"
        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
        keyField="id_detil"
        data={detils}
        columns={columns}
        bootstrap4
      ></BootstrapTable>
    </>
  );
}

export default ProcessDetilTable;
