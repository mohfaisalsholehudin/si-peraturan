import React, { useEffect, useState, useRef } from "react";
import swal from "sweetalert";
import * as Yup from "yup";
import { Field, Formik, Form } from "formik";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";
import {
  DatePickerField,
  Input,
  Textarea,
  Select as Sel
} from "../../../../helpers/";
import CustomFileInput from "../../../../helpers/form/CustomFileInput";
import { useSubheader } from "../../../../../_metronic/layout";
import {
  getJenisPeraturan,
  getJenisSurat,
  getPenyusunanById,
  updateStatusPenyusunan,
  getDraftComposingById
} from "../../../Evaluation/Api";
import { savePeraturan } from "../../../Knowledge/Api";
import { useSelector } from "react-redux";
import CustomFileInputPenetapan from "../../../../helpers/form/CustomFileInputPenetapan";
import { uploadFileNew } from "../../../../references/Api";

function PenetapanPeraturan({
  history,
  match: {
    params: { id_penetapan_peraturan, id }
  }
}) {
  const [title] = useState("Penetapan Peraturan");
  const [jenisPeraturan, setJenisPeraturan] = useState([]);
  const [topik, setTopik] = useState([]);
  const { user } = useSelector(state => state.auth);
  const [loading, setLoading] = useState(false);

  const suhbeader = useSubheader();
  const initValues = {
    no_regulasi: "",
    tgl_regulasi: "",
    perihal: "",
    file: "",
    jns_regulasi: "",
    id_topik: ""
  };
  useEffect(() => {
    // getJenisPeraturan().then(({ data }) => {
    //   // setJenisPeraturan(data);
    //   data.map(dt=> {
    //     return dt.status === 'AKTIF' ? setJenisPeraturan(peraturan => [...peraturan, dt]) : null
    //   })
    // });
    getJenisSurat(1).then(({ data }) => {
      setTopik(data);
    });
    suhbeader.setTitle(title);
  }, [suhbeader, title]);

  useEffect(()=> {
    getJenisPeraturan().then(({ data }) => {
      // setJenisPeraturan(data);
      data.map(dt=> {
        return dt.status === 'AKTIF' ? setJenisPeraturan(peraturan => [...peraturan, dt]) : null
      })
    });
  },[])

  const PenetapanEditSchema = Yup.object().shape({
    no_regulasi: Yup.string()
      .min(2, "Minimum 2 symbols")
      // .max(50, "Maximum 50 symbols")
      .required("Nomor Peraturan is required"),
    tgl_regulasi: Yup.mixed()
      .nullable(false)
      .required("Tanggal Peraturan is required"),
    perihal: Yup.string()
      .min(2, "Minimum 2 symbols")
      // .max(255, "Maximum 50 symbols")
      .required("Perihal is required"),
    jns_regulasi: Yup.string().required("Jenis Peraturan is required"),
    id_topik: Yup.number().required("Topik is required"),
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        value => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        // value => value && SUPPORTED_FORMATS.includes(value.type)
        value => value && SUPPORTED_FORMATS.some(a=> value.type.includes(a))
      )
  });
  const FILE_SIZE = 50000000;
  const SUPPORTED_FORMATS = ["application/pdf"];
  const btnRef = useRef();
  const enableLoading = () => {
    setLoading(true);
  };

  const disableLoading = () => {
    setLoading(false);
  };

  const saveRegulasi = values => {
    enableLoading();
    const formData = new FormData();
    formData.append("file", values.file);
    uploadFileNew(formData)
      .then(({ data }) => {
        disableLoading();
        getDraftComposingById(id).then(data_1 => {
          savePeraturan(
            data.message,
            values.id_topik,
            values.no_regulasi,
            values.perihal,
            "",
            values.tgl_regulasi,
            values.jns_regulasi,
            user.nip9,
            "",
            "",
            data_1.data[0].body_direktur
            ? data_1.data[0].body_direktur
            : data_1.data[0].body_sahli
            ? data_1.data[0].body_sahli
            : data_1.data[0].body_cosign
            ? data_1.data[0].body_cosign
            : data_1.data[0].body_draft,
            // "",
            "",
            user.kantorLegacyKode,
            user.unitEs4LegacyKode
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              updateStatus();
              // swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              //   history.push("/compose/proposal");
              // });
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                history.push(
                  `/compose/process/detils/${id}/penetapan-peraturan`
                );
              });
            }
          });
        });
      })
      .catch(() => window.alert("Oops Something went wrong !"));
  };

  const handleBack = () => {
    history.push(`/compose/process/${id}/detil`);
  };

  const saveForm = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
      // setIsComplete(true);
      // disabled ? setIsComplete(false) : setIsComplete(true);
    }
  };

  const updateStatus = () => {
    getPenyusunanById(id).then(({ data }) => {
      updateStatusPenyusunan(id, 6, data.kd_kantor).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push("/dashboard");
            history.replace(`/compose/process`);
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/dashboard");
            history.replace(
              `/compose/process/detils/${id}/tindak-lanjut-direktorat`
            );
          });
        }
      });
    });
  };
  return (
    <>
      <Card>
        <CardHeader
          title="Penetapan Peraturan"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <>
            <Formik
              enableReinitialize={true}
              initialValues={initValues}
              validationSchema={PenetapanEditSchema}
              onSubmit={values => {
                // console.log(values);
                saveRegulasi(values);
              }}
            >
              {({
                handleSubmit,
                setFieldValue,
                handleBlur,
                handleChange,
                errors,
                touched,
                values,
                isValid,
                isSubmitting
              }) => {
                return (
                  <>
                    <Form className="form form-label-right">
                      {/* FIELD NO PERATURAN */}
                      <div className="form-group row">
                        <Field
                          name="no_regulasi"
                          component={Input}
                          placeholder="Nomor Peraturan"
                          label="Nomor Peraturan"
                        />
                      </div>
                      {/* FIELD TANGGAL PERATURAN */}
                      <div className="form-group row">
                        <DatePickerField
                          name="tgl_regulasi"
                          label="Tanggal Peraturan"
                        />
                      </div>
                      {/* FIELD JENIS SURAT */}
                      <div className="form-group row">
                        <Sel name="jns_regulasi" label="Jenis Peraturan">
                          <option>Pilih Jenis Peraturan</option>
                          {jenisPeraturan.map(data => (
                            <option
                              key={data.id_jnsperaturan}
                              value={data.id_jnsperaturan}
                            >
                              {data.nm_jnsperaturan}
                            </option>
                          ))}
                        </Sel>
                      </div>
                      {/* FIELD PERIHAL */}
                      <div className="form-group row">
                        <Field
                          name="perihal"
                          component={Textarea}
                          placeholder="Perihal"
                          label="Perihal"
                        />
                      </div>
                      {/* FIELD TOPIK */}
                      <div className="form-group row">
                        <Sel name="id_topik" label="Topik">
                          <option>Pilih Jenis Topik</option>
                          {topik.map(data => (
                            <option key={data.id} value={data.id}>
                              {data.nama}
                            </option>
                          ))}
                        </Sel>
                      </div>
                      {/* FIELD UPLOAD FILE */}
                      <div className="form-group row">
                        <label className="col-xl-3 col-lg-3 col-form-label">
                          Upload File
                        </label>
                        <div className="col-lg-9 col-xl-6">
                          <Field
                            name="file"
                            component={CustomFileInputPenetapan}
                            title="Select a file"
                            label="File"
                            style={{ display: "flex" }}
                          />
                        </div>
                      </div>
                      <div className="col-lg-12" style={{ textAlign: "right" }}>
                        <button
                          type="button"
                          onClick={handleBack}
                          className="btn btn-light"
                          style={{
                            boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                          }}
                        >
                          <i className="fa fa-arrow-left"></i>
                          Kembali
                        </button>
                        {`  `}

                        {loading ? (
                          <button
                            type="submit"
                            className="btn btn-success spinner spinner-white spinner-left ml-2"
                            onSubmit={() => handleSubmit()}
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                            }}
                            disabled={isSubmitting}
                          >
                            <span>Simpan</span>
                          </button>
                        ) : (
                          <button
                            type="submit"
                            className="btn btn-success ml-2"
                            onSubmit={() => handleSubmit()}
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                            }}
                            disabled={isSubmitting}
                          >
                            <i className="fas fa-save"></i>
                            <span>Simpan</span>
                          </button>
                        )}
                      </div>
                      {/* <button
                        type="submit"
                        style={{ display: "none" }}
                        ref={btnRef}
                        onSubmit={() => handleSubmit()}
                      ></button> */}
                      {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
                    </Form>
                  </>
                );
              }}
            </Formik>
          </>
        </CardBody>
        {/* <CardFooter style={{ borderTop: "none" }}>
          <div className="col-lg-12" style={{ textAlign: "right" }}>
            <button
              type="button"
              onClick={handleBack}
              className="btn btn-light"
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
            >
              <i className="fa fa-arrow-left"></i>
              Kembali
            </button>
            {`  `}

            <button
              type="submit"
              className="btn btn-success ml-2"
              onClick={saveForm}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
              // disabled={disabled}
            >
              <i className="fas fa-save"></i>
              Simpan
            </button>
          </div>
        </CardFooter> */}
      </Card>
    </>
  );
}

export default PenetapanPeraturan;
