import React,{useEffect, useState} from "react";
import { Modal } from "react-bootstrap";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
import ResearchReject from "./ResearchReject";


function ResearchOpen({ id, show, onHide }) {

  const history = useHistory();
  const [data, setData] = useState([]);
  const [isReject, setIsReject] = useState(false);
  const [isShow, setIsShow] = useState(true);

//   useEffect(() => {
//     if(id){
//       getUsulanById(id).then(({data})=> {
//         setData(data)
//       })
//     }
//   }, [id])


//   const acceptAction = () =>  {
//     updateStatusUsulan(id, 6).then(({status})=> {
//       if (status === 201 || status === 200) {
//         swal("Berhasil", "Data berhasil disimpan", "success").then(
//           () => {
//             history.push("/dashboard");
//             history.replace("/evaluation/research");
//           }
//         );
//       } else {
//         swal("Gagal", "Data gagal disimpan", "error").then(() => {
//           history.push("/dashboard");
//           history.replace("/evaluation/research");
//         });
//       }
//     })
//   }

  const handleReject = () => {
    setIsReject(true);
    setIsShow(false);
  }

  const handleCancel = () => {
    setIsReject(false);
    setIsShow(true);
  }

//   const reject = (val) => {
//     updateStatusUsulan(data.id_usulan, 5 ,val.alasan_penolakan)
//     .then(({status})=> {
//       if (status === 201 || status === 200) {
//         swal("Berhasil", "Data berhasil disimpan", "success").then(
//           () => {
//             history.push("/dashboard");
//             history.replace("/evaluation/research");
//           }
//         );
//       } else {
//         swal("Gagal", "Data gagal disimpan", "error").then(() => {
//           history.push("/dashboard");
//           history.replace("/evaluation/research");
//         });
//       }
//     })
//   }


  return (
    <Modal
      size="md"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Detil Penelitian Penyusunan Peraturan
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Nomor Peraturan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">
            {/* {`: ${data.no_surat}`} */}
            : No.1/PJ.12/2020
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">No Penyusunan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">
            {/* {`: ${data.no_surat}`} */}
            : 15/12/2020
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Tgl Penyusunan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">
            {/* {`: ${DateFormat(data.tgl_surat)}`} */}
            : 15/12/2020
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Jenis Peraturan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">
                {/* {`: ${data.perihal}`} */}
            : PP
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Judul Peraturan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">
                {/* {`: ${data.instansi_unit}`} */}
            : Peraturan 1
            </span>
          </div>
        </div>
        <div className="col-lg-12 text-center mt-5">
        <button
            type="button"
            // onClick={handleReject}
            className="btn btn-primary"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            Lihat Draft Peraturan
          </button>
        </div>
        {isReject ?  
       <ResearchReject
       handleCancel={handleCancel}
    //    reject={reject}
        /> : "" }
       
      </Modal.Body>
      {isShow ? <Modal.Footer style={{ borderTop: "none" }}>
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          <button
            type="button"
            onClick={handleReject}
            className="btn btn-danger"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="flaticon2-cancel icon-nm"></i>
            Tolak
          </button>
          {`  `}
          <button
            type="submit"
            className="btn btn-success ml-2"
            // onClick={acceptAction}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fas fa-check"></i>
            Setuju
          </button>
        </div>
      </Modal.Footer> :  ""}
      
    </Modal>
  );
}

export default ResearchOpen;
