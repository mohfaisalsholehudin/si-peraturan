import React, { useState, useEffect } from "react";
import { Route } from "react-router-dom";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";

import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../../_metronic/_partials/controls";

//Helpers
import {
  sortCaret,
  headerSortingClasses
} from "../../../../../_metronic/_helpers";
import { Pagination } from "../../../../helpers/pagination/Pagination";
import * as columnFormatters from "../../../../helpers/column-formatters";
import { getPeraturanTerkait } from "../../../Evaluation/Api";
import ResearchPeraturanTerkaitOpen from "./ResearchPeraturanTerkaitOpen";

function ResearchPeraturanTerkait({
  history,
  match: {
    params: { id }
  }
}) {
  const [content, setContent] = useState([]);

  const open = (id_perter) =>   history.push(`/compose/research/${id}/view/perter/${id_perter}/open`);

  useEffect(() => {
    getPeraturanTerkait(id).then(({ data }) => {
      setContent(data);
    });
  }, [id]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "nomor_peraturan",
      text: "No Peraturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "tgl_ditetapkan",
      text: "Tgl Ditetapkan",
      sort: true,
      formatter: columnFormatters.DateFormatterComposeProposalPeraturanTerkaitDitetapkan,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "tgl_diundangkan",
      text: "Tgl Diundangkan",
      sort: true,
      formatter: columnFormatters.DateFormatterComposeProposalPeraturanTerkaitDiundangkan,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "perihal",
      text: "Tentang",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.StatusColumnFormatterComposeDetilPeraturanTerkait,
      headerSortingClasses,
      style: {
        minWidth: "150px"
      }
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterComposeResearchProcessPerter,
      formatExtraData: {
        showDialog: open,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];
  const { SearchBar } = Search;
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "nomor_peraturan",
    pageNumber: 1,
    pageSize: 5
  };
  const defaultSorted = [{ dataField: "nomor_peraturan", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };

  const emptyDataMessage = () => {
    return <div className="text-center">No Data to Display</div>;
  };

  return (
    <>
    <Card>
      <CardHeader
        title="Daftar Peraturan Terkait"
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <PaginationProvider pagination={paginationFactory(pagiOptions)}>
            {({ paginationProps, paginationTableProps }) => {
              return (
                <>
                  <ToolkitProvider
                    keyField="nomor_peraturan"
                    data={content}
                    columns={columns}
                    search
                  >
                    {props => (
                      <div>
                        <div className="row">
                          <div className="col-lg-6 col-xl-6 mb-3">
                            <SearchBar
                              {...props.searchProps}
                              style={{ width: "500px" }}
                            />
                            <br />
                          </div>
                          <div
                            className="col-lg-6 col-xl-6 mb-3"
                            style={{ textAlign: "right" }}
                          >
                            <button
                              type="button"
                              className="btn btn-light-success"
                              style={{
                                float: "right"
                              }}
                              onClick={() =>
                                history.push(`/compose/research/${id}/view`)
                              }
                            >
                              <i className="fa fa-arrow-left"></i>
                              Kembali
                            </button>
                          </div>
                        </div>
                        <BootstrapTable
                          {...props.baseProps}
                          wrapperClasses="table-responsive"
                          bordered={false}
                          headerWrapperClasses="thead-light"
                          classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                          defaultSorted={defaultSorted}
                          bootstrap4
                          noDataIndication={emptyDataMessage}
                          {...paginationTableProps}
                        ></BootstrapTable>
                        <Pagination paginationProps={paginationProps} />
                      </div>
                    )}
                  </ToolkitProvider>
                </>
              );
            }}
          </PaginationProvider>
        </>
      </CardBody>

    </Card>
    <Route path="/compose/research/:id/view/perter/:id_peraturan/open">
        {({ history, match }) => (
          <ResearchPeraturanTerkaitOpen
            show={match != null}
            id={match && match.params.id}
            id_peraturan={match && match.params.id_peraturan}
            onHide={() => {
              history.push(`/compose/research/${id}/view/perter`);
            }}
            onRef={() => {
              history.push(`/compose/research/${id}/view/perter`);
            }}
          />
        )}
      </Route>
    </>
  );
}

export default ResearchPeraturanTerkait;
