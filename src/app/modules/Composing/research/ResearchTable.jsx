/* Library */
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";

/* Helpers */
import { Pagination } from "../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../../../helpers/column-formatters";

/* Component */

/* Utility */
import {
  getDraftComposingById,
  getPenyusunanByAdmin,
  getPenyusunanByAdminStatus,
  getPenyusunanBySeksiDanStatus,
  getPenyusunanBySubditDanStatus,
} from "../../Evaluation/Api";

function ResearchTable() {
  const history = useHistory();
  const { user, role } = useSelector(state => state.auth);
  const es3 = role.includes("ROLE_PERATURAN_PENELITI_LVL2");
  const super_admin = role.includes("ROLE_SUPER_ADMIN_PERATURAN");

  const [research, setResearch] = useState([]);
  const open = id => history.push(`/compose/research/${id}/view`);
  useEffect(() => {
    if(es3){
      getPenyusunanBySubditDanStatus(user.unitEs4LegacyKode, "Eselon 4").then(({ data }) => {
        setResearch(data);
      });
    } else if(super_admin) {
        getPenyusunanByAdminStatus("Eselon 4").then(({ data }) => {
          setResearch(data)
        })
    } else {
      getPenyusunanBySeksiDanStatus(user.unitEs4LegacyKode, "Eselon 4").then(({ data }) => {
        setResearch(data);
      });
    }
  }, [super_admin, es3, user.unitEs4LegacyKode]);

  const openDraft = id_penyusunan => {
    getDraftComposingById(id_penyusunan).then(({ data }) => {
      data.map((data) => {
        history.push(
          `/compose/research/${id_penyusunan}/view/draft/${data.id_draftperaturan}/${data.jns_draft}`
        );
      })
    });
  };

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "no_perencanan",
      text: "No Perencanaan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "tgl_penyusunan",
      text: "Tgl Penyusunan",
      sort: true,
      formatter: columnFormatters.DateFormatterComposeProposal,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "jns_peraturan",
      text: "Jenis Peraturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "judul_peraturan",
      text: "Judul Peraturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "draft_peraturan",
      text: "Draft Peraturan",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.FileColumnFormatterComposeResearch,
      formatExtraData: {
        openDraft: openDraft
      },
      headerSortingClasses
    },
    {
      dataField: "wkt_update",
      text: "Waktu Update",
      sort: true,
      hidden: true,
      formatter: columnFormatters.DateFormatterWaktuUpdate,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterComposeResearch,
      formatExtraData: {
        openDialog: open
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];
  const initialFilter = {
    sortOrder: "desc", // asc||desc
    sortField: "wkt_update",
    pageNumber: 1,
    pageSize: 50
  };
  const defaultSorted = [{ dataField: "wkt_update", order: "desc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: research.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_penyusunan"
                  data={research}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
    </>
  );
}

export default ResearchTable;
