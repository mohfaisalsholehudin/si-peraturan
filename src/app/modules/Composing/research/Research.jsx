import React from "react";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../_metronic/_partials/controls";
import ResearchTable from "./ResearchTable";

function Research() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Penelitian Penyusunan Peraturan"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
            <ResearchTable />
        </CardBody>
      </Card>
    </>
  );
}

export default Research;
