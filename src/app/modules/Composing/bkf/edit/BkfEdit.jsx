/* Library */
import React, { useEffect, useState, useRef } from "react";
import Select from "react-select";

// import swal from "sweetalert";

/* Helper */
import { useSubheader } from "../../../../../_metronic/layout";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";


/* Component */
import BkfEditForm from "./BkfEditForm";
import BkfEditFooter from "./BkfEditFooter"

function BkfEdit({
  history,
  match: {
    params: { id }
  }
}) {
  const initValues = {
    no_bkf: "",
    tgl_bkf: "",
    jns_peraturan: "",
    judul_peraturan: "",
    unit_incharge: "",
    file: ""
  };

  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [show, setShow] = useState(false);
  const [bkf, setBkf] = useState([]);

  useEffect(() => {
    let _title = id
      ? "Edit Rancangan Peraturan / Keputusan Inisiasi UE 1 Kemenkeu Terkait TUSI DJP"
      : "Tambah Rancangan Peraturan / Keputusan Inisiasi UE 1 Kemenkeu Terkait TUSI DJP";

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps

    if (id) {
      //   getUsulanById(id).then(({ data }) => {
      //     setBkf({
      //       id_usulan: data.id_usulan,
      //       no_surat: data.no_surat,
      //       tgl_surat: data.tgl_surat,
      //       perihal: data.perihal,
      //       jns_pengusul: data.jns_pengusul,
      //       jns_pajak: data.jns_pajak.split(','),
      //       unit_kerja: data.unit_kerja,
      //       instansi: data.instansi,
      //       alamat: data.alamat,
      //       no_pic: data.no_pic,
      //       nm_pic: data.nm_pic,
      //       nip_perekam: data.nip_perekam,
      //       jns_usul: data.jns_usul,
      //       alasan_tolak: data.alasan_tolak,
      //       file_upload: data.file_upload,
      //       wkt_create: data.wkt_create,
      //       wkt_update: data.wkt_update,
      //       status: data.status,
      //       id_tahapan: data.id_tahapan,
      //       instansi_unit: data.instansi_unit
      //     });
      //   });
    }
  }, [id, suhbeader]);
  const btnRef = useRef();
  //   const saveProposal = values => {
  //     if (!id) {
  //       const formData = new FormData();
  //       if (values.instansi) {
  //         formData.append("file", values.file);
  //         uploadFile(formData)
  //           .then(({ data }) =>
  //             saveUsulan(
  //               values.alamat,
  //               "",
  //               data.message,
  //               values.instansi,
  //               values.jns_pajak.toString(),
  //               "",
  //               values.jns_usul,
  //               values.nip_perekam,
  //               values.nm_pic,
  //               values.no_pic,
  //               values.no_surat,
  //               values.perihal,
  //               values.tgl_surat,
  //               ""
  //             ).then(({ status }) => {
  //               if (status === 201 || status === 200) {
  //                 swal("Berhasil", "Data berhasil disimpan", "success").then(
  //                   () => {
  //                     history.push("/evaluation/proposal");
  //                   }
  //                 );
  //               } else {
  //                 swal("Gagal", "Data gagal disimpan", "error").then(() => {
  //                   history.push("/evaluation/proposal/new");
  //                 });
  //               }
  //             })
  //           )
  //           .catch(() => window.alert("Oops Something went wrong !"));
  //       } else {
  //         console.log(values.file)
  //         formData.append("file", values.file);
  //         uploadFile(formData)
  //           .then(({ data }) =>
  //             saveUsulan(
  //               values.alamat,
  //               "",
  //               data.message,
  //               "",
  //               values.jns_pajak.toString(),
  //               "",
  //               values.jns_usul,
  //               values.nip_perekam,
  //               values.nm_pic,
  //               values.no_pic,
  //               values.no_surat,
  //               values.perihal,
  //               values.tgl_surat,
  //               values.unit_kerja
  //             ).then(({ status }) => {
  //               if (status === 201 || status === 200) {
  //                 swal("Berhasil", "Data berhasil disimpan", "success").then(
  //                   () => {
  //                     history.push("/evaluation/proposal");
  //                   }
  //                 );
  //               } else {
  //                 swal("Gagal", "Data gagal disimpan", "error").then(() => {
  //                   history.push("/evaluation/proposal/new");
  //                 });
  //               }
  //             })
  //           )
  //           .catch(() => swal("Error", "Oops Something went wrong !", "error"));
  //       }
  //     } else {
  //       if (values.file.name) {
  //         const formData = new FormData();
  //         formData.append("file", values.file);
  //         uploadFile(formData)
  //           .then(({ data }) =>
  //             updateUsulan(
  //               values.id_usulan,
  //               values.alamat,
  //               "", //alasan_tolak
  //               data.message, //file_upload
  //               values.instansi,
  //               values.jns_pajak.toString(),
  //               "", //jns_Pengusul
  //               values.jns_usul,
  //               values.nip_perekam,  //nip_perekam
  //               values.nm_pic,
  //               values.no_pic,
  //               values.no_surat,
  //               values.perihal,
  //               values.tgl_surat,
  //               values.unit_kerja
  //             ).then(({ status }) => {
  //               if (status === 201 || status === 200) {
  //                 swal("Berhasil", "Data berhasil disimpan", "success").then(
  //                   () => {
  //                     history.push("/evaluation/proposal");
  //                   }
  //                 );
  //               } else {
  //                 swal("Gagal", "Data gagal disimpan", "error").then(() => {
  //                   history.push("/evaluation/proposal/new");
  //                 });
  //               }
  //             })
  //           )
  //           .catch(() => window.alert("Oops Something went wrong !"));
  //       } else {
  //         updateUsulan(
  //           values.id_usulan,
  //           values.alamat,
  //           "", //alasan_tolak
  //           values.file_upload,
  //           values.instansi,
  //           values.jns_pajak.toString(),
  //           "",  //jns_pengusul
  //           values.jns_usul,
  //           values.nip_perekam, //nip_perekam
  //           values.nm_pic,
  //           values.no_pic,
  //           values.no_surat,
  //           values.perihal,
  //           values.tgl_surat,
  //           values.unit_kerja
  //         ).then(({ status }) => {
  //           if (status === 201 || status === 200) {
  //             swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
  //               history.push("/evaluation/proposal");
  //             });
  //           } else {
  //             swal("Gagal", "Data gagal disimpan", "error").then(() => {
  //               history.push("/evaluation/proposal/new");
  //             });
  //           }
  //         });
  //       }
  //     }
  //   };
  const backAction = () => {
      // setShow(false)
      history.push('/compose/bkf')
  };

  // const handleChangePerencanaan = val => {

  //   console.log(val.value)
  //   switch (val.value) {
  //     case "1":
  //       setBkf({
  //         no_penyusunan: "Per-1/PJ.12/KP.0101/2020/2021",
  //         jns_peraturan: "PPN",
  //         judul_peraturan: "Peraturan 1",
  //         tentang: "Tentang 1",
  //         file_kajian: "kajianperencanaan-1.pdf",
  //         unit_incharge: 'Seksi Pengembangan Sistem Pendukung Manajemen'
  //       });
  //       break;
  //     case "2":
  //       setBkf({
  //         no_penyusunan: "Per-2/PJ.12/KP.0101/2020/2021",
  //         jns_peraturan: "PPh",
  //         judul_peraturan: "Peraturan 2",
  //         tentang: "Tentang 2",
  //         file_kajian: "kajianperencanaan-2.pdf",
  //         unit_incharge: 'Seksi Pengembangan Sistem Pendukung Manajemen'

  //       });
  //       break;
  //     case "3":
  //       setBkf({
  //         no_penyusunan: "Per-3/PJ.12/KP.0101/2020/2021",
  //         jns_peraturan: "Prakerja 3",
  //         judul_peraturan: "Peraturan 3",
  //         tentang: "Tentang 3",
  //         file_kajian: "kajianperencanaan-3.pdf",
  //         unit_incharge: 'Seksi Pengembangan Sistem Pendukung Manajemen'

  //       });
  //       break;

  //     default:
  //       break;
  //   }
  //   setShow(true);

  // };

  // const perencanaan = [
  //   {
  //     label: "Per-1/PJ.12/KP.0101/2020/2021",
  //     value: "1"
  //   },
  //   {
  //     label: "Per-2/PJ.12/KP.0101/2020/2021",
  //     value: "2"
  //   },
  //   {
  //     label: "Per-3/PJ.12/KP.0101/2020/2021",
  //     value: "3"
  //   }
  // ];
  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        {/* {show ? ( */}
          <div className="mt-5">
            <BkfEditForm
              actionsLoading={actionsLoading}
              proposal={bkf || initValues}
              btnRef={btnRef}
              // saveProposal={saveProposal}
            />
          </div>
        {/* ) : (
          <div className="form-group row">
            <label className="col-xl-3 col-lg-3 col-form-label">
              No Perencanaan
            </label>
            <div className="col-lg-9 col-xl-6">
              <Select
                options={perencanaan}
                onChange={value => handleChangePerencanaan(value)}
              />
            </div>
          </div>
        )} */}
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <BkfEditFooter
          backAction={backAction}
          btnRef={btnRef} />
      </CardFooter>
    
    </Card>
  );
}

export default BkfEdit;
