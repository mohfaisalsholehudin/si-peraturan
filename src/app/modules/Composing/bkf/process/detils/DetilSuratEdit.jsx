import React, { useEffect, useState, useRef } from "react";

import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../../_metronic/_partials/controls";
import { useSubheader } from "../../../../../../_metronic/layout";
import DetilSuratForm from "./DetilSuratForm";
import DetilSuratFooter from "./DetilSuratFooter";


export function DetilSuratEdit({history,
    match: {
      params: { id_surat,id }
    }
  }) {
    const initValues = {
        no_bkf: "",
        no_surat: "",
        tgl_surat: "",
        perihal: "",
        instansi_penerbit: "",
        instansi_penerima: ""
      };

  const [title, setTitle] = useState("");
  const suhbeader = useSubheader();

  useEffect(() => {
    let _title = id_surat ? "Edit Surat Menyurat" : "Tambah Surat Menyurat";

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps

  }, [id_surat, suhbeader]);
  const btnRef = useRef();

  const handleBack = () => {
    history.push(`/compose/bkf/process/detils/${id}/surat-undangan`)
  };

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
        <DetilSuratForm 
        initValues={initValues}
        btnRef={btnRef}
        />
        </>
      </CardBody>

      <CardFooter style={{ borderTop: "none" }}>
          <DetilSuratFooter 
          btnRef={btnRef}
          backAction={handleBack}
          />
      </CardFooter>
    </Card>
  );
}
