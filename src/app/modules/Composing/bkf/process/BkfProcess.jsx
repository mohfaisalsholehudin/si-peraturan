import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";
import BkfProcessDetils from "./BkfProcessDetils";
import BkfDraft from "./drafting/BkfDraft";

function BkfProcess({
    history,
    match: {
      params: { id }
    }
  }) {
  return (
    <Card>
      <CardHeader
        title="Daftar Proses Penyusunan Unit Eselon 1 Kemenkeu"
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="row">
            <div className="col-xl-6 col-lg-6 mb-3">
              <div className="row">
                <label className="col-xl-3 col-lg-3 col-form-label">
                  Tanggal Penyusunan
                </label>
                <div className="col-lg-9 col-xl-6">
                  <p className="text-muted pt-2">08-09-2021</p>
                </div>
              </div>
            </div>
            <div className="col-xl-6 col-lg-6 mb-3">
              <div className="row">
                <label className="col-xl-3 col-lg-3 col-form-label">
                  Jenis Peraturan
                </label>
                <div className="col-lg-9 col-xl-6">
                  <p className="text-muted pt-2"> PMK </p>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-xl-6 col-lg-6 mb-3">
              <div className="row">
                <label className="col-xl-3 col-lg-3 col-form-label">
                  Unit In Charge
                </label>
                <div className="col-lg-9 col-xl-6">
                  <p className="text-muted pt-2">
                    Seksi Pengembangan Sistem Pendukung Manajemen
                  </p>
                </div>
              </div>
            </div>
            <div className="col-xl-6 col-lg-6 mb-3">
              <div className="row">
                <label className="col-xl-3 col-lg-3 col-form-label">
                  Judul Peraturan
                </label>
                <div className="col-lg-9 col-xl-6">
                  <p className="text-muted pt-2"> Tindak Lanjut Peraturan </p>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-xl-6 col-lg-6 mb-3">
              <div className="row">
                <label className="col-xl-3 col-lg-3 col-form-label">
                  Usulan Simfoni
                </label>
                <div className="col-lg-9 col-xl-6">
                  <p className="text-muted pt-2">Ya</p>
                </div>
              </div>
            </div>
          </div>
        </>
      <BkfDraft
      id={id}
       />
       <BkfProcessDetils id={id} />
      </CardBody>

      <CardFooter style={{ borderTop: "none" }} />
    </Card>
  );
}

export default BkfProcess;
