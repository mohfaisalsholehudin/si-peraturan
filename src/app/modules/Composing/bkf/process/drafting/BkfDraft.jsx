import React from "react";
import { useHistory } from "react-router-dom";

import BootstrapTable from "react-bootstrap-table-next";
import * as columnFormatters from "../../../../../helpers/column-formatters";

function BkfDraft({ id }) {
  const history = useHistory();

  const draft = [
    // {
    //   id_jenis: "1",
    //   jenis: "PMK",
    //   file_kajian: "filekajian-1.pdf",
    //   status: "Draft",
    //   classes: "text-center pr-0",
    //   headerClasses: "text-center pr-3",
    // }
  ];
  const columns = [
    {
      dataField: "jenis",
      text: "Jenis",
      sort: true,
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3"
    },
    {
      dataField: "file_kajian",
      text: "Kajian",
      sort: true,
      formatter: columnFormatters.FileColumnFormatterComposeDrafting,
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        textAlign: "center"
      }
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      formatter: columnFormatters.StatusColumnFormatterComposeDrafting,
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3"
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterComposeDrafting,
      formatExtraData: {
        openProcess: process
        // openEditDialog: edit,
        // openDeleteDialog: deleteAction,
        // showProposal: open,
        // showReject: reject,
        // applyProposal: applyProposal
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];

  const addDraft = () => history.push(`/compose/bkf/draft/${id}/add`);

  const emptyDataMessage = () => {
    return (
      <div className="col-lg-6 col-xl-6 mb-3">
        <button
          type="button"
          className="btn btn-light-primary"
          style={{ float: "right" }}
          onClick={addDraft}
        >
          Tambah Draft
        </button>
      </div>
    );
  };

  return (
    <>
      <div className="row">
        <div
          className="col-lg-12 col-xl-12 mb-3"
          style={{ textAlign: "right" }}
        >
          <button
            type="button"
            className="btn btn-light-success"
            style={{
              float: "right"
            }}
            onClick={() => history.push(`/compose/bkf`)}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
        </div>
      </div>
      <BootstrapTable
        wrapperClasses="table-responsive"
        bordered={false}
        headerWrapperClasses="thead-light"
        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
        keyField="id_jenis"
        data={draft}
        columns={columns}
        bootstrap4
        noDataIndication={emptyDataMessage}
      ></BootstrapTable>
    </>
  );
}

export default BkfDraft;
