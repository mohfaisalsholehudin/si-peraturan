import React from "react";
import { Modal } from "react-bootstrap";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { Textarea } from "../../../../helpers";

function TindakLanjutSahliApprove({ handleCancel, approve, show, onHide }) {
  const initialValues = {
    catatan_sahli: ""
  };

  const validationSchema = Yup.object().shape({
    catatan_sahli: Yup.string()
      .min(2, "Minimum 2 Characters")
      .required("Alasan is required")
  });

  return (
    <Modal
      size="md"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Catatan
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
    <>
      <Formik
        enableReinitialize={true}
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={values => {
        //   console.log(values);
        approve(values)
        }}
      >
        {({ handleSubmit }) => {
          return (
            <Form className="form form-label-right">
              <div className="form-group row">
                <Field
                  name="catatan_sahli"
                  component={Textarea}
                  placeholder="Catatan"
                  custom={"custom"}
                />
              </div>
              <div className="col-lg-12" style={{ textAlign: "center" }}>
                <button
                  type="button"
                  onClick={onHide}
                  className="btn btn-light"
                  style={{
                    boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                  }}
                >
                  <i className="flaticon2-cancel icon-nm"></i>
                  Batal
                </button>
                {`  `}
                <button
                  type="submit"
                  onSubmit={() => handleSubmit()}
                  className="btn btn-success ml-2"
                  style={{
                    boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                  }}
                >
                  <i className="fas fa-check"></i>
                  Kirim
                </button>
              </div>
            </Form>
          );
        }}
      </Formik>
    </>
    </Modal.Body>
    </Modal>
  );
}

export default TindakLanjutSahliApprove;
