import React, { useEffect, useState } from "react";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../../_metronic/_partials/controls";
import { getDraft } from "../../../Evaluation/Api";
import DocumentEditor from "../../../../helpers/editor/DocumentEditor";
const {  FILE_URL } = window.ENV;

function TindakLanjutSahliDraftLihatReprocess({
  history,
  match: {
    params: { id, id_draft, name },
  },
}) {
  const [draft, setDraft] = useState("");
  const [content, setContent] = useState({
    file: "",
    lampiran: "",
  });

  const validationSchema = Yup.object().shape({
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        (value) => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        (value) => value && SUPPORTED_FORMATS.includes(value.type)
      ),
  });
  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  useEffect(() => {
    if (id_draft) {
      getDraft(id_draft).then(({ data }) => {
        switch (name) {
          case "usulan":
            setContent({
              body_draft: data.body_draft,
              file_upload: data.fileupload,
              lampiran: data.filelampiranr,
            });
            break;
          case "cosign unit":
            setContent({
              body_draft: data.body_cosign,
              file_upload: data.fileupload_cosign,
              lampiran: data.filelampiran_cosign,
            });
            break;
          case "cosign sahli":
            setContent({
              body_draft: data.body_sahli,
              file_upload: data.fieupload_sahli,
              lampiran: data.filelampiran_sahli,
            });
            break;
          default:
            break;
        }
      });
    }
  }, [id_draft, name]);
  return (
    <Card>
      <CardHeader
        title="Lihat Draft Peraturan"
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <DocumentEditor
            content={content}
            setDraft={setDraft}
            isReadOnly={true}
          />
          <Formik
            enableReinitialize={true}
            initialValues={content}
            validationSchema={validationSchema}
            onSubmit={(values) => {
              // saveDraft(values);
              // console.log(values);
            }}
          >
            {({ values }) => {
              return (
                <Form className="form form-label-right">
                  {/* Field File */}
                  <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      File Tersimpan
                    </label>
                    <div
                      className="col-lg-9 col-xl-6"
                      style={{ marginTop: "10px" }}
                    >
                      <a
                        href={FILE_URL + values.file_upload}
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        {values.file_upload
                          ? values.file_upload
                          : null}
                      </a>
                    </div>
                  </div>
                  {/* Field Lampiran */}
                  <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Lampiran Tersimpan
                    </label>
                    <div
                      className="col-lg-9 col-xl-6"
                      style={{ marginTop: "10px" }}
                    >
                      <a
                        href={FILE_URL + values.lampiran}
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        {values.lampiran
                          ? values.lampiran
                          : null}
                      </a>
                    </div>
                  </div>

                  <div className="col-lg-12" style={{ textAlign: "right" }}>
                    <button
                      type="button"
                      className="btn btn-light-success ml-2"
                      onClick={() =>
                        history.push(
                          `/compose/reprocess/detils/${id}/tindak-lanjut-sahli`
                        )
                      }
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                      }}
                    >
                      <i className="fa fa-arrow-left"></i>
                      Kembali
                    </button>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </>
      </CardBody>
    </Card>
  );
}

export default TindakLanjutSahliDraftLihatReprocess;
