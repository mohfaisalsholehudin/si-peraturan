import React, { useState, useEffect } from "react";
import { Route } from "react-router-dom";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";
import { useHistory } from "react-router-dom";

//Helpers
import {
  sortCaret,
  headerSortingClasses
} from "../../../../../_metronic/_helpers";
import { Pagination } from "../../../../helpers/pagination/Pagination";
import * as columnFormatters from "../../../../helpers/column-formatters";
import { getCosignUnit } from "../../../Evaluation/Api";
import TindakLanjutDirektoratUnitOpen from "./PengajuanRancanganJawabanUnitOpen";
import CosignSahliUnitOpen from "./PengajuanRancanganJawabanUnitOpen";
import PengajuanRancanganJawabanUnitOpenReprocess from "./PengajuanRancanganJawabanUnitOpen";

function PengajuanRancanganJawabanUnitReprocess({ id }) {
  const history = useHistory();
  const [content, setContent] = useState([]);

  const openAction = (id_cosign) => {
    history.push(`/compose/reprocess/detils/${id}/pengajuan-rancangan/${id_cosign}/unit/open`)
  };

  useEffect(() => {
   getCosignUnit(id).then(({data})=> {
     setContent(data)
   })
  }, [id]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "nama_unit",
      text: "Nama Unit",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "tgl_surat",
      text: "Tanggal Jawaban",
      sort: true,
      formatter:
        columnFormatters.DateFormatterComposeProcessTindakLanjutDirektoratJawaban,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
        columnFormatters.ActionsColumnFormatterComposeProcessTindakLanjutDirektoratUnit,
      formatExtraData: {
        openProcess: openAction
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];

  const emptyDataMessage = () => {
    return "No Data";
  };
  const { SearchBar } = Search;
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "nama_unit",
    pageNumber: 1,
    pageSize: 50
  };
  const defaultSorted = [{ dataField: "nama_unit", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };

  return (
    <>
    <>
      <PaginationProvider pagination={paginationFactory(pagiOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <>
              <ToolkitProvider
                keyField="id_cosign_unit"
                data={content}
                columns={columns}
                search
              >
                {props => (
                  <div>
                    <div className="row">
                      <div className="col-lg-12 col-xl-12 mb-3 mt-3">
                        <h4> Cosign Unit</h4>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-6 col-xl-6 mb-3">
                        <SearchBar
                          {...props.searchProps}
                          style={{ width: "500px" }}
                        />
                        <br />
                      </div>
                    </div>
                    <BootstrapTable
                      {...props.baseProps}
                      wrapperClasses="table-responsive"
                      bordered={false}
                      headerWrapperClasses="thead-light"
                      classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                      defaultSorted={defaultSorted}
                      bootstrap4
                      noDataIndication={emptyDataMessage}
                      {...paginationTableProps}
                    ></BootstrapTable>
                    <Pagination paginationProps={paginationProps} />
                  </div>
                )}
              </ToolkitProvider>
            </>
          );
        }}
      </PaginationProvider>
    </>
    <Route path="/compose/reprocess/detils/:id/pengajuan-rancangan/:id_cosign/unit/open">
        {({ history, match }) => (
          <PengajuanRancanganJawabanUnitOpenReprocess
            show={match != null}
            id={match && match.params.id}
            id_cosign={match && match.params.id_cosign}
            after={false}
            onHide={() => {
              history.push(
                `/compose/reprocess/detils/${id}/pengajuan-rancangan`
              );
            }}
            onRef={() => {
              history.push(
                `/compose/reprocess/detils/${id}/pengajuan-rancangan`

              );
            }}
          />
        )}
      </Route>
    </>
  );
}

export default PengajuanRancanganJawabanUnitReprocess;
