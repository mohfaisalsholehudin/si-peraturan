import React from "react";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../_metronic/_partials/controls";
import MonitoringTable from "./MonitoringTable";

function Monitoring() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Penyusunan Peraturan"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <MonitoringTable />
        </CardBody>
      </Card>
    </>
  );
}

export default Monitoring;
