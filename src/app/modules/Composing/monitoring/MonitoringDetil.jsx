import React, { useState, useEffect } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../_metronic/_partials/controls";

import { getPenyusunanById } from "../../Evaluation/Api";
import { DateFormat } from "../../../helpers/DateFormat";
import * as auth from "../../Auth/_redux/authRedux";
import { connect } from "react-redux";
import * as columnFormatters from "../../../helpers/column-formatters";

function MonitoringDetil({
  history,
  setStatus,
  match: {
    params: { id }
  }
}) {
  const [content, setContent] = useState([]);

  useEffect(() => {
    // console.log(props)
    getPenyusunanById(id).then(({ data }) => {
      setContent(data);
      setStatus(data.status)
    });
  }, [id, setStatus]);
  const handleBack = () => {
    history.push(`/compose/monitoring`)
  }

  const detils = [
    {
      
      id_detil: "input-lhr",
      nama_proses: "Input LHR Pembahasan"
    },
    {
      id_detil: "surat-undangan",
      nama_proses: "Pembuatan Surat Undangan"
    },
    {
      id_detil: "perekaman-peraturan",
      nama_proses: "Peraturan Terkait"
    },
    {
      id_detil: "nd-permintaan",
      nama_proses: "ND Surat Permintaan Pendapat"
    },
    {
      id_detil: "cosign_djp",
      nama_proses: "Cosign Jawaban Unit DJP"
    },
    {
      id_detil: "cosign-kementerian",
      nama_proses: "Cosign Jawaban Kementerian / KL Lain"
    },
    {
      id_detil: "tindak-lanjut-direktorat",
      nama_proses:
        "Tindak Lanjut Jawaban dari Direktorat Lain / Kanwil / KPP / KL"
    },
    {
      id_detil: "cosign-sahli",
      nama_proses: "Rekam / Review Jawaban Staff Ahli Menteri",
      status: content.status
    },
    {
      id_detil: "tindak-lanjut-sahli",
      nama_proses: "Tindak Lanjut Jawaban dari Staff Ahli Menteri",
      status: content.status
    },
    {
      id_detil: "pengajuan-rancangan",
      nama_proses: "Penyampaian Rancangan ke Direktur Jenderal Pajak",
      status: content.status
    },
    // {
    //   id_detil: "penetapan-peraturan",
    //   nama_proses: "Penetapan Peraturan",
    //   status: content.status
    // }
  ];
  const process = id_detil => {
    switch (id_detil) {
      case "nd-permintaan":
        history.push(`/compose/monitoring/detils/${id}/nd-permintaan`);
        break;
      case "input-lhr":
        history.push(`/compose/monitoring/detils/${id}/input-lhr`);
        break;
      case "cosign_djp":
        history.push(`/compose/monitoring/detils/${id}/cosign-djp`);
        break;
      case "cosign-kementerian":
        history.push(`/compose/monitoring/detils/${id}/cosign-kementerian`);
        break;
      case "tindak-lanjut-direktorat":
        history.push(`/compose/monitoring/detils/${id}/tindak-lanjut-direktorat`);
        break;
      case "cosign-sahli":
        history.push(`/compose/monitoring/detils/${id}/cosign-sahli`);
        break;
      case "tindak-lanjut-sahli":
        history.push(`/compose/monitoring/detils/${id}/tindak-lanjut-sahli`);
        break;
      case "pengajuan-rancangan":
        history.push(`/compose/monitoring/detils/${id}/pengajuan-rancangan`);
        break;
      case "surat-undangan":
        history.push(`/compose/monitoring/detils/${id}/surat-undangan`);
        break;
      case "perekaman-peraturan":
        history.push(`/compose/monitoring/detils/${id}/perekaman-peraturan`);
        break;
      // case "penetapan-peraturan":
      //     history.push(`/compose/monitoring/detils/${id}/penetapan-peraturan`);
      //     break;
      default:
        break;
    }
  };

  const columns = [
    {
      dataField: "nama_proses",
      text: "Detil Proses",
      sort: true
    },

    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterComposeMonitoringDetil,
      formatExtraData: {
        openProcess: process
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];

  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Proses Penyusunan Peraturan"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <>
            <>
              <div className="row">
                <div className="col-xl-6 col-lg-6 mb-3">
                  <div className="row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      No Penyusunan
                    </label>
                    <div className="col-lg-9 col-xl-6">
                      <p className="text-muted pt-2">
                        {`: ${content.no_penyusunan}`}
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-xl-6 col-lg-6 mb-3">
                  <div className="row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Jenis Peraturan
                    </label>
                    <div className="col-lg-9 col-xl-6">
                      <p className="text-muted pt-2">
                        {`: ${content.jns_peraturan}`}
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-xl-6 col-lg-6 mb-3">
                  <div className="row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Tanggal Penyusunan
                    </label>
                    <div className="col-lg-9 col-xl-6">
                      <p className="text-muted pt-2">
                        {`: ${DateFormat(content.tgl_penyusunan)}`}
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-xl-6 col-lg-6 mb-3">
                  <div className="row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Judul Peraturan
                    </label>
                    <div className="col-lg-9 col-xl-6">
                      <p className="text-muted pt-2">
                        {`: ${content.judul_peraturan}`}
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-xl-6 col-lg-6 mb-3">
                  <div className="row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Unit In Charge
                    </label>
                    <div className="col-lg-9 col-xl-6">
                      <p className="text-muted pt-2">
                        {`: ${content.unit_incharge}`}
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-xl-6 col-lg-6 mb-3">
                  <div className="row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Usulan Simfoni
                    </label>
                    <div className="col-lg-9 col-xl-6">
                      <p className="text-muted pt-2">{`: ${content.simfoni}`}</p>
                    </div>
                  </div>
                </div>
              </div>
            </>
            <BootstrapTable
              wrapperClasses="table-responsive"
              bordered={false}
              headerWrapperClasses="thead-light"
              classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
              keyField="id_detil"
              data={detils}
              columns={columns}
              bootstrap4
            ></BootstrapTable>
          </>
        </CardBody>
        <CardFooter style={{ borderTop: "none" }}>
          <>
          <div className="col-lg-12" style={{ textAlign: "right" }}>
          <button
            type="button"
            onClick={handleBack}
            className="btn btn-light-success"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
          
        </div>
          </>
      </CardFooter>
      </Card>
    </>
  );
}

export default connect(null, auth.actions)(MonitoringDetil);
