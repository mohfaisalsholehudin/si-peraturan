import React, { useEffect, useState, useRef } from "react";
import axios from "axios";
import EmailEditor from "react-email-editor";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import swal from "sweetalert";
import CustomFileInput from "../../../../helpers/form/CustomFileInput";
import CustomLampiranInput from "../../../../helpers/form/CustomLampiranInput";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../../_metronic/_partials/controls";
import {
  uploadFile,
  getDraft,
  saveDraftComposingDirjen,
  updateDraftComposingDirjen
} from "../../../Evaluation/Api";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import Editor from "ckeditor5-custom-build/build/ckeditor";
// import "../../proposal/process/drafting/decoupled.css";
const {BACKEND_URL} = window.ENV;


function PengajuanRancanganDraftEdit({
  history,
  match: {
    params: { id, id_draft, name }
  }
}) {
  const [draft, setDraft] = useState("");
  const [show, setShow] = useState(false);
  const [content, setContent] = useState({
    file: "",
    lampiran: ""
  });

  const validationSchema = Yup.object().shape({
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        value => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        value => value && SUPPORTED_FORMATS.includes(value.type)
      )
  });
  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  useEffect(() => {
    // if (id_draft) {
    //   axios.get(`${BACKEND_URL}/api/draftperaturan/byidpenyusunan/${id}`, {
    //     transformResponse: res => {
    //       res[0].body_direktur
    //         ? setDraft(JSON.parse(res[0].body_direktur))
    //         : setDraft(JSON.parse(res[0].body_sahli)) ? setDraft(JSON.parse(res[0].body_sahli)) : setDraft(JSON.parse(res[0].body_draft));
    //       return res;
    //     },
    //     responseType: "json"
    //   });
    //   getDraft(id_draft).then(({ data }) => {
    //     setContent({
    //       jns_draft: data.jns_draft,
    //       file_upload: data.fileupload_direktur
    //         ? data.fileupload_direktur
    //         : data.fieupload_sahli ? data.fieupload_sahli : "",
    //       lampiran: data.filelampiran_direktur
    //         ? data.filelampiran_direktur
    //         : data.filelampiran_sahli ? data.filelampiran_sahli : ""
    //     });
    //   });
    // }
    if(id_draft){
      getDraft(id_draft).then(({ data }) => {
        setContent({
          body_draft: data.body_direktur ? data.body_direktur : data.body_sahli ? data.body_sahli : data.body_draft,
          jns_draft: data.jns_draft,
          file_upload: data.fileupload_direktur ? data.fileupload_direktur : data.fieupload_sahli ? data.fieupload_sahli : "",
          lampiran: data.filelampiran_direktur ? data.filelampiran_direktur : data.filelampiran_sahli ? data.filelampiran_sahli : ""
        });
      });
    }
  }, [id_draft, id]);

  const saveDraft = values => {
    if (!id_draft) {
      const formData = new FormData();
      formData.append("file", values.file);
      uploadFile(formData).then(data_1 => {
        const formLampiran = new FormData();
        formLampiran.append("file", values.lampiran);
        uploadFile(formLampiran).then(data_2 => {
            saveDraftComposingDirjen(
              draft,
              name,
              id,
              data_1.data.message,
              data_2.data.message
            ).then(({ status }) => {
              if (status === 201 || status === 200) {
                swal("Berhasil", "Data berhasil disimpan", "success").then(
                  () => {
                    history.push(
                      `/compose/process/detils/${id}/pengajuan-rancangan`
                    );
                  }
                );
              } else {
                swal("Gagal", "Data gagal disimpan", "error").then(() => {
                  history.push(
                    `/compose/process/detils/${id}/pengajuan-rancangan`
                  );
                });
              }
            });
        });
      });
    } else {
      if (values.file.name) {
        const formData = new FormData();
        formData.append("file", values.file);
        uploadFile(formData).then(data_1 => {
          if (!values.lampiran.name) {
              updateDraftComposingDirjen(
                draft,
                id_draft,
                data_1.data.message
              ).then(({ status }) => {
                if (status === 201 || status === 200) {
                  swal("Berhasil", "Data berhasil disimpan", "success").then(
                    () => {
                      history.push(
                        `/compose/process/detils/${id}/pengajuan-rancangan`
                      );
                    }
                  );
                } else {
                  swal("Gagal", "Data gagal disimpan", "error").then(() => {
                    history.push(
                      `/compose/process/detils/${id}/pengajuan-rancangan`
                    );
                  });
                }
              });
          } else {
            const formLampiran = new FormData();
            formLampiran.append("file", values.lampiran);
            uploadFile(formLampiran).then(data_2 => {
                updateDraftComposingDirjen(
                  draft,
                  id_draft,
                  data_1.data.message,
                  data_2.data.message
                ).then(({ status }) => {
                  if (status === 201 || status === 200) {
                    swal("Berhasil", "Data berhasil disimpan", "success").then(
                      () => {
                        history.push(
                          `/compose/process/detils/${id}/pengajuan-rancangan`
                        );
                      }
                    );
                  } else {
                    swal("Gagal", "Data gagal disimpan", "error").then(() => {
                      history.push(
                        `/compose/process/detils/${id}/pengajuan-rancangan`
                      );
                    });
                  }
                });
            });
          }
        });
      } else if (values.lampiran.name) {
        const formLampiran = new FormData();
        formLampiran.append("file", values.lampiran);
        uploadFile(formLampiran).then(data_1 => {
          if (!values.file.name) {
              updateDraftComposingDirjen(
                draft,
                id_draft,
                values.file_upload,
                data_1.data.message
              ).then(({ status }) => {
                if (status === 201 || status === 200) {
                  swal("Berhasil", "Data berhasil disimpan", "success").then(
                    () => {
                      history.push(
                        `/compose/process/detils/${id}/pengajuan-rancangan`
                      );
                    }
                  );
                } else {
                  swal("Gagal", "Data gagal disimpan", "error").then(() => {
                    history.push(
                      `/compose/process/detils/${id}/pengajuan-rancangan`
                    );
                  });
                }
              });
          } else {
            const formData = new FormData();
            formData.append("file", values.file);
            uploadFile(formData).then(data_2 => {
                updateDraftComposingDirjen(
                  draft,
                  id_draft,
                  data_2.data.message,
                  data_1.data.message
                ).then(({ status }) => {
                  if (status === 201 || status === 200) {
                    swal("Berhasil", "Data berhasil disimpan", "success").then(
                      () => {
                        history.push(
                          `/compose/process/detils/${id}/pengajuan-rancangan`
                        );
                      }
                    );
                  } else {
                    swal("Gagal", "Data gagal disimpan", "error").then(() => {
                      history.push(
                        `/compose/process/detils/${id}/pengajuan-rancangan`
                      );
                    });
                  }
                });
            });
          }
        });
      } else {
          updateDraftComposingDirjen(
            draft,
            id_draft,
            values.file_upload,
            values.lampiran
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
                history.push(
                  `/compose/process/detils/${id}/pengajuan-rancangan`
                );
              });
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                history.push(
                  `/compose/process/detils/${id}/pengajuan-rancangan`
                );
              });
            }
          });
      }
    }
  };

  return (
    <Card>
      <CardHeader
        title="Edit Draft Peraturan"
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
        <div className="row">
            <div className="document-editor" style={{ width: "100%" }}>
              <div className="document-editor__toolbar"></div>
              <div className="document-editor__editable-container">
                <CKEditor
                  onReady={editor => {
                    window.editor = editor;
                    setShow(true);

                    // Add these two lines to properly position the toolbar
                    const toolbarContainer = document.querySelector(
                      ".document-editor__toolbar"
                    );
                    toolbarContainer.appendChild(
                      editor.ui.view.toolbar.element
                    );
                  }}
                  config={{
                    simpleUpload: {
                      // The URL that the images are uploaded to.
                      uploadUrl: `${BACKEND_URL}/api/uploadckeditor`,

                      // Enable the XMLHttpRequest.withCredentials property.
                      withCredentials: false,

                      // Headers sent along with the XMLHttpRequest to the upload server.
                      headers: {
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Credentials": "true"
                      }
                    }
                  }}
                  onChange={(event, editor) => {setDraft(editor.getData()); setShow(true)}}
                  editor={Editor}
                  data={content.body_draft ? content.body_draft : null}
                />
              </div>
            </div>
          </div>
          <div className="mt-4">
            <Formik
              enableReinitialize={true}
              initialValues={content}
              validationSchema={validationSchema}
              onSubmit={values => {
                saveDraft(values);
                // console.log(values);
              }}
            >
              {({ handleSubmit, values }) => {
                return (
                  <Form className="form form-label-right">
                    {draft ? (
                      <>
                        <div className="form-group row">
                          <label className="col-xl-3 col-lg-3 col-form-label">
                            Upload File
                          </label>
                          <div className="col-lg-9 col-xl-6">
                            <Field
                              name="file"
                              component={CustomFileInput}
                              title="Select a file"
                              label="File"
                              style={{ display: "flex" }}
                            />
                          </div>
                        </div>
                      </>
                    ) : null}
                    <div className="form-group row">
                      <label className="col-xl-3 col-lg-3 col-form-label">
                        Upload Lampiran
                      </label>
                      <div className="col-lg-9 col-xl-6">
                        <Field
                          name="lampiran"
                          component={CustomLampiranInput}
                          title="Select a file"
                          label="Lampiran"
                          style={{ display: "flex" }}
                        />
                      </div>
                    </div>
                    <div className="col-lg-12" style={{ textAlign: "right" }}>
                      <button
                        type="button"
                        className="btn btn-light ml-2"
                        // onSubmit={() => handleSubmit()}
                        onClick={() =>
                          history.push(
                            `/compose/process/detils/${id}/pengajuan-rancangan`
                          )
                        }
                        style={{
                          boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                        }}
                      >
                        <i className="fa fa-arrow-left"></i>
                        Kembali
                      </button>
                      <button
                        type="submit"
                        className="btn btn-success ml-2"
                        onSubmit={() => handleSubmit()}
                        style={{
                          boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                        }}
                      >
                        <i className="fas fa-save"></i>
                        Simpan
                      </button>
                    </div>
                  </Form>
                );
              }}
            </Formik>
          </div>
        </>
      </CardBody>
    </Card>
  );
}

export default PengajuanRancanganDraftEdit;
