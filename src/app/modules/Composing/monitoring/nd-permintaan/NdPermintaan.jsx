import React from "react";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../../_metronic/_partials/controls";
import MonitoringNdPermintaanTable from "./NdPermintaanTable";

function MonitoringNdPermintaan({
    history,
    match: {
      params: { id }
    }
  }) {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Surat Permintaan Jawaban"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <MonitoringNdPermintaanTable id={id} />
        </CardBody>
      </Card>
    </>
  );
}

export default MonitoringNdPermintaan;
