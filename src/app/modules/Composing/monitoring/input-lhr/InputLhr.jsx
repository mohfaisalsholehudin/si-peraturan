import React, { useState, useEffect } from "react";
import swal from "sweetalert";
import { useSelector } from "react-redux";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";

import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";

//Helpers
import {
  sortCaret,
  headerSortingClasses
} from "../../../../../_metronic/_helpers";
import { Pagination } from "../../../../helpers/pagination/Pagination";
import * as columnFormatters from "../../../../helpers/column-formatters";
import { getLhr, deleteLhr } from "../../../Evaluation/Api";

function MonitoringInputLhr({
  history,
  match: {
    params: { id }
  }
}) {
  const [content, setContent] = useState([]);
  const { status } = useSelector(state => state.auth);

  const showDetil = id_lhr =>
    history.push(`/compose/monitoring/detils/${id}/input-lhr/${id_lhr}/open`);
    
  useEffect(() => {
    getLhr(id).then(({ data }) => {
      setContent(data);
    });
  }, [id]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter
    },
    {
      dataField: "no_surat_lhr",
      text: "No Surat LHR",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "tgl_rapat",
      text: "Tgl Rapat",
      sort: true,
      formatter: columnFormatters.DateFormatterComposeProposalLhr,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "tmpt_bahas",
      text: "Tempat",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "unit_penyelenggara",
      text: "Unit Penyelenggara",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "file_lhr",
      text: "File LHR",
      sort: true,
      formatter: columnFormatters.FileColumnFormatterComposeDetilLhr,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.StatusColumnFormatterComposeDetilLhr,
      headerSortingClasses,
      style: {
        minWidth: "200px"
      }
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
        status === "Proses Penyusunan"
          ? columnFormatters.ActionsColumnFormatterComposeProcessInputLhr
          : columnFormatters.ActionsColumnFormatterComposeProcessInputLhrJustView,
      formatExtraData: {
        // openEditDialog: edit,
        showDetil: showDetil,
        // openDeleteDialog: deleteAction
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];
  const { SearchBar } = Search;
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "no_surat_lhr",
    pageNumber: 1,
    pageSize: 5
  };
  const defaultSorted = [{ dataField: "no_surat_lhr", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };

  const emptyDataMessage = () => {
    return <div className="text-center">No Data to Display</div>;
  };
  return (
    <Card>
      <CardHeader
        title="Daftar LHR"
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <PaginationProvider pagination={paginationFactory(pagiOptions)}>
            {({ paginationProps, paginationTableProps }) => {
              return (
                <>
                  <ToolkitProvider
                    keyField="no_surat_lhr"
                    data={content}
                    columns={columns}
                    search
                  >
                    {props => (
                      <div>
                        <div className="row">
                          <div className="col-lg-6 col-xl-6 mb-3">
                            <SearchBar
                              {...props.searchProps}
                              style={{ width: "500px" }}
                            />
                            <br />
                          </div>
                          <div
                            className="col-lg-6 col-xl-6 mb-3"
                            style={{ textAlign: "right" }}
                          >
                            {/* {status === "Proses Penyusunan" ||
                            status === "Penelitian Cosign Unit/KL Es4" ||
                            status === "Cosign Jawaban" ||
                            status === "Cosign Sahli" ||
                            status === "Cosign Dirjen" ? (
                              <button
                                type="button"
                                className="btn btn-primary ml-3"
                                style={{
                                  float: "right"
                                }}
                                onClick={() =>
                                  history.push(
                                    `/compose/process/detils/${id}/input-lhr/add`
                                  )
                                }
                              >
                                <i className="fa fa-plus"></i>
                                Tambah
                              </button>
                            ) : null} */}

                            <button
                              type="button"
                              className="btn btn-light-success"
                              style={{
                                float: "right"
                              }}
                              onClick={() =>
                                history.push(`/compose/monitoring/${id}/detil`)
                              }
                            >
                              <i className="fa fa-arrow-left"></i>
                              Kembali
                            </button>
                          </div>
                        </div>
                        <BootstrapTable
                          {...props.baseProps}
                          wrapperClasses="table-responsive"
                          bordered={false}
                          headerWrapperClasses="thead-light"
                          classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                          defaultSorted={defaultSorted}
                          bootstrap4
                          noDataIndication={emptyDataMessage}
                          {...paginationTableProps}
                        ></BootstrapTable>
                        <Pagination paginationProps={paginationProps} />
                      </div>
                    )}
                  </ToolkitProvider>
                </>
              );
            }}
          </PaginationProvider>
        </>
      </CardBody>
    </Card>
  );
}

export default MonitoringInputLhr;
