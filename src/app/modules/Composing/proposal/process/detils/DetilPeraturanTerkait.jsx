import React, { useState, useEffect } from "react";
import swal from "sweetalert";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";

import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../../_metronic/_partials/controls";

//Helpers
import {
  sortCaret,
  headerSortingClasses
} from "../../../../../../_metronic/_helpers";
import { Pagination } from "../../../../../helpers/pagination/Pagination";
import * as columnFormatters from "../../../../../helpers/column-formatters";
import { deletePeraturanTerkait, getPeraturanTerkait } from "../../../../Evaluation/Api";

function DetilPeraturanTerkait({
  history,
  match: {
    params: { id, id_perter }
  }
}) {
  const [content, setContent] = useState([]);

  const edit = (id_perter) =>   history.push(`/compose/proposal/process/detils/${id}/perter/${id_perter}/edit`);
  const deleteAction = id_perter => {
    swal({
      title: "Apakah Anda Yakin?",
      text: "Klik OK untuk melanjutkan",
      icon: "warning",
      buttons: true,
      dangerMode: true
    }).then(willDelete => {
      if (willDelete) {
        deletePeraturanTerkait(id_perter).then(({ status }) => {
          if (status === 200) {
            swal("Berhasil", "Data berhasil dihapus", "success").then(() => {
              history.push("/dashboard");
              history.replace(`/compose/proposal/process/detils/${id}/perter`);
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push("/dashboard");
              history.replace(`/compose/proposal/process/detils/${id}/perter`);
            });
          }
        });
      }
      // else {
      //   swal("Your imaginary file is safe!");
      // }
    });
  };

  useEffect(() => {
    getPeraturanTerkait(id).then(({ data }) => {
      setContent(data);
    });
  }, [id]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "nomor_peraturan",
      text: "No Peraturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "tgl_ditetapkan",
      text: "Tgl Ditetapkan",
      sort: true,
      formatter: columnFormatters.DateFormatterComposeProposalPeraturanTerkaitDitetapkan,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "tgl_diundangkan",
      text: "Tgl Diundangkan",
      sort: true,
      formatter: columnFormatters.DateFormatterComposeProposalPeraturanTerkaitDiundangkan,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "perihal",
      text: "Tentang",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.StatusColumnFormatterComposeDetilPeraturanTerkait,
      headerSortingClasses,
      style: {
        minWidth: "150px"
      }
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterComposeDetilPerter,
      formatExtraData: {
        openEditDialog: edit,
        openDeleteDialog: deleteAction,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];
  const { SearchBar } = Search;
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "nomor_peraturan",
    pageNumber: 1,
    pageSize: 5
  };
  const defaultSorted = [{ dataField: "nomor_peraturan", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };

  const emptyDataMessage = () => {
    return <div className="text-center">No Data to Display</div>;
  };

  return (
    <Card>
      <CardHeader
        title="Daftar Peraturan Terkait"
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <PaginationProvider pagination={paginationFactory(pagiOptions)}>
            {({ paginationProps, paginationTableProps }) => {
              return (
                <>
                  <ToolkitProvider
                    keyField="nomor_peraturan"
                    data={content}
                    columns={columns}
                    search
                  >
                    {props => (
                      <div>
                        <div className="row">
                          <div className="col-lg-6 col-xl-6 mb-3">
                            <SearchBar
                              {...props.searchProps}
                              style={{ width: "500px" }}
                            />
                            <br />
                          </div>
                          <div
                            className="col-lg-6 col-xl-6 mb-3"
                            style={{ textAlign: "right" }}
                          >
                            <button
                              type="button"
                              className="btn btn-primary ml-3"
                              style={{
                                float: "right"
                              }}
                              onClick={() =>
                                history.push(
                                  `/compose/proposal/process/detils/${id}/perter/add`
                                )
                              }
                            >
                              <i className="fa fa-plus"></i>
                              Tambah
                            </button>
                            <button
                              type="button"
                              className="btn btn-light-success"
                              style={{
                                float: "right"
                              }}
                              onClick={() =>
                                history.push(`/compose/proposal/${id}/process`)
                              }
                            >
                              <i className="fa fa-arrow-left"></i>
                              Kembali
                            </button>
                          </div>
                        </div>
                        <BootstrapTable
                          {...props.baseProps}
                          wrapperClasses="table-responsive"
                          bordered={false}
                          headerWrapperClasses="thead-light"
                          classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                          defaultSorted={defaultSorted}
                          bootstrap4
                          noDataIndication={emptyDataMessage}
                          {...paginationTableProps}
                        ></BootstrapTable>
                        <Pagination paginationProps={paginationProps} />
                      </div>
                    )}
                  </ToolkitProvider>
                </>
              );
            }}
          </PaginationProvider>
        </>
      </CardBody>

      <CardFooter style={{ borderTop: "none" }} />
    </Card>
  );
}

export default DetilPeraturanTerkait;
