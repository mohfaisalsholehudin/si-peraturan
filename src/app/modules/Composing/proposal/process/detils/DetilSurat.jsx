import React, { useState, useEffect } from "react";
import swal from "sweetalert";

import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../../_metronic/_partials/controls";
import * as columnFormatters from "../../../../../helpers/column-formatters";
import { deleteSuratMenyurat, getSurat } from "../../../../Evaluation/Api";

//Helpers
import {
  sortCaret,
  headerSortingClasses
} from "../../../../../../_metronic/_helpers";
import { Pagination } from "../../../../../helpers/pagination/Pagination";

function DetilSurat({
  history,
  match: {
    params: { id }
  }
}) {
  const [content, setContent] = useState([]);
  const edit = (id_surat) =>
    history.push(`/compose/proposal/process/detils/${id}/surat/${id_surat}/edit`);

    const showSurat =  async (id) => {
      await getSurat(id).then(({data})=> setContent(data))
    }
    const deleteAction = id_surat => {
      swal({
        title: "Apakah Anda Yakin?",
        text: "Klik OK untuk melanjutkan",
        icon: "warning",
        buttons: true,
        dangerMode: true
      }).then(willDelete => {
        if (willDelete) {
          deleteSuratMenyurat(id_surat).then(({ status }) => {
            if (status === 200) {
              swal("Berhasil", "Data berhasil dihapus", "success").then(() => {
                history.push("/dashboard");
                history.replace(`/compose/proposal/process/detils/${id}/surat`);
              });
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                history.push("/dashboard");
                history.replace(`/compose/proposal/process/detils/${id}/surat`);
              });
            }
          });
        }
        // else {
        //   swal("Your imaginary file is safe!");
        // }
      });
    };
    
      useEffect(() => {
        // getSurat(id).then(({ data }) => {
        //   setContent(data);
        // });
        showSurat(id)
      }, [id]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "nomor_surat",
      text: "No Surat",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "tgl_surat",
      text: "Tgl Surat",
      sort: true,
      formatter: columnFormatters.DateFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "unit_penerbit",
      text: "Instansi Penerbit",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "unit_penerima",
      text: "Instansi Penerima",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.StatusColumnFormatterComposeDetilSurat,
      headerSortingClasses
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterComposeDetilSurat,
      formatExtraData: {
        openEditDialog: edit,
        openDeleteDialog: deleteAction,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];
  const { SearchBar } = Search;
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "nomor_surat",
    pageNumber: 1,
    pageSize: 50
  };
  const defaultSorted = [{ dataField: "nomor_surat", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };

 

  const emptyDataMessage = () => {
    return <div className="text-center">No Data to Display</div>;
  };
  return (
    <Card>
      <CardHeader
        title="Daftar Dokumentasi Administrasi Persuratan"
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <PaginationProvider pagination={paginationFactory(pagiOptions)}>
            {({ paginationProps, paginationTableProps }) => {
              return (
                <>
                  <ToolkitProvider
                    keyField="nomor_surat"
                    data={content}
                    columns={columns}
                    search
                  >
                    {props => (
                      <div>
                        <div className="row">
                          <div className="col-lg-6 col-xl-6 mb-3">
                            <SearchBar
                              {...props.searchProps}
                              style={{ width: "500px" }}
                            />
                            <br />
                          </div>
                          <div
                            className="col-lg-6 col-xl-6 mb-3"
                            style={{ textAlign: "right" }}
                          >
                            <button
                              type="button"
                              className="btn btn-primary ml-3"
                              style={{
                                float: "right"
                              }}
                              onClick={() =>
                                history.push(
                                  `/compose/proposal/process/detils/${id}/surat/add`
                                )
                              }
                            >
                              <i className="fa fa-plus"></i>
                              Tambah
                            </button>
                            <button
                              type="button"
                              className="btn btn-light-success"
                              style={{
                                float: "right"
                              }}
                              onClick={() =>
                                history.push(`/compose/proposal/${id}/process`)
                              }
                            >
                              <i className="fa fa-arrow-left"></i>
                              Kembali
                            </button>
                          </div>
                        </div>
                        <BootstrapTable
                          {...props.baseProps}
                          wrapperClasses="table-responsive"
                          bordered={false}
                          headerWrapperClasses="thead-light"
                          classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                          defaultSorted={defaultSorted}
                          bootstrap4
                          noDataIndication={emptyDataMessage}
                          {...paginationTableProps}
                        ></BootstrapTable>
                        <Pagination paginationProps={paginationProps} />
                      </div>
                    )}
                  </ToolkitProvider>
                </>
              );
            }}
          </PaginationProvider>
        </>
      </CardBody>
    </Card>
  );
}

export default DetilSurat;
