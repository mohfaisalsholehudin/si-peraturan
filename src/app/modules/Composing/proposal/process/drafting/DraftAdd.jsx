/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { toAbsoluteUrl } from "../../../../../../_metronic/_helpers";
import "./Styles.css";

import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../../_metronic/_partials/controls";
import { getJenisPeraturan } from "../../../../Evaluation/Api";
function DraftAdd({
  match: {
    params: { id }
  }
}) {
  const [draft, setDraft] = useState([]);
  const history = useHistory();

  const handleClickDraft = val => {
    history.push(`/compose/proposal/draft/${id}/add/${val}`);
  };

  useEffect(() => {
    getJenisPeraturan().then(({ data }) => {
      // setJenisPeraturan(data);
      data.map(dt=> {
        return dt.status === 'AKTIF' ? setDraft(draft => [...draft, dt]) : null
      })
    });
  }, []);

  const showDraft = () => {
    return draft.map((data, index) => {
      return (
        <div className="col-lg-3 mb-3 text-center" key={index}>
          <img
            src={toAbsoluteUrl("/media/images/image-grey.png")}
            alt="Avatar"
            className="image"
          />
          <div className="middle">
            <button
              type="button"
              className="btn btn-primary"
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                float: "right",
                width: "100px"
              }}
              onClick={() => handleClickDraft(data.nm_jnsperaturan)}
            >
              Pilih
            </button>
          </div>
          <div className="text-center mt-5">
            <h5> {data.nm_jnsperaturan} </h5>
          </div>
        </div>
      );
    });
  };

  return (
    <Card>
      <CardHeader
        title="Buat Draft Peraturan"
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="row">
            <div
              className="col-lg-12 col-xl-12 mb-3"
              style={{ textAlign: "right" }}
            >
              <button
                type="button"
                className="btn btn-light-success"
                style={{
                  float: "right"
                }}
                onClick={() => history.push(`/compose/proposal/${id}/process`)}
              >
                <i className="fa fa-arrow-left"></i>
                Kembali
              </button>
            </div>
          </div>
          <div className="row">{showDraft()}</div>
        </>
      </CardBody>

      <CardFooter style={{ borderTop: "none" }} />
    </Card>
  );
}

export default DraftAdd;
