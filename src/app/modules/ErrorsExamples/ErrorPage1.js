import React from "react";
import { connect } from "react-redux";
import {toAbsoluteUrl} from "../../../_metronic/_helpers";
import * as auth from "../Auth/_redux/authRedux"

function ErrorPage1(props) {
  const {SSO_URL} = window.ENV;
  const wnd = window.open(`${SSO_URL}/logout`);

  setTimeout(() => {
    props.logout()
    wnd.close();
  },5000)
  return (
    <div className="d-flex flex-column flex-root">
      <div
        className="d-flex flex-row-fluid flex-column bgi-size-cover bgi-position-center bgi-no-repeat p-10 p-sm-30"
        style={{
          backgroundImage: `url(${toAbsoluteUrl("/media/bg/error/401.jpg")})`
        }}
      >
      </div>
    </div>
  );
}

export default connect(null, auth.actions)(ErrorPage1)