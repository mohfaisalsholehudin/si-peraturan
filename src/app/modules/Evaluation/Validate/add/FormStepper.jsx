/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-restricted-imports */

import React, { useState, useEffect } from "react";
import swal from "sweetalert";
import { Route, useHistory } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import StepConnector from "@material-ui/core/StepConnector";
import Button from "@material-ui/core/Button";
import { Input, Textarea, Select as Sel } from "../../../../helpers";
import { Field } from "formik";
import {
  getJenisPeraturanByAplikasi,
  getRegulasiPerpajakanTerima,
} from "../../Api";
import CustomZipInput from "../../../../helpers/form/CustomZipInput";
import CircularProgress from "@material-ui/core/CircularProgress";
import { green } from "@material-ui/core/colors";
import SearchModal from "./SearchModal";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "90%",
  },
  button: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  connectorActive: {
    "& $connectorLine": {
      borderColor: theme.palette.secondary.main,
    },
  },
  connectorCompleted: {
    "& $connectorLine": {
      borderColor: theme.palette.primary.main,
    },
  },
  connectorDisabled: {
    "& $connectorLine": {
      borderColor: theme.palette.grey[100],
    },
  },
  connectorLine: {
    transition: theme.transitions.create("border-color"),
  },
  buttonProgress: {
    color: green[500],
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12,
  },
}));

function getSteps() {
  return [
    "Detil Validasi",
    "Peraturan / Penegasan Terkait",
    "Usulan Penyusunan / Perubahan / Penggantian Peraturan / Penegasan",
  ];
}

export default function FormStepper({
  btnRef,
  check,
  handleChange,
  handlePeraturan,
  per,
  handleDownload,
  fileDownload,
  loading,
  data,
  id_usulan,
  emptyPeraturan,
}) {
  const classes = useStyles();
  const history = useHistory();
  const [activeStep, setActiveStep] = React.useState(0);
  const steps = getSteps();
  const [show, setShow] = useState(false);
  const [peraturan, setPeraturan] = useState([]);
  const [jenisPeraturan, setJenisPeraturan] = useState([]);
  const [val, setVal] = useState([]);

  useEffect(() => {
    getJenisPeraturanByAplikasi("Tata Kelola Peraturan").then(({ data }) => {
      // setJenisPeraturan(data);
      data.map((dt) => {
        return dt.status === "AKTIF"
          ? setJenisPeraturan((peraturan) => [...peraturan, dt])
          : null;
      });
    });
    getRegulasiPerpajakanTerima().then(({ data }) => {
      data.content.map((data) => {
        if (data.no_regulasi) {
          setPeraturan((peraturan) => [
            ...peraturan,
            {
              label: data.no_regulasi,
              value: data.id_peraturan,
            },
          ]);
        }
      });
    });
  }, []);
  useEffect(() => {
    if (data.judul_peraturan) {
      setShow(true);
    }
  }, [data.judul_peraturan]);

  function handleNext() {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  }

  function handleBack() {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  }

  // function handleReset() {
  //   setActiveStep(0);
  // }
  const connector = (
    <StepConnector
      classes={{
        active: classes.connectorActive,
        completed: classes.connectorCompleted,
        disabled: classes.connectorDisabled,
        line: classes.connectorLine,
      }}
    />
  );
  const handleChangePeraturan = (id_peraturan, no_regulasi) => {
    handlePeraturan(id_peraturan, no_regulasi);
    setShow(true);
  };

  // const handleChangePeraturan = (e) => {
  //   setVal(e.target.value);
  // };

  const handleSubmit = () => {
    if (data.no_peraturan < 5) {
      swal({
        title: "Harap masukkan minimal 5 karakter!",
        text: "Klik OK untuk melanjutkan",
        icon: "info",
        closeOnClickOutside: false,
      }).then((willApply) => {
        if (willApply) {
          // history.push("/logout");
          // setVal("");
        }
      });
    } else {
      setVal(data.no_peraturan);
      history.push(`/evaluation/validate/${id_usulan}/add/open`);
    }
  };

  const handleResetPeraturan = () => {
    setShow(false);
    emptyPeraturan();
  };

  function getStepContent(step) {
    switch (step) {
      case 0:
        return (
          <>
            {/* Field Unit Incharge */}
            {/* Diambil dari pegawai yang melakukan perekaman */}
            <div className="form-group row">
              <Field
                name="unit_incharge"
                component={Input}
                placeholder="Unit Incharge"
                label="Unit Incharge"
                disabled
                solid={"true"}
              />
            </div>
            {/* Field NIP PIC */}
            {/* Diambil dari pegawai yang melakukan perekaman */}
            <div className="form-group row">
              <Field
                name="nip_perekam"
                component={Input}
                placeholder="NIP PIC"
                label="NIP PIC"
                solid={"true"}
                disabled
              />
            </div>
            {/* Field Nama Pegawai */}
            {/* Diambil dari pegawai yang melakukan perekaman */}
            <div className="form-group row">
              <Field
                name="nm_pegawai"
                component={Input}
                placeholder="Nama Pegawai"
                label="Nama Pegawai"
                solid={"true"}
                disabled
              />
            </div>
            {/* Field Isu Masalah */}
            <div className="form-group row">
              <Field
                name="isu_masalah"
                component={Textarea}
                placeholder="Isu Masalah"
                label="Isu Masalah"
              />
            </div>
          </>
        );
      case 1:
        return (
          <>
            {show ? (
              <>
                {/* Field No Peraturan */}
                <div className="form-group row">
                  <Field
                    name="no_peraturan"
                    component={Input}
                    placeholder="Nomor Peraturan"
                    label="Nomor Peraturan"
                    disabled
                  />
                  <div className="col-lg-3 col-xl-3">
                    <button
                      type="button"
                      onClick={() => handleResetPeraturan()}
                      className="btn btn-light-warning"
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                      }}
                    >
                      <i className="fa fa-redo"></i>
                      Reset
                    </button>
                  </div>
                </div>
                {/* Field Judul Peraturan */}
                <div className="form-group row">
                  <Field
                    name="judul_peraturan"
                    component={Textarea}
                    placeholder="Judul Peraturan"
                    label="Judul Peraturan"
                  />
                </div>

                {/* Field Tentang */}
                <div className="form-group row">
                  <Field
                    name="tentang"
                    component={Textarea}
                    placeholder="Tentang"
                    label="Tentang"
                  />
                </div>
              </>
            ) : (
              <div className="form-group row">
                <Field
                  name="no_peraturan"
                  component={Input}
                  placeholder="No Peraturan"
                  label="No Peraturan"
                />
                <div className="col-lg-3 col-xl-3">
                  <button
                    type="button"
                    onClick={handleSubmit}
                    className="btn btn-primary"
                    style={{
                      boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                    }}
                  >
                    <i className="fa fa-search"></i>
                    Cari
                  </button>
                </div>
                {/* <label className="col-xl-3 col-lg-3 col-form-label">
                  No Peraturan
                </label>
                <div className="col-lg-9 col-xl-6">
                  <Select
                    options={peraturan}
                    onChange={value => handleChangePeraturan(value)}
                  />
                </div> */}
              </div>
            )}
          </>
        );

      case 2:
        return (
          <>
            {/* Field Jenis Peraturan  */}
            <div className="form-group row">
              <Sel
                name="jns_peraturan"
                label="Jenis Peraturan"
                onClick={() => handleChange()}
              >
                <option value="">Pilih Jenis Peraturan</option>
                {jenisPeraturan.map((data) => (
                  <option
                    key={data.id_jnsperaturan}
                    value={data.nm_jnsperaturan}
                  >
                    {data.nm_jnsperaturan}
                  </option>
                ))}
              </Sel>
            </div>
            {/* Field Konten Peraturan */}
            <div className="form-group row">
              <Field
                name="konten_peraturan"
                component={Textarea}
                placeholder="Konten Peraturan"
                label="Konten Peraturan"
              />
            </div>
            {/* Field Alasan */}
            <div className="form-group row">
              <Field
                name="alasan"
                component={Textarea}
                placeholder="Alasan"
                label="Alasan"
              />
            </div>
            {/* Field Analisis Dampak */}
            <div className="form-group row">
              <Field
                name="analisa_dampak"
                component={Textarea}
                placeholder="Analisis Dampak"
                label="Analisis Dampak"
              />
            </div>
            {/* Field Unit Kerja Pengusul */}
            <div className="form-group row">
              <Field
                name="unit_pengusul"
                component={Input}
                placeholder="Pengusul"
                label="Pengusul"
                solid={"true"}
                disabled
              />
            </div>
            {/* Field Upload File */}
            <div className="form-group row">
              <label className="col-xl-3 col-lg-3 col-form-label">
                Upload File Kajian
              </label>
              <div className="col-lg-9 col-xl-6">
                <Field
                  name="file"
                  component={CustomZipInput}
                  title="Select a file"
                  label="File"
                  style={{ display: "flex" }}
                />
                {fileDownload ? (
                  <button
                    type="button"
                    onClick={handleDownload}
                    className="btn btn-light-primary"
                    style={{
                      boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                    }}
                  >
                    <i className="fas fa-download"></i>
                    Download
                  </button>
                ) : null}
              </div>
            </div>
          </>
        );

      default:
        return "Unknown step";
    }
  }

  const saveForm = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
      // setActiveStep(prevActiveStep => prevActiveStep + 1);
    }
  };

  return (
    <>
      <>
        <div className={classes.root}>
          <Stepper
            alternativeLabel
            activeStep={activeStep}
            connector={connector}
          >
            {steps.map((label) => (
              <Step key={label}>
                <StepLabel>{label}</StepLabel>
              </Step>
            ))}
          </Stepper>
          <div>
            {/* {activeStep === steps.length ? (
          <div className="text-right">
            <Typography className={classes.instructions}>
              All steps completed - you&apos;re finished
            </Typography>
            <Button onClick={handleReset} className={classes.button}>
              Kembali
            </Button>
            <Button
                variant="contained"
                color="primary"
                onClick={saveForm}
                style={{ display: "none" }}
                className={classes.button}
              >
                Ajukan
              </Button>
          </div>
        ) : ( */}
            <div>
              {getStepContent(activeStep)}
              <div className="text-right">
                <Button
                  disabled={activeStep === 0}
                  onClick={handleBack}
                  className={classes.button}
                >
                  Kembali
                </Button>

                {activeStep === steps.length - 1 ? (
                  <Button
                    variant="contained"
                    color="secondary"
                    onClick={saveForm}
                    disabled={check}
                    className={classes.button}
                  >
                    {loading && (
                      <CircularProgress
                        size={24}
                        className={classes.buttonProgress}
                        color="secondary"
                      />
                    )}
                    {/* {loading ? (
                  // <span className="ml-3 spinner spinner-blue">Menyimpan</span>
                  <CircularProgress size={24} className={classes.progress} color="secondary" />
                ) : (
                  "Simpan"
                )} */}
                    {loading ? "Menyimpan. ." : "Simpan"}
                  </Button>
                ) : (
                  // loading ? (
                  //   <CircularProgress
                  //     className={classes.progress}
                  //     color="secondary"
                  //     size={24}
                  //   />
                  // ) : (
                  //   <Button
                  //     variant="contained"
                  //     color="secondary"
                  //     onClick={saveForm}
                  //     disabled={check}
                  //     className={classes.button}
                  //   >
                  //     Simpan
                  //   </Button>
                  // )
                  <Button
                    variant="contained"
                    color="secondary"
                    onClick={handleNext}
                    className={classes.button}
                  >
                    Selanjutnya
                  </Button>
                )}
              </div>
            </div>
            {/* )}  */}
          </div>
        </div>
      </>
      {/*Route to Open Details*/}
      <Route path="/evaluation/validate/:id/add/open">
        {({ history, match }) => (
          <SearchModal
            show={match != null}
            id={match && match.params.id}
            val={val}
            handleChangePeraturan={handleChangePeraturan}
            onHide={() => {
              history.push(`/evaluation/validate/${id_usulan}/add`);
            }}
            onRef={() => {
              history.push(`/evaluation/validate/${id_usulan}/add`);
            }}
          />
        )}
      </Route>
    </>
  );
}
