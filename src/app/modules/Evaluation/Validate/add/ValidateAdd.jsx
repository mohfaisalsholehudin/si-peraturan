import React, { useEffect, useState, useRef } from "react";
import { useSelector } from "react-redux";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";
import { useSubheader } from "../../../../../_metronic/layout";
import ValidateForm from "./ValidateForm";
import { getValidasiById, saveValidasi, uploadZip } from "../../Api";
import swal from "sweetalert";

function ValidateAdd({
  history,
  match: {
    params: { id }
  }
}) {
  const { user } = useSelector(state => state.auth);

  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading, setActionsLoading] = useState(true);
  const [loading, setLoading] = useState(false);
  const [usulan, setUsulan] = useState([]);
  const [validate, setValidate] = useState();
  const [idValidator, setIdValidator] = useState();
  const [isDisabled, setIsDisabled] = useState();

  const initValues = {
    unit_incharge: user.unitEs4,
    nip_perekam: user.nip9,
    judul_peraturan: "",
    no_peraturan: "",
    tentang: "",
    jns_peraturan: "",
    konten_peraturan: "",
    alasan: "",
    analisa_dampak: "",
    unit_pengusul: usulan.instansi_unit,
    isu_masalah: "",
    id_usulan: "",
    nm_pegawai: user.namaPegawai,
    id_validator: idValidator,
    file: "",
    tgl_evaluasi: "",
    kd_kantor: user.kantorLegacyKode,
    kd_unit_org: user.unitEs4LegacyKode,
  };

  const enableLoading = () => {
    setLoading(true);
  };

  const disableLoading = () => {
    setLoading(false);
  };

  useEffect(() => {
    let _title = "Detil Validasi Surat Usulan";

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps

    getValidasiById(id).then(({ data }) => {
      setUsulan(data.usulan);
      if (data.alasan !== null) {
        setValidate({
          unit_incharge: data.unit_incharge,
          nip_perekam: data.nip_perekam,
          judul_peraturan: data.judul_peraturan,
          tentang: data.tentang,
          jns_peraturan: data.jns_peraturan,
          konten_peraturan: data.konten_peraturan,
          alasan: data.alasan,
          analisa_dampak: data.analisa_dampak,
          unit_pengusul: data.unit_pengusul,
          isu_masalah: data.isu_masalah,
          id_usulan: data.id_usulan,
          nm_pegawai: data.nm_pegawai,
          id_validator: data.id_validator,
          no_peraturan: data.no_peraturan,
          file_upload: data.file_upload,
          kd_kantor: data.kd_kantor,
          tgl_evaluasi: data.tgl_surat,
          kd_unit_org: data.kd_unit_org
        });
      } else {
        setIdValidator(data.id_validator);
      }
    });
  }, [id, suhbeader, user.namaPegawai]);
  const btnRef = useRef();
  const saveValidation = values => {
    if (values.file.name) {
      // console.log('upload baru')
      enableLoading();
      const formData = new FormData();
      formData.append("file", values.file);
      uploadZip(formData).then(({ data }) => {
        disableLoading();
        saveValidasi(
          values.id_usulan,
          values.id_validator,
          values.alasan,
          values.analisa_dampak,
          data.message,
          values.isu_masalah,
          values.jns_peraturan,
          values.judul_peraturan,
          values.konten_peraturan,
          values.nip_perekam,
          values.tentang,
          values.unit_incharge,
          values.unit_pengusul,
          values.nm_pegawai,
          values.no_peraturan,
          values.kd_kantor,
          values.tgl_evaluasi,
          values.kd_unit_org
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push("/evaluation/validate");
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              // history.push("/evaluation/validate/:id/add");
            });
          }
        });
      }).catch((e)=> {
        disableLoading();
        alert(e);
      });
    } else {
      // console.log('menggunakan file lama')
      saveValidasi(
        values.id_usulan,
        values.id_validator,
        values.alasan,
        values.analisa_dampak,
        values.file_upload,
        values.isu_masalah,
        values.jns_peraturan,
        values.judul_peraturan,
        values.konten_peraturan,
        values.nip_perekam,
        values.tentang,
        values.unit_incharge,
        values.unit_pengusul,
        values.nm_pegawai,
        values.no_peraturan,
        values.kd_kantor,
        values.tgl_evaluasi,
        values.kd_unit_org
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push("/evaluation/validate");
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            // history.push("/evaluation/validate/:id/add");
          });
        }
      });
    }
  };
  const backToProposalList = () => {
    history.push(`/evaluation/validate`);
  };

  const setDisabled = val => {
    setIsDisabled(val);
  };

  // const handleDownload = () => {
  //   console.log(val)
  // }

  return (
    <Card>
      {/* {actionsLoading && <ModalProgressBar />} */}
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <ValidateForm
            actionsLoading={actionsLoading}
            initValues={validate || initValues}
            btnRef={btnRef}
            saveValidation={saveValidation}
            setDisabled={setDisabled}
            usulan={usulan}
            loading={loading}
            id_usulan={id}
          />
        </div>
      </CardBody>
    </Card>
  );
}

export default ValidateAdd;
