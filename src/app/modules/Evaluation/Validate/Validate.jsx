import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
import ValidateTable from "./ValidateTable";

function Validate() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Validasi Usulan Evaluasi Regulasi"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
            <ValidateTable />
        </CardBody>
      </Card>
    </>
  );
}

export default Validate;
