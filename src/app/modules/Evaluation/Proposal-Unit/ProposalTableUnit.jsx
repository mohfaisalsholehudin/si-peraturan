import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";
import React, { useEffect, useState } from "react";
import {
  sortCaret,
  headerSortingClasses
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../../../helpers/column-formatters";
import { useHistory } from "react-router-dom";
import { Route } from "react-router-dom";
// import ProposalEdit from "./proposal-edit/ProposalEdit";
import ProposalOpenUnit from "./ProposalOpenUnit";
import ProposalRejectUnit from "./ProposalRejectUnit";
import { Pagination } from "../../../helpers/pagination/Pagination";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import SVG from "react-inlinesvg";
import { getUsulan } from "../Api";

function ProposalTableUnit() {
  const history = useHistory();
  const [proposal, setProposal] = useState([]);

  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "no_surat",
    pageNumber: 1,
    pageSize: 5
  };
  const openProposal = id => history.push(`/evaluation/unit/${id}/open`);
  const rejectProposal = id => history.push(`/evaluation/unit/${id}/reject`);
  const editProposal = id => history.push(`/evaluation/unit/${id}/edit`);
  const addProposal = () => history.push("/evaluation/unit/new");

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "no_surat",
      text: "No Surat",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "tgl_surat",
      text: "Tgl Surat",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "perihal",
      text: "Perihal",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "instansi",
      text: "Instansi Penerbit",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.StatusColumnFormatter,
      headerSortingClasses
    },
    {
      dataField: "action",
      text: "Aksi",
      // formatter: columnFormatters.UnitActionsColumnFormatter,
      formatExtraData: {
        openEditDialog: editProposal,
        // openDeleteDialog: deleteAction,
        showProposal: openProposal,
        showReject: rejectProposal
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];

  const { SearchBar } = Search;

  useEffect(() => {
    getUsulan().then(({ data }) => {
      setProposal(data);
    });
  }, []);
  const defaultSorted = [{ dataField: "no_surat", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: proposal.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_usulan"
                  data={proposal}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar {...props.searchProps} />
                          <small className="form-text text-muted">
                            <b>Search</b> in all fields
                          </small>
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <button
                            type="button"
                            className="btn btn-primary"
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                              float: "right"
                            }}
                            onClick={addProposal}
                          >
                            <span className="svg-icon menu-icon">
                              <SVG
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Code/Plus.svg"
                                )}
                              />
                            </span>
                            Tambah
                          </button>
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>

      <Route path="/evaluation/unit/:id/reject">
        {({ history, match }) => (
          <ProposalRejectUnit
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/evaluation/unit");
            }}
            onRef={() => {
              history.push("/evaluation/unit");
            }}
          />
        )}
      </Route>
      <Route path="/evaluation/unit/:id/open">
        {({ history, match }) => (
          <ProposalOpenUnit
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/evaluation/unit");
            }}
            onRef={() => {
              history.push("/evaluation/unit");
            }}
          />
        )}
      </Route>

      {/*Route to Add Apps*/}
      {/* <Route path="/evaluation/proposal/add">
            {({ history, match }) => (
                <ProposalEdit
                    show={match != null}
                    onHide={() => {
                       history.push("/evaluation/proposal");
                    }}

                    onRef={() => {
                        history.push("/evaluation/proposal");
                    }}
                />
            )}
        </Route> */}

      {/*Route to Edit Apps*/}
      {/* <Route path="/ads/:id/edit">
            {({ history, match }) => (
                <AdsEdit
                    show={match != null}
                    id={match && match.params.id}
                    onHide={() => {
                        history.push("/ads");
                    }}
                    onRef={() => {
                        history.push("/ads");
                    }}
                />
            )}
        </Route> */}

      {/*Route to Delete Apps*/}
      {/* <Route path="/apps/:id/delete">
            {({ history, match }) => (
                <AppsDeleteDialog
                    show={match != null}
                    id={match && match.params.id}
                    onHide={() => {
                        history.push("/apps");
                    }}
                    onRef={() => {
                        history.push("/monitoring");
                        history.replace("/apps");
                    }}
                />
            )} 
        </Route> */}
    </>
  );
}

export default ProposalTableUnit;
