import React from "react";
import { Modal, Table } from "react-bootstrap";
function ProposalOpenUnit({ id, show, onHide }) {
  return (
    <Modal
      size="md"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Status Pengajuan
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Nomor Surat</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              : No 1/PJ.12/2020
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Tgl Surat</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">: 15/12/2020</span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Perihal</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">: Prakerja</span>
          </div>
        </div>
        <div className="row">
          <Table responsive hover>
            <thead style={{border: '1px solid #3699FF', textAlign: 'center', backgroundColor:'#3699FF'}}>
              <tr>
                <th>NO</th>
                <th style={{textAlign:'left'}}>STATUS</th>
                <th style={{textAlign:'left'}}>TANGGAL</th>
              </tr>
            </thead>
            <tbody style={{border:'1px solid lightgrey', textAlign:'center'}}>
              <tr style={{borderBottom: '1px solid lightgrey'}}>
                <td>1</td>
                <td style={{textAlign:'left'}}>Pengajuan</td>
                <td style={{textAlign:'left'}}>01/01/2021 - 14:00</td>
              </tr>
              <tr style={{borderBottom: '1px solid lightgrey'}}>
                <td>2</td>
                <td style={{textAlign:'left'}}>Proses Pengajuan</td>
                <td style={{textAlign:'left'}}>01/01/2021 - 18:00</td>
              </tr>
            </tbody>
            <tfoot style={{border: '1px solid #3699FF', textAlign: 'center', backgroundColor:'#3699FF'}}>
              <tr style={{height: '40px'}}>
                <td></td>
                <td></td>
                <td></td>
              </tr>
            </tfoot>
          </Table>
        </div>
      </Modal.Body>
      <Modal.Footer style={{ borderTop: "none" }}>
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          <button
            type="button"
            // onClick={backAction}
            onClick={onHide}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="flaticon2-cancel icon-nm"></i>
            Tutup
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}

export default ProposalOpenUnit;
