import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
import ProposalTableUnit from "./ProposalTableUnit";

function ProposalUnit() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Surat Usulan Evaluasi Regulasi"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
            <ProposalTableUnit />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default ProposalUnit;
