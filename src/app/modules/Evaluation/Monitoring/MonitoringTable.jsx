// import BootstrapTable from "react-bootstrap-table-next";
import BootstrapTable from "react-bootstrap-table-next";
import { useSelector } from "react-redux";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";
import { Pagination } from "../../../helpers/pagination/Pagination";
import React, { useEffect, useState } from "react";
import {
  sortCaret,
  headerSortingClasses
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../../../helpers/column-formatters";
import "../../../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import { getValidasiUsulan, getValidasiUsulanByAdmin, getValidasiUsulanKantor } from "../Api";

function MonitoringTable() {
  const [monitoring, setMonitoring] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [sizePage, setSizePage] = useState(50);
  const { user, role } = useSelector(state => state.auth);
  const super_admin = role.includes("ROLE_SUPER_ADMIN_PERATURAN");


  useEffect(() => {
    if(super_admin){
        getValidasiUsulanByAdmin().then(({ data })=> {
          data.map(data => {
            if (data.status === "Terima") {
              setMonitoring(monitoring => [...monitoring, data]);
            }
          });
        })
    } else {
    getValidasiUsulanKantor(user.kantorLegacyKode).then(({ data }) => {
      data.map(data => {
        if (data.status === "Terima") {
          setMonitoring(monitoring => [...monitoring, data]);
        }
      });
    });
  }
  }, [setMonitoring, super_admin, user.kantorLegacyKode]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      // formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: (cell, row, rowIndex) => {
        let rowNumber = (currentPage - 1) * sizePage + (rowIndex + 1);
        return <span>{rowNumber}</span>;
      }
    },
    {
      dataField: "no_evaluasi",
      text: "No Evaluasi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      sortFunc: (a, b, order, dataField) => {
        if (order === 'asc' || !order) {
            return b.localeCompare(a, navigator.languages[0] || navigator.language, {numeric: true, ignorePunctuation: true});
            }
            return a.localeCompare(b, navigator.languages[0] || navigator.language, {numeric: true, ignorePunctuation: true});
            },
            style: {
              minWidth: "200px",
            },
    },
    {
      dataField: "usulan.no_surat",
      text: "No Surat",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      style: {
        minWidth: "200px",
      },
    },
    {
      dataField: "usulan.jns_pajak",
      text: "Jenis Pajak",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      style: {
        minWidth: "200px",
      },
    },
    {
      dataField: "isu_masalah",
      text: "Isu Masalah",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      style: {
        minWidth: "600px",
      },
    },
    {
      dataField: "no_peraturan",
      text: "Nomor Peraturan / Penegasan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      style: {
        minWidth: "200px",
      },
    },
    {
      dataField: "tentang",
      text: "Tentang",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      style: {
        minWidth: "600px",
      },
    },
    {
      dataField: "jns_peraturan",
      text: "Jenis Peraturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      style: {
        minWidth: "200px",
      },
    },
    {
      dataField: "konten_peraturan",
      text: "Konten Pengaturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      style: {
        minWidth: "600px",
      },
    },
    {
      dataField: "alasan",
      text: "Alasan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      style: {
        minWidth: "600px",
      },
    },
    {
      dataField: "analisa_dampak",
      text: "Analisis Dampak Kebijakan (RIA dan CBA)",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      style: {
        minWidth: "600px",
      },
    },
    {
      dataField: "usulan.instansi_unit",
      text: "Unit Kerja DJP/Asosiasi/KL Pengusul",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      style: {
        minWidth: "200px",
      },
    },
    {
      dataField: "nm_pegawai",
      text: "Konseptor",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      style: {
        minWidth: "200px",
      },
    },
    {
      dataField: "unit_incharge",
      text: "Seksi Konseptor",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      style: {
        minWidth: "200px",
      },
    },
    {
      dataField: "usulan.wkt_update",
      text: "Waktu Update",
      sort: true,
      hidden: true,
      formatter: columnFormatters.DateFormatterWaktuUpdate,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    // {
    //   dataField: "action",
    //   text: "Aksi",
    //   formatter: columnFormatters.ValidateActionsColumnFormatter,
    //   formatExtraData: {
    //     openAddDetil: addDetil,
    //     // openDeleteDialog: deleteAction,
    //     showProposal: openProposal,
    //     showReject: rejectProposal,
    //     applyValidation: applyValidation
    //   },
    //   classes: "text-center pr-0",
    //   headerClasses: "text-center pr-3",
    //   style: {
    //     minWidth: "100px",
    //   },
    // },
  ];
  const { SearchBar } = Search;
  const initialFilter = {
    sortOrder: "desc", // asc||desc
    sortField: "usulan.wkt_update",
    pageNumber: currentPage,
    pageSize: sizePage
  };
  const defaultSorted = [{ dataField: "usulan.wkt_update", order: "desc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: monitoring.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
    onPageChange: (page, sizePerPage) => {
      setCurrentPage(page);
    },
    onSizePerPageChange: (page, sizePerPage) => {
      setSizePage(page);
      setCurrentPage(sizePerPage);
    }
  };
  const emptyDataMessage = () => { return 'No Data to Display';}

  return (
    <>
      <PaginationProvider pagination={paginationFactory(pagiOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <>
              <ToolkitProvider
                keyField="id_validator"
                data={monitoring}
                columns={columns}
                search
              >
                {props => (
                  <div>
                    <div className="row">
                      <div className="col-lg-6 col-xl-6 mb-3">
                        <SearchBar
                          {...props.searchProps}
                          style={{ width: "500px" }}
                        />
                        <br />
                      </div>
                    </div>
                    <BootstrapTable
                      {...props.baseProps}
                      wrapperClasses="table-responsive"
                      bordered={false}
                      headerWrapperClasses="thead-light"
                      classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                      defaultSorted={defaultSorted}
                      bootstrap4
                      noDataIndication={emptyDataMessage}
                      {...paginationTableProps}
                    ></BootstrapTable>
                    <Pagination paginationProps={paginationProps} />
                  </div>
                )}
              </ToolkitProvider>
            </>
          );
        }}
      </PaginationProvider>
    </>
  );
}

export default MonitoringTable;
