import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
import MonitoringTable from "./MonitoringTable";

function Monitoring() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Monitoring Surat Usulan Evaluasi Regulasi"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
             
          {/* <div className="row"> */}
            <MonitoringTable />
          {/* </div> */}
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default Monitoring;
