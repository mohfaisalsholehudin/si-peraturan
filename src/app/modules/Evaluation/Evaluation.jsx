import React from "react";
import { useSelector } from "react-redux";
import { Switch, Redirect } from "react-router-dom";
import { ContentRoute } from "../../../_metronic/layout";
import Proposal from "./Proposal/Proposal";
import ProposalEdit from "./Proposal/proposal-edit/ProposalEdit";
import ProposalUnit from "./Proposal-Unit/ProposalUnit";
import ProposalUnitEdit from "./Proposal-Unit/proposal-unit-edit/ProposalUnitEdit";
import Research from "./Research/Research";
import Validate from "./Validate/Validate";
import ValidateAdd from "./Validate/add/ValidateAdd";
import ValidateResearch from "./Validate-Research/ValidateResearch";
import Monitoring from "./Monitoring/Monitoring";
import EvaluationErrors from "./EvaluationErrors";

export default function Evaluation() {
  const { role } = useSelector(state => state.auth);
  const konseptor = role.includes("ROLE_PERATURAN_KONSEPTOR");
  const es4 = role.includes("ROLE_PERATURAN_PENELITI_LVL1");
  const es3 = role.includes("ROLE_PERATURAN_PENELITI_LVL2");
  const admin = role.includes("ROLE_ADMIN_PERATURAN");

  return (
    <Switch>
      <Redirect exact={true} from="/evaluation" to="/evaluation/proposal" />

      {/* Begin of Usulan */}
      <ContentRoute
        path="/evaluation/proposal/new"
        component={
          admin || konseptor  ? ProposalEdit : EvaluationErrors
        }
      />
      <ContentRoute
        path="/evaluation/proposal/:id/edit"
        component={
          admin || konseptor  ? ProposalEdit : EvaluationErrors
        }
      />
      <ContentRoute
        path="/evaluation/proposal"
        component={
          admin || konseptor  ? Proposal : EvaluationErrors
        }
      />
      {/* End of Usulan */}

      {/* Begin of Penelitian */}
      {/* <ContentRoute path="/evaluation/research" component={admin ? Research : EvaluationErrors} /> */}
      <ContentRoute
        path="/evaluation/research"
        component={admin || es4 ? Research : EvaluationErrors}
      />
      {/* End of Penelitian */}

      {/* Begin of Validasi */}
      <ContentRoute
        path="/evaluation/validate/:id/add"
        component={
          admin || konseptor  ? ValidateAdd : EvaluationErrors
        }
      />
      <ContentRoute
        path="/evaluation/validate"
        component={
          admin || konseptor  ? Validate : EvaluationErrors
        }
      />
      {/* End of Validasi */}

      {/* Begin of Penelitian Validasi */}
      <ContentRoute
        path="/evaluation/revalidate"
        component={admin || es4 || es3 ? ValidateResearch : EvaluationErrors}
      />
      {/* End of Penelitian Validasi */}

      {/* Begin of Monitoring */}
      <ContentRoute
        path="/evaluation/monitoring"
        component={
          admin || konseptor || es4 || es3 ? Monitoring : EvaluationErrors
        }
      />
      {/* End of Monitoring */}

      {/* Layout */}
      {/* <ContentRoute from="/google-material/layout" component={LayoutPage} /> */}
    </Switch>
  );
}
