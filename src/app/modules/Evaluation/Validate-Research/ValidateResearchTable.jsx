/* Library */
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { Route } from "react-router-dom";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";

/* Helpers */
import { Pagination } from "../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../../../helpers/column-formatters";

/* Component */
import ValidateResearchOpen from "./ValidateResearchOpen";


/* Utility */

import { getValidasiUsulan, getValidasiUsulanByAdmin, getValidasiUsulanSubdit } from "../Api";

function ValidateResearchTable() {
  const history = useHistory();
  const [research, setResearch] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [sizePage, setSizePage] = useState(50);
  const { user, role } = useSelector(state => state.auth);
  const es3 = role.includes("ROLE_PERATURAN_PENELITI_LVL2");
  const super_admin = role.includes("ROLE_SUPER_ADMIN_PERATURAN");


  const openProposal = (id) => history.push(`/evaluation/revalidate/${id}/open`);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      // formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: (cell, row, rowIndex) => {
        let rowNumber = (currentPage - 1) * sizePage + (rowIndex + 1);
        return <span>{rowNumber}</span>;
      }
    },
    {
      dataField: "usulan.no_surat",
      text: "Nomor Naskah Dinas",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "usulan.tgl_surat",
      text: "Tgl Surat",
      sort: true,
      formatter: columnFormatters.SuratDateFormatterNested,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "usulan.perihal",
      text: "Perihal",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "usulan.instansi_unit",
      text: "Instansi Penerbit",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "usulan.file_upload",
      text: "File Surat",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.FileColumnFormatterEvaRevalidateSurat,
      headerSortingClasses,
    },
    {
      dataField: "file_upload",
      text: "File Validasi",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.FileColumnFormatterEvaRevalidateValidasi,
      headerSortingClasses,
    },
    {
      dataField: "usulan.wkt_update",
      text: "Waktu Update",
      sort: true,
      hidden: true,
      formatter: columnFormatters.DateFormatterWaktuUpdate,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterEvaRevalidate,
      formatExtraData: {
        // openEditDialog: editProposal,
        // openDeleteDialog: deleteAction,
        showProposal: openProposal,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "usulan.wkt_update",
    pageNumber: currentPage,
    pageSize: sizePage
  };
  const { SearchBar } = Search;
  const defaultSorted = [{ dataField: "usulan.wkt_update", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: research.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
    onPageChange: (page, sizePerPage) => {
      setCurrentPage(page);
    },
    onSizePerPageChange: (page, sizePerPage) => {
      setSizePage(page);
      setCurrentPage(sizePerPage);
    }
  };
  
  useEffect(() => {
    if(es3){
        getValidasiUsulanSubdit(user.unitEs4LegacyKode).then(({ data })=> {
          data.map((data)=> {
            if(data.status === 'Eselon 3'){
              setResearch(research => [...research, data])

            }
          })
        })
    } else if (super_admin){
        getValidasiUsulanByAdmin().then(({ data })=> {
          data.map((data)=> {
            if(data.status === 'Eselon 3' || data.status === 'Eselon 4'){
              setResearch(research => [...research, data])

            }
          })
        })
    }else{
      getValidasiUsulan(user.unitEs4LegacyKode).then(({data})=>{
        data.map((data)=>{
          if(user.jabatan === 'Kepala Seksi' ? data.status === 'Eselon 4' : data.status === 'Eselon 3' ){
            setResearch(research=>[...research, data])
          }
        })
      })
    }
    
  }, [es3, super_admin, user.jabatan, user.unitEs4LegacyKode])
  const emptyDataMessage = () => { return 'No Data to Display';}

  return (
    <>
      <>
      <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="usulan.no_surat"
                  data={research}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>

      <Route path="/evaluation/revalidate/:id/open">
        {({ history, match }) => (
          <ValidateResearchOpen
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/evaluation/revalidate");
            }}
            onRef={() => {
              history.push("/evaluation/revalidate");
            }}
          />
        )}
      </Route>
    </>
  );
}

export default ValidateResearchTable;
