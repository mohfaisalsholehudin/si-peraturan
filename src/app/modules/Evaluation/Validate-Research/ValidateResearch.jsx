import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
import ValidateResearchTable from "./ValidateResearchTable";

function ValidateResearch() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Penelitian Validasi Surat Usulan Evaluasi Regulasi"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
            <ValidateResearchTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default ValidateResearch;
