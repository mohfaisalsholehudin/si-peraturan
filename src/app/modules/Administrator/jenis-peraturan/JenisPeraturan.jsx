import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
import JenisPeraturanTable from "./JenisPeraturanTable";

function JenisPeraturan() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Jenis Peraturan"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
            <JenisPeraturanTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default JenisPeraturan;
