import React from "react";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import "../../../../helpers/DatePickerStyles.css";
import { Input, Select as Sel } from "../../../../helpers";

function JenisPeraturanEditForm({ proposal, btnRef, saveForm }) {

  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    nm_jnsperaturan: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Jenis Peraturan is required"),
    status: Yup.string().required("Status is required"),
    aplikasi: Yup.string().required("Aplikasi is required"),
  });


  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={proposal}
        validationSchema={ProposalEditSchema}
        onSubmit={values => {
          // console.log(values);
          saveForm(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
          handleBlur,
          handleChange,
          errors,
          touched,
          values,
          isValid
        }) => {
          return (
            <>
              <Form className="form form-label-right">
                {/* Field Jenis Pajak */}
                <div className="form-group row">
                  <Field
                    name="nm_jnsperaturan"
                    component={Input}
                    placeholder="Jenis Peraturan"
                    label="Jenis Peraturan"
                  />
                </div>
                {/* FIELD APLIKASI */}
                <div className="form-group row">
                  <Sel name="aplikasi" label="Aplikasi">
                    <option>Pilih Aplikasi</option>
                    <option value="Tata Kelola Peraturan">Tata Kelola Peraturan</option>
                    <option value="KM Peraturan">KM Peraturan</option>
                  </Sel>
                </div>
                {/* FIELD STATUS */}
                <div className="form-group row">
                  <Sel name="status" label="Status">
                    <option>Pilih Status</option>
                    <option value="AKTIF">AKTIF</option>
                    <option value="PASIF">PASIF</option>
                  </Sel>
                </div>

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default JenisPeraturanEditForm;
