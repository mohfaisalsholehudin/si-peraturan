import React from "react";
import { useHistory } from "react-router-dom";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../../_metronic/_partials/controls";
import KnowledgeOwnerTable from "./KnowledgeOwnerTable";

function KnowledgeOwner() {
  const history = useHistory();

  const backAction = () => {
    history.push("/admin/role");
  };
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Role Knowledge Owner"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
            <KnowledgeOwnerTable />
        </CardBody>
        <CardFooter style={{ borderTop: "none", paddingRight: "15px"}}>
        <div className="col-lg-12" style={{ textAlign: "right" }}>
        <button
          type="button"
          onClick={backAction}
          className="btn btn-success"
          style={{
            boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
          }}
        >
          <i className="fa fa-arrow-left"></i>
          Kembali
        </button>
      </div>
        </CardFooter>
      </Card>
    </>
  );
}

//#a6c8e6
export default KnowledgeOwner;
