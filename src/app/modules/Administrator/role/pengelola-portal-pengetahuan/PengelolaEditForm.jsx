import React, { useState, useEffect } from "react";
import { Field, Formik, Form } from "formik";
import Select from "react-select";
import * as Yup from "yup";
import "../../../../helpers/DatePickerStyles.css";
import { Select as Sel } from "../../../../helpers";
import axios from "axios";
import { useSelector } from "react-redux";

const { BACKEND_URL } = window.ENV;

function PengelolaEditForm({ content, btnRef, saveForm }) {
  const [es2, setEs2] = useState([]);
  const [valEs2, setValEs2] = useState();
  const [es3, setEs3] = useState([]);
  const [valEs3, setValEs3] = useState();
  const [es4, setEs4] = useState([]);
  const [valEs4, setValEs4] = useState();
  const { token } = useSelector(state => state.auth);

  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    nm_unit_es2: Yup.string().required("Eselon 2 is required"),
    nm_unit_es3: Yup.string().required("Eselon 3 is required"),
    nm_unit_es4: Yup.string().required("Eselon 4 is required"),
    kd_unit_es2: Yup.string().required("Eselon 2 is required"),
    kd_unit_es3: Yup.string().required("Eselon 3 is required"),
    kd_unit_es4: Yup.string().required("Eselon 4 is required")
  });

  useEffect(() => {
    getKantor(2).then(({ data }) => {
      data.map(data => {
        return setEs2(es2 => [
          ...es2,
          {
            label: data.nama,
            value: data.legacyKode
          }
        ]);
      });
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getKantor = code => {
    return axios.post(
      `${BACKEND_URL}/api/iam/getkantorfilter?parentlegacy=${code}`,
      {},
      {
        headers: { Authorization: token, "Content-Type": "application/json" }
      }
    );
  };
  useEffect(() => {
    if (content.kd_unit_es2) {
      es2
        .filter(data => data.value === content.kd_unit_es2)
        .map(data => {
          setValEs2(data.label);
        });

      // getKantor(content.kd_unit_es2).then(({ data }) => {
      //   data.map(data => {
      //     return setEs3(es3 => [
      //       ...es3,
      //       {
      //         label: data.nama,
      //         value: data.legacyKode
      //       }
      //     ]);
      //   });
      // })
      // getKantor(content.kd_unit_es3).then(({ data }) => {
      //   data.map(data => {
      //     return setEs4(es4 => [
      //       ...es4,
      //       {
      //         label: data.nama,
      //         value: data.legacyKode
      //       }
      //     ]);
      //   });
      // })
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [content.kd_unit_es2, es2]);

  useEffect(() => {
    if (content.kd_unit_es3) {
      getKantor(content.kd_unit_es2).then(({ data }) => {
        data.map(data => {
          return setEs3(es3 => [
            ...es3,
            {
              label: data.nama,
              value: data.legacyKode
            }
          ]);
        });
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [content.kd_unit_es3]);

  useEffect(() => {
    if (content.kd_unit_es4) {
      getKantor(content.kd_unit_es3).then(({ data }) => {
        data.map(data => {
          return setEs4(es4 => [
            ...es4,
            {
              label: data.nama,
              value: data.legacyKode
            }
          ]);
        });
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [content.kd_unit_es4]);

  useEffect(() => {
    if (content.kd_unit_es3) {
      es3
        .filter(data => data.value === content.kd_unit_es3)
        .map(data => {
          setValEs3(data.label);
        });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [es3]);

  useEffect(() => {
    if (content.kd_unit_es4) {
      es4
        .filter(data => data.value === content.kd_unit_es4)
        .map(data => {
          setValEs4(data.label);
        });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [es4]);

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={content}
        validationSchema={ProposalEditSchema}
        onSubmit={values => {
          saveForm(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
          handleBlur,
          handleChange,
          errors,
          touched,
          values,
          isValid
        }) => {
          const handleChangeEs2 = val => {
            setFieldValue("nm_unit_es2", val.label);
            setFieldValue("kd_unit_es2", val.value);
            setValEs2(val.label);
            setEs3([]);
            setEs4([]);
            getKantor(val.value).then(({ data }) => {
              data.map(data => {
                return setEs3(es3 => [
                  ...es3,
                  {
                    label: data.nama,
                    value: data.legacyKode
                  }
                ]);
              });
            });
          };

          const handleChangeEs3 = val => {
            setFieldValue("nm_unit_es3", val.label);
            setFieldValue("kd_unit_es3", val.value);
            setValEs3(val.label);
            setEs4([]);
            getKantor(val.value).then(({ data }) => {
              data.map(data => {
                return setEs4(es4 => [
                  ...es4,
                  {
                    label: data.nama,
                    value: data.legacyKode
                  }
                ]);
              });
            });
          };

          const handleChangeEs4 = val => {
            setFieldValue("nm_unit_es4", val.label);
            setFieldValue("kd_unit_es4", val.value);
            setValEs4(val.label);
          };
          return (
            <>
              <Form className="form form-label-right">
                {/* Field Eselon 2 */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Unit Eselon 2
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Select
                      options={es2}
                      onChange={value => handleChangeEs2(value)}
                      value={es2.filter(data => data.label === valEs2)}
                    />
                  </div>
                </div>

                {/* Field Eselon 3 */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Unit Eselon 3
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Select
                      options={es3}
                      onChange={value => handleChangeEs3(value)}
                      value={es3.filter(data => data.label === valEs3)}
                    />
                  </div>
                </div>
                {/* Field Eselon 4 */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Unit Eselon 4
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Select
                      options={es4}
                      onChange={value => handleChangeEs4(value)}
                      value={es4.filter(data => data.label === valEs4)}
                    />
                  </div>
                </div>
                {/* Field Jabatan */}
                {/* <div className="form-group row">
                <label className="col-xl-3 col-lg-3 col-form-label">
                    Jabatan
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Select
                      options={master}
                      onChange={value => handleChangeMaster(value)}
                      value={master.filter(data => data.label === valMaster)}
                    />
                  </div>
                </div> */}

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default PengelolaEditForm;
