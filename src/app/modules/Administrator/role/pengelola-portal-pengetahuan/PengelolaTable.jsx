/* Library */
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import swal from "sweetalert";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";
import SVG from "react-inlinesvg";

/* Helpers */
import { Pagination } from "../../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses
} from "../../../../../_metronic/_helpers";
import { toAbsoluteUrl } from "../../../../../_metronic/_helpers";
import * as columnFormatters from "../../../../helpers/column-formatters";
import { getPengPortal } from "../../../../references/Api";

/* Component */
// import ProposalOpen from "./ProposalOpen";
// import ProposalReject from "./ProposalReject";

/* Utility */

function PengelolaTable() {
  const history = useHistory();
  const [content, setContent] = useState([]);
// const content = [
//     {id: 1, eselon2: "Direktorat A", eselon3: "Kasubdit A", eselon4: "Seksi A"},
//     {id: 2, eselon2: "Direktorat B", eselon3: "Kasubdit B", eselon4: "Seksi B"},
//     {id: 3, eselon2: "Direktorat C", eselon3: "Kasubdit C", eselon4: "Seksi C"},
// ]

  const add = () => history.push("/admin/role/pengelola-portal-pengetahuan/add");
  //   const edit = (id) => history.push(`/admin/detil-jenis/${id}/edit`);
  const edit = id => history.push(`/admin/role/pengelola-portal-pengetahuan/${id}/edit`);
  const deleteAction = id => {
    //     swal({
    //       title: "Apakah Anda Yakin?",
    //       text: "Klik OK untuk melanjutkan",
    //       icon: "warning",
    //       buttons: true,
    //       dangerMode: true
    //     }).then(willDelete => {
    //       if (willDelete) {
    //         deleteDetilJenis(id).then(({ status }) => {
    //           if (status === 200) {
    //             swal("Berhasil", "Data berhasil dihapus", "success").then(() => {
    //               history.push("/dashboard");
    //               history.replace("/admin/detil-jenis");
    //             });
    //           } else {
    //             swal("Gagal", "Data gagal disimpan", "error").then(() => {
    //               history.push("/dashboard");
    //               history.replace("/admin/detil-jenis");
    //             });
    //           }
    //         });
    //       }
    //       // else {
    //       //   swal("Your imaginary file is safe!");
    //       // }
    //     });
  };

    useEffect(()=> {
      getPengPortal().then(({ data }) => {
        setContent(data)
      })
    },[])

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "nm_unit_es2",
      text: "Eselon 2",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "nm_unit_es3",
      text: "Eselon 3",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "nm_unit_es4",
      text: "Eselon 4",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterAdminRolePengelolaPortalPengetahuan,
      formatExtraData: {
        openEditDialog: edit,
        openDeleteDialog: deleteAction
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "nm_unit_es2",
    pageNumber: 1,
    pageSize: 50
  };
  const defaultSorted = [{ dataField: "nm_unit_es2", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_pp_kms"
                  data={content}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <button
                            type="button"
                            className="btn btn-primary"
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                              float: "right"
                            }}
                            onClick={add}
                          >
                            <span className="svg-icon menu-icon">
                              <SVG
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Code/Plus.svg"
                                )}
                              />
                            </span>
                            Tambah
                          </button>
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
    </>
  );
}

export default PengelolaTable;
