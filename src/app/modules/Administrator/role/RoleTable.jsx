import React from "react";
import { useHistory } from "react-router-dom";

import BootstrapTable from "react-bootstrap-table-next";
import * as columnFormatters from "../../../helpers/column-formatters";
import {saveTusiKm} from "../../../references/Api"
import swal from "sweetalert";

function RoleTable({ id, status }) {
  const history = useHistory();

  const detils = [
    {
      id_detil: "penjamin-kualitas-pengetahuan",
      nama_role: "Penjamin Kualitas Pengetahuan"
    },
    {
      id_detil: "knowledge-owner",
      nama_role: "Knowledge Owner"
    },
    {
      id_detil: "subject-matter-expert",
      nama_role: "Subject Matter Expert"
    },
    // {
    //   id_detil: "pengelola-portal-pengetahuan",
    //   nama_role: "Pengelola Portal Pengetahuan"
    // },
  ];
  const process = id_detil => {
    switch (id_detil) {
      case "penjamin-kualitas-pengetahuan":
        history.push(`/admin/role/penjamin-kualitas-pengetahuan`);
        break;
      case "knowledge-owner":
        history.push(`/admin/role/knowledge-owner`);
        break;
      case "subject-matter-expert":
        history.push(`/admin/role/subject-matter-expert`);
        break;
      // case "pengelola-portal-pengetahuan":
      //   history.push(`/admin/role/pengelola-portal-pengetahuan`);
      //   break;
      default:
        break;
    }
  };

  const columns = [
    {
      dataField: "nama_role",
      text: "Nama Role",
      sort: true
    },

    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterAdminRoleList,
      formatExtraData: {
        openDialog: process
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];

  const saveButton = () => {
    saveTusiKm(2,1).then(
      ({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push("/admin/setting/probis");
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/admin/setting/probis/add");
          });
        }
      })
  }

  return (
    <>
      <div className="row">
      </div>
      <BootstrapTable
        wrapperClasses="table-responsive"
        bordered={false}
        headerWrapperClasses="thead-light"
        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
        keyField="id_detil"
        data={detils}
        columns={columns}
        bootstrap4
      ></BootstrapTable>
    </>
  );
}

export default RoleTable;
