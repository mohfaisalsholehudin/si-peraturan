/* Library */
import React, { useEffect, useState, useRef } from "react";
import swal from "sweetalert";

/* Helper */
import { useSubheader } from "../../../../../_metronic/layout";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";
import { getSmeById, saveSme, updateSme } from "../../../../references/Api";
import SubjectEditForm from "./SubjectEditForm"
import SubjectProbisTable from "./SubjectProbisTable";
function SubjectEdit({
  history,
  match: {
    params: { id }
  }
}) {
  const initValues = {
    nama: "",
    nip: "",
    unit: "",
    jabatan: ""
  };

  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [content, setContent] = useState();

  useEffect(() => {
    let _title = id
      ? "Edit Anggota Subject Matter Expert"
      : "Tambah Anggota Subject Matter Expert";

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps

    if (id) {
        getSmeById(id).then(({ data })=> {
          setContent({
            nip: data.nip,
            nama: data.nama,
            unit: data.nm_unit, 
            jabatan: data.nm_jabatan
          })
        })
    }
  }, [id, suhbeader]);
  const btnRef = useRef();
  const saveButton = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
      // setIsComplete(true);
      // disabled ? setIsComplete(false) : setIsComplete(true);
    }
  };

  const backAction = () => {
    history.push("/admin/role/subject-matter-expert");
  };
  const saveForm = values => {
    if (!id) {
        saveSme(values.nip, values.nama, values.unit, values.jabatan).then(
          ({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
                history.push("/admin/role/subject-matter-expert");
              });
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                history.push("/admin/role/subject-matter-expert/add");
              });
            }
          }
        );
    } else {
        updateSme(id, values.nip, values.nama, values.unit, values.jabatan).then(
          ({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
                history.push("/admin/role/subject-matter-expert");
              });
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                history.push("/admin/role/subject-matter-expert/add");
              });
            }
          }
        );
    }
  };

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="mt-5">
            <SubjectEditForm
              content={content || initValues}
              btnRef={btnRef}
              saveForm={saveForm}
            />
          </div>
          {/* {id_lhr ? <DetilLhrTablePokok id={id} id_lhr={id_lhr} /> : null} */}
          {id ? <SubjectProbisTable id={id} /> : null}
        </>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        {/* <DetilJenisEditFooter backAction={backAction} btnRef={btnRef} /> */}
        <div className="col-lg-12" style={{ textAlign: "right" }}>
          <button
            type="button"
            onClick={backAction}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
          {`  `}
          <button
            type="submit"
            className="btn btn-success ml-2"
            onClick={saveButton}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
            // disabled={disabled}
          >
            <i className="fas fa-save"></i>
            Simpan
          </button>
        </div>
      </CardFooter>
    </Card>
  );
}

export default SubjectEdit;
