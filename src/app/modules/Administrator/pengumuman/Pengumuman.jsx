import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
import PengumumanTable from "./PengumumanTable";

function Pengumuman() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Pengiriman Pengumuman / Pesan"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          {/* <RoleTable /> */}
          <PengumumanTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default Pengumuman;
