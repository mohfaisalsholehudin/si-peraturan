import React, { useState, useEffect } from "react";
import { Field, Formik, Form } from "formik";
import Select from "react-select";
import * as Yup from "yup";
import "../../../../helpers/DatePickerStyles.css";
import { Input, Textarea } from "../../../../helpers";
import axios from "axios";
import { useSelector } from "react-redux";
import swal from "sweetalert";

const { BACKEND_URL } = window.ENV;

function PengumumanTargetForm({ content, btnRef, saveForm }) {
  const { iamToken } = useSelector(state => state.auth);

  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    nama: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Nama is required"),
    nip: Yup.string().required("NIP is required"),
    nip_pendek: Yup.number().required("NIP Pendek is required"),
    unit_organisasi: Yup.string().required("Unit is required")
  });

  useEffect(() => {
    // getMasterJenis().then(({ data }) => {
    //   data.map(data => {
    //     return setMaster(master => [
    //       ...master,
    //       {
    //         label: data.nama,
    //         value: data.id
    //       }
    //     ]);
    //   });
    // });
  }, []);

  const getNip = nip => {
    return axios.post(`${BACKEND_URL}/api/iam/getbynip`, nip, {
      headers: { Authorization: iamToken, "Content-Type": "application/json" }
    });
  };
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={content}
        validationSchema={ProposalEditSchema}
        onSubmit={values => {
          saveForm(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
          handleBlur,
          handleChange,
          errors,
          touched,
          values,
          isValid
        }) => {
          const searchNip = () => {
            setFieldValue("nama", "Loading...");
            setFieldValue("unit_organisasi", "Loading...");
            setFieldValue("nip", "Loading...");
            getNip(values.nip_pendek).then(({ data }) => {
              if (data.length > 0) {
                setFieldValue("nama", data[0].nama);
                setFieldValue(
                  "nip",
                  data[0].nip18
                  );
                  setFieldValue("unit_organisasi", data[0].jabatanPegawais[0].kantor.nama);
              } else {
                swal("Gagal", "Data Tidak Tersedia", "error");
                setFieldValue("nama", "");
                setFieldValue("unit_organisasi", "");
                setFieldValue("nip", "");
              }
            });
          };
          return (
            <>
              <Form className="form form-label-right">
                {/* Field NIP */}
                <div className="form-group row">
                  <Field
                    name="nip_pendek"
                    component={Input}
                    placeholder="NIP Pendek"
                    label="NIP Pendek"
                    maxLength={9}
                    withFeedbackLabel={false}
                  />
                  <button
                    type="button"
                    onClick={searchNip}
                    className="btn btn-primary"
                  >
                    <i className="fas fa-search"></i>
                    Cari
                  </button>
                </div>
                {/* Field Nama */}
                <div className="form-group row">
                  <Field
                    name="nama"
                    component={Input}
                    placeholder="Nama"
                    label="Nama"
                    disabled
                  />
                </div>
                {/* Field Unit */}
                <div className="form-group row">
                  <Field
                    name="unit_organisasi"
                    component={Input}
                    placeholder="Unit Organisasi"
                    label="Unit Organisasi"
                    disabled
                  />
                </div>
                {/* Field Jabatan */}
                <div className="form-group row">
                  <Field
                    name="nip"
                    component={Input}
                    placeholder="NIP"
                    label="NIP"
                    disabled
                  />
                </div>

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default PengumumanTargetForm;
