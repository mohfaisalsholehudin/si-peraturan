import React, { useEffect, useState } from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../../_metronic/_partials/controls";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import BootstrapTable from "react-bootstrap-table-next";
import * as columnFormatters from "../../../../helpers/column-formatters";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../../_metronic/_helpers";
import { Pagination } from "../../../../helpers/pagination/Pagination";
import { toAbsoluteUrl } from "../../../../../_metronic/_helpers";
import SVG from "react-inlinesvg";
import swal from "sweetalert";
import {
  deletePengumumanTarget,
  getPengumumanTarget,
  getPengumumanTargetByIdPengumuman,
} from "../../../../references/Api";

function PengumumanTarget({
  history,
  match: {
    params: { id },
  },
}) {
  const [content, setContent] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [sizePage, setSizePage] = useState(50);

  const add = () => history.push(`/admin/pengumuman/${id}/target/add`);
  const upload = () => history.push(`/admin/pengumuman/${id}/target/upload`);

  const deleteAction = (id_target) => {
    swal({
      title: "Apakah Anda Yakin?",
      text: "Klik OK untuk melanjutkan",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        deletePengumumanTarget(id_target).then(({ status }) => {
          if (status === 200) {
            swal("Berhasil", "Data berhasil dihapus", "success").then(() => {
              history.push("/dashboard");
              history.replace(`/admin/pengumuman/${id}/target`);
            });
          } else {
            swal("Gagal", "Data gagal dihapus", "error").then(() => {
              history.push("/dashboard");
              history.replace("/admin/pengumuman");
            });
          }
        });
      }
      // else {
      //   swal("Your imaginary file is safe!");
      // }
    });
  };
  const handleBack = () => {
    history.push(`/admin/pengumuman/${id}/edit`);
  };
  useEffect(() => {
    // getPengumumanTargetByIdPengumuman(id).then(({data})=> {
    //   setContent(data)
    // })
    getPengumumanTargetByIdPengumuman(id).then(({ data }) => {
      setContent(data);
    });
  }, [id]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      // formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: (cell, row, rowIndex) => {
        let rowNumber = (currentPage - 1) * sizePage + (rowIndex + 1);
        return <span>{rowNumber}</span>;
      },
    },
    {
      dataField: "nama",
      text: "Nama",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "nip_pendek",
      text: "NIP Pendek",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "nip",
      text: "NIP",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "unit_organisasi",
      text: "Unit Organisasi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "updated_at",
      text: "Updated",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      hidden: true,
    },
    // {
    //   dataField: "status",
    //   text: "Status",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   headerSortingClasses,
    //   formatter: columnFormatters.StatusColumnFormatterAdminPengumuman,
    // },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterAdminPengumumanTarget,
      formatExtraData: {
        deleteDialog: deleteAction,
        // editDialog: edit,
        // editPublish: editPublish,
        // deleteDialog: deleteAction,
        // apply: apply
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];

  const initialFilter = {
    sortOrder: "desc", // asc||desc
    sortField: "updated_at",
    pageNumber: currentPage,
    pageSize: sizePage,
  };
  const defaultSorted = [{ dataField: "updated_at", order: "desc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
    onPageChange: (page, sizePerPage) => {
      setCurrentPage(page);
    },
    onSizePerPageChange: (page, sizePerPage) => {
      setSizePage(page);
      setCurrentPage(sizePerPage);
    },
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;
  return (
    <>
      <Card>
        <CardHeader
          title="Target Pegawai"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <>
            <>
              <PaginationProvider pagination={paginationFactory(pagiOptions)}>
                {({ paginationProps, paginationTableProps }) => {
                  return (
                    <>
                      <ToolkitProvider
                        keyField="id_target"
                        data={content}
                        columns={columns}
                        search
                      >
                        {(props) => (
                          <div>
                            <div className="row">
                              <div className="col-lg-6 col-xl-6 mb-3">
                                <SearchBar
                                  {...props.searchProps}
                                  style={{ width: "500px" }}
                                />
                                <br />
                              </div>
                              <div className="col-lg-6 col-xl-6 mb-3">
                                <button
                                  type="button"
                                  className="btn btn-primary ml-3"
                                  style={{
                                    boxShadow:
                                      "0px 8px 15px rgb(55 104 165 / 30%)",
                                    float: "right",
                                  }}
                                  onClick={add}
                                >
                                  <span className="svg-icon menu-icon">
                                    <SVG
                                      src={toAbsoluteUrl(
                                        "/media/svg/icons/Code/Plus.svg"
                                      )}
                                    />
                                  </span>
                                  Tambah
                                </button>
                                <button
                                  type="button"
                                  className="btn btn-light-primary ml-3"
                                  style={{
                                    boxShadow:
                                      "0px 8px 15px rgb(55 104 165 / 30%)",
                                    float: "right",
                                  }}
                                  onClick={upload}
                                >
                                  <span className="svg-icon menu-icon">
                                    <SVG
                                      src={toAbsoluteUrl(
                                        "/media/svg/icons/Files/Upload.svg"
                                      )}
                                    />
                                  </span>
                                  Upload Data
                                </button>
                                <button
                                  type="button"
                                  className="btn btn-light"
                                  style={{
                                    boxShadow:
                                      "0px 8px 15px rgb(55 104 165 / 30%)",
                                    float: "right",
                                  }}
                                  onClick={handleBack}
                                >
                                  <span className="svg-icon menu-icon">
                                    <SVG
                                      src={toAbsoluteUrl(
                                        "/media/svg/icons/Navigation/Arrow-left.svg"
                                      )}
                                    />
                                  </span>
                                  Kembali
                                </button>
                              </div>
                            </div>
                            <BootstrapTable
                              {...props.baseProps}
                              wrapperClasses="table-responsive"
                              bordered={false}
                              headerWrapperClasses="thead-light"
                              classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                              defaultSorted={defaultSorted}
                              bootstrap4
                              noDataIndication={emptyDataMessage}
                              {...paginationTableProps}
                            ></BootstrapTable>
                            <Pagination paginationProps={paginationProps} />
                          </div>
                        )}
                      </ToolkitProvider>
                    </>
                  );
                }}
              </PaginationProvider>
            </>
          </>
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default PengumumanTarget;
