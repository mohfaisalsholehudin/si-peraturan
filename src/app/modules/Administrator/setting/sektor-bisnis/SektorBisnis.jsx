import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../../_metronic/_partials/controls";
import SektorBisnisTable from "./SektorBisnisTable";

function SektorBisnis() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Bisnis Sektor"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <SektorBisnisTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default SektorBisnis;
