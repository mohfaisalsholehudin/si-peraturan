/* Library */
import React, { useEffect, useState, useRef } from "react";
import swal from "sweetalert";

/* Helper */
import { useSubheader } from "../../../../../_metronic/layout";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";
import { getBisnisSektorById, saveBisnisSektor, updateBisnisSektor } from "../../../../references/Api";
import SektorBisnisEditForm from "./SektorBisnisEditForm"

function SektorBisnisEdit({
  history,
  match: {
    params: { id_sektor }
  }
}) {
  const initValues = {
    nama: "",
    keterangan: "",
    status: ""
  };

  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [content, setContent] = useState();
  const [loading, setLoading] = useState(false);
  const [isDisabled, setIsDisabled] = useState(false);



  useEffect(() => {
    let _title = id_sektor
      ? "Edit Bisnis Sektor"
      : "Tambah Bisnis Sektor";

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps

    if (id_sektor) {
        getBisnisSektorById(id_sektor).then(({ data })=> {
          setContent(data)
        })
    }
  }, [id_sektor, suhbeader]);
  const btnRef = useRef();
  const saveButton = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
      // setIsComplete(true);
      // disabled ? setIsComplete(false) : setIsComplete(true);
    }
  };

  const backAction = () => {
    history.push("/admin/setting/sektor");
  };
  const saveForm = values => {
    if (!id_sektor) {
      enableLoading();
        saveBisnisSektor(values.nama, values.keterangan, 1).then(
          ({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
                disableLoading();
                history.push("/admin/setting/sektor");
              });
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                disableLoading();
                history.push("/admin/setting/sektor/add");
              });
            }
          }
        );
    } else {
      enableLoading();
        updateBisnisSektor(id_sektor, values.nama, values.keterangan, values.status).then(
          ({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
                disableLoading();
                history.push("/admin/setting/sektor");
              });
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                disableLoading();
                history.push("/admin/setting/sektor/add");
              });
            }
          }
        );
    }
  };
  const enableLoading = () => {
    setLoading(true);
    setIsDisabled(true);
  };

  const disableLoading = () => {
    setLoading(false);
    setIsDisabled(false);
  };

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="mt-5">
            <SektorBisnisEditForm
              content={content || initValues}
              btnRef={btnRef}
              saveForm={saveForm}
            />
          </div>
        </>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        {/* <DetilJenisEditFooter backAction={backAction} btnRef={btnRef} /> */}
        <div className="col-lg-12" style={{ textAlign: "right" }}>
          <button
            type="button"
            onClick={backAction}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
          {`  `}

          {loading ?(
              <button
              type="submit"
              className="btn btn-success spinner spinner-white spinner-left ml-2"
              // onSubmit={() => handleSubmit()}
              onClick={saveButton}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
              }}
              disabled={isDisabled}
            >
              <span>Simpan</span>
            </button>
          ) :
          (
            <button
            type="submit"
            className="btn btn-success ml-2"
            onClick={saveButton}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
            disabled={isDisabled}
            // disabled={disabled}
          >
            <i className="fas fa-save"></i>
            Simpan
          </button>
          )
          }
          
        </div>
      </CardFooter>
    </Card>
  );
}

export default SektorBisnisEdit;
