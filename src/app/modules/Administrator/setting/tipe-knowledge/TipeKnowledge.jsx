import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../../_metronic/_partials/controls";
import TipeKnowledgeTable from "./TipeKnowledgeTable";

function TipeKnowledge() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Tipe Knowledge"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <TipeKnowledgeTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default TipeKnowledge;
