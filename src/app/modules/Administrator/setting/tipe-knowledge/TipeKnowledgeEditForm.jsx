import React, { useState, useEffect } from "react";
import { Field, Formik, Form } from "formik";
import Select from "react-select";
import * as Yup from "yup";
import "../../../../helpers/DatePickerStyles.css";
import { Input, Textarea, Select as Sel } from "../../../../helpers";

function TipeKnowledgeEditForm({ content, btnRef, saveForm }) {
  const [show, setShow] = useState(false);

  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    nama: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Nama is required"),
    jenis: Yup.string().required("Jenis is requried"),
    template: Yup.string().required("Template is requried"),
    keterangan: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Keterangan is required"),
  });

  useEffect(() => {
    // getMasterJenis().then(({ data }) => {
    //   data.map(data => {
    //     return setMaster(master => [
    //       ...master,
    //       {
    //         label: data.nama,
    //         value: data.id
    //       }
    //     ]);
    //   });
    // });
  }, []);
  useEffect(() => {
    content.status !== "" ? setShow(true) : setShow(false);
  }, [content]);
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={content}
        validationSchema={ProposalEditSchema}
        onSubmit={(values) => {
          saveForm(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
          handleBlur,
          handleChange,
          errors,
          touched,
          values,
          isValid,
        }) => {
          return (
            <>
              <Form className="form form-label-right">
                {/* Field nama */}
                <div className="form-group row">
                  <Field
                    name="nama"
                    component={Input}
                    placeholder="Nama"
                    label="Nama"
                  />
                </div>
                {/* FIELD JENIS */}
                <div className="form-group row">
                  <Sel name="jenis" label="Jenis">
                    <option>Pilih Jenis</option>
                    <option value="Tacit">Tacit</option>
                    <option value="Explicit">Explicit</option>
                  </Sel>
                </div>
                {/* FIELD TEMPLATE */}
                <div className="form-group row">
                  <Sel name="template" label="Template">
                    <option>Pilih Template</option>
                    <option value="Success Story">Success Story</option>
                    <option value="SOP">SOP</option>
                    <option value="Other Knowledge">Other Knowledge</option>
                  </Sel>
                </div>
                {/* Field Keterangan */}
                <div className="form-group row">
                  <Field
                    name="keterangan"
                    component={Textarea}
                    placeholder="Keterangan"
                    label="Keterangan"
                  />
                </div>
                {/* FIELD STATUS */}
                {show ? (
                  <div className="form-group row">
                    <Sel name="status" label="Status">
                      <option>Pilih Status</option>
                      <option value="1">AKTIF</option>
                      <option value="0">TIDAK AKTIF</option>
                    </Sel>
                  </div>
                ) : null}

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default TipeKnowledgeEditForm;
