import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../../_metronic/_partials/controls";
import ProbisTable from "./ProbisTable";

function Probis() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Bisnis Proses"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <ProbisTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default Probis;
