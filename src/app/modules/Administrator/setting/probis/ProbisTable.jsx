/* Library */
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import swal from "sweetalert";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";
import SVG from "react-inlinesvg";

/* Helpers */
import { Pagination } from "../../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses
} from "../../../../../_metronic/_helpers";
import { toAbsoluteUrl } from "../../../../../_metronic/_helpers";
import * as columnFormatters from "../../../../helpers/column-formatters";

/* Component */
// import ProposalOpen from "./ProposalOpen";
// import ProposalReject from "./ProposalReject";

/* Utility */
import {getProbis} from "../../../../references/Api"

function ProbisTable() {
  const history = useHistory();
  const [content, setContent] = useState([]);
// const content = [
//     {id: 1, nama: "Business Process 1", keterangan: "Keterangan Business Process 1", case_name: "", status: "AKTIF"},
//     {id: 2, nama: "Business Process 2", keterangan: "Keterangan Business Process 2", case_name: "", status: "TIDAK AKTIF"},
//     {id: 3, nama: "Business Process 3", keterangan: "Keterangan Business Process 3", case_name: "", status: "AKTIF"},
// ]

  const add = () => history.push("/admin/setting/probis/add");
  //   const edit = (id) => history.push(`/admin/detil-jenis/${id}/edit`);
  const edit = id_probis => {history.push(`/admin/setting/probis/${id_probis}/edit`)};
  const deleteAction = id => {
    //     swal({
    //       title: "Apakah Anda Yakin?",
    //       text: "Klik OK untuk melanjutkan",
    //       icon: "warning",
    //       buttons: true,
    //       dangerMode: true
    //     }).then(willDelete => {
    //       if (willDelete) {
    //         deleteDetilJenis(id).then(({ status }) => {
    //           if (status === 200) {
    //             swal("Berhasil", "Data berhasil dihapus", "success").then(() => {
    //               history.push("/dashboard");
    //               history.replace("/admin/detil-jenis");
    //             });
    //           } else {
    //             swal("Gagal", "Data gagal disimpan", "error").then(() => {
    //               history.push("/dashboard");
    //               history.replace("/admin/detil-jenis");
    //             });
    //           }
    //         });
    //       }
    //       // else {
    //       //   swal("Your imaginary file is safe!");
    //       // }
    //     });
  };

    useEffect(()=> {
      getProbis().then(({ data }) => {
        setContent(data)
        // console.log(data)
      })
    },[])

  const openCaseName = (id_probis) => {
    history.push(`/admin/setting/probis/${id_probis}/case-name`)
  }

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "id_probis",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
      hidden: true
    },
    {
      dataField: "nama",
      text: "Nama",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "keterangan",
      text: "Keterangan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "ctas",
      text: "id ctas",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "case_name",
      text: "Case Name",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: columnFormatters.FileColumnFormatterAdminSettingProbis,
      formatExtraData: {
        openCaseName: openCaseName
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3"
    },
    {
      dataField: "updated",
      text: "Waktu Edit",
      sort: true,
      formatter: columnFormatters.DateFormatterProbis,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: columnFormatters.StatusColumnFormatterAdminSettingProbis,
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterAdminSettingProbis,
      formatExtraData: {
        openEditDialog: edit,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];
  const initialFilter = {
    sortOrder: "desc", // asc||desc
    sortField: "status",
    pageNumber: 1,
    pageSize: 50
  };
  // const defaultSorted = [{ dataField: "status", order: "desc" }];
  const defaultSorted = [{ dataField: "status", order: "desc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_probis"
                  data={content}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <button
                            type="button"
                            className="btn btn-primary"
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                              float: "right"
                            }}
                            onClick={add}
                          >
                            <span className="svg-icon menu-icon">
                              <SVG
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Code/Plus.svg"
                                )}
                              />
                            </span>
                            Tambah
                          </button>
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
    </>
  );
}

export default ProbisTable;
