import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../../_metronic/_partials/controls";
import TusiTable from "./TusiTable";

function Tusi() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Tugas dan Fungsi"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <TusiTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default Tusi;
