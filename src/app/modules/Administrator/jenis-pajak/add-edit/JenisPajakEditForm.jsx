import React, { useState, useEffect } from "react";
import { Field, Formik, Form } from "formik";
import Select from "react-select";
import * as Yup from "yup";
import "../../../../helpers/DatePickerStyles.css";
import { Input, Select as Sel } from "../../../../helpers";
import { unitKerja } from "../../../../references/UnitKerja";

function JenisPajakEditForm({ proposal, btnRef, saveForm }) {
  const [kantor, setKantor] = useState([]);

  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    nm_jnspajak: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Jenis Pajak is required"),
    kodeunit: Yup.mixed()
      .nullable(false)
      .required("Unit is required"),
    status: Yup.string().required("Status is required")
  });

  useEffect(() => {
    unitKerja.map(data => {
      return setKantor(kantor => [
        ...kantor,
        {
          label: data.nm_UNIT_KERJA,
          value: data.kd_unit_kerja,
          kodekantor: data.kd_KANTOR
        }
      ]);
    });
  }, []);

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={proposal}
        validationSchema={ProposalEditSchema}
        onSubmit={values => {
          // console.log(values);
          saveForm(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
          handleBlur,
          handleChange,
          errors,
          touched,
          values,
          isValid
        }) => {
          const handleChangeUnitKerja = val => {
            setFieldValue("kodeunit", val.label);
            setFieldValue("kodekantor", val.kodekantor);
          };
          return (
            <>
              <Form className="form form-label-right">
                {/* Field Jenis Pajak */}
                <div className="form-group row">
                  <Field
                    name="nm_jnspajak"
                    component={Input}
                    placeholder="Jenis Pajak"
                    label="Jenis Pajak"
                  />
                </div>
                {/* Field Unit Kerja */}

                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Unit Kerja
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Select
                      options={kantor}
                      onChange={value => handleChangeUnitKerja(value)}
                      value={kantor.filter(
                        data => data.label === values.kodeunit
                      )}
                    />
                  </div>
                </div>
                {/* FIELD STATUS */}
                <div className="form-group row">
                  <Sel name="status" label="Status">
                    <option>Pilih Status</option>
                    <option value="AKTIF">AKTIF</option>
                    <option value="PASIF">PASIF</option>
                  </Sel>
                </div>

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default JenisPajakEditForm;
