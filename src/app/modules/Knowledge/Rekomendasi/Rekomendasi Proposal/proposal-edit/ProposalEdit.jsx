import React, { useEffect, useState, useRef } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../../../_metronic/_partials/controls";
import { useSubheader } from "../../../../../../_metronic/layout";
import RekomendasiEditForm from "./RekomendasiEditForm";
import RekomendasiEditFooter from "./RekomendasiEditFooter";
import {
  uploadFile,
  getRekomendasiById,
  saveRekomendasi,
  updateRekomendasi,
  updateStatusRekomendasi,
} from "../../../Api";
import swal from "sweetalert";
import { useSelector } from "react-redux";

function RekomendasiEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  const { user } = useSelector((state) => state.auth);
  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [loading, setLoading] = useState(false);
  const [isDisabled, setIsDisabled] = useState();
  const [rekomendasi, setRekomendasi] = useState();
  const [content, setContent] = useState();

  const initValues = {
    alasan_tolak: "",
    body: "",
    file_upload: "",
    id_detiljns: "",
    kd_kantor: user.kantorLegacyKode,
    kd_unit_org: user.unitEs4LegacyKode,
    nip_pengusul: user.nip9,
    nip_pjbt_es3: "",
    nip_pjbt_es4: "",
    no_hsl_pemeriksaan: "",
    perihal: "",
    status: "",
    tgl_pemeriksaan: "",
    thn_periksa: "",
  };

  const getCurrentDate = () => {
    const date = new Date();
    return date;
  };

  const enableLoading = () => {
    setLoading(true);
  };

  const disableLoading = () => {
    setLoading(false);
  };

  const handleChangeProposalRekomendasi = (val) => {
    getRekomendasiById(val.value).then(({ data }) => {
      setContent({
        id_rekomendasi: data.id_rekomendasi,
        no_hsl_pemeriksaan: data.no_hsl_pemeriksaan,
        tgl_pemeriksaan: getCurrentDate(),
        id_detiljns: data.id_detiljns,
        thn_periksa: data.thn_periksa,
        perihal: data.perihal,
        file_upload: data.file_upload,
        alasan_tolak: data.alasan_tolak,
        body: data.body,
        nip_pengusul: data.nip_pengusul,
        nip_pjbt_es4: data.nip_pjbt_es4,
        nip_pjbt_es3: data.nip_pjbt_es3,
        wkt_usul: data.wkt_usul,
        wkt_teliti_es4: data.wkt_teliti_es4,
        wkt_teliti_es3: data.wkt_teliti_es3,
        status: data.status,
        wkt_create: data.wkt_create,
        wkt_update: data.wkt_update,
        kd_kantor: data.kd_kantor,
        kd_unit_org: data.kd_unit_org,
      });
    });
  };

  useEffect(() => {
    let _title = id ? "Edit Surat Rekomendasi" : "Tambah Surat Rekomendasi";

    setTitle(_title);
    suhbeader.setTitle(_title);

    if (id) {
      getRekomendasiById(id).then(({ data }) => {
        setRekomendasi({
          id_rekomendasi: data.id_rekomendasi,
          no_hsl_pemeriksaan: data.no_hsl_pemeriksaan,
          tgl_pemeriksaan: data.tgl_pemeriksaan,
          id_detiljns: data.id_detiljns,
          thn_periksa: data.thn_periksa,
          perihal: data.perihal,
          file_upload: data.file_upload,
          alasan_tolak: data.alasan_tolak,
          body: data.body,
          nip_pengusul: data.nip_pengusul,
          nip_pjbt_es4: data.nip_pjbt_es4,
          nip_pjbt_es3: data.nip_pjbt_es3,
          wkt_usul: data.wkt_usul,
          wkt_teliti_es4: data.wkt_teliti_es4,
          wkt_teliti_es3: data.wkt_teliti_es3,
          status: data.status,
          wkt_create: data.wkt_create,
          wkt_update: data.wkt_update,
          kd_kantor: data.kd_kantor,
          kd_unit_org: data.kd_unit_org,
        });
      });
    }
  }, [id, suhbeader]);
  const btnRef = useRef();

  const saveSurat = (values) => {
    if (!id) {
      if (values.file.name) {
        enableLoading();
        const formData = new FormData();

        formData.append("file", values.file);
        //console.log(formData);

        uploadFile(formData)
          .then(({ data }) => {
            disableLoading();
            saveRekomendasi(
              values.alasan_tolak,
              values.body,
              data.message,
              values.id_detiljns,
              values.kd_kantor,
              values.kd_unit_org,
              values.nip_pengusul,
              values.nip_pjbt_es3,
              values.nip_pjbt_es4,
              values.no_hsl_pemeriksaan,
              values.perihal,
              values.status,
              values.tgl_pemeriksaan,
              values.thn_periksa
            ).then(({ status }) => {
              if (status === 201 || status === 200) {
                swal("Berhasil", "Usulan berhasil disimpan", "success").then(
                  () => {
                    history.push("/knowledge/rekomendasi/usulan");
                  }
                );
              } else {
                swal("Gagal", "Usulan gagal disimpan", "error").then(() => {
                  history.push("/knowledge/rekomendasi/usulan/new");
                });
              }
            });
          })
          .catch(() => window.alert("Oops Something went wrong !"));
      } else {
        saveRekomendasi(
          values.alasan_tolak,
          values.body,
          values.file_upload,
          values.id_detiljns,
          values.kd_kantor,
          values.kd_unit_org,
          values.nip_pengusul,
          values.nip_pjbt_es3,
          values.nip_pjbt_es4,
          values.no_hsl_pemeriksaan,
          values.perihal,
          values.status,
          values.tgl_pemeriksaan,
          values.thn_periksa
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Usulan berhasil disimpan", "success").then(() => {
              history.push("/knowledge/rekomendasi/usulan");
            });
          } else {
            swal("Gagal", "Usulan gagal disimpan", "error").then(() => {
              history.push("/knowledge/rekomendasi/usulan/new");
            });
          }
        });
      }
    } else {
      //console.log(values.status)
      // ketika status tolak dan melakukan edit , otomatis status jadi draft lagi

      if (values.status === "Tolak") {
        if (values.file.name) {
          enableLoading();
          const formData = new FormData();
          formData.append("file", values.file);
          uploadFile(formData).then(({ data }) => {
            disableLoading();
            updateRekomendasi(
              values.id_rekomendasi,
              values.alasan_tolak,
              values.body,
              data.message,
              values.id_detiljns,
              values.kd_kantor,
              values.kd_unit_org,
              values.nip_pengusul,
              values.nip_pjbt_es3,
              values.nip_pjbt_es4,
              values.no_hsl_pemeriksaan,
              values.perihal,
              values.status,
              values.tgl_pemeriksaan,
              values.thn_periksa
            ).then(({ status }) => {
              if (status === 201 || status === 200) {
                updateStatus();
              } else {
                swal("Gagal", "Usulan gagal dirubah", "error").then(() => {
                  history.push("/dashboard");
                  history.replace("/knowledge/rekomendasi/usulan");
                });
              }
            });
          });
        } else {
          updateRekomendasi(
            values.id_rekomendasi,
            values.alasan_tolak,
            values.body,
            values.file_upload,
            values.id_detiljns,
            values.kd_kantor,
            values.kd_unit_org,
            values.nip_pengusul,
            values.nip_pjbt_es3,
            values.nip_pjbt_es4,
            values.no_hsl_pemeriksaan,
            values.perihal,
            values.status,
            values.tgl_pemeriksaan,
            values.thn_periksa
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              updateStatus();
            } else {
              swal("Gagal", "Usulan gagal diubah", "error").then(() => {
                history.push("/dashboard");
                history.replace("/knowledge/rekomendasi/usulan");
              });
            }
          });
        }
      } else {
        if (values.file.name) {
          enableLoading();
          //console.log(values.file.name);
          const formData = new FormData();
          formData.append("file", values.file);
          uploadFile(formData)
            .then(({ data }) => {
              disableLoading();
              updateRekomendasi(
                values.id_rekomendasi,
                values.alasan_tolak,
                values.body,
                data.message,
                values.id_detiljns,
                values.kd_kantor,
                values.kd_unit_org,
                values.nip_pengusul,
                values.nip_pjbt_es3,
                values.nip_pjbt_es4,
                values.no_hsl_pemeriksaan,
                values.perihal,
                values.status,
                values.tgl_pemeriksaan,
                values.thn_periksa
              ).then(({ status }) => {
                if (status === 201 || status === 200) {
                  swal("Berhasil", "Usulan berhasil diubah", "success").then(
                    () => {
                      history.push("/knowledge/rekomendasi/usulan");
                    }
                  );
                } else {
                  swal("Gagal", "Usulan gagal diubah", "error").then(() => {
                    history.push("/knowledge/rekomendasi/usulan/new");
                  });
                }
              });
            })
            .catch(() => window.alert("Oops Something went wrong !"));
        } else {
          updateRekomendasi(
            values.id_rekomendasi,
            values.alasan_tolak,
            values.body,
            values.file_upload,
            values.id_detiljns,
            values.kd_kantor,
            values.kd_unit_org,
            values.nip_pengusul,
            values.nip_pjbt_es3,
            values.nip_pjbt_es4,
            values.no_hsl_pemeriksaan,
            values.perihal,
            values.status,
            values.tgl_pemeriksaan,
            values.thn_periksa
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Usulan berhasil diubah", "success").then(() => {
                history.push("/knowledge/rekomendasi/usulan");
              });
            } else {
              swal("Gagal", "Usulan gagal diubah", "error").then(() => {
                history.push("/knowledge/rekomendasi/usulan/new");
              });
            }
          });
        }
      }
    }
  };

  const updateStatus = () => {
    updateStatusRekomendasi(id, 5, 1, user.nip9).then(({ status }) => {
      if (status === 201 || status === 200) {
        swal("Berhasil", "Usulan Berubah Menjadi Draft", "success").then(() => {
          history.push("/dashboard");
          history.replace("/knowledge/rekomendasi/usulan");
        });
      } else {
        swal("Gagal", "Usulan gagal diubah", "error").then(() => {
          history.push("/dashboard");
          history.replace("/knowledge/rekomendasi/usulan");
        });
      }
    });
  };

  const backToRekomendasiList = () => {
    history.push(`/knowledge/rekomendasi/usulan`);
  };

  const setDisabled = (val) => {
    setIsDisabled(val);
  };
  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <RekomendasiEditForm
            actionsLoading={actionsLoading}
            proposal={rekomendasi || initValues}
            btnRef={btnRef}
            saveProposal={saveSurat}
            loading={loading}
            handleChangeProposalRekomendasi={handleChangeProposalRekomendasi}
            setDisabled={setDisabled}
            backAction={backToRekomendasiList}
          />
        </div>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        {/* <RekomendasiEditFooter
          backAction={backToRekomendasiList}
          btnRef={btnRef}
        ></RekomendasiEditFooter> */}
      </CardFooter>
    </Card>
  );
}

export default RekomendasiEdit;
