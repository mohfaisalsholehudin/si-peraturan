import React from "react";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../_metronic/_partials/controls";
import KodefikasiTable from "./KodefikasiTable";

function Kodefikasi() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Kodifikasi"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <KodefikasiTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default Kodefikasi;
