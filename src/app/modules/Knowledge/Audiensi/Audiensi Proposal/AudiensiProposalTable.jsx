/* Library */
import React, { useEffect, useState } from "react";
import { useHistory, Route } from "react-router-dom";
import { useSelector } from "react-redux";
import swal from "sweetalert";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import SVG from "react-inlinesvg";

/* Helpers */
import { Pagination } from "../../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../../_metronic/_helpers";
import { toAbsoluteUrl } from "../../../../../_metronic/_helpers";
import * as columnFormatters from "../../column-formatters";
import {
  deleteAudiensiById,
  getAudiensiNonTerimaByNip,
  updateStatusAudiensi,
} from "../../Api";
import AudiensiReject from "../AudiensiReject";
import AudiensiHistory from "../AudiensiHistory";

function AudiensiProposalTable() {
  const { user } = useSelector((state) => state.auth);
  const history = useHistory();
  const [audiensi, setAudiensi] = useState([]);

  const addAudiensi = () => history.push("/knowledge/audiensi/usulan/add");

  const showHistoryAudiensi = (id) =>
    history.push(`/knowledge/audiensi/usulan/${id}/history`);

  const rejectAudiensi = (id) =>
    history.push(`/knowledge/audiensi/usulan/${id}/reject`);

  const editAudiensi = (id) =>
    history.push(`/knowledge/audiensi/usulan/${id}/edit`);

  const detailAudiensi = (id) =>
    history.push(`/knowledge/audiensi/usulan/${id}/detail`);

  const addBody = (id) => history.push(`/knowledge/audiensi/usulan/${id}/body`);

  const applyAudiensi = (id) => {
    swal({
      title: "Apakah Anda Ingin Mengajukan Usulan Ini?",
      text: "Klik OK untuk melanjutkan",
      icon: "warning",
      buttons: true,
      dangerMode: false,
    }).then((willApply) => {
      if (willApply) {
        updateStatusAudiensi(id, 1, 2, user.nip9).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Usulan berhasil diajukan", "success").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/audiensi/usulan");
            });
          } else {
            swal("Gagal", "Usulan gagal diajukan", "error").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/audiensi/usulan");
            });
          }
        });
      }
    });
  };

  const deleteAction = (id) => {
    swal({
      title: "AApakah Anda Ingin Menghapus Usulan Ini?",
      text: "Klik OK untuk melanjutkan",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        deleteAudiensiById(id).then(({ status }) => {
          if (status === 200) {
            swal("Berhasil", "Usulan berhasil dihapus", "success").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/audiensi/usulan");
            });
          } else {
            swal("Gagal", "Usulan gagal dihapus", "error").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/audiensi/usulan");
            });
          }
        });
      }
    });
  };

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "kmAudiensi.no_nd",
      text: "No ND Hasil Audiensi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "kmAudiensi.tgl_nd",
      text: "Tgl ND Audiensi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "kmAudiensi.perihal",
      text: "Perihal Audiensi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "jenisAudiensi.nama",
      text: "Jenis Audiensi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "kmAudiensi.status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.AudiensiStatusColumnFormatter,
      headerSortingClasses,
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.AudiensiProposalActionColumnFormatter,
      formatExtraData: {
        openEditDialog: editAudiensi,
        openDeleteDialog: deleteAction,
        showHistory: showHistoryAudiensi,
        detailAudiensi: detailAudiensi,
        rejectAudiensi: rejectAudiensi,
        applyAudiensi: applyAudiensi,
        addBody: addBody,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];

  const initialFilter = {
    sortOrder: "desc", // asc||desc
    sortField: "wkt_update",
    pageNumber: 1,
    pageSize: 50,
  };

  const defaultSorted = [{ dataField: "wkt_update", order: "desc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 },
  ];
  const [currentPage, setCurrentPage] = useState(1);
  const [sizePage, setSizePage] = useState(50);
  const pagiOptions = {
    custom: true,
    totalSize: audiensi.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
    onPageChange: (page, sizePerPage) => {
      setCurrentPage(page);
    },
    onSizePerPageChange: (page, sizePerPage) => {
      setSizePage(page);
      setCurrentPage(sizePerPage);
    },
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;

  useEffect(() => {
    getAudiensiNonTerimaByNip(user.nip9).then(({ data }) => {
      // setPlan(data);
      data.map((data) => {
        return data.status !== "Terima"
          ? setAudiensi((audiensi) => [...audiensi, data])
          : // : data.status === "Draft"
            // ? setProposal((proposal) => [...proposal, data])
            null;
      });
    });
  }, [user.kantorLegacyKode]);
  console.log(user);

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="kmAudiensi.id_audiensi"
                  data={audiensi}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <button
                            type="button"
                            className="btn btn-primary"
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                              float: "right",
                            }}
                            onClick={addAudiensi}
                          >
                            <span className="svg-icon menu-icon">
                              <SVG
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Code/Plus.svg"
                                )}
                              />
                            </span>
                            Tambah
                          </button>
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
      <Route path="/knowledge/audiensi/usulan/:id/reject">
        {({ history, match }) => (
          <AudiensiReject
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/audiensi/usulan");
            }}
            onRef={() => {
              history.push("/knowledge/audiensi/usulan");
            }}
          />
        )}
      </Route>
      <Route path="/knowledge/audiensi/usulan/:id/history">
        {({ history, match }) => (
          <AudiensiHistory
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/audiensi/usulan");
            }}
            onRef={() => {
              history.push("/knowledge/audiensi/usulan");
            }}
          />
        )}
      </Route>
    </>
  );
}

export default AudiensiProposalTable;
