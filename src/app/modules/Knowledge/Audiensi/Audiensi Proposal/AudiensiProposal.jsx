import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../../_metronic/_partials/controls";
import AudiensiProposalTable from "../Audiensi Proposal/AudiensiProposalTable";

function AudiensiProposal() {
  return (
    <>
      <Card>
        <CardHeader
          title="Usulan Audiensi"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <AudiensiProposalTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default AudiensiProposal;
