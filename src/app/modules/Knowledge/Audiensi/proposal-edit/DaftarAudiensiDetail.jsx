import React, { useEffect, useState, useRef } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../../_metronic/_partials/controls";
import { useSubheader } from "../../../../../_metronic/layout";
import DaftarAudiensiDetailForm from "./DaftarAudiensiDetailForm";
import DaftarAudiensiDetailFooter from "./DaftarAudiensiDetailFooter";
import {
    getAudiensiById,
} from "../../Api";

const initValues = {
    alasan_tolak: "" ,
    body: "" ,
    file_upload: "" ,
    id_detiljns: "" ,
    nip_pengusul: "" ,
    nip_pjbt_es3: "" ,
    nip_pjbt_es4: "" ,
    no_nd: "" ,
    perihal: "" ,
    status: "" ,
    tgl_nd: "",
};

function DaftarAudiensiDetail({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [isDisabled, setIsDisabled] = useState();
  const [audiensiterima, setAudiensiTerima] = useState();
  const [audiensiterimadetail, setAudiensiTerimaDetail] = useState([]);
  let thePath = document.URL;
  let nextStatus = "";
  const lastPath = thePath.substring(thePath.lastIndexOf("/") + 1);

  useEffect(() => {
      let _title = "Detail Audiensi";
      setTitle(_title);
      suhbeader.setTitle(_title);

    getAudiensiById(id).then(({ data }) => {
      setAudiensiTerima({
        id_audiensi: data.id_audiensi,
          no_nd: data.no_nd,
          tgl_nd: data.tgl_nd,
          id_detiljns: data.id_detiljns,
          perihal: data.perihal,
          file_upload: data.file_upload,
          alasan_tolak: data.alasan_tolak,
          body: data.body,
          nip_pengusul: data.nip_pengusul,
          nip_pjbt_es4: data.nip_pjbt_es4,
          nip_pjbt_es3: data.nip_pjbt_es3,
          wkt_usul: data.wkt_usul,
          wkt_teliti_es4: data.wkt_teliti_es4,
          wkt_teliti_es3: data.wkt_teliti_es3,
          status: data.status,
          wkt_create: data.wkt_create,
          wkt_update: data.wkt_update,
        });
    });
    }, [id, suhbeader]);

  const btnRef = useRef();

  const backToDaftarAudiensiList = () => {
    history.push(`/knowledge/audiensi/monitoring`);
  };

  const setDisabled = (val) => {
    setIsDisabled(val);
  };
  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <DaftarAudiensiDetailForm
            actionsLoading={actionsLoading}
            proposal={audiensiterima || initValues}
            AudiensiTerimaDetail={audiensiterimadetail}
            idAudiensi={id}
            btnRef={btnRef}
            setDisabled={setDisabled}
          />
        </div>

      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <DaftarAudiensiDetailFooter
          backAction={backToDaftarAudiensiList}
          btnRef={btnRef}
        ></DaftarAudiensiDetailFooter>
      </CardFooter>
    </Card>
  );
}

export default DaftarAudiensiDetail;
