import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { Field, Formik, Form } from "formik";
import "./styles.css";
import {
  DatePickerField,
  Input,
  Textarea,
  Select as Sel,
} from "../proposal-edit/helpers";
import { getAudiensiById, getJenisAudiensi } from "../../Api";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import Editor from "ckeditor5-custom-build/build/ckeditor";
const {SLICE_URL} = window.ENV;

function DaftarAudiensiDetailForm({
  proposal,
  idAudiensi
}) {
  const [audiensiterima, setAudiensiTerima] = useState([]);
  const [jenis, setJenis] = useState([]);
  const { user } = useSelector((state) => state.auth);

  const [propbody, setPropbody] = useState("");

  useEffect(() => {
    getAudiensiById(idAudiensi).then(({ data }) => {
      if (data.body) {
        setPropbody(JSON.parse(data.body));
      }
    });
  }, [idAudiensi]);

  useEffect(() => {
    getJenisAudiensi().then(({ data }) => {
      setJenis(data);
    });
  }, []);

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={proposal}
      >
        {() => {
          return (
            <>
              <Form className="form form-label-right">

                {/* FIELD NO SURAT */}
                <div className="form-group row">
                  <Field
                    disabled
                    name="no_nd"
                    component={Input}
                    placeholder="Nomor ND Hasil Audiensi"
                    label="Nomor ND Hasil Audiensi"
                  />
                </div>

                {/* FIELD TANGGAL SURAT */}
                <div className="form-group row">
                  <DatePickerField
                    name="tgl_nd"
                    label="Tanggal ND Audiensi"
                    disabled
                  />
                </div>

                {/* FIELD tentang */}
                <div className="form-group row">
                  <Field
                    name="perihal"
                    component={Textarea}
                    placeholder="Perihal"
                    label="Perihal"
                    disabled
                  />
                </div>

                {/* FIELD jenis */}
                <div className="form-group row">
                  <Sel name="id_detiljns" label="Jenis Audiensi" disabled>
                    <option value=""></option>
                    {jenis.map((data, index) => (

                      <option key={index} value={data.id}>{data.nama}</option>
                    ))}
                  </Sel>
                </div>

                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    File Rekomendasi
                  </label>
                  <div
                    className="col-lg-9 col-xl-6"
                    style={{ marginTop: "10px" }}
                  >
                    <a
                      href={proposal.file_upload}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      {proposal.file_upload.slice(SLICE_URL)}
                    </a>
                  </div>
                </div>
                {/* FIELD BODY */}
                <div>
                  <br />
                  <h5>Isi Materi</h5>
                </div>
                <div className="document-editor">
                  <div className="document-editor__toolbar"></div>
                  <div className="document-editor__editable-container">
                    <CKEditor
                      disabled
                      onReady={(editor) => {
                        console.log("Editor is ready to use!", editor);
                        window.editor = editor;

                        // Add these two lines to properly position the toolbar
                        const toolbarContainer = document.querySelector(
                          ".document-editor__toolbar"
                        );
                        toolbarContainer.appendChild(
                          editor.ui.view.toolbar.element
                        );
                      }}
                      config={{
                        removePlugins: ["Heading", "Link"],
                        toolbar: [],
                        isReadOnly: true,
                      }}
                      editor={Editor}
                      //ICAAAAAAAAAAAAAAALLLLLLLLLLLLLLLLLLLLLLLL
                      //INI BUAT NAMBAHIN TEMPLATE NYA DI BAWAH INI PAKAI HTML
                      data={propbody}
                    />
                  </div>
                </div>
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default DaftarAudiensiDetailForm;
