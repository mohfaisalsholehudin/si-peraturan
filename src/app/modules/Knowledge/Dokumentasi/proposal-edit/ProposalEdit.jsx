import React, { useEffect, useState, useRef } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";
// import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";

import { useSubheader } from "../../../../../_metronic/layout";
import ProposalEditForm from "./ProposalEditForm";
import ProposalEditFooter from "./ProposalEditFooter";
import { getUsulanById, saveUsulan, updateUsulan, uploadFile } from "../../Api";
import swal from "sweetalert";

//12 data yang dikirim, yang belum dimasukkan 'nip_perekam', 'jns_usul' dan yang tidak dimasukkan disini adalah 'alasan_tolak'.
const initValues = {
  // no_surat: "",
  // tgl_surat: "",
  // perihal: "",
  // jns_pajak: "",
  // unit_kerja: "",
  // instansi: "",
  // alamat: "",
  // no_pic: "",
  // nm_pic: "",
  // file: "",
  // jns_pengusul: "",
  // status: "",
  // nip_perekam: "",
  // jns_usul: "",
  // alasan_tolak: ""
};

function DokumentasiEdit({
  history,
  match: {
    params: { id }
  }
}) {
  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [isDisabled, setIsDisabled] = useState();
  const [proposal, setProposal] = useState();

  useEffect(() => {
    let _title = id ? "Edit Dokumentasi Perencanaan" : "Tambah Dokumentasi Perencanaan";

    setTitle(_title);
    suhbeader.setTitle(_title);

  }, [id, suhbeader]);
  const btnRef = useRef();
  const saveProposal = values => {
    if (!id) {
      const formData = new FormData();
    } else {

    }
  };
  const backToProposalList = () => {
    history.push(`/knowledge/doc`);
  };

  const setDisabled = val => {
    setIsDisabled(val);
  };
  return (
    <Card>
      {/* {actionsLoading && <ModalProgressBar />} */}
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <ProposalEditForm
            actionsLoading={actionsLoading}
            proposal={proposal || initValues}
            btnRef={btnRef}
            saveProposal={saveProposal}
            setDisabled={setDisabled}
          />
        </div>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <ProposalEditFooter
          backAction={backToProposalList}
          btnRef={btnRef}
        ></ProposalEditFooter>
      </CardFooter>
    </Card>
  );
}

export default DokumentasiEdit;
