import React, { useEffect, useState, useRef } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../../_metronic/_partials/controls";

import { useSubheader } from "../../../../../_metronic/layout";
import { useSelector } from "react-redux";
import ProposalEditForm from "./ProposalEditForm";
import ProposalEditFooter from "./ProposalEditFooter";
import {
  updateStatusKajian,
  getKajianById,
  uploadFile,
  saveKajian,
  updateKajian,
} from "../../Api";
import swal from "sweetalert";

function RegulasiEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();
  const { user } = useSelector((state) => state.auth);
  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [content, setContent] = useState();
  const [isDisabled, setIsDisabled] = useState();
  const [proposal, setProposal] = useState();

  const initValues = {
    alasan_tolak: "",
    body: "",
    file_upload: "",
    id_detiljns: "",
    kd_kantor: user.kantorLegacyKode,
    kd_unit_org: user.unitEs4LegacyKode,
    nip_pengusul: user.nip9,
    nip_pjbt_es3: "",
    nip_pjbt_es4: "",
    perihal: "",
    status: "",
    tgl_kajian: "",
    unit_kajian: "",
  };

  const getCurrentDate = () => {
    const date = new Date();
    return date;
  };

  const handleChangeProposalRegulasi = (val) => {
    getKajianById(val.value).then(({ data }) => {
      setContent({
        id_kajian: data.id_kajian,
        tgl_kajian: getCurrentDate(),
        id_detiljns: data.id_detiljns,
        unit_kajian: data.unit_kajian,
        perihal: data.perihal,
        file_upload: data.file_upload,
        alasan_tolak: data.alasan_tolak,
        body: data.body,
        nip_pengusul: data.user.nip9,
        nip_pjbt_es4: data.nip_pjbt_es4,
        nip_pjbt_es3: data.nip_pjbt_es3,
        wkt_usul: data.wkt_usul,
        wkt_teliti_es4: data.wkt_teliti_es4,
        wkt_teliti_es3: data.wkt_teliti_es3,
        status: data.status,
        wkt_create: data.wkt_create,
        wkt_update: data.wkt_update,
        kd_kantor: data.kd_kantor,
        kd_unit_org: data.kd_unit_org,
      });
    });
  };

  useEffect(() => {
    let _title = id ? "Edit Kajian" : "Tambah Kajian";

    setTitle(_title);
    suhbeader.setTitle(_title);

    if (id) {
      getKajianById(id).then(({ data }) => {
        setProposal({
          id_kajian: data.id_kajian,
          tgl_kajian: data.tgl_kajian,
          id_detiljns: data.id_detiljns,
          unit_kajian: data.unit_kajian,
          perihal: data.perihal,
          file_upload: data.file_upload,
          alasan_tolak: data.alasan_tolak,
          body: data.body,
          nip_pengusul: data.nip_pengusul,
          nip_pjbt_es4: data.nip_pjbt_es4,
          nip_pjbt_es3: data.nip_pjbt_es3,
          wkt_usul: data.wkt_usul,
          wkt_teliti_es4: data.wkt_teliti_es4,
          wkt_teliti_es3: data.wkt_teliti_es3,
          status: data.status,
          wkt_create: data.wkt_create,
          wkt_update: data.wkt_update,
          kd_kantor: data.kd_kantor,
          kd_unit_org: data.kd_unit_org,
        });
      });
    }
  }, [id, suhbeader]);
  const btnRef = useRef();

  const saveProposal = (values) => {
    if (!id) {
      const formData = new FormData();

      formData.append("file", values.file);
      //console.log(formData);

      uploadFile(formData)
        .then(({ data }) =>
          saveKajian(
            values.alasan_tolak,
            values.body,
            data.message,
            values.id_detiljns,
            values.kd_kantor,
            values.kd_unit_org,
            values.nip_pengusul,
            values.nip_pjbt_es3,
            values.nip_pjbt_es4,
            values.perihal,
            values.status,
            values.tgl_kajian,
            values.unit_kajian
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Usulan berhasil disimpan", "success").then(
                () => {
                  history.push("/knowledge/kajian/penelitian");
                }
              );
            } else {
              swal("Gagal", "Usulan gagal disimpan", "error").then(() => {
                history.push("/knowledge/kajian/penelitian/new");
              });
            }
          })
        )
        .catch(() => window.alert("Oops Something went wrong !"));
    } else {
      //console.log(values.status)
      // ketika status tolak dan melakukan edit , otomatis status jadi draft lagi

      if (values.file) {
        //console.log(values.file.name);
        const formData = new FormData();
        formData.append("file", values.file);
        uploadFile(formData)
          .then(({ data }) =>
            updateKajian(
              values.id_kajian,
              values.alasan_tolak,
              values.body,
              data.message,
              values.id_detiljns,
              values.kd_kantor,
              values.kd_unit_org,
              values.nip_pengusul,
              values.nip_pjbt_es3,
              values.nip_pjbt_es4,
              values.perihal,
              values.status,
              values.tgl_kajian,
              values.unit_kaijan
            ).then(({ status }) => {
              if (status === 201 || status === 200) {
                swal("Berhasil", "Usulan berhasil diubah", "success").then(
                  () => {
                    history.push("/knowledge/kajian/penelitian");
                  }
                );
              } else {
                swal("Gagal", "Usulan gagal diubah", "error").then(() => {
                  history.push("/knowledge/kajian/penelitian/new");
                });
              }
            })
          )
          .catch(() => window.alert("Oops Something went wrong !"));
      } else {
        updateKajian(
          values.id_kajian,
          values.alasan_tolak,
          values.body,
          values.file_upload,
          values.id_detiljns,
          values.kd_kantor,
          values.kd_unit_org,
          values.nip_pengusul,
          values.nip_pjbt_es3,
          values.nip_pjbt_es4,
          values.perihal,
          values.status,
          values.tgl_kajian,
          values.unit_kaijan
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal(
              "Berhasil",
              "Usulan berhasil diteruskan ke atasan",
              "success"
            ).then(() => {
              history.push("/knowledge/kajian/penelitian");
            });
          } else {
            swal("Gagal", "Usulan gagal diteruskan ke atasan", "error").then(
              () => {
                history.push("/knowledge/kajian/penelitian/new");
              }
            );
          }
        });
      }

      if (values.status == "Tolak") {
        updateStatusKajian(id, 5, 1, user.nip9).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal(
              "Berhasil",
              "Usulan berhasil diteruskan ke atasan",
              "success"
            ).then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/kajian/penelitian");
            });
          } else {
            swal("Gagal", "Usulan gagal diteruskan ke atasan", "error").then(
              () => {
                history.push("/dashboard");
                history.replace("/knowledge/kajian/penelitian");
              }
            );
          }
        });
      }
    }
  };

  const backToProposalList = () => {
    history.push(`/knowledge/kajian/penelitian`);
  };

  const setDisabled = (val) => {
    setIsDisabled(val);
  };
  return (
    <Card>
      {/* {actionsLoading && <ModalProgressBar />} */}
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <ProposalEditForm
            actionsLoading={actionsLoading}
            proposal={proposal || initValues}
            btnRef={btnRef}
            saveProposal={saveProposal}
            handleChangeProposalRegulasi={handleChangeProposalRegulasi}
            setDisabled={setDisabled}
          />
        </div>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <ProposalEditFooter
          backAction={backToProposalList}
          btnRef={btnRef}
        ></ProposalEditFooter>
      </CardFooter>
    </Card>
  );
}

export default RegulasiEdit;
