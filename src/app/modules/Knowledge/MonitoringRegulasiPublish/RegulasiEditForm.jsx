import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import "./styles.css";
import { DatePickerField, Input, Textarea, Select as Sel } from "../helpers";
import {
  getJenisPeraturan,
  getTopik,
  getRegulasiById,
  getDokumenPer,
} from "../Api";
import RegulasiTerkaitTableDetail from "./RegulasiTerkaitTableDetail";
import DokumenLainTableDetail from "./DokumenLainTableDetail";
// import "./decoupled.css";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import Editor from "ckeditor5-custom-build/build/ckeditor";
import CustomFileInput from "./helpers/CustomFileInput";

const { BACKEND_URL } = window.ENV;

function RegulasiEditForm({
  proposal,
  btnRef,
  saveRegulasiTerkait,
  peraturanTerkait,
  idPeraturan,
  lastPath
}) {

  const [jenisPeraturan, setJenisPeraturan] = useState([]);
  const [topik, setTopik] = useState([]);
  const [propbody, setPropbody] = useState("");
  const [dokLain, setDoklain] = useState([]);
  const { user } = useSelector((state) => state.auth);
  const [show, setShow] = useState(false);

  const ProposalEditSchema = Yup.object().shape({
    no_regulasi: Yup.string()
      .min(2, "Minimum 2 karakter")
      .max(50, "Maximum 50 karakter")
      .required("No Regulasi is required"),
    tgl_regulasi: Yup.mixed()
      .nullable(false)
      .required("Tanggal Surat is required"),

    perihal: Yup.string()
      .min(2, "Minimum 2 karakter")
      .required("Perihal is required"),

    jns_regulasi: Yup.string().required("Jenis Peraturan is required"),
    id_topik: Yup.string().required("Topik is required"),
    // file: Yup.mixed()
    //   .required("A file is required")
    //   .test(
    //     "fileSize",
    //     "File too large",
    //     (value) => value && value.size <= FILE_SIZE
    //   )
    //   .test(
    //     "fileFormat",
    //     "Unsupported Format",
    //     (value) => value && SUPPORTED_FORMATS.includes(value.type)
    //   ),
  });
  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  useEffect(() => {
    getJenisPeraturan().then(({ data }) => {
      // setJenisPeraturan(data);
      data.map((dt) => {
        return dt.status === "AKTIF"
          ? setJenisPeraturan((peraturan) => [...peraturan, dt])
          : null;
      });
    });

    getTopik().then(({ data }) => {
      setTopik(data);
    });
  }, []);

  useEffect(() => {
    getRegulasiById(idPeraturan).then(({ data }) => {
      if (data.body) {
        // setPropbody(JSON.parse(data.body));
        setPropbody(data.body);
        setShow(true);
      }
    });
  }, [idPeraturan]);

  useEffect(() => {
    getDokumenPer(idPeraturan).then(({ data }) => {
      setDoklain(data);
    });
  }, [idPeraturan]);

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={proposal}
        validationSchema={ProposalEditSchema}
        onSubmit={(values) => {
          //console.log(values);
          saveRegulasiTerkait(values);
        }}
      >
        {({ handleSubmit, setFieldValue, values }) => {
          const handleChangeNip = () => {
            setFieldValue("nip_perekam", user.nip9);
          };

          return (
            <>
              <Form className="form form-label-right">
                {/* FIELD NO SURAT */}
                <div className="form-group row">
                  <Field
                    name="no_regulasi"
                    component={Input}
                    placeholder="Nomor Regulasi"
                    label="Nomor Regulasi"
                    // onClick={()=>handleChangeNip()}
                  />
                </div>
                {/* FIELD TANGGAL SURAT */}
                <div className="form-group row">
                  <DatePickerField
                    name="tgl_regulasi"
                    label="Tanggal Regulasi"
                  />
                </div>
                {/* FIELD jenis */}
                <div className="form-group row">
                  <Sel name="jns_regulasi" label="Jenis Regulasi">
                    {jenisPeraturan.map((data, index) => (
                      <option key={index} value={data.id_jnsperaturan}>
                        {data.nm_jnsperaturan}
                      </option>
                    ))}
                  </Sel>
                </div>
                {/* FIELD tentang */}
                <div className="form-group row">
                  <Field
                    name="perihal"
                    component={Textarea}
                    placeholder="Perihal"
                    label="Perihal"
                  />
                </div>
                <div className="form-group row">
                  <Sel name="id_topik" label="topik">
                    {topik.map((data, index) => (
                      <option key={index} value={data.id}>
                        {data.nama}
                      </option>
                    ))}
                  </Sel>
                </div>
                {/* <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    File
                  </label>
                  <div
                    className="col-lg-9 col-xl-6"
                    style={{ marginTop: "10px" }}
                  >
                    <a
                      href={FILE_URL + proposal.file_upload}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      {proposal.file_upload}
                    </a>
                  </div>
                </div> */}
                   {/* FIELD UPLOAD FILE */}
                {values.no_regulasi ? (
                  <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Upload File
                    </label>
                    <div className="col-lg-8 col-xl-6">
                      <Field
                        name="file"
                        component={CustomFileInput}
                        title="Select a file"
                        label="File"
                        // setFieldValue={setFieldValue}
                        // errorMessage={errors["file"] ? errors["file"] : undefined}
                        // touched={touched["file"]}
                        style={{ display: "flex" }}
                        // onBlur={handleBlur}
                      />
                    </div>
                  </div>
                ) : null}
                <RegulasiTerkaitTableDetail
                  peraturanTerkait={peraturanTerkait}
                  idPeraturan={idPeraturan}
                  lastPath={lastPath}
                />
                <DokumenLainTableDetail
                  dokLain={dokLain}
                  idPeraturan={idPeraturan}
                  lastPath={lastPath}
                />
                <div>
                  <br />
                  <br />
                  <h3>Isi Materi</h3>
                </div>
                {show ? (
                  <div className="document-editor">
                    <div className="document-editor__toolbar"></div>
                    <div className="document-editor__editable-container">
                      <CKEditor
                        onReady={(editor) => {
                          window.editor = editor;

                          // Add these two lines to properly position the toolbar
                          const toolbarContainer = document.querySelector(
                            ".document-editor__toolbar"
                          );
                          toolbarContainer.appendChild(
                            editor.ui.view.toolbar.element
                          );
                        }}
                        config={{
                          simpleUpload: {
                            // The URL that the images are uploaded to.
                            uploadUrl: `${BACKEND_URL}/api/uploadckeditor`,
            
                            // Enable the XMLHttpRequest.withCredentials property.
                            withCredentials: false,
            
                            // Headers sent along with the XMLHttpRequest to the upload server.
                            headers: {
                              "Access-Control-Allow-Origin": "*",
                              "Access-Control-Allow-Credentials": "true"
                            }
                          },
                        }}
                        editor={Editor}
                        data={propbody ? propbody : null}
                        onChange={(event, editor) => {
                          // setDraft(editor.getData());
                          setFieldValue('body',editor.getData());
                        }}
                      />
                    </div>
                  </div>
                ) : null}
                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default RegulasiEditForm;
