import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
import RegulasiTable from "./RegulasiTable";

function Regulasi() {
  return (
    <>
      <Card>
        <CardHeader
          title="Monitoring Regulasi Perpajakan"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <RegulasiTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default Regulasi;
