import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import filterFactory, {
  textFilter,
  Comparator,
} from "react-bootstrap-table2-filter";
import React, { useEffect, useState } from "react";
import {
  sortCaret,
  headerSortingClasses,
  PleaseWaitMessage,
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../column-formatters";
import { useHistory } from "react-router-dom";
import { Route } from "react-router-dom";

import { getRegulasiMonitoringKantor, getRegulasiNonTerima, getRegulasiPagination } from "../Api";

import { Pagination } from "../pagination/Pagination";

import RegulasiHistory from "./RegulasiHistory";
import RegulasiHistoryReject from "./RegulasiHistoryReject";
import RegulasiOpen from "./RegulasiOpen";
import { useSelector } from "react-redux";

function RegulasiTable() {
  const history = useHistory();

  const [currentPage, setCurrentPage] = useState(1);
  const [sizePage, setSizePage] = useState(100);
  const [proposal, setProposal] = useState([]);
  const [data, setData] = useState([]);
  const [searchText, setSearchText] = useState("");


  const {user} = useSelector((state) => state.auth);

  let thePath = document.URL;
  const lastPath = thePath.substring(thePath.lastIndexOf("/") + 1);

  const showHistoryPengajuan = (id) =>
    history.push(`/knowledge/regulasi/monitoring/${id}/showhistory`);
  const showHistoryPenolakan = (id) =>
    history.push(`/knowledge/regulasi/monitoring/${id}/showhistoryreject`);

  const openPeraturan = (id) =>
    history.push(`/knowledge/regulasi/monitoring/${id}/open`);

  const prosesPeraturan = (id) =>
    history.push(`/knowledge/regulasi/monitoring/${id}/proses`);

  const detailPeraturan = (id) =>
    history.push(`/knowledge/regulasi/monitoring/${id}/detail`);

  const openAddPerTerkait = (id) =>
    history.push(`/knowledge/regulasi/monitoring/${id}/terkait`);

  const addBody = (id) =>
    history.push(`/knowledge/regulasi/monitoring/${id}/body`);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: (cell, row, rowIndex) => {
        let rowNumber = (currentPage - 1) * sizePage + (rowIndex + 1);
        return <span>{rowNumber}</span>;
      },
    },
    {
      dataField: "no_regulasi",
      text: "No Peraturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "tgl_regulasi",
      text: "Tgl Peraturan",
      sort: true,
      // formatter: columnFormatters.DateFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "perihal",
      text: "perihal",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "seksi_pengusul",
      text: "Seksi Pengusul",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "direktorat_pengusul",
      text: "Direktorat Pengusul",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.StatusColumnFormatter,
      headerSortingClasses,
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.MonitoringRegulasiActionColoumnFormatter,
      formatExtraData: {
        // deleteDialog: deleteDialog,
        // applyProposal: applyProposal,
        openAddPerTerkait: openAddPerTerkait,
        // editProposal: editProposal,
        showHistory: showHistoryPengajuan,
        openPeraturan: openPeraturan,
        detailPeraturan: detailPeraturan,
        showHistoryReject: showHistoryPenolakan,
        // prosesPeraturan: prosesPeraturan,
        // addBody: addBody,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "150px",
      },
    },
  ];

  const { SearchBar } = Search;

  useEffect(() => {
    if(user.unitEs4 === "Seksi Sinkronisasi Peraturan Perpajakan" || user.nip9 === "917330342"){
      getRegulasiNonTerima(currentPage, sizePage).then(({ data }) => {
        setProposal(data.content);
        setData(data);
      });
    }  else {
      getRegulasiMonitoringKantor(currentPage, sizePage, user.kantorLegacyKode).then(({ data }) => {
        setProposal(data.content);
        setData(data);
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if(user.unitEs4 === "Seksi Sinkronisasi Peraturan Perpajakan" || user.nip9 === "917330342"){
      if(searchText){
        getRegulasiNonTerima(currentPage, sizePage, searchText).then(({ data }) => {
          setProposal(data.content);
          setData(data);
        });
      } else {
        getRegulasiNonTerima(currentPage, sizePage).then(({ data }) => {
          setProposal(data.content);
          setData(data);
        });
      }
    }  else {
      if(searchText){
        getRegulasiMonitoringKantor(currentPage, sizePage, user.kantorLegacyKode, searchText).then(({ data }) => {
          setProposal(data.content);
          setData(data);
        });
      } else {
        getRegulasiMonitoringKantor(currentPage, sizePage, user.kantorLegacyKode).then(({ data }) => {
          setProposal(data.content);
          setData(data);
        });
      }
    }
    // getRegulasiNonTerima(currentPage, sizePage).then(({ data }) => {
    //   setProposal(data.content);
    //   setData(data);
    // });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentPage, sizePage, searchText]);

  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "status",
    pageNumber: currentPage,
    pageSize: sizePage,
  };
  const defaultSorted = [{ dataField: "status", order: "asc" }];
  const sizePerPageList = [
    { text: "100", value: 100 },
    { text: "150", value: 150 },
    { text: "200", value: 200 },
  ];
  const pagiOptions = {
    custom: true,
    // totalSize: proposal.length,
    totalSize: data.totalItems,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
    onPageChange: (page, sizePerPage) => {
      setCurrentPage(page);
    },
    onSizePerPageChange: (page, sizePerPage) => {
      setSizePage(page);
      setCurrentPage(sizePerPage);
    },
  };
  const emptyDataMessage = () => {
    // return "No Data to Display";

    return (
      <div className="text-center">
        <div className="spinner-border" role="status">
          <span className="sr-only">Loading...</span>
        </div>
      </div>
    );
  };

  const handleTableChange = (type, { sortField, sortOrder, data }) => {
    setProposal([]);
    let result;
    if (sortOrder === 'desc') {
      result = data.sort((a, b) => {
        if (a[sortField] > b[sortField]) {
          return 1;
        } else if (b[sortField] > a[sortField]) {
          return -1;
        }
        return 0;
      });
      setProposal(result)
    } else {
      result = data.sort((a, b) => {
        if (a[sortField] > b[sortField]) {
          return -1;
        } else if (b[sortField] > a[sortField]) {
          return 1;
        }
        return 0;
      });
      setProposal(result)

    }
  }

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_peraturan"
                  data={proposal}
                  columns={columns}
                  search
                >
                  {(props) => {
                    setSearchText(props.searchProps.searchText);
                  return (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3"></div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        remote
                        onTableChange={handleTableChange}
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      >
                        <PleaseWaitMessage entities={proposal} />
                      </BootstrapTable>
                      <Pagination
                        paginationProps={paginationProps}
                        isLoading={false}
                      />
                    </div>
                  )}}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>

      <Route path="/knowledge/regulasi/monitoring/:id/showhistory">
        {({ history, match }) => (
          <RegulasiHistory
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/regulasi/monitoring");
            }}
            onRef={() => {
              history.push("/knowledge/regulasi/monitoring");
            }}
          />
        )}
      </Route>

      <Route path="/knowledge/regulasi/monitoring/:id/showhistoryreject">
        {({ history, match }) => (
          <RegulasiHistoryReject
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/regulasi/monitoring");
            }}
            onRef={() => {
              history.push("/knowledge/regulasi/monitoring");
            }}
          />
        )}
      </Route>

      <Route path="/knowledge/regulasi/monitoring/:id/open">
        {({ history, match }) => (
          <RegulasiOpen
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/regulasi/monitoring");
            }}
            onRef={() => {
              history.push("/knowledge/regulasi/monitoring");
            }}
          />
        )}
      </Route>
    </>
  );
}

export default RegulasiTable;
