import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
import {
  getRegulasiById,
  getTopikById,
  getJnsPeraturanById,
  updateStatusPeraturan,
} from "../Api";
import RegulasiReject from "./RegulasiReject";
import { DateFormat } from "../helpers/DateFormat";

function RegulasiOpen({ id, show, onHide }) {
  const history = useHistory();
  const [data, setData] = useState([]);
  const [nmTopik, setNmTopik] = useState("");
  const [nmJnsRegulasi, setNmJenisRegulasi] = useState("");
  const [isReject, setIsReject] = useState(false);
  const [isShow, setIsShow] = useState(true);
  let nextStatus = "";

  useEffect(() => {
    if (id) {
      getRegulasiById(id).then(({ data }) => {
        getTopikById(data.id_topik).then(({ data }) => {
          setNmTopik(data.nm_topik);
        });
        getJnsPeraturanById(data.jns_regulasi).then(({ data }) => {
          setNmJenisRegulasi(data.nm_jnsperaturan);
        });

        setData(data);
      });
    }
  }, [id]);

  //const acceptAction = tes => alert(tes);

  const acceptAction = (statusNow) => {
    if (statusNow == "Eselon 4") {
      nextStatus = 3;
    } else if (statusNow == "Eselon 3") {
      nextStatus = 6;
    }

    updateStatusPeraturan(id, nextStatus).then(({ status }) => {
      if (status === 201 || status === 200) {
        swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
          history.push("/dashboard");
          history.replace("/knowledge/regulasi");
        });
      } else {
        swal("Gagal", "Data gagal disimpan", "error").then(() => {
          history.push("/dashboard");
          history.replace("/knowledge/regulasi");
        });
      }
    });
  };

  const handleReject = () => {
    setIsReject(true);
    setIsShow(false);
  };

  const handleCancel = () => {
    setIsReject(false);
    setIsShow(true);
  };

  const reject = (val) => {
    //alert(val.alasan_penolakan)
    updateStatusPeraturan(data.id_peraturan, 5, val.alasan_penolakan).then(
      ({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push("/dashboard");
            history.replace("/knowledge/regulasi");
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/dashboard");
            history.replace("/knowledge/regulasi");
          });
        }
      }
    );
  };

  return (
    <Modal
      size="lg"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Detil Peraturan
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Nomor Peraturan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${data.no_regulasi}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Tgl Peraturan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${DateFormat(data.tgl_peraturan)}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Perihal</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">{`: ${data.perihal}`}</span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Jenis Regulasi</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">{`: ${nmJnsRegulasi}`}</span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Topik</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">{`: ${nmTopik}`}</span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">File</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: `}
              <a href={data.file_upload} target="_blank">
                download
              </a>
            </span>
          </div>
        </div>
        {isReject ? (
          <RegulasiReject handleCancel={handleCancel} reject={reject} />
        ) : (
          ""
        )}
      </Modal.Body>
      {isShow ? (
        <Modal.Footer style={{ borderTop: "none" }}>
          <div className="col-lg-12" style={{ textAlign: "center" }}>
            <button
              type="button"
              // onClick={backAction}
              // onClick={rejectAction}
              onClick={handleReject}
              className="btn btn-danger"
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
              }}
            >
              <i className="flaticon2-cancel icon-nm"></i>
              Tolak
            </button>
            {`  `}
            <button
              type="submit"
              className="btn btn-success ml-2"
              // onClick={saveForm}

              onClick={() => acceptAction(data.status)}
              //onClick={acceptAction}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
              }}
            >
              <i className="fas fa-check"></i>
              Setuju
            </button>
          </div>
        </Modal.Footer>
      ) : (
        ""
      )}
    </Modal>
  );
}

export default RegulasiOpen;
