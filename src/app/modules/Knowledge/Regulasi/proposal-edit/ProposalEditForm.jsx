import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import Select from "react-select";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import "./styles.css";
import {
  DatePickerField,
  Input,
  Textarea,
  Select as Sel,
  Checkbox,
} from "../proposal-edit/helpers";
// import CustomFileInput from "./helpers/CustomFileInput";
import CustomFileInput from "../../../../helpers/form/CustomFileInput"
import { getJenisPeraturan, getTopik } from "../../Api";
import { getJenisPeraturanByAplikasi } from "../../../Evaluation/Api";

function ProposalEditForm({
  proposal,

  btnRef,
  saveProposal,
  loading,
  backAction,
}) {
  const [jenisPeraturan, setJenisPeraturan] = useState([]);

  const [topik, setTopik] = useState([]);
  const { user } = useSelector((state) => state.auth);

  //const validProductValues = products.map(({ _id }) => _id);

  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    no_regulasi: Yup.string()
      .min(2, "Minimum 2 karakter")
      .max(50, "Maximum 50 karakter")
      .required("No Regulasi is required"),
    tgl_regulasi: Yup.mixed()
      .nullable(false)
      .required("Tanggal Surat is required"),

    perihal: Yup.string()
      .min(2, "Minimum 2 karakter")
      .required("Perihal is required"),

    jns_regulasi: Yup.string().required("Jenis Peraturan is required"),
    id_topik: Yup.string().required("Topik is required"),
    // id: Yup.string()
    //   .min(1, "Topik is required")
    //   .required("Topik is required"),

    // id_peraturan: Yup.string()
    //   .min(1, "Peraturan is required")
    //   .required("Peraturan is required"),
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        (value) => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        value => value && SUPPORTED_FORMATS.some(a=> value.type.includes(a))
        // (value) => value && SUPPORTED_FORMATS.includes(value.type)
      ),
  });

  const FILE_SIZE = 50000000;
  const SUPPORTED_FORMATS = [
    "application/pdf",
    "application/x-rar-compressed",
    "application/octet-stream",
    "application/zip",
    "application/octet-stream",
    "application/x-zip-compressed",
    "multipart/x-zip",
    "application/vnd.rar",
    "application/x-rar"
  ];

  useEffect(() => {
    getJenisPeraturanByAplikasi("KM Peraturan").then(({ data }) => {
      // setJenisPeraturan(data);
      data.map(dt=> {
        return dt.status === 'AKTIF' ? setJenisPeraturan(peraturan => [...peraturan, dt]) : null
      })
    });

    getTopik().then(({ data }) => {
      setTopik(data);
    });
  }, []);

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={proposal}
        validationSchema={ProposalEditSchema}
        onSubmit={(values) => {
          // console.log(values);
          saveProposal(values);
        }}
      >
        {({ handleSubmit, setFieldValue, isSubmitting, values }) => {
          const handleChangeNip = () => {
            setFieldValue("nip_perekam", user.nip9);
          };

          return (
            <>
              <Form className="form form-label-right">
                {/* FIELD NO SURAT */}
                <div className="form-group row">
                  <Field
                    name="no_regulasi"
                    component={Input}
                    placeholder="Nomor Peraturan"
                    label="Nomor Peraturan"
                    // onClick={()=>handleChangeNip()}
                  />
                </div>
                {/* FIELD TANGGAL SURAT */}
                <div className="form-group row">
                  <DatePickerField
                    name="tgl_regulasi"
                    label="Tanggal Penetapan dan/atau Tanggal Pengundangan"
                  />
                </div>
                {/* FIELD jenis */}
                <div className="form-group row">
                  <Sel name="jns_regulasi" label="Jenis Peraturan">
                    <option value="">Pilih Jenis Peraturan</option>
                    {jenisPeraturan.map((data, index) => (
                      <option key={index} value={data.id_jnsperaturan}>
                        {data.nm_jnsperaturan}
                      </option>
                    ))}
                  </Sel>
                </div>

                {/* FIELD tentang */}
                <div className="form-group row">
                  <Field
                    name="perihal"
                    component={Textarea}
                    placeholder="Perihal"
                    label="Perihal"
                  />
                </div>

                <div className="form-group row">
                  <Sel name="id_topik" label="Topik">
                    <option value="">Pilih Topik</option>
                    {topik.map((data, index) => (
                      <option key={index} value={data.id}>
                        {data.nama}
                      </option>
                    ))}
                  </Sel>
                </div>

                {/* FIELD UPLOAD FILE */}
                {values.no_regulasi ? (
                  <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Upload File
                    </label>
                    <div className="col-lg-8 col-xl-6">
                      <Field
                        name="file"
                        component={CustomFileInput}
                        title="Select a file"
                        label="File"
                        // setFieldValue={setFieldValue}
                        // errorMessage={errors["file"] ? errors["file"] : undefined}
                        // touched={touched["file"]}
                        style={{ display: "flex" }}
                        // onBlur={handleBlur}
                      />
                    </div>
                  </div>
                ) : null}

                <div className="col-lg-12" style={{ textAlign: "right" }}>
                  <button
                    type="button"
                    onClick={backAction}
                    className="btn btn-light"
                    style={{
                      boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                    }}
                  >
                    <i className="fa fa-arrow-left"></i>
                    Kembali
                  </button>
                  {`  `}

                  {loading ? (
                    <button
                      type="submit"
                      className="btn btn-success spinner spinner-white spinner-left ml-2"
                      onSubmit={() => handleSubmit()}
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                      }}
                      disabled={isSubmitting}
                    >
                      <span>Simpan</span>
                    </button>
                  ) : (
                    <button
                      type="submit"
                      className="btn btn-success ml-2"
                      onSubmit={() => handleSubmit()}
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                      }}
                      disabled={isSubmitting}
                    >
                      <i className="fas fa-save"></i>
                      <span>Simpan</span>
                    </button>
                  )}
                </div>
                {/* <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>  */}
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default ProposalEditForm;
