import React, { useEffect, useState, useRef } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../../_metronic/_partials/controls";

import { useSubheader } from "../../../../../_metronic/layout";
import RegulasiTerkaitForm from "./RegulasiTerkaitForm";
import RegulasiTerkaitFooter from "./RegulasiTerkaitFooter";
import { getRegulasiById, getPeraturanTerkait } from "../../Api";
import swal from "sweetalert";

const initValues = {
  no_regulasi: "",
  tgl_regulasi: "",
  perihal: "",
  file_upload: "",
  status: "",
  jns_regulasi: "",
  id_topik: "",
  alasan_tolak: "",
  nip_pengusul: "",
  nip_pjbt_es4: "",
  nip_pjbt_es3: "",
};

function RegulasiTerkait({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [isDisabled, setIsDisabled] = useState();
  const [proposal, setProposal] = useState();
  const [peraturanTerkait, setPeraturanTerkait] = useState([]);

  useEffect(() => {
    let _title = "Tambah Peraturan Terkait";

    setTitle(_title);
    suhbeader.setTitle(_title);

    getRegulasiById(id).then(({ data }) => {
      setProposal({
        id_peraturan: data.id_peraturan,
        no_regulasi: data.no_regulasi,
        tgl_regulasi: data.tgl_regulasi,
        perihal: data.perihal,
        status: data.status,
        file_upload: data.file_upload,
        jns_regulasi: data.jns_regulasi,
        id_topik: data.id_topik,
        nip_pengusul: data.nip_pengusul,
        nip_pjbt_es4: data.nip_pjbt_es4,
        nip_pjbt_es3: data.nip_pjbt_es3,
        wkt_usul: data.wkt_usul,
        wkt_teliti_es4: data.wkt_teliti_es4,
        wkt_teliti_es3: data.wkt_teliti_es3,
        wkt_create: data.wkt_create,
        wkt_update: data.wkt_update,
      });
    });
  }, [id, suhbeader]);

  useEffect(() => {
    getPeraturanTerkait(id).then(({ data }) => {
      data.map((dt) => {
        //setPeraturanTerkait(peraturanTerkait => [...peraturanTerkait,dt.kmperaturanTerkait]);

        setPeraturanTerkait((peraturanTerkait) => [
          ...peraturanTerkait,
          {
            id_per_terkait: dt.kmperaturanTerkait.id_per_terkait,
            id_peraturan: dt.kmregulasiPerpajakan.id_peraturan,
            no_regulasi: dt.kmregulasiPerpajakan.no_regulasi,
            tgl_regulasi: dt.kmregulasiPerpajakan.tgl_regulasi,
            perihal: dt.kmregulasiPerpajakan.perihal,
            status: dt.kmregulasiPerpajakan.status,
            file_upload: dt.kmregulasiPerpajakan.file_upload,
            jns_regulasi: dt.kmregulasiPerpajakan.jns_regulasi,
            id_topik: dt.kmregulasiPerpajakan.id_topik,
            body: dt.kmregulasiPerpajakan.body,
          },
        ]);
      });
    });
  }, []);

  const btnRef = useRef();

  const saveRegulasiTerkait = (values) => {};

  //console.log(peraturanTerkait)

  const backToProposalList = () => {
    history.push(`/knowledge/regulasi/usulan`);
  };

  const setDisabled = (val) => {
    setIsDisabled(val);
  };
  return (
    <Card>
      {/* {actionsLoading && <ModalProgressBar />} */}
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <RegulasiTerkaitForm
            actionsLoading={actionsLoading}
            proposal={proposal || initValues}
            peraturanTerkait={peraturanTerkait}
            idPeraturan={id}
            btnRef={btnRef}
            saveRegulasiTerkait={saveRegulasiTerkait}
            setDisabled={setDisabled}
          />
        </div>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <RegulasiTerkaitFooter
          backAction={backToProposalList}
          btnRef={btnRef}
        ></RegulasiTerkaitFooter>
      </CardFooter>
    </Card>
  );
}

export default RegulasiTerkait;
