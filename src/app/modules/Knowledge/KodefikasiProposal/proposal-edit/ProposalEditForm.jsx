import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import Select from "react-select";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import "./styles.css";
import {
  DatePickerField,
  Input,
  Textarea,
  Select as Sel,
  Checkbox,
} from "./helpers";
import CustomFileInput from "./helpers/CustomFileInput";
// import { getJenisPajak } from "../../Api";
// import { unitKerja } from "../../../../references/UnitKerja";
import { getKodefikasiNonTerimaByNip, getJenisPajak } from "../../Api";

function ProposalEditForm({
  proposal,
  btnRef,
  saveProposal,
  backAction,
  loading,
  form,
}) {
  const [Kodefikasi, setKodefikasi] = useState([]);
  const [JenisPajak, setJenisPajak] = useState([]);
  // const [kantor, setKantor] = useState([]);
  // const [pajak, setPajak] = useState([]);
  const { user } = useSelector((state) => state.auth);

  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    no_nd: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("No Surat is required"),
    tgl_nd: Yup.mixed()
      .nullable(false)
      .required("Tanggal Surat is required"),
    // perihal: Yup.string()
    //   .min(2, "Minimum 2 symbols")
    //   .max(50, "Maximum 50 symbols")
    //   .required("Perihal is required"),
    // jns_pajak: Yup.array()
    //   .min(1, "Jenis Pajak is required")
    //   .required("Jenis Pajak is required"),
    perihal: Yup.string()
      .min(2, "Minimum 2 symbols")
      .required("Perihal is required"),

    id_jnspajak: Yup.string().required("Jenis Pajak is required"),

    // alamat: Yup.string()
    //   .min(2, "Minimum 2 symbols")
    //   .required("Alamat is required"),
    // no_pic: Yup.number().required("No Telp PIC is required"),
    // nm_pic: Yup.string()
    //   .min(2, "Minimum 2 symbols")
    //   .max(50, "Maximum 50 symbols")
    //   .required("Nama PIC is required"),
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        (value) => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        (value) => value && SUPPORTED_FORMATS.includes(value.type)
      ),
  });

  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  useEffect(() => {
    getKodefikasiNonTerimaByNip().then(({ data }) => {
      setKodefikasi(data);
    });

    getJenisPajak().then(({ data }) => {
      setJenisPajak(data);
    });
  }, []);

  // useEffect(() => {
  //   getJenisPajak().then(({ data }) => {
  //     setJenisPajak(data);
  //     // data.map((data)=> {
  //     //   setPajak(pajak => [...pajak, data.nm_jnspajak])
  //     // })
  //   });

  //   unitKerja.map(data => {
  //     setKantor(kantor => [
  //       ...kantor,
  //       {
  //         label: data.nm_UNIT_KERJA,
  //         value: data.kd_unit_kerja,
  //         alamat: data.alamat
  //       }
  //     ]);
  //   });
  // }, []);

  // console.log(setKodefikasi);
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={proposal}
        validationSchema={ProposalEditSchema}
        onSubmit={(values) => {
          //console.log(values);
          saveProposal(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
          isSubmitting,
          handleBlur,
          handleChange,
          errors,
          touched,
          values,
          isValid,
        }) => {
          const handleChangeUnitKerja = (val) => {
            setFieldValue("unit_kerja", val.label);
            setFieldValue("alamat", val.alamat);
          };

          const handleChangeNip = () => {
            setFieldValue("nip_perekam", user.nip9);
          };

          return (
            <>
              <Form className="form form-label-right">
                {/* FIELD NO SURAT */}
                <div className="form-group row">
                  <Field
                    name="no_nd"
                    component={Input}
                    placeholder="Nomor ND Kodifikasi"
                    label="Nomor ND Kodifikasi"
                    onClick={() => handleChangeNip()}
                  />
                </div>
                {/* FIELD TANGGAL SURAT */}
                <div className="form-group row">
                  <DatePickerField name="tgl_nd" label="Tanggal ND" />
                </div>
                {/* FIELD jenis */}
                <div className="form-group row">
                  <Sel name="id_jnspajak" label="Jenis Pajak">
                    <option value="">Pilih Jenis Pajak</option>
                    {JenisPajak.map((data, index) => (
                      <option key={index} value={data.id_jnspajak}>
                        {data.nm_jnspajak}
                      </option>
                    ))}
                  </Sel>
                </div>

                {/* FIELD tentang */}
                <div className="form-group row">
                  <Field
                    name="perihal"
                    component={Textarea}
                    placeholder="Perihal Kodifikasi"
                    label="Perihal Kodifikasi"
                  />
                </div>

                {/* FIELD UPLOAD FILE */}
                {values.no_nd ? (
                  <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Upload File
                    </label>
                    <div className="col-lg-8 col-xl-6">
                      <Field
                        name="file"
                        component={CustomFileInput}
                        title="Select a file"
                        label="File"
                        // setFieldValue={setFieldValue}
                        // errorMessage={errors["file"] ? errors["file"] : undefined}
                        // touched={touched["file"]}
                        style={{ display: "flex" }}
                        // onBlur={handleBlur}
                      />
                    </div>

                    {/* <button
                      type="button"
                      className="btn btn-primary"
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                        float: "right",
                      }}
                    >
                      upload
                    </button> */}
                  </div>
                ) : null}
                <div className="col-lg-12" style={{ textAlign: "right" }}>
                  <button
                    type="button"
                    onClick={backAction}
                    className="btn btn-light"
                    style={{
                      boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                    }}
                  >
                    <i className="fa fa-arrow-left"></i>
                    Kembali
                  </button>
                  {`  `}

                  {loading ? (
                    <button
                      type="submit"
                      className="btn btn-success spinner spinner-white spinner-left ml-2"
                      onSubmit={() => handleSubmit()}
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                      }}
                      disabled={isSubmitting}
                    >
                      <span>Simpan</span>
                    </button>
                  ) : (
                    <button
                      type="submit"
                      className="btn btn-success ml-2"
                      onSubmit={() => handleSubmit()}
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                      }}
                      disabled={isSubmitting}
                    >
                      <i className="fas fa-save"></i>
                      <span>Simpan</span>
                    </button>
                  )}
                </div>
                {/* <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button> */}
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default ProposalEditForm;
