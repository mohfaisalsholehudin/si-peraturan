import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
import KodefikasiProposalTable from "./KodefikasiProposalTable";

function KodefikasiProposal() {
  return (
    <>
      <Card>
        <CardHeader
          title="Usulan Kodifikasi"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <KodefikasiProposalTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default KodefikasiProposal;
