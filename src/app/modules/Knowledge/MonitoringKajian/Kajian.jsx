import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
import KajianTable from "./KajianTable";

function Kajian() {
  return (
    <>
      <Card>
        <CardHeader
          title="Monitoring Kajian RIA dan CBA"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <KajianTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default Kajian;
