import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import React, { useEffect, useState } from "react";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../column-formatters";
import { useHistory } from "react-router-dom";
import { Route } from "react-router-dom";

import { Pagination } from "../pagination/Pagination";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import swal from "sweetalert";
import SVG from "react-inlinesvg";
import {
  getSurveiSearchEselon3,
  getSurveiNonTerimaEs4,
  getSurveiNonTerimaByNip,
} from "../Api";
import SurveiHistory from "./SurveiHistory";
import { useSelector } from "react-redux";

function SurveiTable() {
  const { role, user } = useSelector((state) => state.auth);
  const konseptor = role.includes("ROLE_PERATURAN_KONSEPTOR");
  const es4 = role.includes("ROLE_PERATURAN_PENELITI_LVL1");
  const es3 = role.includes("ROLE_PERATURAN_PENELITI_LVL2");
  const admin = role.includes("ROLE_ADMIN_PERATURAN");
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "no_nd",
    pageNumber: 1,
    pageSize: 50,
  };
  const history = useHistory();

  const showHistoryPengajuan = (id) =>
    history.push(`/knowledge/survei/penelitian/${id}/history`);

  const prosesSurvei = (id) =>
    history.push(`/knowledge/survei/penelitian/${id}/proses`);

  const detailSurvei = (id) =>
    history.push(`/knowledge/survei/penelitian/${id}/detail`);

  const [survei, setSurvei] = useState([]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "kmSurvei.no_nd",
      text: "No ND Survei",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "kmSurvei.tgl_nd",
      text: "Tgl ND Survei",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "kmSurvei.perihal",
      text: "Perihal Survei",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "jenisSurvei.nama",
      text: "Jenis Survei",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "kmSurvei.status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.SurveiStatusColumnFormatter,
      headerSortingClasses,
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
        user.jabatan === "Pelaksana"
          ? columnFormatters.SurveiActionColumnFormatterPelaksana
          : user.jabatan === "Kepala Seksi"
          ? columnFormatters.SurveiActionColumnFormatterEselon4
          : columnFormatters.SurveiActionColumnFormatterEselon3,
      formatExtraData: {
        showHistory: showHistoryPengajuan,
        detailSurvei: detailSurvei,
        prosesSurvei: prosesSurvei,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];

  const { SearchBar } = Search;

  useEffect(() => {
    if (es4) {
      getSurveiNonTerimaEs4(user.kantorLegacyKode, user.unitEs4LegacyKode).then(
        ({ data }) => {
          data.map((data) => {
            return data.kmSurvei.status === "Eselon 4"
              ? setSurvei((survei) => [...survei, data])
              : data.kmSurvei.status === "Eselon 3"
              ? setSurvei((survei) => [...survei, data])
              : data.kmSurvei.status === "Tolak"
              ? setSurvei((survei) => [...survei, data])
              : null;
          });
        }
      );
    }
  }, [es4, user]);

  useEffect(() => {
    if (es3) {
      getSurveiSearchEselon3(
        user.kantorLegacyKode,
        user.unitEs4LegacyKode,
        "Eselon%203"
      ).then(({ data }) => {
        data.map((dt) => {
          setSurvei((survei) => [...survei, dt]);
        });
      });
    }
  }, [es3, user]);

  const defaultSorted = [{ dataField: "no_nd", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: survei.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="kmSurvei.id_lapsurvey"
                  data={survei}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
      <Route path="/knowledge/survei/penelitian/:id/history">
        {({ history, match }) => (
          <SurveiHistory
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/survei/penelitian");
            }}
            onRef={() => {
              history.push("/knowledge/survei/penelitian");
            }}
          />
        )}
      </Route>
    </>
  );
}

export default SurveiTable;
