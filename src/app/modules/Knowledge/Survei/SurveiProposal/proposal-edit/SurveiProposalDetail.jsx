import React, { useEffect, useState, useRef } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../../../_metronic/_partials/controls";

import { useSubheader } from "../../../../../../_metronic/layout";
import SurveiDetailForm from "./SurveiDetailForm";
import SurveiDetailFooter from "./SurveiDetailFooter";
import { getSurveiById } from "../../../Api";
import { useSelector } from "react-redux";

const initValues = {
  alasan_tolak: "",
  body: "",
  file_upload: "",
  id_detiljns: "",
  kd_kantor: "",
  kd_unit_org: "",
  nip_pengusul: "",
  nip_pjbt_es3: "",
  nip_pjbt_es4: "",
  no_nd: "",
  perihal: "",
  status: "",
  tgl_nd: ""
};

function SurveiProposalDetail({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  const { role, user } = useSelector((state) => state.auth);
  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [isDisabled, setIsDisabled] = useState();
  const [proposal, setProposal] = useState();
  const [SurveiDetail, setSurveiDetail] = useState([]);
  let thePath = document.URL;
  let nextStatus = "";
  const lastPath = thePath.substring(thePath.lastIndexOf("/") + 1);
  const [statNow, setstatNow] = useState("");

  useEffect(() => {
    if (lastPath == "proses") {
      let _title = "Proses Survei";
      setTitle(_title);
      suhbeader.setTitle(_title);
    } else {
      let _title = "Detail Survei";
      setTitle(_title);
      suhbeader.setTitle(_title);
    }

    getSurveiById(id).then(({ data }) => {
      setProposal({
        id_lapsurvey: data.id_lapsurvey,
        no_nd: data.no_nd,
        tgl_nd: data.tgl_nd,
        id_detiljns: data.id_detiljns,
        perihal: data.perihal,
        file_upload: data.file_upload,
        alasan_tolak: data.alasan_tolak,
        body: data.body,
        nip_pengusul: data.nip_pengusul,
        nip_pjbt_es4: data.nip_pjbt_es4,
        nip_pjbt_es3: data.nip_pjbt_es3,
        wkt_usul: data.wkt_usul,
        wkt_teliti_es4: data.wkt_teliti_es4,
        wkt_teliti_es3: data.wkt_teliti_es3,
        status: data.status,
        wkt_create: data.wkt_create,
        wkt_update: data.wkt_update,
        kd_kantor: data.kd_kantor,
        kd_unit_org: data.kd_unit_org
      });
    });
  }, [id, suhbeader]);

  const btnRef = useRef();

  const saveSurveiDetail = (values) => {};

  //console.log(peraturanTerkait)

  const backToProposalList = () => {
    history.push(`/knowledge/survei/usulan`);
  };

  const setDisabled = (val) => {
    setIsDisabled(val);
  };
  return (
    <Card>
      {/* {actionsLoading && <ModalProgressBar />} */}
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <SurveiDetailForm
            actionsLoading={actionsLoading}
            proposal={proposal || initValues}
            SurveiDetail={SurveiDetail}
            idSurvei={id}
            btnRef={btnRef}
            saveSurveiDetail={saveSurveiDetail}
            setDisabled={setDisabled}
          />
        </div>
        <br></br>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <SurveiDetailFooter
          backAction={backToProposalList}
          btnRef={btnRef}
        ></SurveiDetailFooter>
      </CardFooter>
    </Card>
  );
}

export default SurveiProposalDetail;
