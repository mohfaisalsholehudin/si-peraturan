import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import React, { useEffect, useState } from "react";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../column-formatters";
import { useHistory } from "react-router-dom";
import { Route } from "react-router-dom";
import { Pagination } from "../pagination/Pagination";
import { getSurveiSearchEKantor } from "../Api";
import SurveiHistory from "./SurveiHistory";
import { useSelector } from "react-redux";

function DaftarSurveiTable() {
  const { role, user } = useSelector((state) => state.auth);
  const initialFilter = {
    sortOrder: "desc", // asc||desc
    sortField: "wkt_update",
    pageNumber: 1,
    pageSize: 50,
  };
  const history = useHistory();

  const showHistoryPengajuan = (id) =>
    history.push(`/knowledge/survei/monitoring/${id}/history`);

  const detailSurvei = (id) =>
    history.push(`/knowledge/survei/monitoring/${id}/detail`);

  const [survei, setSurvei] = useState([]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "kmSurvei.no_nd",
      text: "No ND Survei",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "kmSurvei.tgl_nd",
      text: "Tgl ND Survei",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "kmSurvei.perihal",
      text: "Perihal Survei",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "jenisSurvei.nama",
      text: "Jenis Survei",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "kmSurvei.status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.SurveiStatusColumnFormatter,
      headerSortingClasses,
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.DaftarSurveiActionColumnFormatter,
      formatExtraData: {
        showHistory: showHistoryPengajuan,
        detailSurvei: detailSurvei,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];

  const { SearchBar } = Search;

  useEffect(() => {
    getSurveiSearchEKantor(user.kantorLegacyKode).then(({ data }) => {
      data.map((data) => {
        return data.kmSurvei.status === "Eselon 4"
          ? setSurvei((survei) => [...survei, data])
          : data.kmSurvei.status === "Eselon 3"
          ? setSurvei((survei) => [...survei, data])
          : data.kmSurvei.status === "Terima"
          ? setSurvei((survei) => [...survei, data])
          : null;
      });
    });
  }, []);

  const defaultSorted = [{ dataField: "wkt_update", order: "desc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: survei.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="kmSurvei.id_lapsurvey"
                  data={survei}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3"></div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
      <Route path="/knowledge/survei/monitoring/:id/history">
        {({ history, match }) => (
          <SurveiHistory
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/survei/monitoring");
            }}
            onRef={() => {
              history.push("/knowledge/survei/monitoring");
            }}
          />
        )}
      </Route>
    </>
  );
}

export default DaftarSurveiTable;
