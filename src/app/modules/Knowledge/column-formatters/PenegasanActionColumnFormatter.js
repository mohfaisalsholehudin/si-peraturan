// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";

export function PenegasanActionColumnFormatterPelaksana(
  cellContent,
  row,
  rowIndex,
  { detailPenegasan, editPenegasan, deleteDialog, showHistory, applyPenegasan }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Eselon 4":
        return (
          <>
            {/* Show history proses Pengajuan */}
            <a
              title="Status Surat Penegasan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")}
                /> */}
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            {/* Detail  */}
            <a
              title="Detail Surat Penegasan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailPenegasan(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>

            {/* <a
              title="Detail Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openPeraturan(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Settings-2.svg")}
                />
                <i className="fas fa-bars text-primary"></i>
              </span>
            </a> */}
          </>
        );
        break;

      case "Eselon 3":
        return (
          <>
            <a
              title="Status Surat Penegasan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")}
                /> */}
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>
          </>
        );
        break;

      case "Draft":
        return (
          <>
            <a
              title="Edit Surat Penegasan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => editPenegasan(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                {/* <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                /> */}
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>

            <a
              title="Hapus Surat Penegasan"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() => deleteDialog(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
            <> </>

            <a
              title="Ajukan Surat Penegasan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => applyPenegasan(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")}
                /> */}
                <i className="fas fa-check text-success"></i>
              </span>
            </a>

            {/* <a
              title="Isi Materi"
              className="btn btn-icon btn-light btn-hover-warning btn-sm mx-3"
            //onClick={() => openAddPerTerkait(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-warning">
                <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Files/File.svg"
                  )}
                />
              </span>
            </a>


            <a
              title="Tambah Peraturan Terkait"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openAddPerTerkait(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Navigation/Plus.svg"
                  )}
                />
              </span>
            </a> */}

            <> </>
          </>
        );
        break;

      case "Tolak":
        return (
          <>
            {/* <a
              title="Show Reject"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showReject(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                />
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
            <> </> */}
            <a
              title="Edit Surat Penegasan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => editPenegasan(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                {/* <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                /> */}
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>
          </>
        );
        break;

      case "Terima":
        return (
          <>
            <a
              title="Status Surat Penegasan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")}
                /> */}
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>
          </>
        );
        break;
      default:
        break;
    }
  };

  return <>{checkStatus(row.status)}</>;
}

export function PenegasanActionColumnFormatterEselon4(
  cellContent,
  row,
  rowIndex,
  { prosesPenegasan, detailPenegasan, showHistory, rejectPenegasan }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Eselon 4":
        return (
          <>
            {/* Show history proses Pengajuan */}
            <a
              title="Status Surat Penegasan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")}
                /> */}
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            {/* Detail  */}
            {/* <a
              title="Detail Surat Penegasan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailPenegasan(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark"> */}
            {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
            {/* <i className="fas fa-eye text-dark"></i>
              </span>
            </a> */}

            {/* Proses Terima Tolak */}
            <a
              title="Proses Surat Penegasan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              // onClick={() => openPeraturan(row.id_suratpenegasan)}
              onClick={() => prosesPenegasan(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Settings-2.svg")}
                /> */}
                <i className="fas fa-file-signature text-primary"></i>
              </span>
            </a>

            {/* <a
              title="Detail Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openPeraturan(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Settings-2.svg")}
                />
                <i className="fas fa-bars text-primary"></i>
              </span>
            </a> */}
          </>
        );
        break;

      case "Eselon 3":
        return (
          <>
            <a
              title="Status Surat Penegasan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")}
                /> */}
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            {/* Detail  */}
            <a
              title="Detail Surat Penegasan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailPenegasan(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
        break;

      case "Tolak":
        return (
          <>
            {/* <a
              title="Show Reject"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => rejectPenegasan(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Code/Info-circle.svg")}
                />
              </span>
            </a> */}
            {/* <> </> */}
            <a
              title="Status Surat Penegasan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")}
                /> */}
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            {/* Detail  */}
            <a
              title="Detail Surat Penegasan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailPenegasan(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
            <> </>
          </>
        );
        break;

      case "Terima":
        return (
          <>
            <a
              title="Status Surat Penegasan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")}
                /> */}
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>
          </>
        );
        break;
      default:
        break;
    }
  };

  return <>{checkStatus(row.status)}</>;
}

export function PenegasanActionColumnFormatterEselon3(
  cellContent,
  row,
  rowIndex,
  {
    prosesPenegasan,
    detailPenegasan,
    editPenegasan,
    deleteDialog,
    showHistory,
    applyPenegasan,
  }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Eselon 4":
        return (
          <>
            {/* Show history proses Pengajuan */}
            <a
              title="Status Surat Penegasan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")}
                /> */}
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            {/* Detail  */}
            <a
              title="Detail Surat Penegasan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailPenegasan(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>

            {/* <a
              title="Detail Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openPeraturan(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Settings-2.svg")}
                />
                <i className="fas fa-bars text-primary"></i>
              </span>
            </a> */}
          </>
        );
        break;

      case "Eselon 3":
        return (
          <>
            <a
              title="Status Surat Penegasan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")}
                /> */}
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            {/* Proses Terima Tolak */}
            <a
              title="Proses Surat Penegasan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              // onClick={() => openPeraturan(row.id_suratpenegasan)}
              onClick={() => prosesPenegasan(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Settings-2.svg")}
                /> */}
                <i className="fas fa-file-signature text-primary"></i>
              </span>
            </a>
          </>
        );
        break;

      case "Draft":
        return (
          <>
            <a
              title="Edit Surat Penegasan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => editPenegasan(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                {/* <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                /> */}
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>

            <a
              title="Hapus Surat Penegasan"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() => deleteDialog(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
            <> </>

            <a
              title="Ajukan Surat Penegasan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => applyPenegasan(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")}
                /> */}
                <i className="fas fa-check text-success"></i>
              </span>
            </a>

            {/* <a
              title="Isi Materi"
              className="btn btn-icon btn-light btn-hover-warning btn-sm mx-3"
            //onClick={() => openAddPerTerkait(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-warning">
                <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Files/File.svg"
                  )}
                />
              </span>
            </a>

            <a
              title="Tambah Peraturan Terkait"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openAddPerTerkait(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Navigation/Plus.svg"
                  )}
                />
              </span>
            </a> */}
            <> </>
          </>
        );
        break;

      case "Tolak":
        return (
          <>
            {/* <a
              title="Show Reject"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showReject(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                />
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
            <> </> */}
            <a
              title="Edit Surat Penegasan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => editPenegasan(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                {/* <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                /> */}
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>
          </>
        );
        break;

      case "Terima":
        return (
          <>
            <a
              title="Status Surat Penegasan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_suratpenegasan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")}
                /> */}
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>
          </>
        );
        break;
      default:
        break;
    }
  };

  return <>{checkStatus(row.status)}</>;
}
