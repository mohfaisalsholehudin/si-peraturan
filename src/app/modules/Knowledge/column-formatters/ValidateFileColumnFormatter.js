// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
import React from "react";

export function ValidateFileColumnFormatter(cellContent, row) {
  // const CustomerStatusCssClasses = ["warning", "success", "danger", ""];
  // const CustomerStatusTitles = ["Kasi", "Draft", "Tolak", ""];
  // let status = "";

  // switch (row.status) {
  //   case "Kasi":
  //     status = 0;
  //     break;

  //   case "Tolak":
  //     status = 1;
  //     break;

  //   case "Draft":
  //     status = 2;
  //     break;

  //   default:
  //     status = 3;
  // }
  // const getLabelCssClasses = () => {
  //   return `label label-lg label-light-${CustomerStatusCssClasses[status]} label-inline`;
  // };
  return (
    // <span className={getLabelCssClasses()}>{CustomerStatusTitles[status]}</span>
    <a href={row.usulan.file_upload} target="_blank" rel="noopener noreferrer">Lihat</a>
  );
}
