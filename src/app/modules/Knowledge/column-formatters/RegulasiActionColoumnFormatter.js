// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";

export function RegulasiActionColoumnFormatter(
  cellContent,
  row,
  rowIndex,
  {
    addBody,
    prosesPeraturan,
    detailPeraturan,
    editProposal,
    deleteDialog,
    showHistory,
    showHistoryReject,
    applyProposal,
    openAddPerTerkait,
    openPeraturan,
  }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Eselon 4":
        return (
          <>
            {/* Show history proses Pengajuan */}
            <a
              title="Status Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            {/* Detail  */}

            <a
              title="Detail Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailPeraturan(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>

            {/* Proses Terima Tolak */}
            {/* <a
              title="Proses Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              // onClick={() => openPeraturan(row.id_peraturan)}
              onClick={() => prosesPeraturan(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
              <i className="fas fa-file-signature text-primary"></i>
              </span>
            </a> */}

            {/* <a
              title="Detail Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openPeraturan(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
              <i className="fas fa-file-signature text-primary"></i>
              </span>
            </a> */}
          </>
        );
        break;

      case "Eselon 3":
        return (
          <>
            <a
              title="Status Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            <a
              title="Detail Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailPeraturan(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
            {/* Proses Terima Tolak */}
            {/* <a
              title="Proses Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              // onClick={() => openPeraturan(row.id_peraturan)}
              onClick={() => prosesPeraturan(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
              <i className="fas fa-file-signature text-primary"></i>
              </span>
            </a> */}
          </>
        );
        break;

      case "Draft":
        return (
          <>
            <a
              title="Edit Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => editProposal(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>

            <a
              title="Hapus Peraturan"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() => deleteDialog(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
            <> </>

            <a
              title="Ajukan Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => applyProposal(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                <i className="fas fa-check text-success"></i>
              </span>
            </a>

            <a
              title="Isi Materi"
              className="btn btn-icon btn-light btn-hover-warning btn-sm mx-3"
              onClick={() => addBody(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-warning">
                <i className="fas fa-file-alt text-primary"></i>
              </span>
            </a>

            <a
              title="Tambah Peraturan Terkait"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openAddPerTerkait(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-plus text-primary"></i>
              </span>
            </a>

            <> </>
          </>
        );
        break;

      case "Tolak":
        return (
          <>
            <a
              title="Alasan Penolakan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistoryReject(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-info text-info"></i>
              </span>
            </a>
            <> </>

            <a
              title="Hapus Peraturan"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() => deleteDialog(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
            <> </>
            <a
              title="Edit Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => editProposal(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>
          </>
        );
        break;

      case "Terima":
        return (
          <>
            <a
              title="Status Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            <a
              title="Detail Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailPeraturan(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
        break;
      default:
        break;
    }
  };

  return <>{checkStatus(row.status)}</>;
}

export function RegulasiActionColoumnFormatterEs4(
  cellContent,
  row,
  rowIndex,
  {
    addBody,
    prosesPeraturan,
    detailPeraturan,
    editProposal,
    deleteDialog,
    showHistory,
    showHistoryTolak,
    applyProposal,
    openAddPerTerkait,
    openPeraturan,
  }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Eselon 4":
        return (
          <>
            {/* Show history proses Pengajuan */}
            <a
              title="Status Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            {/* Detail  */}

            {/* <a
              title="Detail Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailPeraturan(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a> */}

            {/* Proses Terima Tolak */}
            <a
              title="Proses Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              // onClick={() => openPeraturan(row.id_peraturan)}
              onClick={() => prosesPeraturan(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-file-signature text-primary"></i>
              </span>
            </a>

            {/* <a
              title="Detail Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openPeraturan(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
              <i className="fas fa-file-signature text-primary"></i>
              </span>
            </a> */}
          </>
        );
        break;

      case "Eselon 3":
        return (
          <>
            <a
              title="Status Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            <a
              title="Detail Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailPeraturan(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
            {/* Proses Terima Tolak */}
            {/* <a
              title="Proses Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              // onClick={() => openPeraturan(row.id_peraturan)}
              onClick={() => prosesPeraturan(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
              <i className="fas fa-file-signature text-primary"></i>
              </span>
            </a> */}
          </>
        );
        break;

      case "Draft":
        return (
          <>
            <a
              title="Edit Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => editProposal(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>

            <a
              title="Hapus Peraturan"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() => deleteDialog(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
            <> </>

            <a
              title="Ajukan Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => applyProposal(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                <i className="fas fa-check text-success"></i>
              </span>
            </a>

            <a
              title="Isi Materi"
              className="btn btn-icon btn-light btn-hover-warning btn-sm mx-3"
              onClick={() => addBody(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-warning">
                <i className="fas fa-file-alt text-primary"></i>
              </span>
            </a>

            <a
              title="Tambah Peraturan Terkait"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openAddPerTerkait(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-plus text-primary"></i>
              </span>
            </a>

            <> </>
          </>
        );
        break;

      case "Tolak":
        return (
          <>
            {/* <a
              title="Show Reject"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showReject(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
            <> </> */}
            <a
              title="Alasan Penolakan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistoryTolak(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-info text-info"></i>
              </span>
            </a>
            <> </>
          </>
        );
        break;

      case "Terima":
        return (
          <>
            <a
              title="Status Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            <a
              title="Detail Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailPeraturan(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
        break;
      default:
        break;
    }
  };

  return <>{checkStatus(row.status)}</>;
}

export function RegulasiActionColoumnFormatterEs3(
  cellContent,
  row,
  rowIndex,
  {
    addBody,
    prosesPeraturan,
    detailPeraturan,
    editProposal,
    deleteDialog,
    showHistory,
    applyProposal,
    openAddPerTerkait,
    openPeraturan,
  }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Eselon 4":
        return (
          <>
            {/* Show history proses Pengajuan */}
            <a
              title="Status Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            {/* Detail  */}

            <a
              title="Detail Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailPeraturan(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>

            {/* Proses Terima Tolak */}
            {/* <a
              title="Proses Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              // onClick={() => openPeraturan(row.id_peraturan)}
              onClick={() => prosesPeraturan(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
              <i className="fas fa-file-signature text-primary"></i>
              </span>
            </a> */}

            {/* <a
              title="Detail Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openPeraturan(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
              <i className="fas fa-file-signature text-primary"></i>
              </span>
            </a> */}
          </>
        );
        break;

      case "Eselon 3":
        return (
          <>
            <a
              title="Status Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            {/* <a
              title="Detail Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailPeraturan(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a> */}
            {/* Proses Terima Tolak */}
            <a
              title="Proses Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              // onClick={() => openPeraturan(row.id_peraturan)}
              onClick={() => prosesPeraturan(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-file-signature text-primary"></i>
              </span>
            </a>
          </>
        );
        break;

      case "Draft":
        return (
          <>
            <a
              title="Edit Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => editProposal(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>

            <a
              title="Hapus Peraturan"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() => deleteDialog(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
            <> </>

            <a
              title="Ajukan Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => applyProposal(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                <i className="fas fa-check text-success"></i>
              </span>
            </a>

            <a
              title="Isi Materi"
              className="btn btn-icon btn-light btn-hover-warning btn-sm mx-3"
              onClick={() => addBody(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-warning">
                <i className="fas fa-file-alt text-primary"></i>
              </span>
            </a>

            <a
              title="Tambah Peraturan Terkait"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openAddPerTerkait(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-plus text-primary"></i>
              </span>
            </a>

            <> </>
          </>
        );
        break;

      case "Tolak":
        return (
          <>
            {/* <a
              title="Show Reject"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showReject(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
            <> </> */}
            <a
              title="Edit Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => editProposal(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>
          </>
        );
        break;

      case "Terima":
        return (
          <>
            <a
              title="Status Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            <a
              title="Detail Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailPeraturan(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
        break;
      default:
        break;
    }
  };

  return <>{checkStatus(row.status)}</>;
}

export function MonitoringRegulasiActionColoumnFormatter(
  cellContent,
  row,
  rowIndex,
  {
    addBody,
    prosesPeraturan,
    detailPeraturan,
    editProposal,
    deleteDialog,
    showHistory,
    showHistoryReject,
    applyProposal,
    openAddPerTerkait,
    openPeraturan,
  }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Eselon 4":
        return (
          <>
            {/* Show history proses Pengajuan */}
            <a
              title="Status Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>
            <a
              title="Detail Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailPeraturan(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      case "Draft":
        return (
          <>
            {/* Show history proses Pengajuan */}
            <a
              title="Status Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>
            <a
              title="Detail Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailPeraturan(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      case "Terima":
        return (
          <>
            {/* Show history proses Pengajuan */}
            <a
              title="Status Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>
            <a
              title="Detail Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailPeraturan(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );

      case "Eselon 3":
        return (
          <>
            <a
              title="Status Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>
            <a
              title="Detail Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailPeraturan(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );

      case "Tolak":
        return (
          <>
            <a
              title="Alasan Penolakan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistoryReject(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-info text-info"></i>
              </span>
            </a>
            <a
              title="Detail Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailPeraturan(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
            <> </>
          </>
        );

      default:
        break;
    }
  };

  return <>{checkStatus(row.status)}</>;
}

export function MonitoringRegulasiPublishActionColoumnFormatter(
  cellContent,
  row,
  rowIndex,
  {
    addBody,
    prosesPeraturan,
    detailPeraturan,
    editProposal,
    deleteDialog,
    showHistory,
    showHistoryReject,
    applyProposal,
    openAddPerTerkait,
    openPeraturan,
    editPeraturan
  }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Terima":
        return (
          <>
            {/* Show history proses Pengajuan */}
            <a
              title="Status Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-warning">
                <i className="fas fa-stopwatch text-warning"></i>
              </span>
            </a>
            <a
              title="Detail Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailPeraturan(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
            <a
              title="Edit Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => editPeraturan(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
          </>
        );

      default:
        break;
    }
  };

  return <>{checkStatus(row.status)}</>;
}
