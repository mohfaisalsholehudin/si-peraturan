// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";

export function AudiensiActionColumnFormatterPelaksana(
  cellContent,
  row,
  rowIndex,
  { detailAudiensi,editAudiensi, deleteDialog, showHistory, applyAudiensi }
) {

  const checkStatus = (status) => {
    switch (status) {
      case "Eselon 4":
        return (
          <>
            
            {/* Show history proses Pengajuan */}
            <a
              title="Status Audiensi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.kmAudiensi.id_audiensi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")}
                /> */}
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            {/* Detail  */}
            
            <a
              title="Detail Audiensi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailAudiensi(row.kmAudiensi.id_audiensi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
        break;

      case "Eselon 3":
        return (
          <>
            <a
              title="Status Audiensi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.kmAudiensi.id_audiensi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")}
                /> */}
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            {/* Detail  */}
            <a
              title="Detail Audiensi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailAudiensi(row.kmAudiensi.id_audiensi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
        break;

      case "Draft":
        return (
          <>
            <a
              title="Edit Audiensi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => editAudiensi(row.kmAudiensi.id_audiensi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                {/* <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                /> */}
              <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>

            <a
              title="Hapus Audiensi"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() => deleteDialog(row.kmAudiensi.id_audiensi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
              <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
            <> </>

            <a
              title="Ajukan Audiensi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => applyAudiensi(row.kmAudiensi.id_audiensi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")}
                /> */}
                <i className="fas fa-check text-success"></i>
              </span>
            </a>
            <> </>
          </>
        );
        break;

      case "Tolak":
        return (
          <>
            <a
              title="Edit Audiensi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => editAudiensi(row.kmAudiensi.id_audiensi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                {/* <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                /> */}
              <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>
          </>
        );
        break;

      case "Terima":
        return (
          <>
            <a
              title="Status Audiensi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.kmAudiensi.id_audiensi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")}
                /> */}
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>
          </>
        );
        break;
      default:
        break;
    }
  };

  return <>{checkStatus(row.kmAudiensi.status)}</>;
}

export function AudiensiActionColumnFormatterEselon4(
  cellContent,
  row,
  rowIndex,
  { prosesAudiensi,detailAudiensi,showHistory }
) {

  const checkStatus = (status) => {
    switch (status) {
      case "Eselon 4":
        return (
          <>
            
            {/* Show history proses Pengajuan */}
            <a
              title="Status Audiensi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.kmAudiensi.id_audiensi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")}
                /> */}
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            {/* Detail  */}
            {/* <a
              title="Detail Audiensi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailAudiensi(row.kmAudiensi.id_audiensi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark"> */}
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                {/* <i className="fas fa-eye text-dark"></i>
              </span>
            </a> */}

            {/* Proses Terima Tolak */}
            <a
              title="Proses Audiensi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => prosesAudiensi(row.kmAudiensi.id_audiensi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Settings-2.svg")}
                /> */}
                <i className="fas fa-file-signature text-primary"></i>
              </span>
            </a>
          </>
        );
        break;

      case "Eselon 3":
        return (
          <>
            {/* Show history proses Pengajuan */}
            <a
              title="Status Audiensi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.kmAudiensi.id_audiensi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")}
                /> */}
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            {/* Detail  */}
            <a
              title="Detail Audiensi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailAudiensi(row.kmAudiensi.id_audiensi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
        break;

      case "Tolak":
        return (
          <>
            {/* Show history proses Pengajuan */}
            <a
              title="Status Audiensi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.kmAudiensi.id_audiensi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")}
                /> */}
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            {/* Detail  */}
            <a
              title="Detail Audiensi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailAudiensi(row.kmAudiensi.id_audiensi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
            <> </>
          </>
        );
        break;

      case "Terima":
        return (
          <>
            <a
              title="Status Audiensi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.kmAudiensi.id_audiensi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")}
                /> */}
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

          </>
        );
        break;
      default:
        break;
    }
  };

  return <>{checkStatus(row.kmAudiensi.status)}</>;
}

export function AudiensiActionColumnFormatterEselon3(
  cellContent,
  row,
  rowIndex,
  { prosesAudiensi,detailAudiensi, showHistory }
) {

  const checkStatus = (status) => {
    switch (status) {
      case "Eselon 4":
        return (
          <>
            {/* Show history proses Pengajuan */}
            <a
              title="Status Audiensi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.kmAudiensi.id_audiensi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")}
                /> */}
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            {/* Detail  */}
            <a
              title="Detail Audiensi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailAudiensi(row.kmAudiensi.id_audiensi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
        break;

      case "Eselon 3":
        return (
          <>
            <a
              title="Status Audiensi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.kmAudiensi.id_audiensi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")}
                /> */}
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            {/* Proses Terima Tolak */}
            <a
              title="Proses Audiensi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => prosesAudiensi(row.kmAudiensi.id_audiensi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Settings-2.svg")}
                /> */}
                <i className="fas fa-file-signature text-primary"></i>
              </span>
            </a>
          </>
        );
        break;

        case "Tolak":
          return (
            <>
              {/* Show history proses Pengajuan */}
              <a
                title="Status Audiensi"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() => showHistory(row.kmAudiensi.id_audiensi)}
              >
                <span className="svg-icon svg-icon-md svg-icon-dark">
                  {/* <SVG
                    src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")}
                  /> */}
                  <i className="fas fa-stopwatch text-dark"></i>
                </span>
              </a>
  
              {/* Detail  */}
              <a
                title="Detail Audiensi"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() => detailAudiensi(row.kmAudiensi.id_audiensi)}
              >
                <span className="svg-icon svg-icon-md svg-icon-dark">
                  {/* <SVG
                    src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                  /> */}
                  <i className="fas fa-eye text-dark"></i>
                </span>
              </a>
              <> </>
            </>
          );
          break;

      case "Terima":
        return (
          <>
            <a
              title="Status Audiensi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.kmAudiensi.id_audiensi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")}
                /> */}
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

          </>
        );
        break;
      default:
        break;
    }
  };

  return <>{checkStatus(row.kmAudiensi.status)}</>;
}
