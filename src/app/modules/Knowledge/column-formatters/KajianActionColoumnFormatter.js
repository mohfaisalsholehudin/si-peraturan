// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";

export function KajianActionColoumnFormatter(
  cellContent,
  row,
  rowIndex,
  {
    addBody,
    prosesKajian,
    detailKajian,
    editProposal,
    deleteDialog,
    showHistory,
    showHistoryTolak,
    applyProposal,
    openAddPerTerkait,
  }
) {
  //console.log(row)

  const checkStatus = (status) => {
    switch (status) {
      case "Eselon 4":
        return (
          <>
            {/* Show history proses Pengajuan */}
            <a
              title="Status Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            {/* Detail  */}

            <a
              title="Detail Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailKajian(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>

            {/* Proses Terima Tolak */}
            {/* <a
              title="Proses Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              // onClick={() => openKajian(row.hasilKajian.id_kajian)}
              onClick={() => prosesKajian(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
               <i className="fas fa-file-signature text-primary"></i>
              </span>
            </a>
 */}

            {/* <a
              title="Detail Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openKajian(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
               <i className="fas fa-file-signature text-primary"></i>
              </span>
            </a> */}
          </>
        );
        break;

      case "Eselon 3":
        return (
          <>
            <a
              title="Status Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>
            <a
              title="Detail Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailKajian(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
            {/* Proses Terima Tolak */}
            {/* <a
              title="Proses Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              // onClick={() => openKajian(row.hasilKajian.id_kajian)}
              onClick={() => prosesKajian(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
               <i className="fas fa-file-signature text-primary"></i>
              </span>
            </a> */}
          </>
        );
        break;

      case "Draft":
        return (
          <>
            <a
              title="Edit Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => editProposal(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>

            <a
              title="Hapus Kajian"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() => deleteDialog(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
            <> </>

            <a
              title="Ajukan Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => applyProposal(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                <i className="fas fa-check text-success"></i>
              </span>
            </a>

            <a
              title="Isi Materi"
              className="btn btn-icon btn-light btn-hover-warning btn-sm mx-3"
              onClick={() => addBody(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-warning">
                <i className="fas fa-file-alt text-primary"></i>
              </span>
            </a>

            <a
              title="Tambah Peraturan Terkait"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openAddPerTerkait(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-plus text-primary"></i>
              </span>
            </a>

            <> </>
          </>
        );
        break;

      case "Tolak":
        return (
          <>
            <a
              title="Alasan Penolakan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistoryTolak(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-info text-info"></i>
              </span>
            </a>
            <> </>

            <a
              title="Hapus Kajian"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() => deleteDialog(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
            <> </>
            <a
              title="Edit Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => editProposal(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>
          </>
        );
        break;

      case "Terima":
        return (
          <>
            <a
              title="Status Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            <a
              title="Detail Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailKajian(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
        break;
      default:
        break;
    }
  };

  return <>{checkStatus(row.hasilKajian.status)}</>;
}

export function KajianActionColoumnFormatterEs4(
  cellContent,
  row,
  rowIndex,
  {
    addBody,
    prosesKajian,
    detailKajian,
    editProposal,
    deleteDialog,
    showHistory,
    showHistoryReject,
    applyProposal,
    openAddPerTerkait,
  }
) {
  //console.log(row)

  const checkStatus = (status) => {
    switch (status) {
      case "Eselon 4":
        return (
          <>
            {/* Show history proses Pengajuan */}
            <a
              title="Status Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            {/* Detail  */}

            {/* <a
              title="Detail Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailKajian(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a> */}

            {/* Proses Terima Tolak */}
            <a
              title="Proses Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              // onClick={() => openKajian(row.hasilKajian.id_kajian)}
              onClick={() => prosesKajian(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-file-signature text-primary"></i>
              </span>
            </a>

            {/* <a
              title="Detail Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openKajian(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
               <i className="fas fa-file-signature text-primary"></i>
              </span>
            </a> */}
          </>
        );
        break;

      case "Eselon 3":
        return (
          <>
            <a
              title="Status Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>
            <a
              title="Detail Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailKajian(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
            {/* Proses Terima Tolak */}
            {/* <a
              title="Proses Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              // onClick={() => openKajian(row.hasilKajian.id_kajian)}
              onClick={() => prosesKajian(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
               <i className="fas fa-file-signature text-primary"></i>
              </span>
            </a> */}
          </>
        );
        break;

      case "Draft":
        return (
          <>
            <a
              title="Edit Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => editProposal(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>

            <a
              title="Hapus Kajian"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() => deleteDialog(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
            <> </>

            <a
              title="Ajukan Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => applyProposal(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                <i className="fas fa-check text-success"></i>
              </span>
            </a>

            <a
              title="Isi Materi"
              className="btn btn-icon btn-light btn-hover-warning btn-sm mx-3"
              onClick={() => addBody(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-warning">
                <i className="fas fa-file-alt text-primary"></i>
              </span>
            </a>

            <a
              title="Tambah Peraturan Terkait"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openAddPerTerkait(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-plus text-primary"></i>
              </span>
            </a>

            <> </>
          </>
        );
        break;

      case "Tolak":
        return (
          <>
            <a
              title="Alasan Penolakan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistoryReject(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-info text-info"></i>
              </span>
            </a>
            {/* <a
              title="Edit Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => editProposal(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a> */}
            <> </>
          </>
        );
        break;

      case "Terima":
        return (
          <>
            <a
              title="Status Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            <a
              title="Detail Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailKajian(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
        break;
      default:
        break;
    }
  };

  return <>{checkStatus(row.hasilKajian.status)}</>;
}

export function KajianActionColoumnFormatterEs3(
  cellContent,
  row,
  rowIndex,
  {
    addBody,
    prosesKajian,
    detailKajian,
    editProposal,
    deleteDialog,
    showHistory,
    applyProposal,
    openAddPerTerkait,
  }
) {
  //console.log(row)

  const checkStatus = (status) => {
    switch (status) {
      case "Eselon 4":
        return (
          <>
            {/* Show history proses Pengajuan */}
            <a
              title="Status Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            {/* Detail  */}

            <a
              title="Detail Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailKajian(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>

            {/* Proses Terima Tolak */}
            {/* <a
              title="Proses Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              // onClick={() => openKajian(row.hasilKajian.id_kajian)}
              onClick={() => prosesKajian(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
               <i className="fas fa-file-signature text-primary"></i>
              </span>
            </a> */}

            {/* <a
              title="Detail Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openKajian(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
               <i className="fas fa-file-signature text-primary"></i>
              </span>
            </a> */}
          </>
        );
        break;

      case "Eselon 3":
        return (
          <>
            <a
              title="Status Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>
            {/* <a
              title="Detail Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailKajian(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a> */}
            {/* Proses Terima Tolak */}
            <a
              title="Proses Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              // onClick={() => openKajian(row.hasilKajian.id_kajian)}
              onClick={() => prosesKajian(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-file-signature text-primary"></i>
              </span>
            </a>
          </>
        );
        break;

      case "Draft":
        return (
          <>
            <a
              title="Edit Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => editProposal(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>

            <a
              title="Hapus Kajian"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() => deleteDialog(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
            <> </>

            <a
              title="Ajukan Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => applyProposal(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                <i className="fas fa-check text-success"></i>
              </span>
            </a>

            <a
              title="Isi Materi"
              className="btn btn-icon btn-light btn-hover-warning btn-sm mx-3"
              onClick={() => addBody(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-warning">
                <i className="fas fa-file-alt text-primary"></i>
              </span>
            </a>

            <a
              title="Tambah Peraturan Terkait"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openAddPerTerkait(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-plus text-primary"></i>
              </span>
            </a>

            <> </>
          </>
        );
        break;

      case "Tolak":
        return (
          <>
            {/* <a
              title="Show Reject"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showReject(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
Edit
              </span>
            </a>
            <> </> */}
            <a
              title="Edit Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => editProposal(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>
          </>
        );
        break;

      case "Terima":
        return (
          <>
            <a
              title="Status Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            <a
              title="Detail Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailKajian(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
        break;
      default:
        break;
    }
  };

  return <>{checkStatus(row.hasilKajian.status)}</>;
}

export function MonitoringKajianActionColoumnFormatter(
  cellContent,
  row,
  rowIndex,
  {
    addBody,
    prosesKajian,
    detailKajian,
    editProposal,
    deleteDialog,
    showHistory,
    showHistoryTolak,
    applyProposal,
    openAddPerTerkait,
  }
) {
  //console.log(row)

  const checkStatus = (status) => {
    switch (status) {
      case "Eselon 4":
        return (
          <>
            {/* Show history proses Pengajuan */}
            <a
              title="Status Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            {/* Detail  */}

            {/* <a
              title="Detail Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailKajian(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
Edit
              </span>
            </a> */}

            {/* Proses Terima Tolak */}
            {/* <a
              title="Proses Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              // onClick={() => openKajian(row.hasilKajian.id_kajian)}
              onClick={() => prosesKajian(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
               <i className="fas fa-file-signature text-primary"></i>
              </span>
            </a>
 */}

            {/* <a
              title="Detail Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openKajian(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
               <i className="fas fa-file-signature text-primary"></i>
              </span>
            </a> */}
          </>
        );
        break;

      case "Eselon 3":
        return (
          <>
            <a
              title="Status Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>
            {/* <a
              title="Detail Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailKajian(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
Edit
              </span>
            </a> */}
            {/* Proses Terima Tolak */}
            {/* <a
              title="Proses Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              // onClick={() => openKajian(row.hasilKajian.id_kajian)}
              onClick={() => prosesKajian(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
               <i className="fas fa-file-signature text-primary"></i>
              </span>
            </a> */}
          </>
        );
        break;

      // case "Draft":
      //   return (
      //     <>
      //       <a
      //         title="Edit Kajian"
      //         className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
      //         onClick={() => editProposal(row.hasilKajian.id_kajian)}
      //       >
      //         <span className="svg-icon svg-icon-md svg-icon-primary">
      //           <SVG
      //             src={toAbsoluteUrl(
      //               "/media/svg/icons/Communication/Write.svg"
      //             )}
      //           />
      //         </span>
      //       </a>
      //       <> </>

      //       <a
      //         title="Hapus Kajian"
      //         className="btn btn-icon btn-light btn-hover-danger btn-sm"
      //         onClick={() => deleteDialog(row.hasilKajian.id_kajian)}
      //       >
      //         <span className="svg-icon svg-icon-md svg-icon-danger">
      //           <SVG
      //             src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
      //           />
      //         </span>
      //       </a>
      //       <> </>

      //       <a
      //         title="Ajukan Kajian"
      //         className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
      //         onClick={() => applyProposal(row.hasilKajian.id_kajian)}
      //       >
      //         <span className="svg-icon svg-icon-md svg-icon-success">
      //           <SVG
      //             src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")}
      //           />
      //         </span>
      //       </a>

      //       <a
      //         title="Isi Materi"
      //         className="btn btn-icon btn-light btn-hover-warning btn-sm mx-3"
      //         onClick={() => addBody(row.hasilKajian.id_kajian)}
      //       >
      //         <span className="svg-icon svg-icon-md svg-icon-warning">
      //          <i className="fas fa-file-alt text-primary"></i>
      //         </span>
      //       </a>

      //       <a
      //         title="Tambah Peraturan Terkait"
      //         className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
      //         onClick={() => openAddPerTerkait(row.hasilKajian.id_kajian)}
      //       >
      //         <span className="svg-icon svg-icon-md svg-icon-primary">
      //           <SVG
      //             src={toAbsoluteUrl("/media/svg/icons/Navigation/Plus.svg")}
      //           />
      //         </span>
      //       </a>

      //       <> </>
      //     </>
      //   );
      //   break;

      case "Tolak":
        return (
          <>
            {/* <a
              title="Show Reject"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showReject(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
Edit
              </span>
            </a>
            <> </> */}
            <a
              title="Alasan Penolakan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistoryTolak(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-info text-info"></i>
              </span>
            </a>
            <> </>
          </>
        );
        break;

      case "Terima":
        return (
          <>
            <a
              title="Status Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-stopwatch text-dark"></i>
              </span>
            </a>

            <a
              title="Detail Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailKajian(row.hasilKajian.id_kajian)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
        break;
      default:
        break;
    }
  };

  return <>{checkStatus(row.hasilKajian.status)}</>;
}
