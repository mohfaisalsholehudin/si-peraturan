import React, {useEffect, useState} from "react";
import SVG from "react-inlinesvg";
import BootstrapTable from "react-bootstrap-table-next";
import * as columnFormatters from "../column-formatters";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import {
  sortCaret,
  headerSortingClasses
} from "../../../../_metronic/_helpers";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../_metronic/_partials/controls";
function ProposalOpen({
  history,
  match: {
    params: { id }
  }
}) {

  const addProposal = () => history.push("/compose/proposal/draft/add");
  const backUrl = () => history.push("/compose/proposal");

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "tgl_perencanaan",
      text: "Tgl Perencanaan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "jns_peraturan",
      text: "Jenis Peraturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "judul_peraturan",
      text: "Judul Peraturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    }
  ];



  return (
    <>
    <div>
        <Card>
          <CardHeader
            title="Daftar Proses Penyusunan Peraturan"
            style={{ backgroundColor: "#FFC91B" }}
          ></CardHeader>
          <CardBody>
            <div className="row">
                    <div className="col-xl-6 col-lg-6 mb-3">
                      <div className="row">
                        <label className="col-xl-3 col-lg-3 col-form-label">
                          Tanggal Penyusunan
                        </label>
                        <div className="col-lg-9 col-xl-6">
                          <p className="text-muted pt-2">
                              : 08/09/2020
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-xl-6 col-lg-6 mb-3">
                      <div className="row">
                        <label className="col-xl-3 col-lg-3 col-form-label">
                          Jenis Peraturan
                        </label>
                        <div className="col-lg-9 col-xl-6">
                          <p className="text-muted pt-2">
                              : PMK
                          </p>
                        </div>
                      </div>
                    </div>
            </div>
            <div className="row">
                  <div className="col-xl-6 col-lg-6 mb-3">
                    <div className="row">
                      <label className="col-xl-3 col-lg-3 col-form-label">
                        Unit In Charge
                      </label>
                      <div className="col-lg-9 col-xl-6">
                        <p className="text-muted pt-2">
                            : Seksi A
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-xl-6 col-lg-6 mb-3">
                    <div className="row">
                      <label className="col-xl-3 col-lg-3 col-form-label">
                        Judul Peraturan
                      </label>
                      <div className="col-lg-9 col-xl-6">
                        <p className="text-muted pt-2">
                            : Peraturan 1
                        </p>
                      </div>
                    </div>
                  </div>
            </div>
            <div className="row">
                  <div className="col-xl-6 col-lg-6 mb-3">
                    
                  </div>
                  <div className="col-xl-6 col-lg-6 mb-3">
                    <div className="row">
                      <label className="col-xl-3 col-lg-3 col-form-label">
                        Usulan Simfoni
                      </label>
                      <div className="col-lg-9 col-xl-6">
                        <p className="text-muted pt-2">
                            : Ya
                        </p>
                      </div>
                    </div>
                  </div>
            </div>

            <div className="col-lg-12" style={{ textAlign: "right" }}>
            <button
              type="button"
              onClick={backUrl}
              className="btn btn-light"
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
            >
              <i className="fa fa-arrow-left"></i>
              Kembali
            </button>
            <button
              type="submit"
              className="btn btn-primary ml-2"
              onClick={addProposal}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
            >
              <span className="svg-icon menu-icon">
                                  <SVG
                                    src={toAbsoluteUrl(
                                      "/media/svg/icons/Code/Plus.svg"
                                    )}
                                  />
                </span>
              Tambah Draft
            </button>
          </div>

          </CardBody>
          <CardBody>
          {/* <BootstrapTable
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        bootstrap4
                      ></BootstrapTable> */}
          </CardBody>
        </Card>

    </div>
    
  </>
  );
}

export default ProposalOpen;




      
