import { CollectionsBookmark } from "@material-ui/icons";
import React, {useEffect, useState} from "react";
import { Modal, Table } from "react-bootstrap";
import {getPenegasanById , getHistoryStatusKM} from "../Api";
import { DateFormat } from "../helpers/DateFormat";

function PenegasanReject({ id, show, onHide }) {

  const [penegasan, setPenegasan] = useState([]);
  const [log, setLog] = useState([]);


useEffect(() => {
  if(id){
    getPenegasanById(id).then(({data})=>{
        setPenegasan(data)
    })

    //refkm regulasi = 1
    getHistoryStatusKM(6,id).then(({data})=> {
        setLog(data);
      })
  }
}, [id])


//console.log(log);
const showStatus = () => {
  return log.map((data, index) => {
    const date = new Date(data.wkt_proses);
    const tanggal = String(date.getDate()).padStart(2, "0");
    var bulan = String(date.getMonth() + 1).padStart(2, "0");
    const tahun = date.getFullYear();
    const jam = ((date.getHours()<10?'0':'') + date.getHours());
    const menit = ((date.getMinutes()<10?'0':'') + date.getMinutes());

    const tampilTanggal =
      tanggal + "-" + bulan + "-" + tahun + " - " + jam + ":" + menit;

    return (
      <tr key={index} style={{ borderBottom: "1px solid lightgrey" }}>
        <td key={data.id_log}>{index + 1}</td>
        <td key={data.id_status} style={{ textAlign: "left" }}>
          {data.status}
        </td>
        <td key={data.wkt_proses} style={{ textAlign: "left" }}>
          {tampilTanggal}
        </td>
      </tr>
    );
  });
};

  return (
    <Modal
      size="md"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Status Penolakan
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Nomor Surat Penegasan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${penegasan.no_suratpenegasan}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Tgl Surat</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${DateFormat(penegasan.tgl_suratpenegasan)}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Perihal</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${penegasan.perihal}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Alasan Penolakan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${penegasan.alasan_tolak}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Ditolak Oleh</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
            {`: ${
                penegasan.wkt_teliti_es3 > penegasan.wkt_teliti_es4
                  ? "Eselon 3"
                  : "Eselon 4"
              }`}
            </span>
          </div>
          </div>
      </Modal.Body>
      <Modal.Footer style={{ borderTop: "none" }}>
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
            }}
          >
            <i className="flaticon2-cancel icon-nm"></i>
            Tutup
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}

export default PenegasanReject;
