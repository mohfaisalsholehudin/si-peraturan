import React, { useEffect, useState, useRef } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../../../_metronic/_partials/controls";

import { useSubheader } from "../../../../../../_metronic/layout";
import { useSelector } from "react-redux";
import PenegasanEditForm from "./PenegasanEditForm";
import PenegasanEditFooter from "./PenegasanEditFooter";
import {
  updateStatusPenegasan,
  getPenegasanById,
  uploadFile,
  savePenegasan,
  updatePenegasan,
} from "../../../Api";
import swal from "sweetalert";

function PenegasanEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();
  const { user } = useSelector((state) => state.auth);
  const [title, setTitle] = useState("");
  const [loading, setLoading] = useState(false);
  const [actionsLoading] = useState(true);
  const [content, setContent] = useState();
  const [isDisabled, setIsDisabled] = useState();
  const [penegasan, setPenegasan] = useState();

  const initValues = {
    alasan_tolak: "",
    body: "",
    file_upload: "",
    kd_kantor: user.kantorLegacyKode,
    kd_unit_org: user.unitEs4LegacyKode,
    nip_pengusul: user.nip9,
    nip_pjbt_es3: "",
    nip_pjbt_es4: "",
    no_suratpenegasan: "",
    perihal: "",
    status: "",
    tgl_suratpenegasan: "",
  };

  const getCurrentDate = () => {
    const date = new Date();
    return date;
  };

  const enableLoading = () => {
    setLoading(true);
  };

  const disableLoading = () => {
    setLoading(false);
  };

  const handleChangeProposalPenegasan = (val) => {
    getPenegasanById(val.value).then(({ data }) => {
      setContent({
        id_suratpenegasan: data.id_suratpenegasan,
        no_suratpenegasan: data.no_suratpenegasan,
        tgl_suratpenegasan: getCurrentDate(),
        perihal: data.perihal,
        file_upload: data.file_upload,
        status: data.status,
        alasan_tolak: data.alasan_tolak,
        body: data.body,
        nip_pengusul: data.nip_pengusul,
        nip_pjbt_es4: data.nip_pjbt_es4,
        nip_pjbt_es3: data.nip_pjbt_es3,
        wkt_usul: data.wkt_usul,
        wkt_teliti_es4: data.wkt_teliti_es4,
        wkt_teliti_es3: data.wkt_teliti_es3,
        wkt_create: data.wkt_create,
        wkt_update: data.wkt_update,
        kd_kantor: data.kd_kantor,
        kd_unit_org: data.kd_unit_org,
      });
    });
  };

  useEffect(() => {
    let _title = id ? "Edit Surat Penegasan" : "Tambah Surat Penegasan";

    setTitle(_title);
    suhbeader.setTitle(_title);

    if (id) {
      getPenegasanById(id).then(({ data }) => {
        setPenegasan({
          id_suratpenegasan: data.id_suratpenegasan,
          no_suratpenegasan: data.no_suratpenegasan,
          tgl_suratpenegasan: data.tgl_suratpenegasan,
          perihal: data.perihal,
          file_upload: data.file_upload,
          status: data.status,
          alasan_tolak: data.alasan_tolak,
          body: data.body,
          nip_pengusul: data.nip_pengusul,
          nip_pjbt_es4: data.nip_pjbt_es4,
          nip_pjbt_es3: data.nip_pjbt_es3,
          wkt_usul: data.wkt_usul,
          wkt_teliti_es4: data.wkt_teliti_es4,
          wkt_teliti_es3: data.wkt_teliti_es3,
          wkt_create: data.wkt_create,
          wkt_update: data.wkt_update,
          kd_kantor: data.kd_kantor,
          kd_unit_org: data.kd_unit_org,
          jns_pajak: data.jns_pajak.split(','),
        });
      });
    }
  }, [id, suhbeader]);
  const btnRef = useRef();

  const saveSurat = (values) => {
    if (!id) {
      if (values.file.name) {
        enableLoading();
        const formData = new FormData();

        formData.append("file", values.file);
        //console.log(formData);

        uploadFile(formData)
          .then(({ data }) => {
            disableLoading();
            savePenegasan(
              values.alasan_tolak,
              values.body,
              data.message,
              values.kd_kantor,
              values.kd_unit_org,
              values.nip_pengusul,
              values.nip_pjbt_es3,
              values.nip_pjbt_es4,
              values.no_suratpenegasan,
              values.perihal,
              values.status,
              values.tgl_suratpenegasan,
              values.jns_pajak.toString(),
            ).then(({ status }) => {
              if (status === 201 || status === 200) {
                swal("Berhasil", "Usulan berhasil disimpan", "success").then(
                  () => {
                    history.push("/knowledge/penegasan/usulan");
                  }
                );
              } else {
                swal("Gagal", "Usulan gagal disimpan", "error").then(() => {
                  history.push("/knowledge/penegasan/usulan/new");
                });
              }
            });
          })
          .catch(() => window.alert("Oops Something went wrong !"));
      } else {
        savePenegasan(
          values.alasan_tolak,
          values.body,
          values.file_upload,
          values.kd_kantor,
          values.kd_unit_org,
          values.nip_pengusul,
          values.nip_pjbt_es3,
          values.nip_pjbt_es4,
          values.no_suratpenegasan,
          values.perihal,
          values.status,
          values.tgl_suratpenegasan,
          values.jns_pajak.toString(),
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Usulan berhasil disimpan", "success").then(() => {
              history.push("/knowledge/penegasan/usulan");
            });
          } else {
            swal("Gagal", "Usulan gagal disimpan", "error").then(() => {
              history.push("/knowledge/penegasan/usulan/new");
            });
          }
        });
      }
    } else {
      //console.log(values.status)
      // ketika status tolak dan melakukan edit , otomatis status jadi draft lagi

      if (values.status === "Tolak") {
        if (values.file.name) {
          enableLoading();
          const formData = new FormData();
          formData.append("file", values.file);
          uploadFile(formData).then(({ data }) => {
            disableLoading();
            updatePenegasan(
              values.id_suratpenegasan,
              values.alasan_tolak,
              values.body,
              data.message,
              values.kd_kantor,
              values.kd_unit_org,
              values.nip_pengusul,
              values.nip_pjbt_es3,
              values.nip_pjbt_es4,
              values.no_suratpenegasan,
              values.perihal,
              values.status,
              values.tgl_suratpenegasan,
              values.jns_pajak.toString(),
            ).then(({ status }) => {
              if (status === 201 || status === 200) {
                updateStatus();
              } else {
                swal("Gagal", "Usulan gagal dirubah", "error").then(() => {
                  history.push("/dashboard");
                  history.replace("/knowledge/penegasan/usulan");
                });
              }
            });
          });
        } else {
          updatePenegasan(
            values.id_suratpenegasan,
            values.alasan_tolak,
            values.body,
            values.file_upload,
            values.kd_kantor,
            values.kd_unit_org,
            values.nip_pengusul,
            values.nip_pjbt_es3,
            values.nip_pjbt_es4,
            values.no_suratpenegasan,
            values.perihal,
            values.status,
            values.tgl_suratpenegasan,
            values.jns_pajak.toString(),
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              updateStatus();
            } else {
              swal("Gagal", "Usulan gagal diubah", "error").then(() => {
                history.push("/dashboard");
                history.replace("/knowledge/penegasan/usulan");
              });
            }
          });
        }
      } else {
        if (values.file.name) {
          enableLoading();
          //console.log(values.file.name);
          const formData = new FormData();
          formData.append("file", values.file);
          uploadFile(formData)
            .then(({ data }) => {
              disableLoading();
              updatePenegasan(
                values.id_suratpenegasan,
                values.alasan_tolak,
                values.body,
                data.message,
                values.kd_kantor,
                values.kd_unit_org,
                values.nip_pengusul,
                values.nip_pjbt_es3,
                values.nip_pjbt_es4,
                values.no_suratpenegasan,
                values.perihal,
                values.status,
                values.tgl_suratpenegasan,
              values.jns_pajak.toString(),
              ).then(({ status }) => {
                if (status === 201 || status === 200) {
                  swal("Berhasil", "Usulan berhasil diubah", "success").then(
                    () => {
                      history.push("/knowledge/penegasan/usulan");
                    }
                  );
                } else {
                  swal("Gagal", "Usulan gagal diubah", "error").then(() => {
                    history.push("/knowledge/penegasan/usulan/new");
                  });
                }
              });
            })
            .catch(() => window.alert("Oops Something went wrong !"));
        } else {
          updatePenegasan(
            values.id_suratpenegasan,
            values.alasan_tolak,
            values.body,
            values.file_upload,
            values.kd_kantor,
            values.kd_unit_org,
            values.nip_pengusul,
            values.nip_pjbt_es3,
            values.nip_pjbt_es4,
            values.no_suratpenegasan,
            values.perihal,
            values.status,
            values.tgl_suratpenegasan,
            values.jns_pajak.toString(),
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Usulan berhasil diubah", "success").then(() => {
                history.push("/knowledge/penegasan/usulan");
              });
            } else {
              swal("Gagal", "Usulan gagal diubah", "error").then(() => {
                history.push("/knowledge/penegasan/usulan/new");
              });
            }
          });
        }
      }
    }
  };

  const updateStatus = () => {
    updateStatusPenegasan(id, 5, 1, user.nip9).then(({ status }) => {
      if (status === 201 || status === 200) {
        swal("Berhasil", "Usulan Berubah Menjadi Draft", "success").then(() => {
          history.push("/dashboard");
          history.replace("/knowledge/penegasan/usulan");
        });
      } else {
        swal("Gagal", "Usulan gagal diubah", "error").then(() => {
          history.push("/dashboard");
          history.replace("/knowledge/penegasan/usulan");
        });
      }
    });
  };

  const backToProposalList = () => {
    history.push(`/knowledge/penegasan/usulan`);
  };

  const setDisabled = (val) => {
    setIsDisabled(val);
  };
  return (
    <Card>
      {/* {actionsLoading && <ModalProgressBar />} */}
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <PenegasanEditForm
            actionsLoading={actionsLoading}
            proposal={penegasan || initValues}
            btnRef={btnRef}
            saveProposal={saveSurat}
            loading={loading}
            handleChangeProposalPenegasan={handleChangeProposalPenegasan}
            setDisabled={setDisabled}
            backAction={backToProposalList}
          />
        </div>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        {/* <PenegasanEditFooter
          backAction={backToProposalList}
          btnRef={btnRef}
        ></PenegasanEditFooter> */}
      </CardFooter>
    </Card>
  );
}

export default PenegasanEdit;
