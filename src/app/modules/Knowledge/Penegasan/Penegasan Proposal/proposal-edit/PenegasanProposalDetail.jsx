import React, { useEffect, useState, useRef } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../../../_metronic/_partials/controls";

import { useSubheader } from "../../../../../../_metronic/layout";
import PenegasanDetailForm from "./PenegasanDetailForm";
import PenegasanDetailFooter from "./PenegasanDetailFooter";
import {
  getPenegasanById,
} from "../../../Api";
import { useSelector } from "react-redux";

const initValues = {
  file_upload: "",
  body: "",
  nip_pengusul: "",
  status: "",
  nip_pjbt_es3: "",
  nip_pjbt_es4: "",
  no_suratpenegasan: "",
  perihal: "",
  tgl_suratpenegasan: "",
  alasan_tolak: "",
};

function PenegasanProposalDetail({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  const { role, user } = useSelector((state) => state.auth);
  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [isDisabled, setIsDisabled] = useState();
  const [penegasan, setPenegasan] = useState();
  const [PenegasanDetail, setPenegasanDetail] = useState([]);
  let thePath = document.URL;
  let nextStatus = "";
  const lastPath = thePath.substring(thePath.lastIndexOf("/") + 1);
  const [statNow, setstatNow] = useState("");

  useEffect(() => {
    
      let _title = "Detail Surat Penegasan";
      setTitle(_title);
      suhbeader.setTitle(_title);

    getPenegasanById(id).then(({ data }) => {
      setPenegasan({
          id_suratpenegasan: data.id_suratpenegasan,
          no_suratpenegasan: data.no_suratpenegasan,
          tgl_suratpenegasan: data.tgl_suratpenegasan,
          perihal: data.perihal,
          file_upload: data.file_upload,
          status: data.status,
          alasan_tolak: data.alasan_tolak,
          body: data.body,
          nip_pengusul: data.nip_pengusul,
          nip_pjbt_es4: data.nip_pjbt_es4,
          nip_pjbt_es3: data.nip_pjbt_es3,
          wkt_usul: data.wkt_usul,
          wkt_teliti_es4: data.wkt_teliti_es4,
          wkt_teliti_es3: data.wkt_teliti_es3,
          wkt_create: data.wkt_create,
          wkt_update: data.wkt_update
      });
      if (data.status == "Eselon 4") {
        setstatNow(2);
      } else if (data.status == "Eselon 3") {
        setstatNow(3);
      }
    });
  }, [id, suhbeader]);

  const btnRef = useRef();

  const savePenegasanDetail = (values) => {};

  const backToPenegasanList = (values) => {
    history.push(`/knowledge/penegasan/usulan`);
  };

  const setDisabled = (val) => {
    setIsDisabled(val);
  };
  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <PenegasanDetailForm
            actionsLoading={actionsLoading}
            proposal={penegasan || initValues}
            PenegasanDetail={PenegasanDetail}
            idPenegasan={id}
            btnRef={btnRef}
            savePenegasanDetail={savePenegasanDetail}
            setDisabled={setDisabled}
          />
        </div>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <PenegasanDetailFooter
          backAction={backToPenegasanList}
          btnRef={btnRef}
        ></PenegasanDetailFooter>
      </CardFooter>
    </Card>
  );
}

export default PenegasanProposalDetail;
