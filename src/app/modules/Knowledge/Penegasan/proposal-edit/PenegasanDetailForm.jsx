import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { Field, Formik, Form } from "formik";
import "./styles.css";
import {
  DatePickerField,
  Input,
  Textarea
} from "../proposal-edit/helpers";
import { getPenegasanById } from "../../Api";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import Editor from "ckeditor5-custom-build/build/ckeditor";
// import "../Penegasan Proposal/proposal-edit/decoupled.css"
const {SLICE_URL} = window.ENV;

function PenegasanDetailForm({
  proposal,
  savePenegasan,
  idPenegasan
}) {

  const { user } = useSelector((state) => state.auth);
  const [propbody, setPropbody] = useState("");

  useEffect(() => {
    getPenegasanById(idPenegasan).then(({ data }) => {
      if (data.body) {
        setPropbody(JSON.parse(data.body));
      }
    });
  }, [idPenegasan]);

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={proposal}
        onSubmit={(values) => {
          savePenegasan(values);
        }}
      >
        {({ handleSubmit, setFieldValue, values }) => {
          const handleChangeNip = () => {
            setFieldValue("nip_perekam", user.nip9);
          };

          return (
            <>
              <Form className="form form-label-right">

                {/* FIELD NO SURAT */}
                <div className="form-group row">
                  <Field
                    disabled
                    name="no_suratpenegasan"
                    component={Input}
                    placeholder="Nomor Surat Penegasan"
                    label="Nomor Surat Penegasan"
                  />
                </div>

                {/* FIELD TANGGAL SURAT */}
                <div className="form-group row">
                  <DatePickerField
                    name="tgl_suratpenegasan"
                    label="Tanggal Surat Penegasan"
                    disabled
                  />
                </div>
                
                {/* FIELD tentang */}
                <div className="form-group row">
                  <Field
                    name="perihal"
                    component={Textarea}
                    placeholder="Perihal"
                    label="Perihal"
                    disabled
                  />
                </div>

                {/* FIELD FILE */}
              <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                   File
                  </label>
                  <div className="col-lg-9 col-xl-6"
                  style={{ marginTop: "10px" }}>
                    <a
                    href={values.file_upload}
                    target="_blank"
                    rel="noopener noreferrer"
                    >
                      {values.file_upload.slice(SLICE_URL)}
                    </a>
                  </div>
                </div>

                {/* FIELD BODY */}
                <div>
                  <br />
                  <h5>Isi Materi</h5>
                </div>
                <div className="document-editor">
                  <div className="document-editor__toolbar"></div>
                  <div className="document-editor__editable-container">
                    <CKEditor
                      disabled
                      onReady={(editor) => {
                        console.log("Editor is ready to use!", editor);
                        window.editor = editor;

                        // Add these two lines to properly position the toolbar
                        const toolbarContainer = document.querySelector(
                          ".document-editor__toolbar"
                        );
                        toolbarContainer.appendChild(
                          editor.ui.view.toolbar.element
                        );
                      }}
                      config={{
                        removePlugins: ["Heading", "Link"],
                        toolbar: [],
                        isReadOnly: true,
                      }}
                      editor={Editor}
                      //ICAAAAAAAAAAAAAAALLLLLLLLLLLLLLLLLLLLLLLL
                      //INI BUAT NAMBAHIN TEMPLATE NYA DI BAWAH INI PAKAI HTML
                      data={propbody}
                    />
                  </div>
                </div>
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default PenegasanDetailForm;