import React, { useEffect, useState, useRef } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../../_metronic/_partials/controls";

import { useSubheader } from "../../../../../_metronic/layout";
import PenegasanDetailForm from "./PenegasanDetailForm";
import PenegasanDetailFooter from "./PenegasanDetailFooter";
import {
  getPenegasanById,
  updateStatusPenegasan,
} from "../../Api";
import swal from "sweetalert";
import Swal from "sweetalert2";
import { useSelector } from "react-redux";

const initValues = {
  file_upload: "",
  body: "",
  nip_pengusul: "",
  status: "",
  nip_pjbt_es3: "",
  nip_pjbt_es4: "",
  no_suratpenegasan: "",
  perihal: "",
  tgl_suratpenegasan: "",
  alasan_tolak: "",
};

function PenegasanDetail({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  const { role, user } = useSelector((state) => state.auth);
  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [isDisabled, setIsDisabled] = useState();
  const [penegasan, setPenegasan] = useState();
  const [PenegasanDetail, setPenegasanDetail] = useState([]);
  let thePath = document.URL;
  let nextStatus = "";
  const lastPath = thePath.substring(thePath.lastIndexOf("/") + 1);
  const [statNow, setstatNow] = useState("");
  
  const acceptAction = (statusNow) => {
    if (statusNow == "Eselon 4") {
      nextStatus = 3;
    } else if (statusNow == "Eselon 3") {
      nextStatus = 6;
    }

    updateStatusPenegasan(id, statNow, nextStatus, user.nip9).then(({ status }) => {
      if (status === 201 || status === 200) {
        swal("Berhasil", "Usulan berhasil disetujui", "success").then(() => {
          history.push("/dashboard");
          history.replace("/knowledge/penegasan/penelitian");
        });
      } else {
        swal("Gagal", "Usulan gagal disetujui", "error").then(() => {
          history.push("/dashboard");
          history.replace("/knowledge/penegasan/penelitian");
        });
      }
    });
  };

  function tolakDialog(idPer, statNow) {
    Swal.fire({
      title: "Tolak",
      input: "textarea",
      inputPlaceholder: "Masukkan alasan tolak..",
      inputAttributes: {
        autocapitalize: "on",
      },
      text: "Apakah Anda yakin menolak usulan ini ?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#7cd1f9",
      cancelButtonColor: "#cacad3",
      confirmButtonText: "Tolak",
      reverseButtons: true,
    }).then((data) => {
      if (data.value != null) {
        if (data.value == "") {
          swal("Gagal", "Alasan tolak harus diisi", "error");
        } else {
          // alert(idPer + "" + statNow + "" + 5 + "" + user.nip9 + "" + value);
          updateStatusPenegasan(idPer, statNow, 5, user.nip9, data.value).then(
            ({ status }) => {
              if (status === 201 || status === 200) {
                swal("Berhasil", "Usulan berhasil ditolak", "success").then(
                  () => {
                    history.push("/dashboard");
                    history.replace("/knowledge/penegasan/penelitian");
                  }
                );
              } else {
                swal("Gagal", "Usulan gagal ditolak", "error").then(() => {
                  history.push("/dashboard");
                  history.replace("/knowledge/penegasan/penelitian");
                });
              }
            }
          );
        }
      }
    });
  }

  function tombolProses() {
    if (lastPath == "proses") {
      return (
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          <button
            type="button"
            onClick={() => tolakDialog(id, statNow)}
            className="btn btn-danger"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
            }}
          >
            <i className="flaticon2-cancel icon-nm"></i>
            Tolak
          </button>
          {`  `}
          <button
            type="submit"
            className="btn btn-success ml-2"
            onClick={() => setujuDialog(id)}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
            }}
          >
            <i className="fas fa-check"></i>
            Setuju
          </button>
        </div>
      );
    } else {
      return "";
    }
  }

  const setujuDialog = (id) => {
    swal({
      title: "Setuju",
      text: "Apakah Anda yakin menyetujui usulan ini ?",
      icon: "warning",
      buttons: true,
    }).then((ret) => {
      if (ret == true) {
        acceptAction(penegasan.status);
      }
    });
  };

  useEffect(() => {
    if (lastPath == "proses") {
      let _title = "Proses Surat Penegasan";
      setTitle(_title);
      suhbeader.setTitle(_title);
    } else {
      let _title = "Detail Surat Penegasan";
      setTitle(_title);
      suhbeader.setTitle(_title);
    }

    getPenegasanById(id).then(({ data }) => {
      setPenegasan({
          id_suratpenegasan: data.id_suratpenegasan,
          no_suratpenegasan: data.no_suratpenegasan,
          tgl_suratpenegasan: data.tgl_suratpenegasan,
          perihal: data.perihal,
          file_upload: data.file_upload,
          status: data.status,
          alasan_tolak: data.alasan_tolak,
          body: data.body,
          nip_pengusul: data.nip_pengusul,
          nip_pjbt_es4: data.nip_pjbt_es4,
          nip_pjbt_es3: data.nip_pjbt_es3,
          wkt_usul: data.wkt_usul,
          wkt_teliti_es4: data.wkt_teliti_es4,
          wkt_teliti_es3: data.wkt_teliti_es3,
          wkt_create: data.wkt_create,
          wkt_update: data.wkt_update
      });
      if (data.status == "Eselon 4") {
        setstatNow(2);
      } else if (data.status == "Eselon 3") {
        setstatNow(3);
      }
    });
  }, [id, suhbeader]);

  const btnRef = useRef();

  const savePenegasanDetail = (values) => {};

  const backToPenegasanList = (values) => {
    history.push(`/knowledge/penegasan/penelitian`);
  };

  const setDisabled = (val) => {
    setIsDisabled(val);
  };
  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <PenegasanDetailForm
            actionsLoading={actionsLoading}
            proposal={penegasan || initValues}
            PenegasanDetail={PenegasanDetail}
            idPenegasan={id}
            btnRef={btnRef}
            savePenegasanDetail={savePenegasanDetail}
            setDisabled={setDisabled}
          />
        </div>
        <br></br>

        {tombolProses()}
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <PenegasanDetailFooter
          backAction={backToPenegasanList}
          btnRef={btnRef}
        ></PenegasanDetailFooter>
      </CardFooter>
    </Card>
  );
}

export default PenegasanDetail;
