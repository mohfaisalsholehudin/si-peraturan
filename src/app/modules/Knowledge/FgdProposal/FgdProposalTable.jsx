/* Library */
import React, { useEffect, useState } from "react";
import { useHistory, Route } from "react-router-dom";
import { useSelector } from "react-redux";
import swal from "sweetalert";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import SVG from "react-inlinesvg";

/* Helpers */
import { Pagination } from "../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../_metronic/_helpers";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import * as columnFormatters from "../column-formatters";
/* Component */
import FgdProposalHistory from "./FgdProposalHistory";
import FgdProposalHistoryReject from "./FgdProposalHistoryReject";

/* Utility */
import { updateStatusFgd, deleteFgd, getFgdNonTerimaByNip } from "../Api";
import NdPermintaanForm from "../../Composing/process/nd-permintaan/NdPermintaanForm";

function FgdProposalTable() {
  const { role, user } = useSelector((state) => state.auth);
  const konseptor = role.includes("ROLE_PERATURAN_KONSEPTOR");
  const es4 = role.includes("ROLE_PERATURAN_PENELITI_LVL1");
  const es3 = role.includes("ROLE_PERATURAN_PENELITI_LVL2");
  const admin = role.includes("ROLE_ADMIN_PERATURAN");
  const history = useHistory();
  const [proposal, setProposal] = useState([]);

  const addProposal = () => history.push("/knowledge/fgd/usulan/add");
  const showHistoryFgd = (id) =>
    history.push(`/knowledge/fgd/usulan/${id}/showhistory`);
  const showHistoryPenolakan = (id) =>
    history.push(`/knowledge/fgd/usulan/${id}/showhistoryreject`);
  const detailFgd = (id) => history.push(`/knowledge/fgd/usulan/${id}/detail`);

  const editProposal = (id) => history.push(`/knowledge/fgd/usulan/${id}/edit`);
  const applyProposal = (id) => {
    swal({
      title: "Apakah Anda Ingin Mengajukan Usulan Ini?",
      text: "Klik OK untuk melanjutkan",
      icon: "warning",
      buttons: true,
      dangerMode: false,
    }).then((willApply) => {
      if (willApply) {
        updateStatusFgd(id, 1, 2, user.nip9).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Usulan berhasil diajukan", "success").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/fgd/usulan");
            });
          } else {
            swal("Gagal", "Usulan gagal diajukan", "error").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/fgd/usulan");
            });
          }
        });
      }
    });
  };

  const deleteAction = (id) => {
    swal({
      title: "Apakah Anda Ingin Menghapus Usulan Ini?",
      text: "Klik OK untuk melanjutkan",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        deleteFgd(id).then(({ status }) => {
          if (status === 200) {
            swal("Berhasil", "Usulan berhasil dihapus", "success").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/fgd/usulan");
            });
          } else {
            swal("Gagal", "Usulan gagal dihapus", "error").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/fgd/usulan");
            });
          }
        });
      }
    });
  };

  const addBody = (id) => history.push(`/knowledge/fgd/usulan/${id}/body`);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "no_nd",
      text: "No ND Hasil FGD",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "tgl_nd",
      text: "Tgl ND Hasil FGD",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "perihal",
      text: "Perihal FGD",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.StatusColumnFormatter,
      headerSortingClasses,
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.FgdProposalActionColumnFormatter,
      formatExtraData: {
        openEditDialog: editProposal,
        openDeleteDialog: deleteAction,
        showHistory: showHistoryFgd,
        showHistoryTolak: showHistoryPenolakan,
        detailFgd: detailFgd,
        applyProposal: applyProposal,
        addBody: addBody,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];

  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "no_surat",
    pageNumber: 1,
    pageSize: 50,
  };

  const defaultSorted = [{ dataField: "no_perencanaan", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: proposal.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;

  useEffect(() => {
    getFgdNonTerimaByNip(user.nip9).then(({ data }) => {
      // setPlan(data);
      data.map((data) => {
        return data.status !== "Terima"
          ? setProposal((proposal) => [...proposal, data])
          : // : data.status === "Draft"
            // ? setProposal((proposal) => [...proposal, data])
            null;
      });
    });
  }, [user.kantorLegacyKode]);
  console.log(user);

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_fgd"
                  data={proposal}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <button
                            type="button"
                            className="btn btn-primary"
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                              float: "right",
                            }}
                            onClick={addProposal}
                          >
                            <span className="svg-icon menu-icon">
                              <SVG
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Code/Plus.svg"
                                )}
                              />
                            </span>
                            Tambah
                          </button>
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
      <Route path="/knowledge/fgd/usulan/:id/showhistoryreject">
        {({ history, match }) => (
          <FgdProposalHistoryReject
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/fgd/usulan");
            }}
            onRef={() => {
              history.push("/knowledge/fgd/usulan");
            }}
          />
        )}
      </Route>
      <Route path="/knowledge/fgd/usulan/:id/showhistory">
        {({ history, match }) => (
          <FgdProposalHistory
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/fgd/usulan");
            }}
            onRef={() => {
              history.push("/knowledge/fgd/usulan");
            }}
          />
        )}
      </Route>
    </>
  );
}

export default FgdProposalTable;
