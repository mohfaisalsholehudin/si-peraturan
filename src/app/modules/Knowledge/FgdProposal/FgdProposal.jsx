import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
import FgdProposalTable from "./FgdProposalTable";

function FgdProposal() {
  return (
    <>
      <Card>
        <CardHeader
          title="Usulan FGD"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <FgdProposalTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default FgdProposal;
