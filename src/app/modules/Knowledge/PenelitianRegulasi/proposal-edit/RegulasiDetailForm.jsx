import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { Field, Formik, Form } from "formik";
import "./styles.css";
import {
  DatePickerField,
  Input,
  Textarea,
  Select as Sel,
} from "../proposal-edit/helpers";
import {
  getJenisPeraturan,
  getTopik,
  getRegulasiById,
  getDokumenPer,
} from "../../Api";
import RegulasiTerkaitTableDetail from "./RegulasiTerkaitTableDetail";
import DokumenLainTableDetail from "./DokumenLainTableDetail";
// import "./decoupled.css";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import Editor from "ckeditor5-custom-build/build/ckeditor";
import BodyEditor from "./helpers/BodyEditor";
import DocumentEditorKM from "../../../../helpers/editor/DocumentEditorKM";
const {SLICE_URL} = window.ENV;

function RegulasiDetailForm({
  proposal,
  btnRef,
  saveRegulasiTerkait,
  peraturanTerkait,
  idPeraturan,
}) {
  const [jenisPeraturan, setJenisPeraturan] = useState([]);
  const [topik, setTopik] = useState([]);
  const [body, setBody] = useState('');
  const [propbody, setPropbody] = useState("");
  const [dokLain, setDoklain] = useState([]);
  const { user } = useSelector((state) => state.auth);
  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  useEffect(() => {
    getJenisPeraturan().then(({ data }) => {
      // setJenisPeraturan(data);
      data.map(dt=> {
        return dt.status === 'AKTIF' ? setJenisPeraturan(peraturan => [...peraturan, dt]) : null
      })
    });

    getTopik().then(({ data }) => {
      setTopik(data);
    });
  }, []);

  useEffect(() => {
    getRegulasiById(idPeraturan).then(({ data }) => {
      if (data.body) {
        // setPropbody(JSON.parse(data.body));
        setPropbody(data.body)
      }
    });
  }, [idPeraturan]);

  useEffect(() => {
    getDokumenPer(idPeraturan).then(({ data }) => {
      setDoklain(data);
    });
  }, [idPeraturan]);

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={proposal}
        onSubmit={(values) => {
          //console.log(values);
          saveRegulasiTerkait(values);
        }}
      >
        {({ handleSubmit, setFieldValue }) => {
          const handleChangeNip = () => {
            setFieldValue("nip_perekam", user.nip9);
          };

          return (
            <>
              <Form className="form form-label-right">
                {/* FIELD NO SURAT */}
                <div className="form-group row">
                  <Field
                    disabled
                    name="no_regulasi"
                    component={Input}
                    placeholder="Nomor Regulasi"
                    label="Nomor Regulasi"
                    // onClick={()=>handleChangeNip()}
                  />
                </div>
                {/* FIELD TANGGAL SURAT */}
                <div className="form-group row">
                  <DatePickerField
                    name="tgl_regulasi"
                    label="Tanggal Regulasi"
                    disabled
                  />
                </div>
                {/* FIELD jenis */}
                <div className="form-group row">
                  <Sel name="jns_regulasi" label="Jenis Regulasi" disabled>
                    {jenisPeraturan.map((data, index) => (
                      <option key={index} value={data.id_jnsperaturan}>
                        {data.nm_jnsperaturan}
                      </option>
                    ))}
                  </Sel>
                </div>
                {/* FIELD tentang */}
                <div className="form-group row">
                  <Field
                    name="perihal"
                    component={Textarea}
                    placeholder="Perihal"
                    label="Perihal"
                    disabled
                  />
                </div>
                <div className="form-group row">
                  <Sel name="id_topik" label="topik" disabled>
                    {topik.map((data, index) => (
                      <option key={index} value={data.id}>
                        {data.nama}
                      </option>
                    ))}
                  </Sel>
                </div>
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    File
                  </label>
                  <div
                    className="col-lg-9 col-xl-6"
                    style={{ marginTop: "10px" }}
                  >
                    <a
                      href={proposal.file_upload}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      {proposal.file_upload.slice(SLICE_URL)}
                    </a>
                  </div>
                </div>
                <RegulasiTerkaitTableDetail
                  peraturanTerkait={peraturanTerkait}
                  idPeraturan={idPeraturan}
                />
                <DokumenLainTableDetail
                  dokLain={dokLain}
                  idPeraturan={idPeraturan}
                />
                <div>
                  <br />
                  <br />
                  <h3>Isi Materi</h3>
                </div>
                <DocumentEditorKM
                  content={propbody}
                  setDraft={setBody}
                  isReadOnly={true}
                />
                {/* <div className="document-editor">
                  <div className="document-editor__toolbar"></div>
                  <div className="document-editor__editable-container">
                    <CKEditor
                      disabled
                      onReady={(editor) => {
                        console.log("Editor is ready to use!", editor);
                        window.editor = editor;

                        // Add these two lines to properly position the toolbar
                        const toolbarContainer = document.querySelector(
                          ".document-editor__toolbar"
                        );
                        toolbarContainer.appendChild(
                          editor.ui.view.toolbar.element
                        );
                      }}
                      config={{
                        removePlugins: ["Heading", "Link"],
                        toolbar: [],
                        isReadOnly: true,
                      }}
                      editor={Editor}
                      //ICAAAAAAAAAAAAAAALLLLLLLLLLLLLLLLLLLLLLLL
                      //INI BUAT NAMBAHIN TEMPLATE NYA DI BAWAH INI PAKAI HTML
                      data={propbody}
                    />
                  </div>
                </div> */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default RegulasiDetailForm;
