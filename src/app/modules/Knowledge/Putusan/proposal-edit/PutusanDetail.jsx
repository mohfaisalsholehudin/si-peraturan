import React, { useEffect, useState, useRef } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";

import { useSubheader } from "../../../../../_metronic/layout";
import RegulasiDetailForm from "./RegulasiDetailForm";
import RegulasiTerkaitFooter from "./RegulasiTerkaitFooter";
import { getKajianById, getPeraturanTerkaitKajian, getPeraturanTerkaitPutusan, getPutusanById, updateStatusKajian, updateStatusPutusan } from "../../Api";
import swal from "sweetalert";
import Swal from "sweetalert2";
import { useSelector } from "react-redux";

const initValues = {
  alasan_tolak: "",
  body: "",
  file_upload: "",
  id_detiljns: "",
  kd_kantor: "",
  kd_unit_org: "",
  nip_pengusul: "",
  nip_pjbt_es3: "",
  nip_pjbt_es4: "",
  no_putusan: "",
  perihal: "",
  status: "",
  tgl_putusan: ""
};





function PutusanDetail({
  history,
  match: {
    params: { id }
  }
}) {
  // Subheader
  const subheader = useSubheader();

  const { role, user } = useSelector((state) => state.auth);
  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [isDisabled, setIsDisabled] = useState();
  const [proposal, setProposal] = useState();
  const [peraturanTerkait, setPeraturanTerkait] = useState([]);
  let thePath = document.URL;
  let nextStatus = '';
  const lastPath = thePath.substring(thePath.lastIndexOf('/') + 1);
  const [statNow, setstatNow] = useState("");

  const acceptAction = (statusNow) => {
    if (statusNow == 'Eselon 4') {
      nextStatus = 3;

    } else if (statusNow == 'Eselon 3') {
      nextStatus = 6;
    }

    updateStatusPutusan(id, statNow, nextStatus, user.nip9).then(({ status }) => {
      if (status === 201 || status === 200) {
        swal("Berhasil", "Usulan berhasil disetujui", "success").then(
          () => {
            history.push("/dashboard");
            history.replace("/knowledge/putusan/penelitian");
          }
        );
      } else {
        swal("Gagal", "Usulan gagal disetujui", "error").then(() => {
          history.push("/dashboard");
          history.replace("/knowledge/putusan/penelitian");
        });
      }
    })
  }


  function tolakDialog(idPer, statNow) {
    Swal.fire({
      title: "Tolak",
      input: "textarea",
      inputPlaceholder: "Masukkan alasan tolak..",
      inputAttributes: {
        autocapitalize: "on",
      },
      text: "Apakah Anda yakin menolak usulan ini ?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#7cd1f9",
      cancelButtonColor: "#cacad3",
      confirmButtonText: "Tolak",
      reverseButtons: true,
    }).then((data) => {
      if (data.value != null) {
        if (data.value == "") {
          swal("Gagal", "Alasan tolak harus diisi", "error");
        } else {
          // alert(idPer + "" + statNow + "" + 5 + "" + user.nip9 + "" + value);
          updateStatusPutusan(idPer, statNow, 5, user.nip9, data.value).then(
            ({ status }) => {
              if (status === 201 || status === 200) {
                swal("Berhasil", "Usulan berhasil ditolak", "success").then(
                  () => {
                    history.push("/dashboard");
                    history.replace("/knowledge/putusan/penelitian");
                  }
                );
              } else {
                swal("Gagal", "Usulan gagal ditolak", "error").then(() => {
                  history.push("/dashboard");
                  history.replace("/knowledge/putusan/penelitian");
                });
              }
            }
          );
        }
      }
    });
  }

  function tombolProses() {
    if (lastPath == "proses") {
      return (
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          <button
            type="button"
            onClick={() => tolakDialog(id, statNow)}
            className="btn btn-danger"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="flaticon2-cancel icon-nm"></i>
            Tolak
          </button>
          {`  `}
          <button
            type="submit"
            className="btn btn-success ml-2"
            onClick={() => setujuDialog(id)}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fas fa-check"></i>
            Setuju
          </button>
        </div>
      );
    } else {
      return ("");
    }

  }

  const setujuDialog = id => {

    swal({
      title: "Setuju",
      text: "Apakah Anda yakin menyetujui usulan ini ?",
      icon: "warning",
      buttons: true,
      //successMode : true
      //dangerMode: true,
    })
      .then((ret) => {

        if (ret == true) {
          acceptAction(proposal.status)

        }
      });
  };

  useEffect(() => {


    if (lastPath == "proses") {
      let _title = "Proses Putusan Pengadilan";
      setTitle(_title);
      subheader.setTitle(_title);
    } else {
      let _title = "Detail Putusan Pengadilan";
      setTitle(_title);
      subheader.setTitle(_title);
    }




    getPutusanById(id).then(({ data }) => {

      setProposal({
          id_putpengadilan: data.id_putpengadilan,
          no_putusan: data.no_putusan,
          tgl_putusan: data.tgl_putusan,
          id_detiljns: data.id_detiljns,
          perihal: data.perihal,
          file_upload: data.file_upload,
          alasan_tolak: data.alasan_tolak,
          body: data.body,
          nip_pengusul: data.nip_pengusul,
          nip_pjbt_es4: data.nip_pjbt_es4,
          nip_pjbt_es3: data.nip_pjbt_es3,
          wkt_usul: data.wkt_usul,
          wkt_teliti_es4: data.wkt_teliti_es4,
          wkt_teliti_es3: data.wkt_teliti_es3,
          status: data.status,
          wkt_create: data.wkt_create,
          wkt_update: data.wkt_update,
          kd_kantor: data.kd_kantor,
          kd_unit_org: data.kd_unit_org,
      });
      if (data.status == "Eselon 4") {
        setstatNow(2);
      } else if (data.status == "Eselon 3") {
        setstatNow(3);
      }
    });
  }, [id, subheader]);


  useEffect(() => {

    getPeraturanTerkaitPutusan(id).then(({ data }) => {
      data.map((dt) => {
        //setPeraturanTerkait(peraturanTerkait => [...peraturanTerkait,dt.kmperaturanTerkait]);

        setPeraturanTerkait(peraturanTerkait => [...peraturanTerkait, {
          id_per_terkait: dt.kmperaturanTerkait.id_per_terkait,
          id_peraturan: dt.kmregulasiPerpajakan.id_peraturan,
          no_regulasi: dt.kmregulasiPerpajakan.no_regulasi,
          tgl_regulasi: dt.kmregulasiPerpajakan.tgl_regulasi,
          perihal: dt.kmregulasiPerpajakan.perihal,
          status: dt.kmregulasiPerpajakan.status,
          file_upload: dt.kmregulasiPerpajakan.file_upload,
          jns_regulasi: dt.kmregulasiPerpajakan.jns_regulasi,
          id_topik: dt.kmregulasiPerpajakan.id_topik,
          body: dt.kmregulasiPerpajakan.body,

        }]);

      })

    });



  }, []);



  const btnRef = useRef();

  const saveRegulasiTerkait = values => {
  };


  const backToProposalList = () => {
      history.push(`/knowledge/putusan/penelitian`);
  };

  const setDisabled = val => {
    setIsDisabled(val);
  };
  return (
    <Card>
      {/* {actionsLoading && <ModalProgressBar />} */}
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <RegulasiDetailForm
            actionsLoading={actionsLoading}
            proposal={proposal || initValues}
            peraturanTerkait={peraturanTerkait}
            idPutusan={id}
            btnRef={btnRef}
            saveRegulasiTerkait={saveRegulasiTerkait}
            setDisabled={setDisabled}
          />
        </div>
        <br></br>

        {tombolProses()}

      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <RegulasiTerkaitFooter
          backAction={backToProposalList}
          btnRef={btnRef}
        ></RegulasiTerkaitFooter>
      </CardFooter>
    </Card>
  );
}

export default PutusanDetail;
