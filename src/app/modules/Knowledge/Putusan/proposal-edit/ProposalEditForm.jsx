import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import Select from "react-select";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import "./styles.css";
import {
  DatePickerField,
  Input,
  Textarea,
  Select as Sel,
  Checkbox,
} from "../proposal-edit/helpers";
import CustomFileInput from "./helpers/CustomFileInput";
import { getJenisPengadilan } from "../../Api";

function ProposalEditForm({ proposal, btnRef, saveProposal }) {
  const [bentukPengadilan, setBentukPengadilan] = useState([]);
  const { user } = useSelector((state) => state.auth);

  //const validProductValues = products.map(({ _id }) => _id);

  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    tgl_putusan: Yup.mixed()
      .nullable(false)
      .required("Tanggal Surat is required"),

    no_putusan: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(250, "Maximum 50 symbols")
      .required("No Putusan is required"),

    perihal: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(250, "Maximum 50 symbols")
      .required("Perihal is required"),
  });

  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  useEffect(() => {
    getJenisPengadilan().then(({ data }) => {
      setBentukPengadilan(data);
    });
  }, []);

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={proposal}
        validationSchema={ProposalEditSchema}
        onSubmit={(values) => {
          //console.log(values);
          saveProposal(values);
        }}
      >
        {({ handleSubmit, setFieldValue }) => {
          const handleChangeNip = () => {
            setFieldValue("nip_pengusul", user.nip9);
          };
          const handleChangeKdKantor = () => {
            setFieldValue("kd_kantor", user.kantorLegacyKode);
          };
          const handleChangeKdUnit = () => {
            setFieldValue("kd_unit_org", user.unitEs4LegacyKode);
          };

          return (
            <>
              <Form className="form form-label-right">
                <div className="form-group row">
                  <Field
                    name="no_putusan"
                    component={Input}
                    placeholder="Nomor Putusan"
                    label="Nomor Putusan"
                    onClick={()=>handleChangeNip()}
                  />
                </div>

                {/* FIELD TANGGAL SURAT */}
                <div className="form-group row">
                  <DatePickerField name="tgl_putusan" label="Tanggal Putusan" />
                </div>

                {/* FIELD jenis */}
                <div className="form-group row">
                  <Sel name="id_detiljns" label="Bentuk Pengadilan">
                    <option value=""></option>
                    {bentukPengadilan.map((data, index) => (
                      <option key={index} value={data.id}>
                        {data.nama}
                      </option>
                    ))}
                  </Sel>
                </div>

                {/* FIELD tentang */}
                <div className="form-group row">
                  <Field
                    name="perihal"
                    component={Textarea}
                    placeholder="Perihal"
                    label="Perihal"
                    onClick={()=>handleChangeKdKantor()}
                  />
                </div>

                {/* FIELD UPLOAD FILE */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Upload File
                  </label>
                  <div className="col-lg-8 col-xl-6">
                    <Field
                      name="file"
                      component={CustomFileInput}
                      title="Select a file"
                      label="File"
                      style={{ display: "flex" }}
                    />
                  </div>
                </div>

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                  onClick={()=>handleChangeKdUnit()}
                ></button>
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default ProposalEditForm;
