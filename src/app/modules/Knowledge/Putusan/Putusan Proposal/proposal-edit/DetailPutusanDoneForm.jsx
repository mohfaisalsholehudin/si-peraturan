import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import Select from "react-select";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import "./styles.css";
import {
  DatePickerField,
  Input,
  Textarea,
  Select as Sel,
  Checkbox,
} from "../proposal-edit/helpers";
import CustomFileInput from "./helpers/CustomFileInput";
import { getJenisPengadilan } from "../../Api";
const {SLICE_URL} = window.ENV;

function DaftarPutusanDetailForm({ proposal, btnRef, saveProposal }) {
    const [bentukPengadilan, setBentukPengadilan] = useState([]);
    const { user } = useSelector((state) => state.auth);
  
    useEffect(() => {
        getJenisPengadilan().then(({ data }) => {
          setBentukPengadilan(data);
        });
      }, []);
    
      return (
        <>
          <Formik
            enableReinitialize={true}
            initialValues={proposal}
          >
            {({ setFieldValue }) => {
              const handleChangeNip = () => {
                setFieldValue("nip_perekam", user.nip9);
              };
    
              return (
                <>
                  <Form className="form form-label-right">
                    <div className="form-group row">
                      <Field
                        name="no_putusan"
                        component={Input}
                        placeholder="Nomor Putusan"
                        label="Nomor Putusan"
                      />
                    </div>
    
                    {/* FIELD TANGGAL SURAT */}
                    <div className="form-group row">
                      <DatePickerField name="tgl_putusan" label="Tanggal Putusan" disabled/>
                    </div>
                    
                    {/* FIELD jenis */}
                    <div className="form-group row">
                      <Sel name="id_detiljns" label="Bentuk Pengadilan" disabled>
                        <option value=""></option>
                        {bentukPengadilan.map((data, index) => (
                          <option key={index} value={data.id}>
                            {data.nama}
                          </option>
                        ))}
                      </Sel>
                    </div>
    
                    {/* FIELD tentang */}
                    <div className="form-group row">
                      <Field
                        name="perihal"
                        component={Textarea}
                        placeholder="Perihal"
                        label="Perihal"
                        disabled
                      />
                    </div>
    
                    {/* FIELD UPLOAD FILE */}
                    <div className="form-group row">
                        <label className="col-xl-3 col-lg-3 col-form-label">
                            File
                        </label>
                    <div
                        className="col-lg-9 col-xl-6"
                        style={{ marginTop: "10px" }}
                    >
                        <a
                        href={proposal.file_upload}
                        target="_blank"
                        rel="noopener noreferrer"
                        >
                        {proposal.file_upload.slice(SLICE_URL)}
                        </a>
                  </div>
                </div>
                  </Form>
                </>
              );
            }}
          </Formik>
        </>
      );
    }

export default DaftarPutusanDetailForm;