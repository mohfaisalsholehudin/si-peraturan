import React, { useEffect, useState, useRef } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../../_metronic/_partials/controls";

import { useSubheader } from "../../../../../../_metronic/layout";
import RegulasiTerkaitForm from "./RegulasiTerkaitForm";
import RegulasiTerkaitFooter from "./RegulasiTerkaitFooter";
import { getPutusanById, getPeraturanTerkaitPutusan } from "../../../Api";
import swal from "sweetalert";

const initValues = {
    id_putpengadilan: "",
    no_putusan: "",
    id_detiljns: "",
    perihal: "",
    file_upload: "",
    alasan_tolak: "",
    body: "",
    nip_pengusul: "",
    nip_pjbt_es4: "",
    nip_pjbt_es3: "",
    status: "",
  };

function PutusanTerkait({
  history,
  match: {
    params: { id }
  }
}) {
  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [isDisabled, setIsDisabled] = useState();
  const [proposal, setProposal] = useState();
  const [peraturanTerkait, setPeraturanTerkait] = useState([]);

  useEffect(() => {
    let _title = "Tambah Peraturan Terkait";

    setTitle(_title);
    suhbeader.setTitle(_title);

    getPutusanById(id).then(({ data }) => {

        setProposal({
          id_putpengadilan: data.id_putpengadilan,
          no_putusan: data.no_putusan,
          tgl_putusan: data.tgl_putusan,
          id_detiljns: data.id_detiljns,
          perihal: data.perihal,
          file_upload: data.file_upload,
          alasan_tolak: data.alasan_tolak,
          body: data.body,
          nip_pengusul: data.nip_pengusul,
          nip_pjbt_es4: data.nip_pjbt_es4,
          nip_pjbt_es3: data.nip_pjbt_es3,
          wkt_usul: data.wkt_usul,
          wkt_teliti_es4: data.wkt_teliti_es4,
          wkt_teliti_es3: data.wkt_teliti_es3,
          status: data.status,
          wkt_create: data.wkt_create,
          wkt_update: data.wkt_update,
          kd_kantor: data.kd_kantor,
          kd_unit_org: data.kd_unit_org,
          });


      });


  }, [id, suhbeader]);


  useEffect(() => {

    getPeraturanTerkaitPutusan(id).then(({ data }) => {
        //console.log(data)
      data.map((dt) => {
        setPeraturanTerkait(peraturanTerkait => [...peraturanTerkait, {
          id_per_terkait: dt.kmperaturanTerkait.id_per_terkait,
          id_peraturan: dt.kmregulasiPerpajakan.id_peraturan,
          no_regulasi: dt.kmregulasiPerpajakan.no_regulasi,
          tgl_regulasi: dt.kmregulasiPerpajakan.tgl_regulasi,
          perihal: dt.kmregulasiPerpajakan.perihal,
          status: dt.kmregulasiPerpajakan.status,
          file_upload: dt.kmregulasiPerpajakan.file_upload,
          jns_regulasi: dt.kmregulasiPerpajakan.jns_regulasi,
          id_topik: dt.kmregulasiPerpajakan.id_topik,
          body: dt.kmregulasiPerpajakan.body,

        }]);

      })

    });



  }, []);


  //console.log(peraturanTerkait);



  const btnRef = useRef();

  const saveRegulasiTerkait = values => {
  };


  //console.log(peraturanTerkait)





  const backToProposalList = () => {
    history.push(`/knowledge/putusan/usulan`);
  };

  const setDisabled = val => {
    setIsDisabled(val);
  };
  return (
    <Card>
      {/* {actionsLoading && <ModalProgressBar />} */}
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <RegulasiTerkaitForm
            actionsLoading={actionsLoading}
            proposal={proposal || initValues}
            peraturanTerkait={peraturanTerkait}
            idPutusan = {id}
            btnRef={btnRef}
            saveRegulasiTerkait={saveRegulasiTerkait}
            setDisabled={setDisabled}
          />
        </div>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <RegulasiTerkaitFooter
          backAction={backToProposalList}
          btnRef={btnRef}
        ></RegulasiTerkaitFooter>
      </CardFooter>
    </Card>
  );
}

export default PutusanTerkait;
