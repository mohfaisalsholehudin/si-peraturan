import React from "react";
import {FieldFeedbackLabel} from "./FieldFeedbackLabel";

const getFieldCSSClasses = (touched, errors) => {
  const classes = ["form-control form-control-lg"];
  if (touched && errors) {
    classes.push("is-invalid");
  }

  if (touched && !errors) {
    classes.push("is-valid");
  }

  return classes.join(" ");
};


export function Checkbox({
  field, // { name, value, onChange, onBlur }
  form: { touched, errors, handleChange, values }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
  label,
  key,
  content,
  withFeedbackLabel = true,
  customFeedbackLabel,
  type = "checkbox",
  check,
  ...props
}) {
// console.log(touched[field.name])
  return (
    <>
      <label key={key} className="checkbox">
      <input
        type={type}
        className={getFieldCSSClasses(touched[field.name], errors[field.name])}
        checked={values[field.name]}
        onChange={handleChange}
        {...field}
        {...props}
      />
      <span></span>{content}
      </label>
      {withFeedbackLabel && (
        <FieldFeedbackLabel
          error={errors[field.name]}
          touched={touched[field.name]}
          label={label}
          type={type}
          customFeedbackLabel={customFeedbackLabel}
        />
      )}
    </>
  );
}
