/* Library */
import React, { useEffect, useState } from "react";
import { useHistory, Route } from "react-router-dom";
import { useSelector } from "react-redux";
import swal from "sweetalert";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import SVG from "react-inlinesvg";

/* Helpers */
import { Pagination } from "../../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../../_metronic/_helpers";
import { toAbsoluteUrl } from "../../../../../_metronic/_helpers";
import * as columnFormatters from "../../column-formatters";
import {
  deletePutusanById,
  getPutusanNonTerimaByNip,
  updateStatusPutusan,
} from "../../Api";
import PutusanHistory from "../PutusanHistory";
import PutusanReject from "../PutusanReject";

function PutusanProposalTable() {
  const { user } = useSelector((state) => state.auth);
  const history = useHistory();
  const [proposal, setProposal] = useState([]);

  const addProposal = () => history.push("/knowledge/putusan/usulan/add");

  const showHistoryPengajuan = (id) =>
    history.push(`/knowledge/putusan/usulan/${id}/history`);

  const openAddPerTerkait = (id) =>
    history.push(`/knowledge/putusan/usulan/${id}/terkait`);

  const rejectPutusan = (id) =>
    history.push(`/knowledge/putusan/usulan/${id}/reject`);

  const editProposal = (id) =>
    history.push(`/knowledge/putusan/usulan/${id}/edit`);

  const detailProposal = (id) =>
    history.push(`/knowledge/putusan/usulan/${id}/detail`);

  const addBody = (id) => history.push(`/knowledge/putusan/usulan/${id}/body`);

  const applyProposal = (id) => {
    swal({
      title: "Apakah Anda Ingin Mengajukan Usulan Ini?",
      text: "Klik OK untuk melanjutkan",
      icon: "warning",
      buttons: true,
      dangerMode: false,
    }).then((willApply) => {
      if (willApply) {
        updateStatusPutusan(id, 1, 2, user.nip9).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Usulan berhasil diajukan", "success").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/putusan/usulan");
            });
          } else {
            swal("Gagal", "Usulan gagal diajukan", "error").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/putusan/usulan");
            });
          }
        });
      }
    });
  };

  const deleteAction = (id) => {
    swal({
      title: "Apakah Anda Ingin Menghapus Usulan Ini?",
      text: "Klik OK untuk melanjutkan",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        deletePutusanById(id).then(({ status }) => {
          if (status === 200) {
            swal("Berhasil", "Usulan berhasil dihapus", "success").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/putusan/usulan");
            });
          } else {
            swal("Gagal", "Usulan gagal dihapus", "error").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/putusan/usulan");
            });
          }
        });
      }
    });
  };

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "putusanPengadilan.no_putusan",
      text: "No Putusan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "putusanPengadilan.tgl_putusan",
      text: "Tgl Putusan",
      sort: true,
      //formatter: columnFormatters.DateFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "bentukPengadilan.nama",
      text: "Bentuk Pengadilan",
      sort: true,
      //formatter: columnFormatters.DateFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
    },

    {
      dataField: "putusanPengadilan.perihal",
      text: "perihal",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "putusanPengadilan.status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.StatusPutusanColumnFormatter,
      headerSortingClasses,
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.PutusanProposalActionColumnFormatter,
      formatExtraData: {
        openEditDialog: editProposal,
        openDeleteDialog: deleteAction,
        showHistory: showHistoryPengajuan,
        detailProposal: detailProposal,
        openAddPerTerkait: openAddPerTerkait,
        addBody: addBody,
        rejectPutusan: rejectPutusan,
        applyProposal: applyProposal,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];

  const initialFilter = {
    sortOrder: "desc", // asc||desc
    sortField: "wkt_update",
    pageNumber: 1,
    pageSize: 50,
  };

  const defaultSorted = [{ dataField: "wkt_update", order: "desc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: proposal.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;

  useEffect(() => {
    getPutusanNonTerimaByNip(user.nip9).then(({ data }) => {
      // setPlan(data);
      data.map((data) => {
        return data.status !== "Terima"
          ? setProposal((proposal) => [...proposal, data])
          : // : data.status === "Draft"
            // ? setProposal((proposal) => [...proposal, data])
            null;
      });
    });
  }, [user.kantorLegacyKode]);
  console.log(user);

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="putusanPengadilan.id_putpengadilan"
                  data={proposal}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <button
                            type="button"
                            className="btn btn-primary"
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                              float: "right",
                            }}
                            onClick={addProposal}
                          >
                            <span className="svg-icon menu-icon">
                              <SVG
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Code/Plus.svg"
                                )}
                              />
                            </span>
                            Tambah
                          </button>
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
      <Route path="/knowledge/putusan/usulan/:id/reject">
        {({ history, match }) => (
          <PutusanReject
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/putusan/usulan");
            }}
            onRef={() => {
              history.push("/knowledge/putusan/usulan");
            }}
          />
        )}
      </Route>
      <Route path="/knowledge/putusan/usulan/:id/history">
        {({ history, match }) => (
          <PutusanHistory
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/putusan/usulan");
            }}
            onRef={() => {
              history.push("/knowledge/putusan/usulan");
            }}
          />
        )}
      </Route>
    </>
  );
}

export default PutusanProposalTable;
