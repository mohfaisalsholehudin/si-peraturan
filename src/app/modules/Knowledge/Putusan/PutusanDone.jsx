import React from "react";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../_metronic/_partials/controls";
import PutusanDoneTable from "./PutusanDoneTable";

function PutusanDone() {
  return (
    <>
      <Card>
        <CardHeader
          title="Monitoring Putusan Pengadilan"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <PutusanDoneTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default PutusanDone;
