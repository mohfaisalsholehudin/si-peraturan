import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { Field, Formik, Form } from "formik";
import "./styles.css";
import {
  DatePickerField,
  Input,
  Textarea,
  Select as Sel,
} from "../proposal-edit/helpers";
import { getFgd, getJenisPajak, getFgdById, getDokumenPer } from "../../Api";
// import "./decoupled.css";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import Editor from "ckeditor5-custom-build/build/ckeditor";
const {SLICE_URL} = window.ENV;

// import RegulasiTerkaitTableDetail from "./RegulasiTerkaitTableDetail";
// import DokumenLainTableDetail from "./DokumenLainTableDetail";

function FgdPenelitianDetailForm({ proposal, btnRef, saveFgd, idFgd }) {
  const [Fgd, setFgd] = useState([]);
  const [jenisPajak, setJenisPajak] = useState([]);
  const [propbody, setPropbody] = useState("");
  const [dokLain, setDoklain] = useState([]);
  const { user } = useSelector((state) => state.auth);
  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  useEffect(() => {
    // getFgd().then(({ data }) => {
    //   setFgd(data);
    // });

    getJenisPajak().then(({ data }) => {
      setJenisPajak(data);
    });
  }, []);

  useEffect(() => {
    getFgdById(idFgd).then(({ data }) => {
      if (data.body) {
        setPropbody(JSON.parse(data.body));
      }
    });
  }, [idFgd]);
  useEffect(() => {
    getDokumenPer(idFgd).then(({ data }) => {
      setDoklain(data);
    });
  }, [idFgd]);

  //console.log(dokLain);

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={proposal}
        onSubmit={(values) => {
          //console.log(values);
          saveFgd(values);
        }}
      >
        {({ handleSubmit, setFieldValue, values }) => {
          const handleChangeNip = () => {
            setFieldValue("nip_perekam", user.nip9);
          };

          return (
            <>
              <Form className="form form-label-right">
                {/* FIELD NO SURAT */}
                <div className="form-group row">
                  <Field
                    disabled
                    name="no_nd"
                    component={Input}
                    placeholder="Nomor ND FGD"
                    label="Nomor ND FGD"
                    // onClick={()=>handleChangeNip()}
                  />
                </div>
                {/* FIELD TANGGAL SURAT */}
                <div className="form-group row">
                  <DatePickerField
                    name="tgl_nd"
                    label="Tanggal ND FGD"
                    disabled
                  />
                </div>
                {/* FIELD jenis */}
                {/* <div className="form-group row">
                  <Sel name="jns_regulasi" label="Jenis Regulasi" disabled>
                    {jenisPeraturan.map((data, index) => (

                      <option  key = {index} value={data.id_jnsperaturan}>{data.nm_jnsperaturan}</option>

                    ))}
                  </Sel>
                </div> */}
                {/* FIELD tentang */}
                <div className="form-group row">
                  <Field
                    name="perihal"
                    component={Textarea}
                    placeholder="Perihal"
                    label="Perihal"
                    disabled
                  />
                </div>
                <div className="form-group row">
                  <Sel name="id_jnspajak" label="Jenis Pajak" disabled>
                    {jenisPajak.map((data, index) => (
                      <option key={index} value={data.id_jnspajak}>
                        {data.nm_jnspajak}
                      </option>
                    ))}
                  </Sel>
                </div>

                {/* Field FILE */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    File FGD
                  </label>
                  <div
                    className="col-lg-9 col-xl-6"
                    style={{ marginTop: "10px" }}
                  >
                    <a
                      href={values.file_upload}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      {/* {values.file_Fgd} */}
                      {values.file_upload.slice(SLICE_URL)}
                    </a>
                  </div>
                </div>
                {/* 

                <RegulasiTerkaitTableDetail
                  peraturanTerkait={peraturanTerkait}
                  idPeraturan={idPeraturan} />

                <DokumenLainTableDetail
                  dokLain={dokLain}
                  idPeraturan={idPeraturan} /> */}
                <div>
                  <br />
                  <br />
                  <h3>Isi Materi</h3>
                </div>
                <div className="document-editor">
                  <div className="document-editor__toolbar"></div>
                  <div className="document-editor__editable-container">
                    <CKEditor
                      disabled
                      onReady={(editor) => {
                        console.log("Editor is ready to use!", editor);
                        window.editor = editor;

                        // Add these two lines to properly position the toolbar
                        const toolbarContainer = document.querySelector(
                          ".document-editor__toolbar"
                        );
                        toolbarContainer.appendChild(
                          editor.ui.view.toolbar.element
                        );
                      }}
                      config={{
                        removePlugins: ["Heading", "Link"],
                        toolbar: [],
                        isReadOnly: true,
                      }}
                      editor={Editor}
                      //ICAAAAAAAAAAAAAAALLLLLLLLLLLLLLLLLLLLLLLL
                      //INI BUAT NAMBAHIN TEMPLATE NYA DI BAWAH INI PAKAI HTML
                      data={propbody}
                    />
                  </div>
                </div>
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default FgdPenelitianDetailForm;
