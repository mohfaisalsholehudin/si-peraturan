import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Route } from "react-router-dom";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";
import { Pagination } from "../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../../../helpers/column-formatters";
import { useHistory } from "react-router-dom";
import { getPerencanaan, getPerencanaanByAdmin } from "../../Evaluation/Api";
import MonitoringOpen from "./MonitoringOpen";

function MonitoringTable() {
  const history = useHistory();

  const openProposal = id => history.push(`/plan/monitoring/${id}/open`);
  const [monitoring, setMonitoring] = useState([]);
  const { user, role } = useSelector(state => state.auth);
  const super_admin = role.includes("ROLE_SUPER_ADMIN_PERATURAN");


  useEffect(() => {
    if(super_admin){
        getPerencanaanByAdmin().then(({ data }) => {
          data.map(data => {
            return data.no_perencanaan
              ? setMonitoring(monitoring => [...monitoring, data])
              : null;
          });
        })
    } else {
    getPerencanaan(user.kantorLegacyKode).then(({ data }) => {
      data.map(data => {
        return data.no_perencanaan
          ? setMonitoring(monitoring => [...monitoring, data])
          : null;
      });
    });
  }
  }, [super_admin, user.kantorLegacyKode]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.IdColumnFormatter,
      headerSortingClasses
    },
    {
      dataField: "no_perencanaan",
      text: "No Perencanaan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      sortFunc: (a, b, order, dataField) => {
        if (order === 'asc' || !order) {
            return b.localeCompare(a, navigator.languages[0] || navigator.language, {numeric: true, ignorePunctuation: true});
            }
            return a.localeCompare(b, navigator.languages[0] || navigator.language, {numeric: true, ignorePunctuation: true});
            },
    },
    {
      dataField: "no_evaluasi",
      text: "No Evaluasi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      sortFunc: (a, b, order, dataField) => {
        if (order === 'asc' || !order) {
            return b.localeCompare(a, navigator.languages[0] || navigator.language, {numeric: true, ignorePunctuation: true});
            }
            return a.localeCompare(b, navigator.languages[0] || navigator.language, {numeric: true, ignorePunctuation: true});
            },
    },
    {
      dataField: "tgl_perencanaan",
      text: "Tgl Perencanaan",
      sort: true,
      formatter: columnFormatters.ProposalDateColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "jns_peraturan",
      text: "Jenis Peraturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "judul_peraturan",
      text: "Judul Peraturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "file_kajian",
      text: "File Kajian",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.ProposalFileColumnFormatter,
      headerSortingClasses
    },
    {
      dataField: "nm_perekam",
      text: "Konseptor",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "unit_incharge",
      text: "Seksi Konseptor",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "wkt_update",
      text: "Waktu Update",
      sort: true,
      hidden: true,
      formatter: columnFormatters.DateFormatterWaktuUpdate,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterPlanMonitoring,
      formatExtraData: {
        showProposal: openProposal
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];
  const initialFilter = {
    sortOrder: "desc", // asc||desc
    sortField: "wkt_update",
    pageNumber: 1,
    pageSize: 50
  };
  const defaultSorted = [{ dataField: "wkt_update", order: "desc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: monitoring.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_perencanaan"
                  data={monitoring}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
      {/*Route to Open Details*/}
      <Route path="/plan/monitoring/:id/open">
        {({ history, match }) => (
          <MonitoringOpen
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/plan/monitoring");
            }}
            onRef={() => {
              history.push("/plan/monitoring");
            }}
          />
        )}
      </Route>
    </>
  );
}

export default MonitoringTable;
