import React, { useEffect, useState, useRef } from "react";
import { useSelector } from "react-redux";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";
import { useSubheader } from "../../../../../_metronic/layout";
import ProposalForm from "./ProposalForm";
import {
  getPerencanaanById,
  getValidasiById,
  getValidasiSort,
  getValidasiUsulan,
  getValidasiUsulanKantor,
  savePerencanaan,
  updatePerencanaan,
  uploadFile,
  uploadZip
} from "../../../Evaluation/Api";
import Select from "react-select";
import swal from "sweetalert";

function ProposalEdit({
  history,
  match: {
    params: { id }
  }
}) {
  // Subheader
  const suhbeader = useSubheader();
  const { user } = useSelector(state => state.auth);
  const [loading, setLoading] = useState(false);


  const [title, setTitle] = useState("");
  const [actionsLoading, setActionsLoading] = useState(true);
  const [proposal, setProposal] = useState([]);
  const [isDisabled, setIsDisabled] = useState();
  const [content, setContent] = useState();
  const [evaluasi, setEvaluasi] = useState([]);
  const [show, setShow] = useState(false);

  const initValues = {
    no_evaluasi: "",
    tgl_perencanaan: "",
    jns_peraturan: "",
    judul_peraturan: "",
    simfoni: "Tidak",
    unit_incharge: "",
    file: "",
    jns_pajak: "",
    isu_masalah: "",
    no_peraturan: "",
    tentang: "",
    konten_peraturan: "",
    alasan: "",
    analisa_dampak: "",
    id_usulan: "",
    id_validator: "",
    nip_perekam: "",
    nm_perekam: "",
    kd_kantor : user.kantorLegacyKode,
    kd_unit_org: user.unitEs4LegacyKode,
  };

  const getCurrentDate = () => {
    const date = new Date();
    return date;
  };
  const enableLoading = () => {
    setLoading(true);
  };

  const disableLoading = () => {
    setLoading(false);
  };

  const handleChangeEvaluation = val => {
    getValidasiById(val.value).then(({ data }) => {
      setContent({
        no_evaluasi: data.no_evaluasi,
        tgl_perencanaan: getCurrentDate(),
        jns_peraturan: data.jns_peraturan,
        judul_peraturan: data.judul_peraturan,
        unit_incharge: data.unit_incharge,
        file_upload: data.file_upload,
        jns_pajak: data.usulan.jns_pajak.split(","),
        isu_masalah: data.isu_masalah,
        no_peraturan: data.no_peraturan,
        tentang: data.tentang,
        konten_peraturan: data.konten_peraturan,
        alasan: data.alasan,
        analisa_dampak: data.analisa_dampak,
        id_usulan: data.id_usulan,
        id_validator: data.id_validator,
        nip_perekam: user.nip9,
        kd_kantor: data.kd_kantor,
        nm_perekam: data.nm_pegawai,
        kd_unit_org: data.kd_unit_org,
        simfoni: "Tidak"
      });
      setShow(true);
    });
  };


  useEffect(() => {
    let _title =
      "Tambah Pembuatan Usulan Perencanaan Penyusunan Regulasi Perpajakan";

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps

    // getCurrentDate();
    if (id) {
      getPerencanaanById(id).then(({ data }) => {
        setContent({
          no_evaluasi: data.no_evaluasi,
          tgl_perencanaan: data.tgl_perencanaan,
          jns_peraturan: data.jns_peraturan,
          judul_peraturan: data.judul_peraturan,
          unit_incharge: data.unit_incharge,
          file_upload: data.file_kajian,
          simfoni: data.simfoni,
          jns_pajak: data.jns_pajak.split(","),
          isu_masalah: data.isu_masalah,
          no_peraturan: data.no_peraturan,
          tentang: data.tentang,
          konten_peraturan: data.konten_peraturan,
          alasan: data.alasan,
          analisa_dampak: data.analisa_dampak,
          id_usulan: data.id_usulan,
          id_validator: data.id_validator,
          nip_perekam: data.nip_perekam,
          kd_kantor: data.kd_kantor, 
          nm_perekam: data.nm_perekam,
          kd_unit_org: data.kd_unit_org
        });
      });
      setShow(true);
    }
  }, [suhbeader, id]);

  useEffect(() => {
    getValidasiSort(user.kantorLegacyKode, 'Terima').then(({ data }) => {
      // console.log(data)
      // data.map(data => {
      //   setEvaluasi(evaluasi => [...evaluasi, {label}]
      //   })
      // })
      data.map(data => {
        if (data.no_evaluasi) {
          setEvaluasi(evaluasi => [
            ...evaluasi,
            {
              label: data.no_evaluasi,
              value: data.id_validator
            }
          ]);
        }
      });
    });
  }, [user.kantorLegacyKode]);
  const btnRef = useRef();
  const savePlan = values => {
    if (!id) {
      if (values.file.name) {
      enableLoading();
        const formData = new FormData();
        formData.append("file", values.file);
        uploadZip(formData).then(({ data }) => {
          disableLoading();
          savePerencanaan(
            values.alasan,
            values.analisa_dampak,
            data.message,
            values.id_usulan,
            values.id_validator,
            values.isu_masalah,
            values.jns_pajak.toString(),
            values.jns_peraturan,
            values.judul_peraturan,
            values.konten_peraturan,
            values.nip_perekam,
            values.no_evaluasi,
            values.no_peraturan,
            values.simfoni,
            values.tentang,
            values.tgl_perencanaan,
            values.unit_incharge,
            values.kd_kantor,
            values.nm_perekam,
            values.kd_unit_org
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
                history.push("/plan/proposal");
              });
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                history.push("/plan/proposal/add");
              });
            }
          });
        }).catch((e)=> {
          disableLoading();
          alert(e);
        });
      } else {
        savePerencanaan(
          values.alasan,
          values.analisa_dampak,
          values.file_upload,
          values.id_usulan,
          values.id_validator,
          values.isu_masalah,
          values.jns_pajak.toString(),
          values.jns_peraturan,
          values.judul_peraturan,
          values.konten_peraturan,
          values.nip_perekam,
          values.no_evaluasi,
          values.no_peraturan,
          values.simfoni,
          values.tentang,
          values.tgl_perencanaan,
          values.unit_incharge,
          values.kd_kantor,
          values.nm_perekam,
          values.kd_unit_org
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push("/plan/proposal");
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push("/plan/proposal/add");
            });
          }
        });
      }
    } else {
      if (values.file.name) {
      enableLoading();
        const formData = new FormData();
        formData.append("file", values.file);
        uploadZip(formData).then(({ data }) => {
          disableLoading();
          updatePerencanaan(
            id,
            values.alasan,
            values.analisa_dampak,
            data.message,
            values.id_usulan,
            values.id_validator,
            values.isu_masalah,
            values.jns_pajak.toString(),
            values.jns_peraturan,
            values.judul_peraturan,
            values.konten_peraturan,
            values.nip_perekam,
            values.no_evaluasi,
            values.no_peraturan,
            values.simfoni,
            values.tentang,
            values.tgl_perencanaan,
            values.unit_incharge,
            values.kd_kantor,
            values.nm_perekam,
            values.kd_unit_org
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
                history.push("/plan/proposal");
              });
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                history.push("/plan/proposal/add");
              });
            }
          });
        }).catch((e)=> {
          disableLoading();
          alert(e);
        });
      } else {
        updatePerencanaan(
          id,
          values.alasan,
          values.analisa_dampak,
          values.file_upload,
          values.id_usulan,
          values.id_validator,
          values.isu_masalah,
          values.jns_pajak.toString(),
          values.jns_peraturan,
          values.judul_peraturan,
          values.konten_peraturan,
          values.nip_perekam,
          values.no_evaluasi,
          values.no_peraturan,
          values.simfoni,
          values.tentang,
          values.tgl_perencanaan,
          values.unit_incharge,
          values.kd_kantor,
          values.nm_perekam,
          values.kd_unit_org
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push("/plan/proposal");
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push("/plan/proposal/add");
            });
          }
        });
      }
    }
  };
  const backToProposalList = () => {
    history.push(`/plan/proposal`);
  };

  const setDisabled = val => {
    setIsDisabled(val);
  };

  const changeShow = () => {
    setShow(false);
  };

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        {show ? (
          <div className="mt-5">
            <ProposalForm
              actionsLoading={actionsLoading}
              validate={content || initValues}
              btnRef={btnRef}
              savePlan={savePlan}
              setDisabled={setDisabled}
              handleChangeEvaluation={handleChangeEvaluation}
              setShow={changeShow}
              isEdit={id ? true : false}
              loading={loading}
            />
          </div>
        ) : (
          <div className="form-group row">
            <label className="col-xl-3 col-lg-3 col-form-label">
              No Evaluasi
            </label>
            <div className="col-lg-9 col-xl-6">
              <Select
                options={evaluasi}
                onChange={value => handleChangeEvaluation(value)}
              />
            </div>
          </div>
        )}
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}></CardFooter>
    </Card>
  );
}

export default ProposalEdit;
