import React, { useEffect, useState }  from "react";
import { Modal, Table } from "react-bootstrap";
import { getLogPerencanaan, getLogUsulan, getPerencanaanById, getUsulanById } from "../../Evaluation/Api";
import {DateFormat, DateTimeFormat} from "../../../helpers/DateFormat"

function ProposalReject({ id, show, onHide }) {
  const [proposal, setProposal] = useState([]);
  const [data, setData] = useState([]);
  const [log, setLog] = useState([]);

  useEffect(() => {
    if (id) {
      getPerencanaanById(id).then(({ data }) => {
        setData(data.alasan_tolak)
        getUsulanById(data.id_usulan).then(({ data }) => {
          setProposal(data);
        })
      });

      getLogPerencanaan(id).then(({ data }) => {
        setLog(data);
      });
    }
  }, [id]);

  const showStatus = () => {
    return log.map((data, index) => {
      const date = new Date(data.wkt_proses);
      const tanggal = String(date.getDate()).padStart(2, "0");
      var bulan = String(date.getMonth() + 1).padStart(2, "0");
      const tahun = date.getFullYear();

      const jam = date.getHours();
      const menit = date.getMinutes();

      const tampilTanggal =
        tanggal + "-" + bulan + "-" + tahun + " - " + jam + ":" + menit;

      return (
        <tr key={index} style={{ borderBottom: "1px solid lightgrey" }}>
          <td key={data.id_log}>{index + 1}</td>
          <td key={data.refStatus.id_status} style={{ textAlign: "left" }}>
          {checkName(data.refStatus.nama)}
          </td>
          <td key={data.wkt_proses} style={{ textAlign: "left" }}>
          {DateTimeFormat(data.wkt_proses)}
          </td>
        </tr>
      );
    });
  };
  const checkName = (name) => {
    switch(name){
      case "Draft":
        return "Input Usulan";
      case "Eselon 4":
        return "Pengajuan Usulan ke Eselon 4"
      case "Tolak":
          return "Tolak Pejabat Terkait"
      case "Eselon 3":
            return "Pengajuan Usulan ke Eselon 3"
      default:
        break;
    }
  }

  return (
    <Modal
      size="md"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Detil Penolakan
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Nomor Surat</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
            {`: ${proposal.no_surat}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Tgl Surat</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
            {`: ${DateFormat(proposal.tgl_surat)}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Perihal</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
            {`: ${proposal.perihal}`}

            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">
              Instansi Penerbit/ Unit Kerja
            </span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
            {`: ${proposal.instansi_unit}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Alamat</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
            {`: ${proposal.alamat}`}

            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Nama PIC</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
            {`: ${proposal.nm_pic}`}

            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Telp PIC</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
            {`: ${proposal.no_pic}`}

            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Alasan Penolakan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
            {`: ${data}`}
            </span>
          </div>
        </div>
        {/* Status Pengajuan Begin */}
        <div className="row">
          <Table responsive hover>
            <thead
              style={{
                border: "1px solid #3699FF",
                textAlign: "center",
                backgroundColor: "#3699FF"
              }}
            >
              <tr>
                <th>NO</th>
                <th style={{ textAlign: "left" }}>STATUS</th>
                <th style={{ textAlign: "left" }}>TANGGAL</th>
              </tr>
            </thead>
            <tbody
              style={{ border: "1px solid lightgrey", textAlign: "center" }}
            >
              {showStatus()}
             
            </tbody>
            <tfoot
              style={{
                border: "1px solid #3699FF",
                textAlign: "center",
                backgroundColor: "#3699FF"
              }}
            >
              <tr style={{ height: "40px" }}>
                <td></td>
                <td></td>
                <td></td>
              </tr>
            </tfoot>
          </Table>
        </div>
        {/* Status Pengajuan End */}
      </Modal.Body>
      <Modal.Footer style={{ borderTop: "none" }}>
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          <button
            type="button"
            // onClick={backAction}
            onClick={onHide}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="flaticon2-cancel icon-nm"></i>
            Tutup
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}

export default ProposalReject;
