import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
import ProposalTable from "./ProposalTable";

function Proposal() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Perencanaan Penyusunan Peraturan"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          {/* <div className="row"> */}
            <ProposalTable />
          {/* </div> */}
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default Proposal;
