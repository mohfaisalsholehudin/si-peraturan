// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
import React from "react";

export function StatusColumnFormatter(cellContent, row) {
  const CustomerStatusCssClasses = ["warning", "success", "danger", ""];
  const CustomerStatusTitles = ["Kabid", "Draft", "Tolak", ""];
  let status = "";

  switch (row.status) {
    case "Kabid":
      status = 0;
      break;

    case "Draft":
      status = 1;
      break;

    case "Tolak":
      status = 2;
      break;

    default:
      status = 3;
  }
  const getLabelCssClasses = () => {
    if(row.status === ""){
      return '';
    } else {
      return `label label-lg label-light-${CustomerStatusCssClasses[status]} label-inline`;
    }
    
  };
  return (
    <span className={getLabelCssClasses()}>{CustomerStatusTitles[status]}</span>
  );
}
