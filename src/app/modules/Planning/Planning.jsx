import React from "react";
import { useSelector } from "react-redux";
import { Switch } from "react-router-dom";
import { ContentRoute } from "../../../_metronic/layout";
import Proposal from "./Proposal/Proposal";
import ProposalEdit from "./Proposal/Edit/ProposalEdit";
import Research from "./Research/Research";
import Monitoring from "./Monitoring/Monitoring";
import EvaluationErrors from "../Evaluation/EvaluationErrors";

export default function Planning() {
  const { role } = useSelector(state => state.auth);
  const konseptor = role.includes("ROLE_PERATURAN_KONSEPTOR");
  const es4 = role.includes("ROLE_PERATURAN_PENELITI_LVL1");
  const es3 = role.includes("ROLE_PERATURAN_PENELITI_LVL2");
  const admin = role.includes("ROLE_ADMIN_PERATURAN");

  return (
    <Switch>
      {/* Proposal */}
      <ContentRoute
        path="/plan/proposal/add"
        component={
          admin || konseptor  ? ProposalEdit : EvaluationErrors
        }
      />
      <ContentRoute
        path="/plan/proposal/:id/edit"
        component={
          admin || konseptor  ? ProposalEdit : EvaluationErrors
        }
      />
      <ContentRoute
        path="/plan/proposal"
        component={
          admin || konseptor ? Proposal : EvaluationErrors
        }
      />

      {/* Research */}
      <ContentRoute
        path="/plan/research"
        component={admin || es4 || es3 ? Research : EvaluationErrors}
      />

      {/* Monitoring */}
      <ContentRoute
        path="/plan/monitoring"
        component={
          admin || konseptor || es4 || es3 ? Monitoring : EvaluationErrors
        }
      />
    </Switch>
  );
}
