import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import { Route } from "react-router-dom";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";
import React, { useEffect, useState } from "react";
import {
  sortCaret,
  headerSortingClasses
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../../../helpers/column-formatters";
import { Pagination } from "../../../helpers/pagination/Pagination";

import ResearchOpen from "./ResearchOpen";
import { getPerencanaanByAdmin, getPerencanaanSeksi, getPerencanaanSubdit } from "../../Evaluation/Api";

function ResearchTable() {
  const history = useHistory();
  const [research, setResearch] = useState([]);
  const { user, role } = useSelector(state => state.auth);
  const es3 = role.includes("ROLE_PERATURAN_PENELITI_LVL2");
  const super_admin = role.includes("ROLE_SUPER_ADMIN_PERATURAN");

  const openProposal = id => history.push(`/plan/research/${id}/open`);
  const rejectProposal = id => history.push(`/plan/research/${id}/reject`);
  const editProposal = id => history.push(`/plan/research/${id}/edit`);

  useEffect(() => {
    if(es3){
      getPerencanaanSubdit(user.unitEs4LegacyKode).then(({ data }) => {
        data.map(data => {
          if(data.status === 'Eselon 3' ){
            setResearch(research=>[...research, data])
          }
        });
      });
    } else if (super_admin) {
        getPerencanaanByAdmin().then(({ data }) => {
          data.map((data)=> {
            if(data.status === 'Eselon 3' || data.status === 'Eselon 4'){
              setResearch(research => [...research, data])

            }
          })
        })
    } else {
      getPerencanaanSeksi(user.unitEs4LegacyKode).then(({ data }) => {
        data.map(data => {
          if(user.jabatan === 'Kepala Seksi' ? data.status === 'Eselon 4' : data.status === 'Eselon 3' ){
            setResearch(research=>[...research, data])
          }
        });
      });
    }
  }, [super_admin, es3, user.jabatan, user.kantorLegacyKode, user.unitEs4LegacyKode]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.IdColumnFormatter,
      headerSortingClasses
    },
    {
      dataField: "no_evaluasi",
      text: "No Evaluasi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "tgl_perencanaan",
      text: "Tgl Perencanaan",
      sort: true,
      formatter: columnFormatters.ProposalDateColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "jns_peraturan",
      text: "Jenis Peraturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "judul_peraturan",
      text: "Judul Peraturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "file_kajian",
      text: "File Kajian",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.ProposalFileColumnFormatter,
      headerSortingClasses
    },
    {
      dataField: "wkt_update",
      text: "Waktu Update",
      sort: true,
      hidden: true,
      formatter: columnFormatters.DateFormatterWaktuUpdate,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterPlanResearch,
      formatExtraData: {
        openEditDialog: editProposal,
        // openDeleteDialog: deleteAction,
        showProposal: openProposal,
        showReject: rejectProposal
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];
  const initialFilter = {
    sortOrder: "desc", // asc||desc
    sortField: "wkt_update",
    pageNumber: 1,
    pageSize: 50
  };
  const { SearchBar } = Search;
  const defaultSorted = [{ dataField: "wkt_update", order: "desc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: research.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_perencanaan"
                  data={research}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>

      {/*Route to Open Details*/}
      <Route path="/plan/research/:id/open">
        {({ history, match }) => (
          <ResearchOpen
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/plan/research");
            }}
            onRef={() => {
              history.push("/plan/research");
            }}
          />
        )}
      </Route>
      {/*Route to Delete Apps*/}
      {/* <Route path="/apps/:id/delete">
            {({ history, match }) => (
                <AppsDeleteDialog
                    show={match != null}
                    id={match && match.params.id}
                    onHide={() => {
                        history.push("/apps");
                    }}
                    onRef={() => {
                        history.push("/monitoring");
                        history.replace("/apps");
                    }}
                />
            )} 
        </Route> */}
    </>
  );
}

export default ResearchTable;
