import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Modal } from "react-bootstrap";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
import {
  getPerencanaanById,
  getUsulanById,
  updateStatusPerencanaan
} from "../../Evaluation/Api";

import ResearchReject from "./ResearchReject";
import ResearchApprove from "./ResearchApprove";
import { DateFormat } from "../../../helpers/DateFormat";

function ResearchOpen({ id, show, onHide }) {
  const history = useHistory();
  const { user } = useSelector(state => state.auth);
  const [data, setData] = useState([]);
  const [usulan, setUsulan] = useState([]);
  const [isReject, setIsReject] = useState(false);
  const [isApprove, setIsApprove] = useState(false);
  const [isShow, setIsShow] = useState(true);

  useEffect(() => {
    if (id) {
      getPerencanaanById(id).then(({ data }) => {
        setData(data);
        getUsulanById(data.id_usulan).then(({ data }) => {
          setUsulan(data);
        });
      });
    }
  }, [id]);

  const handleReject = () => {
    setIsReject(true);
    setIsShow(false);
  };
  const handleApprove = () => {
    setIsApprove(true);
    setIsShow(false);
  };
  const handleCancel = () => {
    setIsReject(false);
    setIsApprove(false);
    setIsShow(true);
  };

  const reject = val => {
    updateStatusPerencanaan(
      id,
      5,
      user.kantorLegacyKode,
      val.alasan_penolakan
    ).then(({ status }) => {
      if (status === 201 || status === 200) {
        swal("Berhasil", "Data berhasil ditolak", "success").then(() => {
          history.push("/dashboard");
          history.replace("/plan/research");
        });
      } else {
        swal("Gagal", "Data gagal ditolak", "error").then(() => {
          history.push("/dashboard");
          history.replace("/plan/research");
        });
      }
    });
  };
  const approve = val => {
    updateStatusPerencanaan(
      id,
      user.jabatan === "Kepala Seksi" ? 3 : 6,
      user.kantorLegacyKode,
      null,
      val.catatan
    ).then(({ status }) => {
      if (status === 201 || status === 200) {
        swal("Berhasil", "Data berhasil diajukan", "success").then(() => {
          history.push("/dashboard");
          history.replace("/plan/research");
        });
      } else {
        swal("Gagal", "Data gagal diajukan", "error").then(() => {
          history.push("/dashboard");
          history.replace("/plan/research");
        });
      }
    });
  };
  return (
    <Modal
      size="md"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Detil Usulan Perencanaan
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Nomor Surat</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${usulan.no_surat}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Tgl Surat</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${DateFormat(usulan.tgl_surat)}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Perihal</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${usulan.perihal}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">
              Instansi Penerbit/ Unit Kerja
            </span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${usulan.instansi_unit}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-9 col-xl-6">
            <h5 className="mb-6 mt-6" style={{ fontWeight: "600" }}>
              ISU PERMASALAHAN
            </h5>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">No Evaluasi</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${data.no_evaluasi}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Jenis Peraturan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${data.jns_peraturan}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Judul Peraturan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${data.judul_peraturan}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Isu Permasalahan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${data.isu_masalah}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Nomor Peraturan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${data.no_peraturan}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Tentang</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${data.tentang}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Konten Peraturan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${data.konten_peraturan}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Alasan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${data.alasan}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">
              Analisa Dampak Kebijakan
            </span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${data.analisa_dampak}`}
            </span>
          </div>
        </div>
        {data.catatan_es4 ? (
          <div className="row">
            <div className="col-lg-6 col-xl-6 mb-3">
              <span className="font-weight-bold mr-2">Catatan</span>
            </div>
            <div className="col-lg-6 col-xl-6 mb-3">
              <span className="text-muted text-hover-primary">
                {`: ${data.catatan_es4}`}
              </span>
            </div>
          </div>
        ) : null}
        {isReject ? (
          <ResearchReject handleCancel={handleCancel} reject={reject} />
        ) : (
          ""
        )}
         {isApprove ? (
          <ResearchApprove
            handleCancel={handleCancel}
            approve={approve}
          />
        ) : (
          ""
        )}
      </Modal.Body>
      {isShow ? (
        <Modal.Footer style={{ borderTop: "none" }}>
          <div className="col-lg-12" style={{ textAlign: "center" }}>
            <button
              type="button"
              onClick={handleReject}
              className="btn btn-danger"
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
            >
              <i className="flaticon2-cancel icon-nm"></i>
              Tolak
            </button>
            {`  `}
            <button
              type="submit"
              className="btn btn-success ml-2"
              onClick={handleApprove}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
            >
              <i className="fas fa-check"></i>
              Setuju
            </button>
          </div>
        </Modal.Footer>
      ) : (
        ""
      )}
    </Modal>
  );
}

export default ResearchOpen;
