/* eslint-disable react-hooks/exhaustive-deps */
/* Library */
import React, { useEffect, useState, useRef } from "react";
import swal from "sweetalert";

/* Helper */
import { useSubheader } from "../../../../../../_metronic/layout";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../../_metronic/_partials/controls";
import DasarHukumTable from "./dasar-hukum/DasarHukumTable";
import TugasFungsiTable from "./tugas-fungsi/TugasFungsiTable";
import UnitOrganisasiTable from "./unit-organisasi/UnitOrganisasiTable";
import ReviewPKPSopForm from "./ReviewPKPSopForm";
import ReviewPKPSopFormTwo from "./ReviewPKPSopFormTwo";
import UploadFileTable from "./upload-file/UploadFileTable";
import KategoriPengetahuanForm from "./kategori-pengetahuan/KategoriPengetahuanForm";
import {getKnowledgebyId, updateKnowledge, getSopbyIdKM, updateSop, updateSopMateri} from "../../../Api"
import DetailPKPSopForm from "./DetailPKPSopForm";
import KategoriPengetahuanDetailForm from "./kategori-pengetahuan/KategoriPengetahuanDetailForm";
import { useSelector } from "react-redux";

function ReviewPKPSopExplicit({
  history,
  match: {
    params: { id_tipe_km, id_km_pro, media, step, jenis  }
  }
}) {

  const [showTable, setShowTable] = useState(false)
  
  let thePath = document.URL;
  const initValues = {
    tipe_konten: media,
    id_probis: "",
    id_sektor: "",
    id_csname: "",
    id_subcase: "",
    id_tipe_km: id_tipe_km,
    id_km_pro: id_km_pro,
    judul: "",
    file: ""
  };

  const getCurrentDate = () => {
    const day = new Date();
    const YYYY = day.getFullYear();
      let MM = day.getMonth()+1;
      let DD = day.getDate();
      let HH = day.getHours() ;
      let mm = day.getMinutes()
      let ss = day.getSeconds();
      if(DD<10)
      {
          DD=`0${DD}`;
      }
      if(MM<10)
      {
          MM=`0${MM}`;
      }

      if(HH<10)
      {
          HH=`0${HH}`;
      }
      if(mm<10)
      {
          mm=`0${mm}`;
      }
      if(ss<10)
      {
          ss=`0${ss}`;
      }
    return YYYY + "-" + MM + "-" + DD + " " + HH + ":" + mm + ":" + ss
  }

  // Subheader
  const subheader = useSubheader();
  const { user } = useSelector((state) => state.auth);
  const [title, setTitle] = useState("");
  const [content, setContent] = useState();
  const [idTemp, setIdTemp] = useState();
  const [isShow, setIsShow] = useState(false);
  const [isShowForm, setIsShowForm] = useState(false)
  const [draft, setDraft] = useState("");
  const [materi, setMateri] = useState();
  const lastPath = thePath.substring(thePath.lastIndexOf("/") + 1);
  
  const checkIdTemp = () => {
    getSopbyIdKM(id_km_pro).then(({ data }) => {
      if (data.length > 0) {
        setIdTemp(data[0].sopTemp.id_sop_temp);
      }
    });
  };

  useEffect(() => {
    if (lastPath == "review") {
      let _title = "Review Knowledge Explicit";
      setTitle(_title);
      subheader.setTitle(_title);
    } else {
      let _title = "Detail Knowledge Explicit";
      setTitle(_title);
      subheader.setTitle(_title);
    }
    getKnowledgebyId(id_km_pro).then(({data}) => {
      getSopbyIdKM(id_km_pro).then(data_2 => {
        setContent({
            tipe_konten: data.tipe_konten,
            id_probis: data.id_probis,
            id_sektor: data.id_sektor,
            id_csname: data.id_csname,
            id_subcase: data.id_subcase,
            id_tipe_km: data.id_tipe_km,
            id_km_pro: data.id_km_pro,
            judul: data.judul,
            media_upload: data.media_upload,
            kategori: data.kategori,
            level_knowledge: data.level_knowledge,
            wkt_pkp: data.wkt_pkp,
            wkt_ko: data.wkt_ko,
            wkt_sme: data.wkt_sme,
            wkt_publish: data.wkt_publish
        });
        setMateri(data_2.data[0].sopTemp.materi);
        })
        checkIdTemp();
    });
  }, [id_km_pro, id_tipe_km, media, subheader])

  const btnRef = useRef();
  const btnPublish = useRef();
  
  const saveButton = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
    }
  };

  const publishButton = () => {
    if (btnPublish && btnPublish.current) {
      btnPublish.current.click();
    }
  };

  const hideButton = () => {
    setIsShow(false);
  };
  const showButton = () => {
    setIsShow(true);
  };
  const hideForm = () => {
    setIsShowForm(false);
  }
  const showForm = () => {
    setIsShowForm(true);
  }

  const backAction = () => {
    if(lastPath === "review"){
      switch (step) {
        case "2":
          history.push(
            `/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/1/review`
          );
          break;
        case "3":
          history.push(
            `/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/2/review`
          );
          break;
        case "4":
          history.push(
            `/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/3/review`
          );
          break;
        case "5":
          history.push(
            `/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/4/review`
          );
          break;
        default:
          break;
      }
    } else {
      switch (step) {
        case "2":
          history.push(
            `/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/1/detail`
          );
          break;
        case "3":
          history.push(
            `/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/2/detail`
          );
          break;
        case "4":
          history.push(
            `/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/3/detail`
          );
          break;
        case "5":
          history.push(
            `/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/4/detail`
          );
          break;
        default:
          break;
      }
    }
  };

  function saveSurat (values) {
    updateSop(
      idTemp,
      content.id_km_pro,
      content.id_tipe_km,
      materi
    ).then(({ status }) => {
      if (status === 201 || status === 200) {
        switch (step) {
          case "1":
            history.push(
              `/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/2/review`
            );
            break;
          case "2":
            history.push(
              `/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/3/review`
            );
            break;
          case "3":
            history.push(
              `/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/4/review`
            );
            break;
          case "4":
            history.push(
              `/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/5/review`
            );
            break;
          case "5":
            history.push(`/process-knowledge/review-pkp/`);
            break;
          default:
            break;
        }
      }
    });
    updateKnowledge(
      values.id_km_pro,
      values.id_csname,
      values.id_ko,
      values.id_probis,
      values.id_sektor,
      values.id_sme,
      values.id_subcase,
      values.id_tipe_km,
      values.judul,
      user.nip18,
      content.nip_ko,
      content.nip_sme,
      content.wkt_ko,
      getCurrentDate(),
      content.wkt_publish,
      content.wkt_sme,
      user.namaPegawai
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
          switch (step) {
          case "1":
            history.push(
              `/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/2/review`
            );
            break;
          case "2":
            history.push(
              `/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/3/review`
            );
            break;
          case "3":
            history.push(
              `/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/4/review`
            );
            break;
          case "4":
            history.push(
              `/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/5/review`
            );
            break;
          case "5":
            history.push(`/process-knowledge/review-pkp/`);
            break;
          default:
            break;
        }
      }
    })
}

const saveFormTwo = () => {
  updateSopMateri(idTemp, draft ? draft : materi).then(({ status }) => {
    if (status === 201 || status === 200) {
      switch (step) {
        case "1":
          history.push(
            `/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/2/review`
          );
          break;
        case "2":
          history.push(
            `/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/3/review`
          );
          break;
        case "3":
          history.push(
            `/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/4/review`
          );
          break;
        case "4":
          history.push(
            `/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/5/review`
          );
          break;
        case "5":
          history.push(`/process-knowledge/review-pkp/`);
          break;
        default:
          break;
      }
    }
  })
}

const nextStep = () => {
  if(lastPath === "review"){
    switch (step) {
      case "3":
        history.push(
          `/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/4/review`
        );
        break;
      case "4":
        history.push(
          `/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/5/review`
        );
        break;
      default:
        break;
    }
  } else {
    switch (step) {
      case "1":
        history.push(
          `/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/2/detail`
        );
        break;
      case "2":
        history.push(
          `/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/3/detail`
        );
        break;
      case "3":
        history.push(
          `/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/4/detail`
        );
        break;
      case "4":
        history.push(
          `/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/5/detail`
        );
        break;
      default:
        break;
    }
  }
};

  const checkStep = val => {
    switch (val) {
      case "1":
        if (lastPath == "review") {
          return (
            <ReviewPKPSopForm
              content={content || initValues}
              btnRef={btnRef}
              saveForm={saveSurat}
              media={media}
            />
          );
          } else {
            return (
            <DetailPKPSopForm
              content={content || initValues}
              btnRef={btnRef}
              saveForm={saveSurat}
              media={media}
            />
            )
          }
      case "2":
        return (
          <ReviewPKPSopFormTwo
            content={content || initValues}
            btnRef={btnRef}
            saveFormTwo={saveFormTwo}
            media={media}
            draft={materi}
            setDraft={setDraft}
            lastPath={lastPath}
          />
        );
      case "3":
        return (
          <>
            <UploadFileTable media={media} step={step} id_km_pro={id_km_pro} jenis={jenis} lastPath={lastPath}/>
            <br></br>
            <DasarHukumTable media={media} step={step} id_km_pro={id_km_pro} jenis={jenis} lastPath={lastPath}/>
          </>
        );
      case "4":
        return (
          <>
            <TugasFungsiTable media={media} step={step} id_km_pro={id_km_pro} jenis={jenis} lastPath={lastPath}/>
            <br></br>
            <UnitOrganisasiTable media={media} step={step} id_km_pro={id_km_pro} jenis={jenis} lastPath={lastPath}/>
          </>
        );
        case "5":
          if(lastPath === "review"){
            return (
              <>
                <KategoriPengetahuanForm
                  content={content}
                  btnRef={btnRef}
                  btnPublish={btnPublish}
                  media={media}
                  step={step}
                  id_km_pro={id_km_pro}
                  jenis={jenis}
                  hideButton={hideButton}
                  showButton={showButton}
                  showForm={showForm}
                  hideForm={hideForm}
                  lastPath={lastPath}
                  showButtonTable={showButtonTable}
                  hideButtonTable={hideButtonTable}
                />
              </>
            );
          } else if (lastPath === "open"){
            return (
              <>
                <KategoriPengetahuanForm
                  content={content}
                  btnRef={btnRef}
                  btnPublish={btnPublish}
                  media={media}
                  step={step}
                  id_km_pro={id_km_pro}
                  jenis={jenis}
                  hideButton={hideButton}
                  showButton={showButton}
                  showForm={showForm}
                  hideForm={hideForm}
                  lastPath={lastPath}
                  showButtonTable={showButtonTable}
                  hideButtonTable={hideButtonTable}
                />
              </>
            );
            }
            else {
              return (
                <>
                  <KategoriPengetahuanDetailForm
                    content={content}
                    btnRef={btnRef}
                    btnPublish={btnPublish}
                    media={media}
                    step={step}
                    id_km_pro={id_km_pro}
                    jenis={jenis}
                    hideButton={hideButton}
                    showButton={showButton}
                    showForm={showForm}
                    hideForm={hideForm}
                    lastPath={lastPath}
                    showButtonTable={showButtonTable}
                    hideButtonTable={hideButtonTable}
                  />
                </>
              );
            }
          default:
            break;
    }
  };
  const showButtonTable = () => {
    setShowTable(true)
}

const hideButtonTable = () => {
  setShowTable(false)
}


  const tolakDialog = id => {
    swal({
      title: "Tolak",
      text: "Apakah Anda yakin menolak usulan ini ?",
      icon: "warning",
      buttons: true
    }).then(ret => {
      if (ret == true) {
        history.push(`/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/tolak`);
      }
    });
  };

  const disposisiDialog = id => {
    swal({
      title: "Disposisi",
      text: "Apakah Anda yakin disposisi usulan ini ?",
      icon: "warning",
      buttons: true
    }).then(ret => {
      if (ret == true) {
        history.push(`/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/disposisi`);
      }
    });
  };

  const homeDialog = id => {
    swal({
      title: "Kembali",
      text: "Apakah Anda Ingin Kembali Ke Halaman Review PKP ?",
      icon: "warning",
      buttons: true
    }).then(ret => {
      if (ret == true) {
        history.push(`/process-knowledge/review-pkp`);
      }
    });
  }

  function tombolDetail(){
    if (step === "5") {
      return (
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          <button
            type="button"
            onClick={backAction}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
          {`  `}
          <button
            type="button"
            className="btn btn-primary ml-2"
            onClick={homeDialog}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fas fa-home"></i>
            Kembali ke Halaman Utama
          </button>
        </div>
      );
    } else {
      if (step === "1") {
        return (
          <div className="col-lg-12" style={{ textAlign: "center" }}>
            <button
              type="submit"
              className="btn btn-success ml-2"
              onClick={nextStep}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
            >
              Selanjutnya {`  `}
              <i className="fa fa-arrow-right"></i>
            </button>
          </div>
        );
      } else if (step === "2"){
        return (
          <div className="col-lg-12" style={{ textAlign: "center" }}>
            <button
              type="button"
              onClick={backAction}
              className="btn btn-light"
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
            >
              <i className="fa fa-arrow-left"></i>
              Kembali
            </button>
            <button
              type="submit"
              className="btn btn-success ml-2"
              onClick={nextStep}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
            >
              Selanjutnya {`  `}
              <i className="fa fa-arrow-right"></i>
            </button>
          </div>
        );
        }
        else if (step === "3"){
          return (
            <div className="col-lg-12" style={{ textAlign: "center" }}>
              <button
                type="button"
                onClick={backAction}
                className="btn btn-light"
                style={{
                  boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                }}
              >
                <i className="fa fa-arrow-left"></i>
                Kembali
              </button>
              <button
                type="submit"
                className="btn btn-success ml-2"
                onClick={nextStep}
                style={{
                  boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                }}
              >
                Selanjutnya {`  `}
                <i className="fa fa-arrow-right"></i>
              </button>
            </div>
          );
        }
      else if (step === "4"){
        return (
          <div className="col-lg-12" style={{ textAlign: "center" }}>
            <button
              type="button"
              onClick={backAction}
              className="btn btn-light"
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
            >
              <i className="fa fa-arrow-left"></i>
              Kembali
            </button>
            <button
              type="submit"
              className="btn btn-success ml-2"
              onClick={nextStep}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
            >
              Selanjutnya {`  `}
              <i className="fa fa-arrow-right"></i>
            </button>
          </div>
        );
      }
    }
  }

  function tombolProses() {
    if (step === "5") {
      return isShow ? (
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          <button
            type="button"
            onClick={backAction}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
          {`  `}
          <button
            type="button"
            className="btn btn-success ml-2"
            onClick={saveButton}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fas fa-save"></i>
            Simpan Draft
          </button>
          {`  `}
          <button
            type="submit"
            className="btn btn-info ml-2"
            onClick={disposisiDialog}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fas fa-share"></i>
            Disposisi
          </button>
        </div>
      ) : (
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          <button
            type="button"
            onClick={backAction}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
          {`  `}
          {showTable ? 
          <>
          <button
            type="button"
            className="btn btn-success ml-2"
            onClick={saveButton}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fas fa-save"></i>
            Simpan Draft
          </button>
          <button
            type="submit"
            className="btn btn-primary ml-2"
            onClick={publishButton}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fas fa-check"></i>
            Siap Publish
          </button>
          {`  `}
          <button
            type="submit"
            className="btn btn-danger ml-2"
            onClick={tolakDialog}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="flaticon2-cancel icon-nm"></i>
            Tolak
          </button>
          </>
          : null}
        </div>
      );
    } else {
      if (step === "1") {
        return (
          <div className="col-lg-12" style={{ textAlign: "center" }}>
            <button
              type="submit"
              className="btn btn-success ml-2"
              onClick={saveButton}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
            >
              Selanjutnya {`  `}
              <i className="fa fa-arrow-right"></i>
            </button>
          </div>
        );
      } else if (step === "2"){
        return (
          <div className="col-lg-12" style={{ textAlign: "center" }}>
            <button
              type="button"
              onClick={backAction}
              className="btn btn-light"
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
            >
              <i className="fa fa-arrow-left"></i>
              Kembali
            </button>
            <button
              type="submit"
              className="btn btn-success ml-2"
              onClick={saveButton}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
            >
              Selanjutnya {`  `}
              <i className="fa fa-arrow-right"></i>
            </button>
          </div>
        );
        }
        else if (step === "3"){
          return (
            <div className="col-lg-12" style={{ textAlign: "center" }}>
              <button
                type="button"
                onClick={backAction}
                className="btn btn-light"
                style={{
                  boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                }}
              >
                <i className="fa fa-arrow-left"></i>
                Kembali
              </button>
              <button
                type="submit"
                className="btn btn-success ml-2"
                onClick={nextStep}
                style={{
                  boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                }}
              >
                Selanjutnya {`  `}
                <i className="fa fa-arrow-right"></i>
              </button>
            </div>
          );
        }
      else if (step === "4"){
        return (
          <div className="col-lg-12" style={{ textAlign: "center" }}>
            <button
              type="button"
              onClick={backAction}
              className="btn btn-light"
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
            >
              <i className="fa fa-arrow-left"></i>
              Kembali
            </button>
            <button
              type="submit"
              className="btn btn-success ml-2"
              onClick={nextStep}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
            >
              Selanjutnya {`  `}
              <i className="fa fa-arrow-right"></i>
            </button>
          </div>
        );
      }
    }
  }

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="mt-5">{checkStep(step)}</div>
        </>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
      {lastPath === "review" ? 
        tombolProses()
        : tombolDetail()
      }
      </CardFooter>
    </Card>
  );
}

export default ReviewPKPSopExplicit;
