import React, { useState, useEffect } from "react";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { Textarea } from "../../../../../helpers";

function ReviewPKPTolakForm({ content, btnRef, saveForm }) {

  const [ValidateSchema, setValidateSchema] = useState();

  const initialValues = {
    id_km_pro: content.id_km_pro,
    catatan_tolak: ""
  }

  useEffect(() => {
    setValidateSchema(
      Yup.object().shape({
        catatan_tolak: Yup.string().required("Catatan Tolak is required"),
      })
      );
  }, []);
  
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={initialValues}
        validationSchema={ValidateSchema}
        onSubmit={values => {
          saveForm(values);
        }}
      >
        {({
          handleSubmit
        }) => {
          return (
            <>
              <Form className="form form-label-right">
                {/* Field Catatan Tolak */}
                <div className="form-group row">
                <Field
                    name="catatan_tolak"
                    component={Textarea}
                    placeholder="Alasan Tolak"
                    label="Alasan Tolak"
                />
                </div>
                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default ReviewPKPTolakForm;
