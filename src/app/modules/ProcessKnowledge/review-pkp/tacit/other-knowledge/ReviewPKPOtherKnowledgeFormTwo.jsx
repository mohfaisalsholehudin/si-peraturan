import React from "react";
import MateriPokok from "./materi-pokok/MateriPokok";

function ReviewPKPOtherKnowledgeFormTwo({
  btnRef,
  saveFormTwo,
  draft,
  setDraft,
  lastPath
}) {
  return (
    <>
      <MateriPokok
      draft={draft}
      setDraft={setDraft}
      lastPath={lastPath}
       />
      <button
        type="submit"
        style={{ display: "none" }}
        ref={btnRef}
        onClick={() => saveFormTwo()}
      ></button>
    </>
  );
}

export default ReviewPKPOtherKnowledgeFormTwo;