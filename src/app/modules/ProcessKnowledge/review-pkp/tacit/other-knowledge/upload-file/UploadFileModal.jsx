import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
import { useSubheader } from "../../../../../../../_metronic/layout";
import CustomFileInput from "../../../../../Knowledge/helpers/CustomFileInput";
import { getUploadLampiranbyId, saveUploadLampiran, updateUploadLampiran, uploadFile } from "../../../../Api";

function UploadFileModal({ id_upd_terkait, show, onHide, id_km_pro, step, jenis }) {
  
  const history = useHistory();
  const [content, setContent] = useState();
  const suhbeader = useSubheader();
  const [ValidateSchema, setValidateSchema] = useState();
  const [loading, setLoading] = useState(false);
  const {SLICE_UPLOAD} = window.ENV

  const enableLoading = () => {
    setLoading(true);
  };

  const disableLoading = () => {
    setLoading(false);
  };

  const [title, setTitle] = useState("");

  useEffect(() => {
    let _title = id_upd_terkait ? "Edit File Lampiran" : "Tambah File Lampiran";

    setTitle(_title);
    suhbeader.setTitle(_title);

    if(id_upd_terkait){
      getUploadLampiranbyId(id_upd_terkait).then(({data}) => {
        setContent({
          id_upd_terkait: data.id_upd_terkait,
          id_km_pro: data.id_km_pro,
          link: data.link
        })
      })
    }
  }, [id_upd_terkait, suhbeader]);

  const initialValues = {
    id_km_pro: id_km_pro,
    link: "",
  };

  useEffect(() => {
      setValidateSchema(
        Yup.object().shape({
          file: Yup.mixed()
            .required("A file is required")
            .test(
              "fileSize",
              "File too large",
              value => value && value.size <= FILE_SIZE
            )
            .test(
              "fileFormat", 
              "Unsupported Format",
              value => value && SUPPORTED_FORMATS.includes(value.type)
            )
        })
      );
    }, [SUPPORTED_FORMATS]);

  const FILE_SIZE = 300000000;
  const SUPPORTED_FORMATS = [
    // pdf
    "application/pdf",
    // xls
    "application/vnd.ms-excel",
    // xlsx
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    // doc
    "application/msword",
    // docx
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    // ppt
    "application/vnd.ms-powerpoint",
    // pptx
    "application/vnd.openxmlformats-officedocument.presentationml.presentation",
  ];

  const saveFile = (values) => {
    if(values.file.name){
      enableLoading()
      const formData = new FormData();
        formData.append("file", values.file);
      uploadFile(formData)
      .then(({ data }) => {
        disableLoading()
        saveUploadLampiran(
          values.id_km_pro,
          data.message.slice(SLICE_UPLOAD)
        )
        .then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Usulan berhasil disimpan", "success").then(
              () => {
                history.push("/dashboard");
                history.replace(`/process-knowledge/review-pkp/${id_km_pro}/other-knowledge/${jenis}/${step}/review`);
              }
            );
          } else {
            swal("Gagal", "Usulan gagal disimpan", "error").then(() => {
              history.push("/dashboard");
              history.replace(`/process-knowledge/review-pkp/${id_km_pro}/other-knowledge/${jenis}/${step}/review/upload-file/open`);
            });
          }
        });
      })
      .catch(() => window.alert("Oops Something went wrong !"));
      } else {
      updateUploadLampiran(
        content.id_upd_terkait,
        values.id_km_pro,
        values.link
      )
      .then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Usulan berhasil disimpan", "success").then(
            () => {
              history.push("/dashboard");
              history.replace(`/process-knowledge/review-pkp/${id_km_pro}/other-knowledge/${jenis}/${step}/review`);
            }
          );
        } else {
          swal("Gagal", "Usulan gagal disimpan", "error").then(() => {
            history.push("/dashboard");
            history.replace(`/process-knowledge/review-pkp/${id_km_pro}/other-knowledge/${jenis}/${step}/review/upload-file/open`);
          });
        }
      });
    }
  }
  
  return (
    <Modal
      size="lg"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          {title}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <>
          <Formik
            enableReinitialize={true}
            initialValues={content || initialValues}
            validationSchema={ValidateSchema}
            onSubmit={values => {
              saveFile(values);
            }}
          >
            {({ handleSubmit, errors, isSubmitting }) => {
              console.log(errors)
              return (
                <Form className="form form-label-right">
                  {/* FIELD UPLOAD FILE */}
                  <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Upload File
                    </label>
                    <div className="col-lg-8 col-xl-6">
                      <Field
                        name="file"
                        component={CustomFileInput}
                        title="Select a file"
                        label="File"
                        style={{ display: "flex" }}
                      />
                    </div>
                  </div>
                  <div className="col-lg-12" style={{ textAlign: "center" }}>
                    <button
                      type="button"
                      onClick={onHide}
                      className="btn btn-light"
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                      }}
                    >
                      <i className="flaticon2-cancel icon-nm"></i>
                      Batal
                    </button>
                    {`  `}
                    {loading ? (
                    <button
                      type="submit"
                      className="btn btn-success spinner spinner-white spinner-left ml-2"
                      onSubmit={() => handleSubmit()}
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                      }}
                      disabled={isSubmitting}
                    >
                      <span>Simpan</span>
                    </button>
                  ) : (
                    <button
                      type="submit"
                      className="btn btn-success ml-2"
                      onSubmit={() => handleSubmit()}
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                      }}
                      disabled={isSubmitting}
                    >
                      <i className="fas fa-save"></i>
                      <span>Simpan</span>
                    </button>
                  )}
                  </div>
                </Form>
              );
            }}
          </Formik>
        </>
      </Modal.Body>
    </Modal>
  );
}

export default UploadFileModal;
