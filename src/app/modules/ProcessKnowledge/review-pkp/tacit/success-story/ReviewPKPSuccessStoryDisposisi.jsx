/* Library */
import React, { useEffect, useState, useRef } from "react";
import swal from "sweetalert";

/* Helper */
import { useSubheader } from "../../../../../../_metronic/layout";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../../_metronic/_partials/controls";
import { getKnowledgebyId, updateKnowledgeDisposisiPKP, updateStatusPKP } from "../../../Api";
import ReviewPKPSuccessStoryDisposisiForm from "./ReviewPKPSuccessStoryDisposisiForm";

function ReviewPKPSuccessStoryDisposisi({
  history,
  match: {
    params: { id_km_pro, jenis}
  }
}) {

  // Subheader
  const subheader = useSubheader();

  const [title, setTitle] = useState("");
  const [knowledge, setKnowledge] = useState([])

  useEffect(() => {
    let _title = "Disposisi Pegawai" ;

    setTitle(_title);
    subheader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [subheader]);

  useEffect(() => {
    getKnowledgebyId(id_km_pro).then(({data}) => {
      setKnowledge({
        id_km_pro: data.id_km_pro,
        id_csname: data.id_csname,
        id_probis: data.id_probis,
        id_sektor: data.id_sektor,
        id_sme: data.id_sme,
        id_subcase: data.id_subcase,
        id_tipe_km: data.id_tipe_km,
        judul: data.judul,
        nip_pkp: data.nip_pkp
        })
    })
  }, [])

  const btnRef = useRef();

  const saveButton = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
    }
  };

  const backAction = () => {
    history.push(`/process-knowledge/review-pkp/${id_km_pro}/success-story/${jenis}/5/review`);
  };

  const saveForm = values => {
    swal({
      title: "Setuju",
      text: "Apakah Anda yakin mendisposisi usulan ini ?",
      icon: "warning",
      buttons: true
    }).then(ret => {
      if (ret == true) {
    updateKnowledgeDisposisiPKP(
      knowledge.id_km_pro,
      values.catatan_pkp,
      knowledge.id_csname,
      values.id_ko,
      knowledge.id_probis,
      knowledge.id_sektor,
      knowledge.id_sme,
      knowledge.id_subcase,
      knowledge.id_tipe_km,
      "sulit",
      " "
    ).then(({ status }) => {
      if (status === 201 || status === 200) {
        updateStatusPKP(knowledge.id_km_pro, 2, 4, values.id_ko, values.catatan_pkp).then(({status}) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Usulan berhasil didisposisi", "success").then(() => {
                history.push("/dashboard");
                history.replace(`/process-knowledge/review-pkp`);
          })
      } else {
        swal("Gagal", "Data Gagal Disimpan", "error").then(() => {
          history.push("/dashboard");
          history.replace(`/process-knowledge/review-pkp/${id_km_pro}/success-story/${jenis}/5/review`);
        });
      }
    });
    }
  })
}
    })
  }

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="mt-5">
            <ReviewPKPSuccessStoryDisposisiForm
            content={knowledge}
              btnRef={btnRef}
              saveForm={saveForm}
            />
          </div>
        </>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <div className="col-lg-12" style={{ textAlign: "right" }}>
          <button
            type="button"
            onClick={backAction}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
          {`  `}
          <button
            type="submit"
            className="btn btn-success ml-2"
            onClick={saveButton}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fas fa-save"></i>
            Simpan
          </button>
        </div>
      </CardFooter>
    </Card>
  );
}

export default ReviewPKPSuccessStoryDisposisi;