import React, { useEffect, useState, useRef } from "react";
import { useSubheader } from "../../../../../_metronic/layout";
import { useHistory } from "react-router-dom";
import swal from "sweetalert";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../../_metronic/_partials/controls";
import ReviewUpdateSuccessStoryForm from "./ReviewUpdateSuccessStoryForm";
import {
  getKnowledgeProcessById,
  getSuccessStoryByIdKmPro,
  saveSuccessStory,
  updateKnowledgeProcess,
  updateKnowledgeProcessMedia,
  updateSuccessStory,
  updateSuccessStoryMateri,
  uploadKmi,
} from "../../../../references/Api";
import ReviewUpdateSuccessStoryFormTwo from "./ReviewUpdateSuccessStoryFormTwo";
import UploadFileTable from "./upload-file/UploadFileTable";
import ReferensiTable from "./referensi/ReferensiTable";
import PeraturanTerkaitTable from "./peraturan-terkait/PeraturanTerkaitTable";
import TugasFungsiTable from "./tugas-fungsi/TugasFungsiTable";
import UnitOrganisasiTable from "./unit-organisasi/UnitOrganisasiTable";
import KategoriPengetahuanForm from "./kategori-pengetahuan/KategoriPengetahuanForm";
import LihatPulaTable from "./lihat-pula/LihatPulaTable";
import BacaanLanjutanTable from "./bacaaan-lanjutan/BacaanLanjutanTable";



function ReviewUpdateSuccessStory({
  id,
  step,
  id_km_pro,
  id_tipe_km,
  tipe_knowledge,
  path,
}) {
  const suhbeader = useSubheader();
  const history = useHistory();
  const [showTable, setShowTable] = useState(false)


  const [title, setTitle] = useState("");
  const [loading, setLoading] = useState(false);
  const [isDisabled, setIsDisabled] = useState(false);
  const [content, setContent] = useState();
  const [materi, setMateri] = useState();
  const [media, setMedia] = useState("");
  const [idTemp, setIdTemp] = useState();
  const [draft, setDraft] = useState("");
  const [isShow, setIsShow] = useState(false);
  const [isShowForm, setIsShowForm] = useState(false);

  const initValues = {
    tipe_knowledge: tipe_knowledge,
    // id_tipe_km === "1"
    //   ? "Success Story"
    //   : id_tipe_km === "2"
    //   ? "SOP"
    //   : "Other Knowledge",
    tipe_konten: media,
    id_probis: "",
    id_sektor: "",
    id_csname: "",
    id_subcase: "",
    id_tipe_km: id_tipe_km,
    id_km_pro: id_km_pro,
    nama: "",
    pendahuluan: "",
    daftar_isi: "",
    definisi: "",
    tagging: "",
    file: "",
  };

  const checkIdTemp = () => {
    getSuccessStoryByIdKmPro(id_km_pro).then(({ data }) => {
      if (data.length > 0) {
        setIdTemp(data[0].successStoryTemp.id_ss_temp);
      }
    });
  };

  useEffect(() => {
    // let _title = id ? "Edit KM Tacit" : "Update Review Konten";

    // setTitle(_title);
    // suhbeader.setTitle(_title);
    if (path === "view") {
      let _title = "Detail Knowledge";
      setTitle(_title);
      suhbeader.setTitle(_title);
    } else {
      let _title = "Review Knowledge";
      setTitle(_title);
      suhbeader.setTitle(_title);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    if (id_km_pro) {
      getKnowledgeProcessById(id_km_pro).then(({ data }) => {
        setMedia(data.tipe_konten);
        if (data.id_probis) {
          getSuccessStoryByIdKmPro(id_km_pro).then((data_2) => {
            // console.log(data_2.data[0].successStoryTemp);
            setContent({
              tipe_knowledge: data_2.data[0].tipeKnowledge.nama,
              // data.knowledgeProses.id_tipe_km === 1
              //   ? "Success Story"
              //   : data.knowledgeProses.id_tipe_km === 2
              //   ? "SOP"
              //   : "Other Knowledge",
              tipe_konten: data.tipe_konten,
              id_probis: data.id_probis,
              id_sektor: data.id_sektor,
              id_csname: data.id_csname,
              id_subcase: data.id_subcase,
              id_tipe_km: data.id_tipe_km,
              id_km_pro: data.id_km_pro,
              judul: data.judul,
              pendahuluan: data_2.data[0].successStoryTemp.pendahuluan,
              daftar_isi: data_2.data[0].successStoryTemp.daftar_isi,
              definisi: data_2.data[0].successStoryTemp.definisi,
              tagging: data_2.data[0].successStoryTemp.tagging,
              file_upload: data.media_upload,
              level_knowledge: data.level_knowledge,
              kategori: data.kategori,
            });
            setMateri(data_2.data[0].successStoryTemp.materi);
          });
          checkIdTemp();
        } else {
          checkIdTemp();
        }
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id, id_km_pro, id_tipe_km, media, suhbeader]);
  const btnRef = useRef();
  const btnPublish = useRef();

  const saveButton = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
      // setIsComplete(true);
      // disabled ? setIsComplete(false) : setIsComplete(true);
    }
  };
  const publishButton = () => {
    if (btnPublish && btnPublish.current) {
      btnPublish.current.click();
      // setIsComplete(true);
      // disabled ? setIsComplete(false) : setIsComplete(true);
    }
  };
  const disposisiDialog = (id) => {
    swal({
      title: "Disposisi",
      text: "Apakah Anda yakin disposisi usulan ini ?",
      icon: "warning",
      buttons: true,
    }).then((ret) => {
      if (ret === true) {
        // acceptAction(penegasan.status);
        // history.push(`/process-knowledge/update-review/${id_km_pro}/sop/${jenis}/disposisi`);
        history.push(
          `/process-knowledge/update-review/${id_tipe_km}/${id_km_pro}/disposisi/detil`
        );
      }
    });
  };
  const tolakDialog = (id) => {
    swal({
      title: "Tolak",
      text: "Apakah Anda yakin menolak usulan ini ?",
      icon: "warning",
      buttons: true,
    }).then((ret) => {
      if (ret == true) {
        // acceptAction(penegasan.status);
        // history.push(`/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/tolak`);
        history.push(
          `/process-knowledge/update-review/${id_tipe_km}/${id_km_pro}/tolak`
        );
      }
    });
  };

  const enableLoading = () => {
    setLoading(true);
    setIsDisabled(true);
  };

  const disableLoading = () => {
    setLoading(false);
    setIsDisabled(false);
  };
  const hideButton = () => {
    setIsShow(false);
  };
  const showButton = () => {
    setIsShow(true);
  };
  const hideForm = () => {
    setIsShowForm(false);
  };
  const showForm = () => {
    setIsShowForm(true);
  };

  const backAction = () => {
    switch (step) {
      case "2":
        history.push(
          `/process-knowledge/update-review/${id_tipe_km}/${id_km_pro}/1/${path}`
        );
        break;
      case "3":
        history.push(
          `/process-knowledge/update-review/${id_tipe_km}/${id_km_pro}/2/${path}`
        );
        break;
      case "4":
        history.push(
          `/process-knowledge/update-review/${id_tipe_km}/${id_km_pro}/3/${path}`
        );
        break;
      case "5":
        history.push(
          `/process-knowledge/update-review/${id_tipe_km}/${id_km_pro}/4/${path}`
        );
        break;

      default:
        break;
    }
  };

  const saveFormOne = (values) => {
    if (!id) {
      if (values.file) {
        if (values.file.name) {
          enableLoading();
          const formData = new FormData();
          formData.append("file", values.file);
          uploadKmi(formData).then(({ data }) => {
            disableLoading();
            updateKnowledgeProcessMedia(
              values.id_km_pro,
              values.id_tipe_km,
              values.tipe_konten,
              values.id_sektor,
              values.id_probis,
              values.id_csname,
              values.id_subcase,
              values.judul,
              data.message
            ).then(({ status }) => {
              if (status === 201 || status === 200) {
                if (idTemp) {
                  updateSuccessStory(
                    idTemp,
                    values.id_km_pro,
                    values.id_tipe_km,
                    values.pendahuluan,
                    values.daftar_isi,
                    values.definisi,
                    values.tagging
                  ).then(({ status }) => {
                    if (status === 201 || status === 200) {
                      switch (step) {
                        case "1":
                          history.push(
                            `/process-knowledge/update-review/${id_tipe_km}/${id_km_pro}/2/${path}`
                          );
                          break;
                        case "2":
                          history.push(
                            // `/process-knowledge/update-review/success-story/3/${path}`
                            `/process-knowledge/update-review/${id_tipe_km}/${id_km_pro}/3/${path}`
                          );
                          break;
                        case "3":
                          history.push(`/process-knowledge/update-review`);
                          break;

                        default:
                          break;
                      }
                    }
                  });
                } else {
                  saveSuccessStory(
                    values.id_km_pro,
                    values.id_tipe_km,
                    values.pendahuluan,
                    values.daftar_isi,
                    values.definisi,
                    values.tagging
                  ).then(({ status }) => {
                    if (status === 201 || status === 200) {
                      switch (step) {
                        case "1":
                          history.push(
                            `/process-knowledge/update-review/${id_tipe_km}/${id_km_pro}/2/${path}`
                          );
                          break;
                        case "2":
                          history.push(
                            // `/process-knowledge/update-review/success-story/3/${path}`
                            `/process-knowledge/update-review/${id_tipe_km}/${id_km_pro}/3/${path}`
                          );
                          break;
                        case "3":
                          history.push(`/process-knowledge/update-review`);
                          break;

                        default:
                          break;
                      }
                    }
                  });
                }
              } else {
                swal("Gagal", "Data gagal disimpan", "error").then(() => {
                  history.push("/process-knowledge/update-review/detil");
                });
              }
            });
          });
        } else {
          updateKnowledgeProcessMedia(
            values.id_km_pro,
            values.id_tipe_km,
            values.tipe_konten,
            values.id_sektor,
            values.id_probis,
            values.id_csname,
            values.id_subcase,
            values.judul,
            values.file_upload
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              if (idTemp) {
                updateSuccessStory(
                  idTemp,
                  values.id_km_pro,
                  values.id_tipe_km,
                  values.pendahuluan,
                  values.daftar_isi,
                  values.definisi,
                  values.tagging
                ).then(({ status }) => {
                  if (status === 201 || status === 200) {
                    switch (step) {
                      case "1":
                        history.push(
                          // `/process-knowledge/update-review/${id_tipe_km}/${id_km_pro}/${media}/2/detil`
                          `/process-knowledge/update-review/${id_tipe_km}/${id_km_pro}/2/${path}`
                        );
                        break;
                      case "2":
                        history.push(
                          // `/process-knowledge/update-review/success-story/${media}/3/${path}`
                          `/process-knowledge/update-review/${id_tipe_km}/${id_km_pro}/3/${path}`
                        );
                        break;
                      case "3":
                        history.push(`/process-knowledge/update-review`);
                        break;

                      default:
                        break;
                    }
                  }
                });
              } else {
                saveSuccessStory(
                  values.id_km_pro,
                  values.id_tipe_km,
                  values.pendahuluan,
                  values.daftar_isi,
                  values.definisi,
                  values.tagging
                ).then(({ status }) => {
                  if (status === 201 || status === 200) {
                    switch (step) {
                      case "1":
                        history.push(
                          `/process-knowledge/update-review/${id_tipe_km}/${id_km_pro}/2/${path}`
                        );
                        break;
                      case "2":
                        history.push(
                          // `/process-knowledge/update-review/success-story/3/${path}`
                          `/process-knowledge/update-review/${id_tipe_km}/${id_km_pro}/3/${path}`
                        );
                        break;
                      case "3":
                        history.push(`/process-knowledge/update-review`);
                        break;

                      default:
                        break;
                    }
                  }
                });
              }
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                history.push("/process-knowledge/update-review/detil");
              });
            }
          });
        }
      } else {
        updateKnowledgeProcess(
          values.id_km_pro,
          values.id_tipe_km,
          values.tipe_konten,
          values.id_sektor,
          values.id_probis,
          values.id_csname,
          values.id_subcase,
          values.judul
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            if (idTemp) {
              updateSuccessStory(
                idTemp,
                values.id_km_pro,
                values.id_tipe_km,
                values.pendahuluan,
                values.daftar_isi,
                values.definisi,
                values.tagging
              ).then(({ status }) => {
                if (status === 201 || status === 200) {
                  switch (step) {
                    case "1":
                      history.push(
                        `/process-knowledge/update-review/${id_tipe_km}/${id_km_pro}/2/${path}`
                      );
                      break;
                    case "2":
                      history.push(
                        // `/process-knowledge/update-review/success-story/3/${path}`
                        `/process-knowledge/update-review/${id_tipe_km}/${id_km_pro}/3/${path}`
                      );
                      break;
                    case "3":
                      history.push(`/process-knowledge/update-review`);
                      break;

                    default:
                      break;
                  }
                }
              });
            } else {
              saveSuccessStory(
                values.id_km_pro,
                values.id_tipe_km,
                values.pendahuluan,
                values.daftar_isi,
                values.definisi,
                values.tagging
              ).then(({ status }) => {
                if (status === 201 || status === 200) {
                  switch (step) {
                    case "1":
                      history.push(
                        `/process-knowledge/update-review/${id_tipe_km}/${id_km_pro}/2/${path}`
                      );
                      break;
                    case "2":
                      history.push(
                        // `/process-knowledge/update-review/success-story/3/${path}`
                        `/process-knowledge/update-review/${id_tipe_km}/${id_km_pro}/3/${path}`
                      );
                      break;
                    case "3":
                      history.push(`/process-knowledge/update-review`);
                      break;

                    default:
                      break;
                  }
                }
              });
            }
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push("/process-knowledge/update-review/detil");
            });
          }
        });
      }
    } else {
      // console.log(values);
      //Endpoint untuk Edit
    }
  };
  const saveFormTwo = () => {
    if (idTemp) {
      // console.log('ada id temp')
      updateSuccessStoryMateri(idTemp, draft ? draft : materi).then(
        ({ status }) => {
          if (status === 201 || status === 200) {
            switch (step) {
              case "1":
                history.push(
                  `/process-knowledge/update-review/${id_tipe_km}/${id_km_pro}/2/${path}`
                );
                break;
              case "2":
                history.push(
                  // `/process-knowledge/update-review/success-story/3/${path}`
                  `/process-knowledge/update-review/${id_tipe_km}/${id_km_pro}/3/${path}`
                );
                break;
              case "3":
                history.push(`/process-knowledge/update-review`);
                break;

              default:
                break;
            }
          }
        }
      );
    }
  };
  const saveForm = (values) => {
    switch (step) {
      case "1":
        saveFormOne(values);
        break;
      case "2":
        saveFormTwo();
        break;
      case "3":
        history.push(
          `/process-knowledge/update-review/${id_tipe_km}/${id_km_pro}/4/${path}`
        );
        break;
      case "4":
        history.push(
          `/process-knowledge/update-review/${id_tipe_km}/${id_km_pro}/5/${path}`
        );
        break;
      // case "4":
      // history.push(
      //   `/process-knowledge/update-review/${id_tipe_km}/${id_km_pro}/5/detil`
      // );
      // break;

      default:
        break;
    }
  };

  const checkStep = (val) => {
    switch (val) {
      case "1":
        return (
          <ReviewUpdateSuccessStoryForm
            content={content || initValues}
            btnRef={btnRef}
            saveForm={saveForm}
            media={media}
            path={path}
          />
        );
      case "2":
        return (
          <ReviewUpdateSuccessStoryFormTwo
            content={initValues}
            btnRef={btnRef}
            saveForm={saveForm}
            // media={media}
            draft={materi}
            setDraft={setDraft}
            path={path}
          />
        );
      case "3":
        return (
          <>
            {/* <PranalaLuarTable media={media} step={step} /> */}
            <ReferensiTable
              // media={media}
              step={step}
              id_km_pro={id_km_pro}
              id_tipe_km={id_tipe_km}
              path={path}
            />
           <LihatPulaTable 
              step={step}
              id_km_pro={id_km_pro}
              id_tipe_km={id_tipe_km}
              path={path} />
               <BacaanLanjutanTable  
               step={step}
              id_km_pro={id_km_pro}
              id_tipe_km={id_tipe_km}
              path={path} />
            <PeraturanTerkaitTable
              // media={media}
              step={step}
              id_km_pro={id_km_pro}
              id_tipe_km={id_tipe_km}
              path={path}
            />
            <UploadFileTable
              // media={media}
              step={step}
              id_km_pro={id_km_pro}
              id_tipe_km={id_tipe_km}
              path={path}
            />

            <button
              type="submit"
              style={{ display: "none" }}
              ref={btnRef}
              onClick={() => saveForm()}
            ></button>
          </>
        );

      case "4":
        return (
          <>
            <TugasFungsiTable
              media={media}
              step={step}
              id_km_pro={id_km_pro}
              id_tipe_km={id_tipe_km}
              path={path}
            />
            <UnitOrganisasiTable
              media={media}
              step={step}
              id_km_pro={id_km_pro}
              id_tipe_km={id_tipe_km}
              path={path}
            />
            <button
              type="submit"
              style={{ display: "none" }}
              ref={btnRef}
              onClick={() => saveForm()}
            ></button>
          </>
        );
      case "5":
        return (
          <>
            <KategoriPengetahuanForm
              content={content}
              btnRef={btnRef}
              btnPublish={btnPublish}
              media={media}
              step={step}
              id_km_pro={id_km_pro}
              id_tipe_km={id_tipe_km}
              //   jenis={jenis}
              hideButton={hideButton}
              showButton={showButton}
              showForm={showForm}
              hideForm={hideForm}
              path={path}
              showButtonTable={showButtonTable}
              hideButtonTable={hideButtonTable}
              //   lastPath={lastPath}
            />
          </>
        );

      default:
        break;
    }
  };
  const showButtonTable = () => {
    setShowTable(true)
}

const hideButtonTable = () => {
  setShowTable(false)
}
  const homeDialog = (id) => {
    swal({
      title: "Kembali",
      text: "Apakah Anda Ingin Kembali Ke Halaman Review Update Konten PKP ?",
      icon: "warning",
      buttons: true,
    }).then((ret) => {
      if (ret == true) {
        // acceptAction(penegasan.status);
        history.push(`/process-knowledge/update-review/`);
      }
    });
  };

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="mt-5">{checkStep(step)}</div>
        </>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <div className="col-lg-12" style={{ textAlign: "right" }}>
          {step !== "1" ? (
            step === "5" ? null : (
              <button
                type="button"
                onClick={backAction}
                className="btn btn-light"
                style={{
                  boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                }}
              >
                <i className="fa fa-arrow-left"></i>
                Kembali
              </button>
            )
          ) : null}
          {`  `}

          {
          // path === "view" ? (
            // <div className="col-lg-12" style={{ textAlign: "center" }}>
            //   <button
            //     type="button"
            //     onClick={backAction}
            //     className="btn btn-light"
            //     style={{
            //       boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
            //     }}
            //   >
            //     <i className="fa fa-arrow-left"></i>
            //     Kembali
            //   </button>
            //   <button
            //     type="button"
            //     className="btn btn-primary ml-2"
            //     onClick={homeDialog}
            //     style={{
            //       boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
            //     }}
            //     // disabled={disabled}
            //   >
            //     <i className="fas fa-home"></i>
            //     Kembali ke Halaman Utama
            //   </button>
            // </div>
          // ) : 
          step === "5" ? (
            // <button
            //   type="submit"
            //   className="btn btn-success ml-2"
            //   // onClick={saveForm}
            //   style={{
            //     boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            //   }}
            // >
            //   <i className="fas fa-save"></i>
            //   Simpan
            // </button>
          path === 'view' ? (
            <div className="col-lg-12" style={{ textAlign: "center" }}>
              <button
                type="button"
                onClick={backAction}
                className="btn btn-light"
                style={{
                  boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                }}
              >
                <i className="fa fa-arrow-left"></i>
                Kembali
              </button>
              <button
                type="button"
                className="btn btn-primary ml-2"
                onClick={homeDialog}
                style={{
                  boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                }}
                // disabled={disabled}
              >
                <i className="fas fa-home"></i>
                Kembali ke Halaman Utama
              </button>
            </div>
          ) : (
            isShow ? (
              <div className="col-lg-12" style={{ textAlign: "center" }}>
                <button
                  type="button"
                  onClick={backAction}
                  className="btn btn-light"
                  style={{
                    boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                  }}
                >
                  <i className="fa fa-arrow-left"></i>
                  Kembali
                </button>
                <button
                  type="button"
                  className="btn btn-success ml-2"
                  onClick={saveButton}
                  style={{
                    boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                  }}
                  // disabled={disabled}
                >
                  <i className="fas fa-save"></i>
                  Simpan Draft
                </button>
                {`  `}
                <button
                  type="submit"
                  className="btn btn-info ml-2"
                  onClick={disposisiDialog}
                  style={{
                    boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                  }}
                  // disabled={disabled}
                >
                  <i className="fas fa-share"></i>
                  Disposisi
                </button>
              </div>
            ) : (
              <div className="col-lg-12" style={{ textAlign: "center" }}>
                <button
                  type="button"
                  onClick={backAction}
                  className="btn btn-light"
                  style={{
                    boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                  }}
                >
                  <i className="fa fa-arrow-left"></i>
                  Kembali
                </button>
                {`  `}
                {showTable ? 
                <>
                <button
                  type="button"
                  className="btn btn-success ml-2"
                  onClick={saveButton}
                  style={{
                    boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                  }}
                  // disabled={disabled}
                >
                  <i className="fas fa-save"></i>
                  Simpan Draft
                </button>
                <button
                  type="submit"
                  className="btn btn-primary ml-2"
                  onClick={publishButton}
                  style={{
                    boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                  }}
                  // disabled={disabled}
                >
                  <i className="fas fa-check"></i>
                  Siap Publish
                </button>
                {`  `}
                <button
                  type="submit"
                  className="btn btn-danger ml-2"
                  onClick={tolakDialog}
                  style={{
                    boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                  }}
                  // disabled={disabled}
                >
                  <i className="flaticon2-cancel icon-nm"></i>
                  Tolak
                </button>
                </>
                : null}
              </div>
            )
          )
          ) : loading ? (
            <button
              type="submit"
              className="btn btn-success spinner spinner-white spinner-left ml-2"
              // onSubmit={() => handleSubmit()}
              onClick={saveButton}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
              }}
              disabled={isDisabled}
            >
              <span>Selanjutnya</span>
            </button>
          ) : (
            <button
              type="submit"
              // onSubmit={() => handleSubmit()}
              onClick={saveButton}
              className="btn btn-success ml-2"
              disabled={isDisabled}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
              }}
            >
              {/* <i className="fas fa-check"></i> */}
              <i className="fa fa-arrow-right"></i>
              Selanjutnya
            </button>
          )}
        </div>
      </CardFooter>
    </Card>
  );
}

export default ReviewUpdateSuccessStory;
