import React, { useState, useEffect, useRef } from "react";
import swal from "sweetalert";
import { useSubheader } from "../../../../../_metronic/layout";
import {
    Card,
    CardBody,
    CardHeader,
    CardFooter,
} from "../../../../../_metronic/_partials/controls";
import { updateStatusKnowledgeProcessDisposisi } from "../../../../references/Api";
import { getKnowledgebyId, updateKnowledge, updateKnowledgeDisposisiPKP } from "../../Api";
import ReviewUpdateSuccessStoryDisposisiForm from "./ReviewUpdateSuccessStoryDisposisiForm";

function ReviewUpdateSuccessStoryDisposisi({
    history,
    match: {
        params: { id_tipe_km, id_km_pro },
    },
}) {
    const suhbeader = useSubheader();

    const [title, setTitle] = useState("");
    const [actionsLoading] = useState(true);
    const [knowledge, setKnowledge] = useState([]);

    useEffect(() => {
        let _title = "Disposisi Pegawai";

        setTitle(_title);
        suhbeader.setTitle(_title);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [suhbeader]);

    useEffect(() => {
        getKnowledgebyId(id_km_pro).then(({ data }) => {
            setKnowledge({
                id_km_pro: data.id_km_pro,
                nama: data.nama,
                id_tipe_km: data.id_tipe_km,
                jenis: data.jenis,
                id_status_km: data.id_status_km,
                kategori: data.kategori,
                level_knowledge: data.level_knowledge,
                catatan_tolak: data.catatan_tolak,
                catatan_pkp: data.catatan_pkp,
                catatan_ko: data.catatan_ko,
                id_ko: data.id_ko,
                id_sme: data.id_sme,
                nip_sme: data.nip_sme,
                nip_ko: data.nip_ko,
                nip_pkp: data.nip_pkp,
                nip_kontri: data.nip_kontri,
                nama_kontri: data.nama_kontri,
                nm_unit_kontri: data.nm_unit_kontri,
                ket_review: data.ket_review,
                relevan: data.relevan,
                cat_review: data.cat_review,
                jml_view: data.jml_view,
                kodekantor_kontri: data.kodekantor_kontri,
                id_probis: data.id_probis,
                id_sektor: data.id_sektor,
                id_csname: data.id_csname,
                id_subcase: data.id_subcase,
                tipe_konten: data.tipe_konten,
                media_upload: data.media_upload,
                judul: data.judul,
            });
        });
    }, [id_km_pro]);
    const btnRef = useRef();

    const saveButton = () => {
        if (btnRef && btnRef.current) {
            btnRef.current.click();
        }
    };

    const backAction = () => {
        history.push(
            `/process-knowledge/update-review/${id_tipe_km}/${id_km_pro}/5/detil`
        );
    };

    const saveForm = (values) => {
        swal({
            title: "Setuju",
            text: "Apakah Anda yakin mendisposisi usulan ini ?",
            icon: "warning",
            buttons: true,
        }).then((ret) => {
            if (ret === true) {
                updateKnowledgeDisposisiPKP(
                    knowledge.id_km_pro,
                    values.catatan_pkp,
                    knowledge.id_csname,
                    values.id_ko,
                    knowledge.id_probis,
                    knowledge.id_sektor,
                    knowledge.id_sme,
                    knowledge.id_subcase,
                    knowledge.id_tipe_km,
                    "sulit",
                    " "
                  ).then(({ status }) => {
                    if (status === 201 || status === 200) {
                        console.log(knowledge.jenis)
                        updateStatusKnowledgeProcessDisposisi(
                            knowledge.id_km_pro,
                            knowledge.jenis === 'Tacit' ? 15 : 16,
                            values.id_ko,
                            values.catatan_pkp
                        ).then(({ status }) => {
                            if (status === 201 || status === 200) {
                                swal("Berhasil", "Usulan berhasil didisposisi", "success").then(
                                    () => {
                                        history.push("/dashboard");
                                        history.replace(`/process-knowledge/update-review`);
                                    }
                                );
                            } else {
                                swal("Gagal", "Data Gagal Disimpan", "error").then(() => {
                                    history.push("/dashboard");
                                    history.replace(`/process-knowledge/update-review`);
                                    // history.replace(
                                    //     `/process-knowledge/review-pkp/${id_km_pro}/success-story/${jenis}/5/review`
                                    // );
                                });
                            }
                        })
                        // updateStatusKnowledgeProcessDisposisi(
                        //   knowledge.id_km_pro,
                        //   knowledge.jenis,
                        // //   2,
                        // //   4,
                        //   values.id_ko,
                        //   values.catatan_pkp
                        // ).then(({ status }) => {
                        //   if (status === 201 || status === 200) {
                        //     swal("Berhasil", "Usulan berhasil didisposisi", "success").then(
                        //       () => {
                        //         history.push("/dashboard");
                        //         history.replace(`/process-knowledge/review-pkp`);
                        //       }
                        //     );
                        //   } else {
                        //     swal("Gagal", "Data Gagal Disimpan", "error").then(() => {
                        //       history.push("/dashboard");
                        //       history.replace(
                        //         `/process-knowledge/review-pkp/${id_km_pro}/success-story/${jenis}/5/review`
                        //       );
                        //     });
                        //   }
                        // });
                    }
                });
            }
        });
    };
    return (
        <Card>
            <CardHeader
                title={title}
                style={{ backgroundColor: "#FFC91B" }}
            ></CardHeader>
            <CardBody>
                <>
                    <div className="mt-5">
                        <ReviewUpdateSuccessStoryDisposisiForm
                            content={knowledge}
                            btnRef={btnRef}
                            saveForm={saveForm}
                        />
                    </div>
                </>
            </CardBody>
            <CardFooter style={{ borderTop: "none" }}>
                <div className="col-lg-12" style={{ textAlign: "right" }}>
                    <button
                        type="button"
                        onClick={backAction}
                        className="btn btn-light"
                        style={{
                            boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                        }}
                    >
                        <i className="fa fa-arrow-left"></i>
                        Kembali
                    </button>
                    {`  `}
                    <button
                        type="submit"
                        className="btn btn-success ml-2"
                        onClick={saveButton}
                        style={{
                            boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                        }}
                    // disabled={disabled}
                    >
                        <i className="fas fa-save"></i>
                        Simpan
                    </button>
                </div>
            </CardFooter>
        </Card>
    );
}

export default ReviewUpdateSuccessStoryDisposisi;
