import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import swal from "sweetalert";

/* Helpers */
import { Pagination } from "../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../_metronic/_helpers";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import * as columnFormatters from "../../../helpers/column-formatters";
import { getKnowledgeProcess, updateStatusKnowledgeProcess } from "../../../references/Api";

function ReviewUpdateTable() {
  const history = useHistory();
  const [content, setContent] = useState([]);
  // const content = [
  //   {
  //     id: "1",
  //     judul: "KPP A",
  //     tipe_knowledge: "Success Story",
  //     alasan_perubahan: 'Alasan',
  //     status: 'Review PKP Tacit'
  //   },
  //   {
  //     id: "2",
  //     judul: "Business Process 2",
  //     tipe_knowledge: "Tax Learning",
  //     alasan_perubahan: 'Feedback',
  //     status: 'Siap Review Knowledge'
  //   },
  //   {
  //     id: "3",
  //     judul: "Business Process 3",
  //     tipe_knowledge: "SOP",
  //     alasan_perubahan: 'Alasan',
  //     status: 'Review KO Tacit'
  //   }
  // ];
  const review = (id_tipe_km, id_km_pro) => {
    history.push(
      `/process-knowledge/update-review/${id_tipe_km}/${id_km_pro}/1/detil`
    );
  };
  const detil = (id_tipe_km, id_km_pro) => {
    history.push(
      `/process-knowledge/update-review/${id_tipe_km}/${id_km_pro}/1/view`
    );
  };

  useEffect(() => {
    getKnowledgeProcess().then(({ data }) => {
      // console.log(data)
      data.map((data) => {
        // console.log(data.statusKm.id_status)
        return data.statusKm.id_status_km === 13 ||
          data.statusKm.id_status_km === 14 ||
          data.statusKm.id_status_km === 15 ||
          data.statusKm.id_status_km === 16 ||
          data.statusKm.id_status_km === 17 ||
          data.statusKm.id_status_km === 18
          ? setContent((content) => [...content, data])
          : null;
        // return data.statusKm.id_status_km === 9 ? console.log(data) : null
      });
    });
  }, []);

  const ajukanKnowledge = (id_tipe_km, id_km_pro) => {
    // alert(`${id_tipe_km}, ${id_km_pro}`)
    updateStatusKnowledgeProcess(id_km_pro, '10').then(({ status }) => {
      if (status === 201 || status === 200) {
        swal("Berhasil", "Data Berhasil Disimpan", "success").then(() => {
          history.push("/dashboard");
          history.replace("/process-knowledge/update-review");
        });
      } else {
        swal("Gagal", "Data Gagal Disimpan", "error").then(() => {
          history.push("/dashboard");
          history.replace("/process-knowledge/update-review");
        });
      }
    });
  }

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "updated_at",
      text: "No",
      sort: true,
      hidden: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "judul",
      text: "Judul",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "tipeKnowledge.nama",
      text: "Tipe Knowledge",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "cat_review",
      text: "Alasan Perubahan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "nama_kontri",
      text: "Kontributor",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "review_pkp",
      text: "Reviewer",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "wkt_pkp",
      text: "Waktu Review",
      sort: true,
      formatter: columnFormatters.DateFormatterReviewPkp,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "statusKm.nama",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: columnFormatters.StatusColumnFormatterReviewUpdate,
    }, {
      dataField: "statusKm.id_status_km",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: columnFormatters.StatusColumnFormatterReviewUpdate,
      hidden: true,
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterReviewUpdate,
      formatExtraData: {
        reviewProposal: review,
        detilProposal: detil,
        ajukanKnowledge: ajukanKnowledge
        // openEditDialog: edit,
        // openDeleteDialog: deleteAction,
        // applyProposal: apply
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    // sortField: "id",
    sortField: "statusKm.id_status_km",
    pageNumber: 1,
    pageSize: 50,
  };
  const defaultSorted = [{ dataField: "statusKm.id_status_km", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;

  // console.log(content)

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_km_pro"
                  data={content}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
    </>
  );
}

export default ReviewUpdateTable;
