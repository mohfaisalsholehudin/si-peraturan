/* Library */
import React, { useRef, useState} from "react";
import { getFeedbackbyIsi } from "../Api";
import FeedbackForm from "./FeedbackForm";
import FeedbackTable from "./FeedbackTable";

function FeedbackMain() {

  const [content, setContent] = useState([]);
  const [isShow, setIsShow] = useState(false)

  const btnRef = useRef();
  const cariButton = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
    }
  };

  function tombolCari() {
    return (
        <div className="col-lg-10" style={{ textAlign: "right" }}>
          <button
            type="submit"
            onClick={cariButton}
            className="btn btn-primary"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fa fa-search"></i>
            Cari
          </button>
        </div>
    )
  }

  const saveForm = values => {
      setIsShow(false)
      setContent([])
      getFeedbackbyIsi(values.isi).then(({data}) =>{
        console.log(values)
        console.log(data[0].id_feedback)
        data.map((data) => {
            setContent((content) => [...content, data])
        })
      })
      setIsShow(true)
  }

  return (
        <>
          <FeedbackForm
          saveForm={saveForm}
          btnRef={btnRef}
          />
          {tombolCari()}
          <br></br>
          <br></br>
          {isShow ? <FeedbackTable 
          content={content}
          /> : null }
      </>
  );
}

export default FeedbackMain;