import React, { useEffect, useState } from "react";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { Textarea } from "../../../helpers";

function FeedbackForm({
    saveForm, btnRef
  }) {

    const initialValues = {
      isi:""
    }

    const [ValidateSchema, setValidateSchema] = useState();

    useEffect(() => {
      setValidateSchema(
        Yup.object().shape({
          isi: Yup.string()
            .min(1, "Minimum 1 symbols")
            .required("Isi Feedback is required")
        })
      );
  }, []);

    return (
      <>
        <Formik
          enableReinitialize={true}
          initialValues={initialValues}
          validationSchema={ValidateSchema}
          onSubmit={values => {
            saveForm(values);
          }}
        >
          {({ handleSubmit, values }) => {
            console.log(values)
            return (
              <>
                <Form className="form form-label-right">
                  {/* Field Isi Feedback */}
                  <div className="form-group row">
                    <Field
                      name="isi"
                      component={Textarea}
                      placeholder="Isi Feedback"
                      label="Isi Feedback"
                    />
                  </div>
                  <button
                    type="submit"
                    style={{ display: "none" }}
                    ref={btnRef}
                    onSubmit={() => handleSubmit()}
                  ></button>
                </Form>
              </>
            );
          }}
        </Formik>
      </>
    );
}

export default FeedbackForm