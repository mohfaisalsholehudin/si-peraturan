import axios from "axios";
const { BACKEND_URL } = window.ENV;

export function uploadFile(formData) {
      const config = {
        headers: {
          "Content-type": "multipart/form-data",
        },
      };
      return axios.post(`${BACKEND_URL}/api/uploadkmi`, formData, config);
    }

export function getReview()
{
    return axios.get(`${BACKEND_URL}/api/knowledgeproses/`);
}

export function getTusi()
{
    return axios.get(`${BACKEND_URL}/api/tusi/`);
}

export function getTusibyStatus(
    status
    )
{
    return axios.get(`${BACKEND_URL}/api/tusi/bystatus/${status}`)
}

export function getTusibyId(id)
{
    return axios.get(`${BACKEND_URL}/api/tusiknowledge/${id}`)
}

export function getTusibyIdKM(id)
{
    return axios.get(`${BACKEND_URL}/api/tusiknowledge/byidknowledgeproses/${id}`)
}

export function saveTusiKM(
    id_km_pro,
    id_tusi_km
) {
    return axios.post(`${BACKEND_URL}/api/tusiknowledge/`, {
        id_km_pro,
        id_tusi_km
        })
}

export function updateTusiKM(
    id,
    id_km_pro,
    id_tusi_km
) {
    return axios.put(`${BACKEND_URL}/api/tusiknowledge/${id}`, {
        id_km_pro,
        id_tusi_km
    })
}

export function deleteTusi(id)
{
    return axios.delete(`${BACKEND_URL}/api/tusiknowledge/${id}`)
}

export function getUnitbyId(id)
{
    return axios.get(`${BACKEND_URL}/api/unitorgknowledge/${id}`)
}

export function getUnitbyIdKM(id)
{
    return axios.get(`${BACKEND_URL}/api/unitorgknowledge/byidknowledgeproses/${id}`)
}

export function getUnitKerja()
{
    return axios.get(`${BACKEND_URL}/api/unitkerja/`)
}

export function saveUnit(
    id_km_pro,
    nama
)
{
    return axios.post(`${BACKEND_URL}/api/unitorgknowledge/`, {
    id_km_pro,
    nama
    })
}

export function updateUnit(
    id,
    id_km_pro,
    nama
    )
{
    return axios.put(`${BACKEND_URL}/api/unitorgknowledge/${id}`, {
    id_km_pro,
    nama
    })
}

export function deleteUnit(id)
{
    return axios.delete(`${BACKEND_URL}/api/unitorgknowledge/${id}`)
}

export function getKowner()
{
    return axios.get(`${BACKEND_URL}/api/kowner/`);
}

export function getKownerbyCaseName(id)
{
    return axios.get(`${BACKEND_URL}/api/kownerdetil/IdCaseName/${id}`)
}

export function getSme()
{
    return axios.get(`${BACKEND_URL}/api/smedetil/`)
}

export function getSmebyId(id)
{
    return axios.get(`${BACKEND_URL}/api/sme/${id}`)
}

export function getSmebyCasename(id)
{
    return axios.get(`${BACKEND_URL}/api/smedetil/IdCaseName/${id}`)
}

export function getProbis()
{
    return axios.get(`${BACKEND_URL}/api/bisnisproses/`)
}

export function getProbisByStatus(status)
{
    return axios.get(`${BACKEND_URL}/api/bisnisproses/bystatus/${status}`)
}

export function getSektor()
{
    return axios.get(`${BACKEND_URL}/api/bisnisektor/`)
}

export function getSektorbyStatus(status)
{
    return axios.get(`${BACKEND_URL}/api/bisnisektor/bystatus/${status}`)
}

export function getCasename()
{
    return axios.get(`${BACKEND_URL}/api/casename/`)
}

export function getCasenamebyIdProbis(id)
{
    return axios.get(`${BACKEND_URL}/api/casename/probis/${id}`)
}

export function getSubcase()
{
    return axios.get(`${BACKEND_URL}/api/subcase/`)
}

export function getSubcasebyIdCaseName(id)
{
    return axios.get(`${BACKEND_URL}/api/subcase/bycasename/${id}`)
}

export function getTipeKnowledge()
{
    return axios.get(`${BACKEND_URL}/api/tipeknowledge/`);
}

export function getTipeKnowledgebyId(id)
{
    return axios.get(`${BACKEND_URL}/api/tipeknowledge/${id}`)
}

export function getTipeKnowledgebyStatus(status)
{
    return axios.get(`${BACKEND_URL}/api/tipeknowledge/bystatus/${status}`)
}

export function getTipeKnowledgebyTemplate(template)
{
    return axios.get(`${BACKEND_URL}/api/tipeknowledge/bytemplate/${template}`)
}

export function getKnowledgebyId(id)
{
    return axios.get(`${BACKEND_URL}/api/knowledgeproses/${id}`)
}

export function getKnowledgebyJudul(judul)
{
    return axios.get(`${BACKEND_URL}/api/knowledgeproses/byjudul/${judul}`)
}

export function updateKnowledge(
    id,
    id_csname,
    id_ko,
    id_probis,
    id_sektor,
    id_sme,
    id_subcase,
    id_tipe_km,
    judul,
    nip_pkp,
    nip_ko,
    nip_sme,
    wkt_ko,
    wkt_pkp,
    wkt_publish,
    wkt_sme,
    reviewer_pkp,
    reviewer_ko,
    reviewer_sme
) {
    return axios.put(`${BACKEND_URL}/api/knowledgeproses/${id}`, {
        id_csname,
        id_ko,
        id_probis,
        id_sektor,
        id_sme,
        id_subcase,
        id_tipe_km,
        judul,
        nip_pkp,
        nip_ko,
        nip_sme,
        wkt_ko,
        wkt_pkp,
        wkt_publish,
        wkt_sme,
        reviewer_pkp,
        reviewer_ko,
        reviewer_sme
    })
}

export function updateKnowledgeDisposisiPKP(
    id,
    catatan_pkp,
    id_csname,
    id_ko,
    id_probis,
    id_sektor,
    id_sme,
    id_subcase,
    id_tipe_km,
    kategori,
    level_knowledge
) {
    return axios.put(`${BACKEND_URL}/api/knowledgeproses/${id}`, {
        catatan_pkp,
        id_csname,
        id_ko,
        id_probis,
        id_sektor,
        id_sme,
        id_subcase,
        id_tipe_km,
        kategori,
        level_knowledge
    })
}

export function updateKnowledgeDisposisiKO(
    id,
    catatan_ko,
    id_csname,
    id_ko,
    id_probis,
    id_sektor,
    id_sme,
    id_subcase,
    id_tipe_km,
    kategori
) {
    return axios.put(`${BACKEND_URL}/api/knowledgeproses/${id}`, {
        catatan_ko,
        id_csname,
        id_ko,
        id_probis,
        id_sektor,
        id_sme,
        id_subcase,
        id_tipe_km,
        kategori
    })
}

export function updateKnowledgePublishPKP(
    id,
    id_csname,
    id_ko,
    id_probis,
    id_sektor,
    id_sme,
    id_subcase,
    id_tipe_km,
    kategori,
    level_knowledge
) {
    return axios.put(`${BACKEND_URL}/api/knowledgeproses/${id}`, {
        id_csname,
        id_ko,
        id_probis,
        id_sektor,
        id_sme,
        id_subcase,
        id_tipe_km,
        kategori,
        level_knowledge
    })
}

export function updateKnowledgeTolak(
    id,
    catatan_tolak,
    id_csname,
    id_ko,
    id_probis,
    id_sektor,
    id_sme,
    id_subcase,
    id_tipe_km,
    reviewer_tolak
) {
    return axios.put(`${BACKEND_URL}/api/knowledgeproses/${id}`, {
        catatan_tolak,
        id_csname,
        id_ko,
        id_probis,
        id_sektor,
        id_sme,
        id_subcase,
        id_tipe_km,
        reviewer_tolak
    })
}

export function getSuccessStory()
{
    return axios.get(`${BACKEND_URL}/api/successstory/`)
}

export function getSuccessStorybyId(id)
{
    return axios.get(`${BACKEND_URL}/api/successstory/${id}`)
}

export function getSuccessStorybyIdKM(id)
{
    return axios.get(`${BACKEND_URL}/api/successstory/bykmpro/${id}`)
}

export function updateSuccessStory(
  id,
  daftar_isi,
  definisi,
  id_km_pro,
  id_tipe_km,
  materi,
  pendahuluan,
  tagging
) {
    return axios.put(`${BACKEND_URL}/api/successstory/${id}`, {
    daftar_isi,
    definisi,
    id_km_pro,
    id_tipe_km,
    materi,
    pendahuluan,
    tagging
    });
  }

export function updateSuccessStoryMateri(
    id,
    materi
){
    return axios.put(`${BACKEND_URL}/api/successstory/${id}`, {
        materi
    })
}

export function getPeraturanTerkaitbyIdKM(id)
{
    return axios.get(`${BACKEND_URL}/api/peraturanterkait/byidknowledgeproses/${id}`)
}

export function getPeraturanTerkaitbyId(id)
{
    return axios.get(`${BACKEND_URL}/api/peraturanterkait/${id}`)
}

export function addPeraturanTerkait(
    id_km_pro,
    id_peraturan
  ) {
    return axios.post(`${BACKEND_URL}/api/peraturanterkait/`, {
    id_km_pro: id_km_pro,
    id_peraturan: id_peraturan
    });
  }

export function addLihatPula(
    id_km_pro,
    judul,
    link
  ) {
    return axios.post(`${BACKEND_URL}/api/lihatpula/`, {
    id_km_pro: id_km_pro,
    judul: judul,
    link: link
    });
  }

export function getLihatPulabyIdKM(id)
  {
    return axios.get(`${BACKEND_URL}/api/lihatpula/byidknowledgeproses/${id}`)
  }

export function deleteLihatPula(id)
{
    return axios.delete(`${BACKEND_URL}/api/lihatpula/${id}`)
}

export function updatePeraturanTerkait(
    id,
    id__km_pro,
    id_peraturan
    )
{
    return axios.put(`${BACKEND_URL}/api/peraturanterkait/${id}`,{
    id__km_pro,
    id_peraturan
    })
}

export function deletePeraturanTerkait(id)
{
    return axios.delete(`${BACKEND_URL}/api/peraturanterkait/${id}`)
}

export function getLevelAksesbyIdKM(id)
{
    return axios.get(`${BACKEND_URL}/api/leveldetil/byidknowledgeproses/${id}`)
}

export function getLevelAksesbyId(id)
{
    return axios.get(`${BACKEND_URL}/api/leveldetil/${id}`)
}

export function saveLevelAkses(
    id_km_pro,
    kd_jabatan,
    kd_unit_es2,
    kd_unit_es3,
    kd_unit_es4,
    nm_es2,
    nm_es3,
    nm_es4,
    nm_jabatan
    )
{
    return axios.post(`${BACKEND_URL}/api/leveldetil/`, {
        id_km_pro,
        kd_jabatan,
        kd_unit_es2,
        kd_unit_es3,
        kd_unit_es4,
        nm_es2,
        nm_es3,
        nm_es4,
        nm_jabatan
    })
}

export function updateLevelAkses(
    id,
    id_km_pro,
    kd_jabatan,
    kd_unit_es2,
    kd_unit_es3,
    kd_unit_es4,
    nm_es2,
    nm_es3,
    nm_es4,
    nm_jabatan
    )
{
    return axios.put(`${BACKEND_URL}/api/leveldetil/${id}`, {
        id_km_pro,
        kd_jabatan,
        kd_unit_es2,
        kd_unit_es3,
        kd_unit_es4,
        nm_es2,
        nm_es3,
        nm_es4,
        nm_jabatan
    })
}

export function deleteLevelAkses(id)
{
    return axios.delete(`${BACKEND_URL}/api/leveldetil/${id}`)
}

export function getReferensibyIdKM(id)
{
    return axios.get(`${BACKEND_URL}/api/referensi/byidknowledgeproses/${id}`)
}

export function getReferensibyId(id)
{
    return axios.get(`${BACKEND_URL}/api/referensi/${id}`)
}

export function saveReferensi(
    id_km_pro,
    judul,
    link
  ) {
    return axios.post(`${BACKEND_URL}/api/referensi/`, {
    id_km_pro,
    judul,
    link
    });
  }

export function updateReferensi(
    id,
    id_km_pro,
    judul,
    link
  ) {
    return axios.put(`${BACKEND_URL}/api/referensi/${id}`, {
    id_km_pro,
    judul,
    link
    });
  }

export function deleteReferensi(id)
{
    return axios.delete(`${BACKEND_URL}/api/referensi/${id}`)
}

export function getBacaanbyIdKM(id)
{
    return axios.get(`${BACKEND_URL}/api/bacaanlanjutan/byidknowledgeproses/${id}`)
}

export function getBacaanbyId(id)
{
    return axios.get(`${BACKEND_URL}/api/bacaanlanjutan/${id}`)
}

export function saveBacaan(
    id_km_pro,
    judul,
    link
  ) {
    return axios.post(`${BACKEND_URL}/api/bacaanlanjutan/`, {
    id_km_pro,
    judul,
    link
    });
  }

export function updateBacaan(
    id,
    id_km_pro,
    judul,
    link
  ) {
    return axios.put(`${BACKEND_URL}/api/bacaanlanjutan/${id}`, {
    id_km_pro,
    judul,
    link
    });
  }

export function deleteBacaan(id)
{
    return axios.delete(`${BACKEND_URL}/api/bacaanlanjutan/${id}`)
}

export function getDasarHukumbyIdKM(id)
{
    return axios.get(`${BACKEND_URL}/api/dasarhukum/byidknowledgeproses/${id}`)
}

export function getDasarHukumbyId(id)
{
    return axios.get(`${BACKEND_URL}/api/dasarhukum/${id}`)
}

export function saveDasarHukum(
    id_km_pro,
    judul,
    link
  ) {
    return axios.post(`${BACKEND_URL}/api/dasarhukum/`, {
    id_km_pro,
    judul,
    link
    });
  }

export function updateDasarHukum(
    id,
    id_km_pro,
    judul,
    link
  ) {
    return axios.put(`${BACKEND_URL}/api/dasarhukum/${id}`, {
    id_km_pro,
    judul,
    link
    });
  }

export function deleteDasarHukum(id)
{
    return axios.delete(`${BACKEND_URL}/api/dasarhukum/${id}`)
}

export function getUploadLampiranbyId(id)
{
    return axios.get(`${BACKEND_URL}/api/uploadterkait/byidknowledgeproses/${id}`)
}

export function saveUploadLampiran(
    id_km_pro,
    link
    ){
        return axios.post(`${BACKEND_URL}/api/uploadterkait/`,{
            id_km_pro,
            link
        })
    }

export function updateUploadLampiran(
    id,
    id_km_pro,
    link
    ){
        return axios.put(`${BACKEND_URL}/api/uploadterkait/${id}`,{
            id_km_pro,
            link
        })
    }
export function deleteUploadLampiran(id)
{
    return axios.delete(`${BACKEND_URL}/api/uploadterkait/${id}`)
}

export function getSopbyIdKM(id)
{
    return axios.get(`${BACKEND_URL}/api/soptemp/bykmpro/${id}`)
}

export function updateSop(
    id,
    id_km_pro,
    id_tipe_km,
    materi
  )
{
    return axios.put(`${BACKEND_URL}/api/soptemp/${id}`, {
        id_km_pro,
        id_tipe_km,
        materi
    })
}

export function updateSopMateri(
    id,
    materi
    )
{
    return axios.put(`${BACKEND_URL}/api/soptemp/${id}`, {
        materi
    })
}

export function getOtherKnowledgebyIdKM(id)
{
    return axios.get(`${BACKEND_URL}/api/defaulttemp/bykmpro/${id}`)
}

export function updateOtherKnowledge(
    id,
    id_km_pro,
    id_tipe_km,
    materi
) {
    return axios.put(`${BACKEND_URL}/api/defaulttemp/${id}`, {
    id_km_pro,
    id_tipe_km,
    materi
    })
}

export function updateOtherKnowledgeMateri(
    id,
    materi
){
    return axios.put(`${BACKEND_URL}/api/defaulttemp/${id}`, {
        materi
    })
}

export function updateStatusPKP(
    id,
    statusNow,
    statusNext,
    kategori = null,
    level_knowledge = null,
    id_ko = null,
    alasan = null
  ) {
    if (statusNext == 7) {
        if (statusNow == 2) {
        return axios.put(
        `${BACKEND_URL}/api/knowledgeproses/updateknowledgeprosesstatus/${id}/${statusNext}`,
        { kategori: kategori, level_knowledge: level_knowledge }
      );
        }
      // return axios.put(`${BACKEND_BACKEND_URL}/api/kmkodefikasi/updatekmkodefikasistatus/${id}/${status}`, {
      //   alasan_tolak: alasan,
  
      // });
    } else if (statusNext == 4) {
        if (statusNow == 2) {
        return axios.put(
            `${BACKEND_URL}/api/knowledgeproses/updateknowledgeprosesstatus/${id}/${statusNext}`,
            { id_ko: id_ko, catatan_pkp: alasan }
        );
        }
    } else if (statusNext == 5) {
        if (statusNow == 3) {
        return axios.put(
            `${BACKEND_URL}/api/knowledgeproses/updateknowledgeprosesstatus/${id}/${statusNext}`,
            { id_ko: id_ko, catatan_pkp: alasan }
        );
        }
    } else if (statusNext == 8) {
        if (statusNow == 3) {
        return axios.put(
            `${BACKEND_URL}/api/knowledgeproses/updateknowledgeprosesstatus/${id}/${statusNext}`,
            { kategori: kategori, level_knowledge: level_knowledge }
        );
        }
  }
}

export function updateStatusKO(
    id,
    statusNow,
    statusNext,
    kategori = null,
    level_knowledge = null,
    id_sme = null,
    alasan = null
  ) {
    if (statusNext == 7) {
        if (statusNow == 4) {
        return axios.put(
        `${BACKEND_URL}/api/knowledgeproses/updateknowledgeprosesstatus/${id}/${statusNext}`,
        { kategori: kategori, level_knowledge: level_knowledge }
      );
        }
      // return axios.put(`${BACKEND_BACKEND_URL}/api/kmkodefikasi/updatekmkodefikasistatus/${id}/${status}`, {
      //   alasan_tolak: alasan,
  
      // });
    } else if (statusNext == 6) {
        if (statusNow == 4) {
        return axios.put(
            `${BACKEND_URL}/api/knowledgeproses/updateknowledgeprosesstatus/${id}/${statusNext}`,
            { id_sme: id_sme, catatan_ko: alasan }
        );
        }
    }
    if (statusNext == 8) {
        if (statusNow == 5) {
        return axios.put(
        `${BACKEND_URL}/api/knowledgeproses/updateknowledgeprosesstatus/${id}/${statusNext}`,
        { kategori: kategori, level_knowledge: level_knowledge }
      );
        }
      // return axios.put(`${BACKEND_BACKEND_URL}/api/kmkodefikasi/updatekmkodefikasistatus/${id}/${status}`, {
      //   alasan_tolak: alasan,
  
      // });
    }
}

export function updateStatusSME(id){
    return axios.put(
        `${BACKEND_URL}/api/knowledgeproses/updateknowledgeprosesstatus/${id}/7`,{
            
        }
    )
}

export function updateStatusPublish(id){
    return axios.put(
        `${BACKEND_URL}/api/knowledgeproses/updateknowledgeprosesstatus/${id}/9`,{
            
        }
    )
}

export function tolakPKP(
    id,
    id_status_km,
    catatan_tolak
    )
{
    return axios.put(`${BACKEND_URL}/api/knowledgeproses/updateknowledgeprosesstatus/${id}/${id_status_km}`,
    {
        catatan_tolak: catatan_tolak
    })
}

export function getReviewKonten()
{
    return axios.get(`${BACKEND_URL}/api/reviewknowledge/`)
}

export function getFeedback()
{
    return axios.get(`${BACKEND_URL}/api/feedback/`)
}

export function getFeedbackbyIsi(isi)
{
    return axios.get(`${BACKEND_URL}/api/feedback/byisi/${isi}`)
}

export function getFeedbackbyId(id)
{
    return axios.get(`${BACKEND_URL}/api/feedback/${id}`)
}

export function tolakFeedback(
    id,
    status,
    catatan_deactivate
  ) {
        return axios.put(`${BACKEND_URL}/api/feedback/${id}/${status}`,
        { catatan_deactivate: catatan_deactivate }
      );
}

export function setujuFeedback(
    id,
    status,
    catatan_activate
  ) {
        return axios.put(`${BACKEND_URL}/api/feedback/${id}/${status}`,
        { catatan_activate: catatan_activate }
      );
}

export function updateWaktuPublish(
    id,
    wkt_publish
) {
    return axios.put(`${BACKEND_URL}/api/knowledgeproses/${id}`, {
        wkt_publish
    })
}

export function getRatingbyIdKM(id)
{
    return axios.get(`${BACKEND_URL}/api/rating/byidknowledgeproses/${id}`)
}

export function getRatingRata()
{
    return axios.get(`${BACKEND_URL}/api/rating/allidkowledgeproses`)
}