import React, { useState, useEffect } from "react";

function PengajuanUsulanFooter({ backAction, btnRef, loading, path }) {
  // const [isComplete, setIsComplete] = useState(false);
  const saveForm = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
      // setIsComplete(true);
      // disabled ? setIsComplete(false) : setIsComplete(true);
    }
  };

  return (
    <>
      <div className="col-lg-12" style={{ textAlign: "right" }}>
        <button
          type="button"
          onClick={backAction}
          className="btn btn-light"
          style={{
            boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
          }}
        >
          <i className="fa fa-arrow-left"></i>
          Kembali
        </button>
        {`  `}
        {path === 'view' ? null : (
          <button
            type="submit"
            className="btn btn-success ml-2"
            onClick={saveForm}
            disabled={false}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
            }}
            // disabled={disabled}
          >
            <i className="fas fa-save"></i>
            Simpan
          </button>
        )}
      </div>
    </>
  );
}

export default PengajuanUsulanFooter;
