/* Library */
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import swal from "sweetalert";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import SVG from "react-inlinesvg";

/* Helpers */
import { Pagination } from "../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../_metronic/_helpers";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import * as columnFormatters from "../../../helpers/column-formatters";
import {
  deleteKnowledgeProcess,
  deleteUsulanPerubahan,
  getKnowledgeProcessByMenu,
  getUsulanPerubahan,
  updateStatusUsulanPerubahan,
} from "../../../references/Api";
import { updateStatusPublish, updateWaktuPublish } from "../Api";

/* Utility */

function PengajuanUsulanTable({ tab = "proses" }) {
  const history = useHistory();
  const [content, setContent] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [sizePage, setSizePage] = useState(50);
  const { user } = useSelector((state) => state.auth);

  const add = () => history.push("/process-knowledge/pengajuan/add");
  const edit = (id_usulan_template) =>
    history.push(
      `/process-knowledge/pengajuan/${id_usulan_template}/edit`
    );
  const review = (id_usulan_template) =>
    history.push(
      `/process-knowledge/pengajuan/${id_usulan_template}/view`

    );
  const deleteAction = (id) => {
    swal({
      title: "Apakah Anda Yakin?",
      text: "Klik OK untuk melanjutkan",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        deleteUsulanPerubahan(id).then(({ status }) => {
          if (status === 200) {
            swal("Berhasil", "Data berhasil dihapus", "success").then(() => {
              history.push("/dashboard");
              history.replace("/process-knowledge/pengajuan");
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push("/dashboard");
              history.replace("/process-knowledge/pengajuan");
            });
          }
        });
      }
      // else {
      //   swal("Your imaginary file is safe!");
      // }
    });
  };
  const getCurrentDate = () => {
    const day = new Date();
    const YYYY = day.getFullYear();
    let MM = day.getMonth() + 1;
    let DD = day.getDate();
    let HH = day.getHours();
    let mm = day.getMinutes();
    let ss = day.getSeconds();
    if (DD < 10) {
      DD = `0${DD}`;
    }
    if (MM < 10) {
      MM = `0${MM}`;
    }

    if (HH < 10) {
      HH = `0${HH}`;
    }
    if (mm < 10) {
      mm = `0${mm}`;
    }
    if (ss < 10) {
      ss = `0${ss}`;
    }
    return YYYY + "-" + MM + "-" + DD + " " + HH + ":" + mm + ":" + ss;
  };
  const apply = (id_usulan_template) => {
    swal({
      title: "Apakah Anda Ingin Mengajukan Usulan Ini?",
      text: "Klik OK untuk melanjutkan",
      icon: "warning",
      buttons: true,
      dangerMode: false,
    }).then((willApply) => {
      if (willApply) {
        updateStatusUsulanPerubahan(id_usulan_template,"","Review Pengajuan Template").then(({ status }) => {
          if (status === 201 || status === 200) {
            // updateWaktuPublish(id_usulan_template, getCurrentDate());
            swal("Berhasil", "Usulan berhasil diajukan", "success").then(
              () => {
                history.push("/dashboard");
                history.replace("/process-knowledge/pengajuan");
              }
            );
          } else {
            swal("Gagal", "Usulan gagal diajukan", "error").then(() => {
              history.push("/dashboard");
              history.replace("/process-knowledge/pengajuan");
            });
          }
        });
      }
    });
  };

  useEffect(() => {
    switch (tab) {
      case "proses":
        getUsulanPerubahan().then(({data})=> {
          data.map(dt => {
            return dt.status !== "Terima Pengajuan Template" ? setContent(content => [...content, dt]): null
          })
          // setContent(data)
          // console.log(data)
        })
        // getKnowledgeProcessByMenu({
        //   menu: ["simple_tacit", "simple_explicit"],
        //   nip: user.nip9,
        //   status: [1, 2, 3, 4, 5, 6, 7, 8, 11, 19],
        // }).then(({ data }) => {
        //   setContent(data);
        // });
        break;
      case "selesai":
        getUsulanPerubahan().then(({data})=> {
          data.map(dt => {
            return dt.status === "Terima Pengajuan Template" ? setContent(content => [...content, dt]): null
          })
          // setContent(data)
          // console.log(data)
        })
        // getKnowledgeProcessByMenu({
        //   menu: ["simple_tacit", "simple_explicit"],
        //   nip: user.nip9,
        //   status: [9, 10],
        // }).then(({ data }) => {
        //   setContent(data);
        // });
        break;

      default:
        break;
    }
  }, [tab]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      // formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: (cell, row, rowIndex) => {
        let rowNumber = (currentPage - 1) * sizePage + (rowIndex + 1);
        return <span>{rowNumber}</span>;
      },
    },
    {
      dataField: "id_usulan_template",
      text: "id",
      sort: true,
      hidden: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "judul_usulan",
      text: "Judul",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "jenis_usulan",
      text: "Jenis Usulan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "tipe",
      text: "Tipe",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "catatan",
      text: "catatan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      hidden: tab === 'selesai' ? false : true
    },
    {
      dataField: "updated_at",
      text: "Updated",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      hidden: true
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: columnFormatters.StatusColumnFormatterProcessKnowledgeUsulanTemplate,
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
        columnFormatters.ActionsColumnFormatterProcessKnowledgeUsulanTemplate,
      formatExtraData: {
        openEditDialog: edit,
        openDeleteDialog: deleteAction,
        publishKnowledge: apply,
        showReview: review,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];
  const initialFilter = {
    sortOrder: "desc", // asc||desc
    sortField: "updated_at",
    pageNumber: currentPage,
    pageSize: sizePage,
  };
  const defaultSorted = [{ dataField: "updated_at", order: "desc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
    onPageChange: (page, sizePerPage) => {
      setCurrentPage(page);
    },
    onSizePerPageChange: (page, sizePerPage) => {
      setSizePage(page);
      setCurrentPage(sizePerPage);
    },
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_usulan_template"
                  data={content}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <button
                            type="button"
                            className="btn btn-primary"
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                              float: "right",
                            }}
                            onClick={add}
                          >
                            <span className="svg-icon menu-icon">
                              <SVG
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Code/Plus.svg"
                                )}
                              />
                            </span>
                            Tambah
                          </button>
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
    </>
  );
}

export default PengajuanUsulanTable;
