import React, { useState, useEffect } from "react";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import "../../../../../helpers/DatePickerStyles.css";
import { Input, Textarea, Select as Sel } from "../../../../../helpers";
import { getSmebyCasename, getSmebyId } from "../../../Api";
import swal from "sweetalert";
import Select from "react-select";

function KnowledgeOwnerDisposisiForm({ content, btnRef, saveForm }) {

  const [ValidateSchema, setValidateSchema] = useState();

  const initialValues = {
    id_km_pro: content.id_km_pro,
    id_sme: "",
    nama: "",
    unit: "",
    jabatan: "",
    catatan_ko: ""
  }

  const [subjectMatter, setSubjectMatter] = useState([])
  const [nip, setNip] = useState([]);
  const [valNip, setValNip] = useState();

  useEffect(() => {
    setValidateSchema(
      Yup.object().shape({
        id_sme: Yup.string().required("Knowledge Owner is required"),
        catatan_ko: Yup.string().required("Catatan PKP is required"),
      })
      );
  }, []);

  useEffect(()=> {
    if(content.id_csname){
      getSmebyCasename(content.id_csname).then(({data}) => {
        data.map(data => {
          return setSubjectMatter(subjectMatter => [...subjectMatter,{
            label: data.sme.nama,
            value: data.sme.id_sme
          }])
        })
      })
    }
  }, [content])

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={initialValues}
        validationSchema={ValidateSchema}
        onSubmit={values => {
          saveForm(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
          values
        }) => {
          const searchNip = () => {
            setFieldValue("id_sme", values.id_sme)
            setFieldValue("nip", "Loading...");
            setFieldValue("unit", "Loading...");
            setFieldValue("jabatan", "Loading...");
            getSmebyId(values.id_sme).then(({ data }) => {
              if (data.id_sme) {
                setFieldValue("nip", data.nip);
                setFieldValue("unit", data.nm_unit);
                setFieldValue(
                  "jabatan",
                  data.nm_jabatan
                );
              } else {
                swal("Gagal", "Data Tidak Tersedia", "error");
                setFieldValue("nip", "");
                setFieldValue("unit", "");
                setFieldValue("jabatan", "");
              }
            });
          }
          const handleSearchNip = (val) => {
            setFieldValue("id_sme", val.value);
            setValNip(val.label);
            setFieldValue("nip", "Loading...");
            setFieldValue("unit", "Loading...");
            setFieldValue("jabatan", "Loading...");
            getSmebyId(val.value).then(({ data }) => {
              if (data.id_sme) {
                setFieldValue("nip", data.nip);
                setFieldValue("unit", data.nm_unit);
                setFieldValue(
                  "jabatan",
                  data.nm_jabatan
                );
              } else {
                swal("Gagal", "Data Tidak Tersedia", "error");
                setFieldValue("nip", "");
                setFieldValue("unit", "");
                setFieldValue("jabatan", "");
              }
            });
          }
          return (
            <>
            <Form className="form form-label-right">
                {/* Field NIP */}
                <div className="form-group row">
                <label className="col-xl-3 col-lg-3 col-form-label">
                    Nama
                  </label>
                  <div className="col-lg-9 col-xl-6">
                  <Select
                      options={subjectMatter}
                      onChange={value => handleSearchNip(value)}
                      value={subjectMatter.filter(
                        data => data.label === valNip
                      )}
                    />
                  </div>

                </div>
                {/* Field Nama */}
                <div className="form-group row">
                  <Field
                    name="nip"
                    component={Input}
                    placeholder="NIP"
                    label="NIP"
                    disabled
                  />
                </div>
                {/* Field Unit */}
                <div className="form-group row">
                  <Field
                    name="unit"
                    component={Input}
                    placeholder="Unit"
                    label="Unit"
                    disabled
                  />
                </div>
                {/* Field Jabatan */}
                <div className="form-group row">
                  <Field
                    name="jabatan"
                    component={Input}
                    placeholder="Jabatan"
                    label="Jabatan"
                    disabled
                  />
                </div>
                {/* Field Catatan Disposisi */}
                <div className="form-group row">
                <Field
                    name="catatan_ko"
                    component={Textarea}
                    placeholder="Catatan Disposisi"
                    label="Catatan Disposisi"
                />
                </div>

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default KnowledgeOwnerDisposisiForm;
