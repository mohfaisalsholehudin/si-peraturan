/* Library */
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";

/* Helpers */
import { Pagination } from "../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../../../helpers/column-formatters";
import { getReview } from "../Api"

function KnowledgeOwnerTable() {
  
  const history = useHistory();
  const [content, setContent] = useState([]);

  useEffect(()=> {
    getReview().then(({ data }) => {
      data.map(dt => {
        return dt.statusKm.id_status_km === 4 ?
        setContent(content => [...content, dt])
      : dt.statusKm.id_status_km === 5 ?
        setContent(content => [...content, dt])
      : dt.statusKm.id_status_km === 6 ?
        setContent(content => [...content, dt])
      : null
      })
      })
    },[])

    const review = (id_km_pro, jenis, template) => {
      switch (template) {
        case "Success Story":
            history.push(`/process-knowledge/review-ko/${id_km_pro}/success-story/${jenis}/1/review`);
          break;
        case "Other Knowledge":
          history.push(`/process-knowledge/review-ko/${id_km_pro}/other-knowledge/${jenis}/1/review`);
          break;
        case "SOP":
          history.push(`/process-knowledge/review-ko/${id_km_pro}/sop/${jenis}/1/review`);
          break;
        default:
          break;
      }
    };

  const deleteAction = id => {
  
  };
  const apply = () => {
     
  }

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "judul",
      text: "Judul",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "tipeKnowledge.template",
      text: "Tipe Knowledge",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "tipeKnowledge.jenis",
      text: "Jenis",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "nama_kontri",
      text: "Kontributor",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "reviewer_ko",
      text: "Reviewer",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "wkt_ko",
      text: "Waktu Review",
      sort: true,
      formatter: columnFormatters.DateFormatterReviewKo,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "statusKm.nama",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: columnFormatters.StatusColumnFormatterProcessKnowledgeAdded,
    },
    {
      dataField: "statusKm.id_status_km",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: columnFormatters.StatusColumnFormatterProcessKnowledgeAdded,
      hidden: true,
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterProcessKnowledgeKO,
      formatExtraData: {
        reviewProposal: review,
        openDeleteDialog: deleteAction,
        applyProposal: apply
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "statusKm.id_status_km",
    pageNumber: 1,
    pageSize: 50
  };
  const defaultSorted = [{ dataField: "statusKm.id_status_km", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_km_pro"
                  data={content}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
    </>
  );
}

export default KnowledgeOwnerTable;
