/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { Modal } from "react-bootstrap";
import { Field, Formik, Form, ErrorMessage } from "formik";
import Select from "react-select";
import * as Yup from "yup";
import "../../../../../../helpers/DatePickerStyles.css";
import axios from "axios";
import swal from "sweetalert";
import { useSelector } from "react-redux";
import { useSubheader } from "../../../../../../../_metronic/layout";
import { saveLevelAkses, updateKnowledge, updateKnowledgePublishPKP } from "../../../../Api";
import { useHistory } from "react-router-dom";

function KategoriPengetahuanModal({ id_level_detil, show, onHide, content, id_km_pro, jenis, step, knowledge }) {
  const { BACKEND_URL } = window.ENV;
  const history = useHistory();
  const [title, setTitle] = useState("");
  const [es2, setEs2] = useState([]);
  const [valEs2, setValEs2] = useState();
  const [es3, setEs3] = useState([]);
  const [valEs3, setValEs3] = useState();
  const [es4, setEs4] = useState([]);
  const [valEs4, setValEs4] = useState();
  const [jabatan, setJabatan] = useState([]);
  const [valJabatan, setValJabatan] = useState()
  const { iamToken } = useSelector(state => state.auth);
  const subheader = useSubheader();

  const initialValues = {
    id_km_pro: id_km_pro,
    nm_es2: "",
    nm_es3: "",
    nm_es4: "",
    nm_jabatan:"",
    kd_unit_es2: "",
    kd_unit_es3: "",
    kd_unit_es4: "",
    kd_jabatan:""
  };

  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    nm_es2: Yup.string().required("Eselon 2 is required"),
    nm_es3: Yup.string().required("Eselon 3 is required"),
    kd_unit_es2: Yup.string().required("Eselon 2 is required"),
    kd_unit_es3: Yup.string().required("Eselon 3 is required"),
    nm_jabatan: Yup.string().required("Jabatan is required"),
    kd_jabatan: Yup.string().required("Jabatan is required")
  });

  const renderError = (message) => <p style={{ color: "red" }}>{message}</p>;

  useEffect(() => {
    let _title = id_level_detil ? "Edit" : "Tambah";
    setTitle(_title);
    subheader.setTitle(_title);
  }, [id_level_detil, subheader]);

  useEffect(() => {
    getKantor(2).then(({ data }) => {
      if(data.slice(0,3) !== '401'){
        data.map(data => {
          return setEs2(es2 => [
            ...es2,
            {
              label: data.nama,
              value: data.legacyKode
            }
          ]);
        });
      }else{
        // history.push("/logout");
        swal({
          title: "Maaf session anda telah habis, silahkan login ulang",
          text: "Klik OK untuk melanjutkan",
          icon: "info",
          closeOnClickOutside:false
        }).then((willApply) => {
          if (willApply) {
            history.push('/logout');
          }
        });
      }
    });
  }, []);

  useEffect(()=> {
    if(content.kd_unit_es2){
      es2
      .filter(data => data.value === content.kd_unit_es2)
      .map(data => {
        setValEs2(data.label);
      });
    }
  },[content.kd_unit_es2, es2])

  useEffect(()=> {
    if(content.kd_unit_es3){
        getKantor(content.kd_unit_es2).then(({ data }) => {
        data.map(data => {
          return setEs3(es3 => [
            ...es3,
            {
              label: data.nama,
              value: data.legacyKode
            }
          ]);
        });
      })
    }
  },[content.kd_unit_es3])

  useEffect(()=> {
    if(content.kd_unit_es4){
        getKantor(content.kd_unit_es3).then(({ data }) => {
        data.map(data => {
          return setEs4(es4 => [
            ...es4,
            {
              label: data.nama,
              value: data.legacyKode
            }
          ]);
        });
      })
    }
  },[content.kd_unit_es4])

  useEffect(()=> {
    if(content.kd_jabatan){
      getKantor(content.kd_unit_es4).then(({ data }) => {
      data.map(data => {
        return setJabatan(jabatan => [
          ...jabatan,
          {
            label: data.nama,
            value: data.legacyKode
          }
        ]);
      });
    })
  }
},[content.kd_jabatan])

  useEffect(()=> {
    if(content.kd_unit_es3){
      es3
      .filter(data => data.value === content.kd_unit_es3)
      .map(data => {
        setValEs3(data.label);
      });
    }
  },[es3])

  useEffect(()=> {
    if(content.kd_unit_es4){
      es4
      .filter(data => data.value === content.kd_unit_es4)
      .map(data => {
        setValEs4(data.label);
      });
    }
  },[es4])

  useEffect(()=> {
    if(content.kd_jabatan){
      jabatan
      .filter(data => data.value === content.kd_jabatan)
      .map(data => {
        setValJabatan(data.label);
      });
    }
  },[jabatan])

  const getKantor = (code => {
    return axios.post(
      `${BACKEND_URL}/api/iam/getkantorfilter?parentlegacy=${code}`,
      {},
      {
        headers: { Authorization: iamToken, "Content-Type": "application/json" }
      }
    );
  });

  const getJabatan = (code => {
    return axios.post(
      `${BACKEND_URL}/api/iam/getjabatanfilter?unitlegacy=${code}`,
      {},
      {
        headers: { Authorization: iamToken, "Content-Type": "application/json" }
      }
    );
  });

  const saveForm = values => {
      saveLevelAkses(
        values.id_km_pro,
        values.kd_jabatan,
        values.kd_unit_es2,
        values.kd_unit_es3,
        valEs4 ? values.kd_unit_es4 : " ",
        values.nm_es2,
        values.nm_es3,
        valEs4 ? values.nm_es4 : " ",
        values.nm_jabatan
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Level Akses berhasil disimpan", "success").then(
            () => {
              history.push('/dashboard');
              history.replace(`/process-knowledge/review-ko/${id_km_pro}/other-knowledge/${jenis}/${step}/review`);
            }
          );
        } else {
          swal("Gagal", "Level Akses gagal disimpan", "error").then(() => {
            history.push('/dashboard');
            history.replace(`/process-knowledge/review-ko/${id_km_pro}/other-knowledge/${jenis}/${step}/review/kategori-pengetahuan/open`);
          });
        }
      })
      updateKnowledgePublishPKP(
        id_km_pro,
        knowledge.id_csname,
        knowledge.id_ko,
        knowledge.id_probis,
        knowledge.id_sektor,
        knowledge.id_sme,
        knowledge.id_subcase,
        knowledge.id_tipe_km,
        knowledge.kategori,
        1
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Level Akses berhasil disimpan", "success").then(
            () => {
              history.push('/dashboard');
              history.replace(`/process-knowledge/review-ko/${id_km_pro}/other-knowledge/${jenis}/${step}/review`);
            }
          );
        } else {
          swal("Gagal", "Level Akses gagal disimpan", "error").then(() => {
            history.push('/dashboard');
            history.replace(`/process-knowledge/review-ko/${id_km_pro}/other-knowledge/${jenis}/${step}/review/kategori-pengetahuan/open`);
          });
        }
      })
  }

  return (
    <Modal
      size="lg"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          {title}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <>
          <Formik
            enableReinitialize={true}
            initialValues={initialValues}
            validationSchema={ProposalEditSchema}
            onSubmit={values => {
              saveForm(values);
            }}
          >
            {({
              handleSubmit,
              setFieldValue
            }) => {
              const handleChangeEs2 = val => {
                setFieldValue("nm_es2", val.label);
                setFieldValue("kd_unit_es2", val.value);
                setFieldValue("nm_es3", []);
                setFieldValue("kd_unit_es3", []);
                setFieldValue("nm_es4", []);
                setFieldValue("kd_unit_es4", []);
                setFieldValue("nm_jabatan", []);
                setFieldValue("kd_jabatan", []);
                setValEs2(val.label);
                setValEs3([]);
                setEs3([]);
                setValEs4([]);
                setEs4([]);
                setValJabatan([]);
                setJabatan([]);
                getKantor(val.value).then(({ data }) => {
                  data.map(data => {
                    return setEs3(es3 => [
                      ...es3,
                      {
                        label: data.nama,
                        value: data.legacyKode
                      }
                    ]);
                  });
                });
              };

              const handleChangeEs3 = val => {
                setFieldValue("nm_es3", val.label);
                setFieldValue("kd_unit_es3", val.value);
                setFieldValue("nm_es4", "");
                setFieldValue("kd_unit_es4", "");
                setFieldValue("nm_jabatan", []);
                setFieldValue("kd_jabatan", []);
                setValEs3(val.label);
                setValEs4([]);
                setEs4([]);
                setValJabatan([]);
                getJabatan(val.value).then(({data}) => {
                  data.map(data => {
                    return setJabatan(jabatan => [
                      ...jabatan,
                      {
                        label: data.nama,
                        value: data.legacyKode
                      }
                    ])
                  })
                })
                getKantor(val.value).then(({ data }) => {
                  data.map(data => {
                    return setEs4(es4 => [
                      ...es4,
                      {
                        label: data.nama,
                        value: data.legacyKode
                      }
                    ]);
                  });
                });
              };

              const handleChangeEs4 = val => {
                setFieldValue("nm_es4", val.label);
                setFieldValue("kd_unit_es4", val.value);
                setFieldValue("nm_jabatan", []);
                setFieldValue("kd_jabatan", []);
                setValEs4(val.label);
                setJabatan([]);
                setValJabatan([]);
                getJabatan(val.value).then(({data}) => {
                  data.map(data => {
                    return setJabatan(jabatan => [
                      ...jabatan,
                      {
                        label: data.nama,
                        value: data.legacyKode
                      }
                    ])
                  })
                })
              };

              const handleChangeJabatan = val => {
                setFieldValue("nm_jabatan", val.label);
                setFieldValue("kd_jabatan", val.value);
                setValJabatan(val.label);
              };

              return (
                <>
                  <Form className="form form-label-right">
                    {/* Field Eselon 2 */}
                    <div className="form-group row">
                      <label className="col-xl-3 col-lg-3 col-form-label">
                        Unit Eselon 2
                      </label>
                      <div className="col-lg-9 col-xl-6">
                        <Select
                          options={es2}
                          onChange={value => handleChangeEs2(value)}
                          value={es2.filter(data => data.label === valEs2)}
                        />
                        <ErrorMessage name="kd_unit_es2" render={renderError} />
                      </div>
                    </div>

                    {/* Field Eselon 3 */}
                    <div className="form-group row">
                      <label className="col-xl-3 col-lg-3 col-form-label">
                        Unit Eselon 3
                      </label>
                      <div className="col-lg-9 col-xl-6">
                        <Select
                          options={es3}
                          onChange={value => handleChangeEs3(value)}
                          value={es3.filter(data => data.label === valEs3)}
                        />
                        <ErrorMessage name="kd_unit_es3" render={renderError} />
                      </div>
                    </div>
                    {/* Field Eselon 4 */}
                    <div className="form-group row">
                      <label className="col-xl-3 col-lg-3 col-form-label">
                        Unit Eselon 4
                      </label>
                      <div className="col-lg-9 col-xl-6">
                        <Select
                          options={es4}
                          onChange={value => handleChangeEs4(value)}
                          value={es4.filter(data => data.label === valEs4)}
                        />
                        <ErrorMessage name="kd_unit_es4" render={renderError} />
                      </div>
                    </div>
                    {/* Field Jabatan */}
                    <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                        Jabatan
                      </label>
                      <div className="col-lg-9 col-xl-6">
                        <Select
                          options={jabatan}
                          onChange={value => handleChangeJabatan(value)}
                          value={jabatan.filter(data => data.label === valJabatan)}
                        />
                        <ErrorMessage name="kd_jabatan" render={renderError} />
                      </div>
                    </div>
                    <div className="col-lg-12" style={{ textAlign: "center" }}>
                    <button
                      type="button"
                      onClick={onHide}
                      className="btn btn-light"
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                      }}
                    >
                      <i className="flaticon2-cancel icon-nm"></i>
                      Batal
                    </button>
                    {`  `}
                    <button
                      type="submit"
                      onSubmit={() => handleSubmit()}
                      className="btn btn-success ml-2"
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                      }}
                    >
                      <i className="fas fa-check"></i>
                      Kirim
                    </button>
                  </div>
                  </Form>
                </>
              );
            }}
          </Formik>
        </>
      </Modal.Body>
    </Modal>
  );
}

export default KategoriPengetahuanModal;
