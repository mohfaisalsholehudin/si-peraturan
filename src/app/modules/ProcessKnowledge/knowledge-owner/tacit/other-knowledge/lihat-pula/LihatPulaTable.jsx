import React, { useState, useEffect } from "react";
import swal from "sweetalert";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";
import { Route, useHistory } from "react-router-dom";

//Helpers
import {
  sortCaret,
  headerSortingClasses
} from "../../../../../../../_metronic/_helpers";
import { Pagination } from "../../../../../../helpers/pagination/Pagination";
import * as columnFormatters from "../../../../../../helpers/column-formatters";
import LihatPulaModal from "./LihatPulaModal";
import { deleteLihatPula, getLihatPulabyIdKM } from "../../../../Api"

function LihatPulaTable({ id_km_pro, step, jenis, lastPath }) {
    
    const history = useHistory();
    const [content, setContent] = useState([]);

  useEffect(() => {
    getLihatPulabyIdKM(id_km_pro).then(({ data }) => {
      setContent(data);
    });
  }, []);

  const add = () => {
    history.push(
      `/process-knowledge/review-ko/${id_km_pro}/other-knowledge/${jenis}/${step}/review/lihat-pula/open`
    );
  };

  const deleteDialog = id_lihatpula => {
    swal({
      title: "Hapus",
      text: "Apakah Anda yakin menghapus data ini ?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
      .then((ret) => {
        if (ret == true) {
          deleteLihatPula(id_lihatpula)
            .then(({ data }) => {
              if (data.deleted == true) {
                swal("Berhasil", "Data berhasil dihapus", "success").then(
                  () => {
                    history.push('/dashboard');
                    history.replace(`/process-knowledge/review-ko/${id_km_pro}/other-knowledge/${jenis}/${step}/review`);
                  }
                );
              } else {
                swal("Gagal", "Data gagal dihapus", "error").then(
                  () => {
                    history.push('/dashboard');
                    history.replace(`/process-knowledge/review-ko/${id_km_pro}/other-knowledge/${jenis}/${step}/review`);
                  }
                );
              }
            })
        }
      });
  };

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "judul",
      text: "Judul Pengetahuan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "link",
      text: "Jenis Pengetahuan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
        columnFormatters.ActionsColumnFormatterProcessKnowledgeLihatPula,
      formatExtraData: {
        openDeleteDialog: deleteDialog
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];

  const emptyDataMessage = () => {
    return <div className="text-center">No Data to Display</div>;
  };
  const { SearchBar } = Search;
  const initialFilter = {
    sortOrder: "desc", // asc||desc
    sortField: "id_lihatpula",
    pageNumber: 1,
    pageSize: 50
  };
  const defaultSorted = [{ dataField: "id_lihatpula", order: "desc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_lihatpula"
                  data={content}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-12 col-xl-12 mb-3">
                          <h4> Lihat Pula</h4>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div
                          className="col-lg-6 col-xl-6 mb-3"
                          style={{ textAlign: "right" }}
                        >
                          <button
                            type="button"
                            className="btn btn-primary ml-3"
                            style={{
                              float: "right"
                            }}
                            onClick={add}
                          >
                            <i className="fa fa-plus"></i>
                            Tambah
                          </button>
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
      {/*Route to Open Details*/}
      <Route path="/process-knowledge/review-ko/:id_km_pro/other-knowledge/:jenis/:step/review/lihat-pula/open">
        {({ history, match }) => (
          <LihatPulaModal
            show={match != null}
            id_lihatpula={match && match.params.id_lihatpula}
            id_km_pro={id_km_pro}
            jenis={jenis}
            step={step}
            after={false}
            onHide={() => {
              history.push(
                `/process-knowledge/review-ko/${id_km_pro}/other-knowledge/${jenis}/${step}/review`
              );
            }}
            onRef={() => {
              history.push(
                `/process-knowledge/review-ko/${id_km_pro}/other-knowledge/${jenis}/${step}/review`
              );
            }}
          />
        )}
      </Route>
      {/*Route to Edit Details*/}
      <Route path="/process-knowledge/review-ko/:id_km_pro/other-knowledge/:jenis/:step/review/lihat-pula/:id_lihatpula/edit">
        {({ history, match }) => (
          <LihatPulaModal
            show={match != null}
            id_lihatpula={match && match.params.id_lihatpula}
            id_km_pro={id_km_pro}
            jenis={jenis}
            step={step}
            after={false}
            onHide={() => {
              history.push(
                `/process-knowledge/review-ko/${id_km_pro}/other-knowledge/${jenis}/${step}/review`
              );
            }}
            onRef={() => {
              history.push(
                `/process-knowledge/review-ko/${id_km_pro}/other-knowledge/${jenis}/${step}/review`
              );
            }}
          />
        )}
      </Route>
    </>
  );
}

export default LihatPulaTable;
