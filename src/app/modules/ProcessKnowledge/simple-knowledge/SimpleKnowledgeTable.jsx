/* Library */
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import swal from "sweetalert";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import SVG from "react-inlinesvg";

/* Helpers */
import { Pagination } from "../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../_metronic/_helpers";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import * as columnFormatters from "../../../helpers/column-formatters";
import {
  deleteKnowledgeProcess,
  getKnowledgeProcessByMenu,
  getKnowledgeProcessByNipStatus,
  updateStatusKnowledgeProcess,
} from "../../../references/Api";
import { updateStatusPublish, updateWaktuPublish } from "../Api";

/* Utility */

function SimpleKnowledgeTable({ tab = "proses" }) {
  const history = useHistory();
  const [content, setContent] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [sizePage, setSizePage] = useState(50);
  const { user } = useSelector((state) => state.auth);

  const add = () => history.push("/process-knowledge/simple-knowledge/add");
  const edit = (id_km_pro, id_tipe_km) =>
    history.push(
      `/process-knowledge/simple-knowledge/${id_tipe_km}/${id_km_pro}/1/edit`
    );
  const review = (id_km_pro, id_tipe_km) =>
    history.push(
      `/process-knowledge/simple-knowledge/${id_tipe_km}/${id_km_pro}/1/view`
    );
  const deleteAction = (id) => {
    swal({
      title: "Apakah Anda Yakin?",
      text: "Klik OK untuk melanjutkan",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        deleteKnowledgeProcess(id).then(({ status }) => {
          if (status === 200) {
            swal("Berhasil", "Data berhasil dihapus", "success").then(() => {
              history.push("/dashboard");
              history.replace("/process-knowledge/simple-knowledge");
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push("/dashboard");
              history.replace("/process-knowledge/simple-knowledge");
            });
          }
        });
      }
      // else {
      //   swal("Your imaginary file is safe!");
      // }
    });
  };
  const getCurrentDate = () => {
    const day = new Date();
    const YYYY = day.getFullYear();
    let MM = day.getMonth() + 1;
    let DD = day.getDate();
    let HH = day.getHours();
    let mm = day.getMinutes();
    let ss = day.getSeconds();
    if (DD < 10) {
      DD = `0${DD}`;
    }
    if (MM < 10) {
      MM = `0${MM}`;
    }

    if (HH < 10) {
      HH = `0${HH}`;
    }
    if (mm < 10) {
      mm = `0${mm}`;
    }
    if (ss < 10) {
      ss = `0${ss}`;
    }
    return YYYY + "-" + MM + "-" + DD + " " + HH + ":" + mm + ":" + ss;
  };
  const apply = (id_km_pro) => {
    swal({
      title: "Apakah Anda Ingin Mengajukan Usulan Ini?",
      text: "Klik OK untuk melanjutkan",
      icon: "warning",
      buttons: true,
      dangerMode: false,
    }).then((willApply) => {
      if (willApply) {
        updateStatusPublish(id_km_pro).then(({ status }) => {
          if (status === 201 || status === 200) {
            updateWaktuPublish(id_km_pro, getCurrentDate());
            swal("Berhasil", "Knowledge berhasil diterbitkan", "success").then(
              () => {
                history.push("/dashboard");
                history.replace("/process-knowledge/simple-knowledge");
              }
            );
          } else {
            swal("Gagal", "Knowledge gagal diterbitkan", "error").then(() => {
              history.push("/dashboard");
              history.replace("/process-knowledge/simple-knowledge");
            });
          }
        });
      }
    });
  };

  useEffect(() => {
    switch (tab) {
      case "proses":
        getKnowledgeProcessByMenu({
          menu: ["simple_tacit", "simple_explicit"],
          nip: user.nip9,
          status: [1, 2, 3, 4, 5, 6, 7, 8, 11, 19],
        }).then(({ data }) => {
          setContent(data);
        });
        break;
      case "Publish":
        getKnowledgeProcessByMenu({
          menu: ["simple_tacit", "simple_explicit"],
          nip: user.nip9,
          status: [9, 10],
        }).then(({ data }) => {
          setContent(data);
        });
        break;

      default:
        break;
    }
  }, [user.nip9, tab]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      // formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: (cell, row, rowIndex) => {
        let rowNumber = (currentPage - 1) * sizePage + (rowIndex + 1);
        return <span>{rowNumber}</span>;
      },
    },
    {
      dataField: "id_km_pro",
      text: "id",
      sort: true,
      hidden: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "judul",
      text: "Judul",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "tipeKnowledge.nama",
      text: "Tipe Knowledge",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "tipeKnowledge.jenis",
      text: "Jenis",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "updated_at",
      text: "Waktu Tambah / Edit",
      sort: true,
      formatter: columnFormatters.DateFormatterTambahKnowledge,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "reviewer_tolak",
      text: "Reviewer Tolak",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      hidden: tab !== "Tolak" ? true : false,
    },
    {
      dataField: "catatan_tolak",
      text: "Alasan Tolak",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      hidden: tab !== "Tolak" ? true : false,
    },
    {
      dataField: "statusKm.nama",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: columnFormatters.StatusColumnFormatterProcessKnowledgeAdded,
    },
    {
      dataField: "statusKm.id_status_km",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: columnFormatters.StatusColumnFormatterProcessKnowledgeAdded,
      hidden: true,
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
        columnFormatters.ActionsColumnFormatterProcessKnowledgeSimpleAdded,
      formatExtraData: {
        openEditDialog: edit,
        openDeleteDialog: deleteAction,
        publishKnowledge: apply,
        showReview: review,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "statusKm.id_status_km",
    pageNumber: currentPage,
    pageSize: sizePage,
  };
  const defaultSorted = [{ dataField: "statusKm.id_status_km", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
    onPageChange: (page, sizePerPage) => {
      setCurrentPage(page);
    },
    onSizePerPageChange: (page, sizePerPage) => {
      setSizePage(page);
      setCurrentPage(sizePerPage);
    },
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_km_pro"
                  data={content}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <button
                            type="button"
                            className="btn btn-primary"
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                              float: "right",
                            }}
                            onClick={add}
                          >
                            <span className="svg-icon menu-icon">
                              <SVG
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Code/Plus.svg"
                                )}
                              />
                            </span>
                            Tambah
                          </button>
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
    </>
  );
}

export default SimpleKnowledgeTable;
