import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import { Field, Formik, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import Select from "react-select";
import { Textarea } from ".././../../../../helpers";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
import { useSubheader } from "../../../../../../_metronic/layout";
import { getUnitbyId, getUnitKerja, saveUnit, updateUnit } from "../../../Api";

function UnitOrganisasiModal({ id_unit_org, id_km_pro, id_tipe_km, step, show, onHide }) {
  
  const history = useHistory();
  const [content, setContent] = useState();
  const suhbeader = useSubheader();
  const [unit, setUnit] = useState([]);
  const [valUnit, setValUnit] = useState([]);
  const [title, setTitle] = useState("");
  const [isShow, setIsShow] = useState(true);
  const [check, setCheck] = useState(false);

  const initialValues = {
    id_km_pro: id_km_pro,
    nama: ""
  };

  useEffect(() => {
    let _title = id_unit_org ? "Edit Unit Organisasi" : "Tambah Unit Organisasi";

    setTitle(_title);
    suhbeader.setTitle(_title);

    if (id_unit_org) {
      getUnitbyId(id_unit_org).then(({ data }) => {
        setContent({
          id_unit_org: data.id_unit_org,
          id_km_pro: data.id_km_pro,
          nama: data.nama
        });
      });
      setCheck(true);
      setIsShow(false);
    }
  }, [id_unit_org, suhbeader]);
 
  const validationSchema = Yup.object().shape({
    nama: Yup.string()
      .required("Unit Organisasi is required")
  });

  const saveForm = values => {
    if (!id_unit_org) {
      saveUnit(
        values.id_km_pro,
        values.nama,
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Unit Organisasi berhasil disimpan", "success").then(
            () => {
              history.push('/dashboard');
              history.replace(`/process-knowledge/simple-knowledge/${id_tipe_km}/${id_km_pro}/${step}/add`);
            }
          );
        } else {
          swal("Gagal", "Unit Organisasi gagal disimpan", "error").then(() => {
            history.push('/dashboard');
            history.replace(`/process-knowledge/simple-knowledge/${id_tipe_km}/${id_km_pro}/${step}/add`);
          });
        }
      }
      )
    } else {
      updateUnit(
        values.id_unit_org,
        values.id_km_pro,
        values.nama
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Unit Organisasi berhasil disimpan", "success").then(() => {
            history.push('/dashboard');
            history.replace(`/process-knowledge/simple-knowledge/${id_tipe_km}/${id_km_pro}/${step}/add`);
          });
        } else {
          swal("Gagal", "Unit Organisasi gagal disimpan", "error").then(() => {
            history.push('/dashboard');
            history.replace(`/process-knowledge/simple-knowledge/${id_tipe_km}/${id_km_pro}/${step}/add`);
          });
        }
      });
    }
  };

  useEffect(() => {
    getUnitKerja().then(({ data }) => {
      data.map(data => {
        return setUnit(unit => [
          ...unit,
          {
            label: data.nm_UNIT_KERJA,
            value: data.nm_UNIT_KERJA
          }
        ]);
      });
    });
  }, []);

  const renderError = (message) => <p style={{ color: "red" }}>{message}</p>;

  return (
    <Modal
      size="lg"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
         {title}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <>
          <Formik
            enableReinitialize={true}
            initialValues={content || initialValues}
            validationSchema={validationSchema}
            onSubmit={values => {
              saveForm(values);
            }}
          >
            {({ handleSubmit, setFieldValue }) => {
              const handleChangeUnit = val => {
                setFieldValue("nama", val.value);
                setValUnit(val.label);
              };

              const handleChangeCheck = () => {
                setCheck(true)
                setIsShow(false)
              }

              const handleChangeCheckReturn = () => {
                setIsShow(true)
                setCheck(false)
              }

              return (
                <Form className="form form-label-right">
                { isShow ? (
                  <>
                  <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Unit Organisasi
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Select
                        options={unit}
                        onChange={value => handleChangeUnit(value)}
                        value={unit.filter(data => data.label === valUnit)}
                      />
                    <ErrorMessage name="nama" render={renderError} />
                </div>
                </div>
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                  <input type="checkbox"
                  className="col-xl-3 col-lg-3" 
                  checked={ check } 
                  onChange={ handleChangeCheck } 
                  />
                  Lainnya
                  </label>
                </div>
                </>
                ) : <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                  <input type="checkbox"
                  className="col-xl-3 col-lg-3" 
                  checked={ check } 
                  onChange={ handleChangeCheckReturn } 
                  />
                  Lainnya
                  </label>
                  <Field
                    name="nama"
                    component={Textarea}
                    placeholder="Unit Organisasi"
                  />
                </div>
                }
                  <div className="col-lg-12" style={{ textAlign: "center" }}>
                    <button
                      type="button"
                      onClick={onHide}
                      className="btn btn-light"
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                      }}
                    >
                      <i className="flaticon2-cancel icon-nm"></i>
                      Batal
                    </button>
                    {`  `}
                    <button
                      type="submit"
                      onSubmit={() => handleSubmit()}
                      className="btn btn-success ml-2"
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                      }}
                    >
                      <i className="fas fa-save"></i>
                      Simpan
                    </button>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </>
      </Modal.Body>
    </Modal>
  );
}

export default UnitOrganisasiModal;
