import React, { useState, useEffect } from "react";
import { Formik, Form } from "formik";
import Select from "react-select";
import * as Yup from "yup";
import { getTipeKnowledgeByStatus } from "../../../references/Api";

function SimpleKnowledgeEditForm({
  content,
  btnRef,
  saveForm,
  saveDisposisi,
  btnDis,
}) {
  const [tipeKm, setTipeKm] = useState([]);
  const [valTipeKm, setValTipeKm] = useState();
  const [valTipeKonten, setValTipeKonten] = useState();

  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    id_tipe_km: Yup.number().required("Tipe Knowledge is requried"),
    tipe_konten: Yup.string().required("Tipe Konten is requried"),
    jenis: Yup.string().required("Jenis is requried"),
  });

  const tipeKonten = [
    {
      label: "Video",
      value: "video",
    },
    {
      label: "Audio",
      value: "audio",
    },
    {
      label: "Dokumen",
      value: "dokumen",
    },
  ];

  useEffect(() => {
    getTipeKnowledgeByStatus().then(({ data }) => {
      data.map((data) => {
        return setTipeKm((tipeKm) => [
          ...tipeKm,
          {
            label: data.nama,
            value: data.id_tipe_km,
            jenis: data.jenis,
          },
        ]);
      });
    });
  }, []);
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={content}
        validationSchema={ProposalEditSchema}
        onSubmit={(values) => {
          saveForm(values);
        }}
      >
        {({
          handleSubmit,
          handleClick,
          setFieldValue,
          handleBlur,
          handleChange,
          errors,
          touched,
          values,
          isValid,
        }) => {
          const handleChangeTipeKm = (val) => {
            setFieldValue("id_tipe_km", val.value);
            setFieldValue("jenis", val.jenis);
            setValTipeKm(val.label);
          };

          const handleChangeTipeKonten = (val) => {
            setFieldValue("tipe_konten", val.value);
            setValTipeKonten(val.label);
          };
          return (
            <>
              <Form className="form form-label-right">
                {/* FIELD TIPE KNOWLEDGE */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Pilih Tipe Knowledge
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Select
                      options={tipeKm}
                      onChange={(value) => handleChangeTipeKm(value)}
                      value={tipeKm.filter((data) => data.label === valTipeKm)}
                    />
                  </div>
                </div>

                {/* FIELD TIPE KONTEN */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Pilih Tipe Konten
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Select
                      options={tipeKonten}
                      onChange={(value) => handleChangeTipeKonten(value)}
                      value={tipeKonten.filter(
                        (data) => data.label === valTipeKonten
                      )}
                    />
                  </div>
                </div>

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default SimpleKnowledgeEditForm;
