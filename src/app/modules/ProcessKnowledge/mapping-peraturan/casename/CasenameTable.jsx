import React, { useState, useEffect } from "react";
import swal from "sweetalert";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { Route, useHistory } from "react-router-dom";

//Helpers
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../../_metronic/_helpers";
import { Pagination } from "../../../../helpers/pagination/Pagination";
import * as columnFormatters from "../../../../helpers/column-formatters";
import {
  deleteMappingCasename,
  getMappingCasenameByIdPeraturan,
} from "../../../../references/Api";
import CasenameModal from "./CasenameModal";

function CasenameTable({ id_peraturan, step }) {
  const history = useHistory();
  const [content, setContent] = useState([]);
  const [idMap, setIdMap] = useState(false);

  useEffect(() => {
    getMappingCasenameByIdPeraturan(id_peraturan).then(({ data }) => {
     setContent(data);
     data.length > 0 ? setIdMap(true) : setIdMap(false)
      // setContent(
      //   [...new Map(data.map((m) => [m.id_csname, m])).values()].map((dt) => ({
      //     id_mapping_casename: dt.id_mapping_casename,
      //     id_peraturan: dt.id_peraturan,
      //     id_csname: dt.id_csname,
      //     nama_casename: dt.nama_casename,
      //   }))
      // );
    });
  }, [id_peraturan]);

  const add = () => {
    history.push(
      `/process-knowledge/mapping/${id_peraturan}/${step}/edit/casename/open`
    );
  };

  const deleteAction = (id_mapping_casename) => {
    swal({
      title: "Apakah Anda Yakin?",
      text: "Klik OK untuk melanjutkan",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        deleteMappingCasename(id_mapping_casename).then(({ status }) => {
          if (status === 200) {
            swal("Berhasil", "Data berhasil dihapus", "success").then(() => {
              history.push("/dashboard");
              history.replace(
                `/process-knowledge/mapping/${id_peraturan}/${step}/edit`
              );
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push("/dashboard");
              history.replace(
                `/process-knowledge/mapping/${id_peraturan}/${step}/edit`
              );
            });
          }
        });
      }
      // else {
      //   swal("Your imaginary file is safe!");
      // }
    });
  };

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
      // classes: "text-center pr-0",
      // headerClasses: "text-center pr-3",
      style: {
        minWidth: "2px",
      },
    },
    {
      dataField: "nama_casename",
      text: "Nama Casename",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      style: {
        minWidth: "250px",
      },
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
        columnFormatters.ActionsColumnFormatterProcessKnowledgeSuccessStoryMappingCasename,
      formatExtraData: {
        openDeleteDialog: deleteAction,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];

  const emptyDataMessage = () => {
    return <div className="text-center">No Data to Display</div>;
  };
  const { SearchBar } = Search;
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "id_mapping_casename",
    pageNumber: 1,
    pageSize: 50,
  };
  const defaultSorted = [{ dataField: "id_mapping_casename", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
  };

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_mapping_casename"
                  data={content}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-12 col-xl-12 mb-3 mt-3">
                          <h4> Casename </h4>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                          <div
                            className="col-lg-6 col-xl-6 mb-3"
                            style={{ textAlign: "right" }}
                          >
                            <button
                              type="button"
                              className="btn btn-primary ml-3"
                              style={{
                                float: "right",
                              }}
                              onClick={add}
                            >
                              <i className="fa fa-plus"></i>
                              Tambah
                            </button>
                          </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
      {/*Route to Open Details*/}
      <Route path="/process-knowledge/mapping/:id_peraturan/:step/edit/casename/open">
        {({ history, match }) => (
          <CasenameModal
            show={match != null}
            idMap={idMap}
            step={step}
            id={match && match.params.id}
            id_peraturan={id_peraturan}
            after={false}
            onHide={() => {
              history.push(
                `/process-knowledge/mapping/${id_peraturan}/${step}/edit`
              );
            }}
            onRef={() => {
              history.push(
                `/process-knowledge/mapping/${id_peraturan}/${step}/edit`
              );
            }}
          />
        )}
      </Route>
    </>
  );
}

export default CasenameTable;
