import React, { useState, useRef } from "react";
import { Modal } from "react-bootstrap";
import {
  saveDokLainPeraturanMapping,
  uploadFileNew,
} from "../../../references/Api";
import { useHistory } from "react-router-dom";
import { Field, Formik, Form } from "formik";
import swal from "sweetalert";
import * as Yup from "yup";
import { Textarea } from "../../../helpers";
import CustomFileInput from "../../../helpers/form/CustomFileInput";

function MappingPeraturanDokumenLainModal({ id, show, onHide, step }) {
  const [loading, setLoading] = useState(false);

  const enableLoading = () => {
    setLoading(true);
  };

  const disableLoading = () => {
    setLoading(false);
  };
  const addDokLain = (values) => {
    enableLoading();
    const formData = new FormData();
    formData.append("file", values.file);
    uploadFileNew(formData)
      .then(({ data }) => {
        disableLoading();
        saveDokLainPeraturanMapping(data.message, id, values.keterangan).then(
          ({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
                history.push(`/process-knowledge/mapping`);
                history.replace(`/process-knowledge/mapping/${id}/${step}/edit`);
              });
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                history.push(`/process-knowledge/mapping`);
                history.replace(`/process-knowledge/mapping/${id}/${step}/edit`);
              });
            }
          }
        );
      })
      .catch(() => window.alert("Oops Something went wrong !"));
  };

  const ProposalEditSchema = Yup.object().shape({
    keterangan: Yup.string()
      .min(2, "Minimum 2 karakter")
      .required("Keterangan is required"),
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        (value) => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        (value) => value && SUPPORTED_FORMATS.includes(value.type)
      ),
  });

  const FILE_SIZE = 50000000;
  const SUPPORTED_FORMATS = [
    "application/pdf",
    "application/x-rar-compressed",
    "application/octet-stream",
    "application/zip",
    "application/octet-stream",
    "application/x-zip-compressed",
    "multipart/x-zip",
    "application/vnd.rar",
  ];
  const btnRef = useRef();
  const history = useHistory();

  const saveForm = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
    }
  };
  return (
    <Modal
      size="lg"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Upload Dokumen Lainnya
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <Formik
          enableReinitialize={true}
          initialValues={{ keterangan: "", file: "" }}
          validationSchema={ProposalEditSchema}
          onSubmit={(values) => {
            //console.log(values);
            addDokLain(values);
          }}
        >
          {({ handleSubmit, setFieldValue }) => {
            return (
              <>
                <Form className="form form-label-right">
                  {/* FIELD tentang */}
                  <div className="form-group row">
                    <Field
                      name="keterangan"
                      component={Textarea}
                      placeholder="Keterangan"
                      label="Keterangan"
                    />
                  </div>

                  {/* FIELD UPLOAD FILE */}

                  <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Upload File
                    </label>
                    <div className="col-lg-8 col-xl-6">
                      <Field
                        name="file"
                        component={CustomFileInput}
                        title="Select a file"
                        label="File"
                        style={{ display: "flex" }}
                      />
                    </div>
                  </div>
                  <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                </Form>
              </>
            );
          }}
        </Formik>
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          {loading ? (
            <button
              type="submit"
              className="btn btn-success spinner spinner-white spinner-left ml-2"
              onClick={saveForm}
              disabled={true}
              style={{
                textAlign: "center",
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
              }}
            >
              Upload
            </button>
          ) : (
            <button
              type="submit"
              className="btn btn-success"
              style={{
                textAlign: "center",
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
              }}
              onClick={saveForm}
            >
              Upload
            </button>
          )}
          {"  "}

          {loading ?
          <button
          type="button"
          onClick={onHide}
          className="btn btn-light"
          style={{
            display:"none"
          }}
        >
          <i className="flaticon2-cancel icon-nm"></i>
          Tutup
        </button>
           : 
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
            }}
          >
            <i className="flaticon2-cancel icon-nm"></i>
            Tutup
          </button>
          }
        </div>
      </Modal.Body>
      <Modal.Footer style={{ borderTop: "none" }}></Modal.Footer>
    </Modal>
  );
}

export default MappingPeraturanDokumenLainModal;
