import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
import MappingPeraturanTable from "./MappingPeraturanTable";

function MappingPeraturan() {
  return (
    <>
      <Card>
        <CardHeader
          title="Mapping Peraturan Perpajakan"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <MappingPeraturanTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default MappingPeraturan;
