import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import React,{useState} from "react";
import {
  sortCaret,
  headerSortingClasses,
  toAbsoluteUrl,
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../../../helpers/column-formatters";
import { useHistory } from "react-router-dom";

import { Pagination } from "../../../helpers/pagination/Pagination";
import swal from "sweetalert";
import { deleteDokLainMapping } from "../../../references/Api";
import SVG from "react-inlinesvg";
// import DokumenLainModal from "./DokumenLainModal";
import { Route } from "react-router-dom";
import MappingPeraturanDokumenLainModal from "./MappingPeraturanDokumenLainModal";

function MappingPeraturanDokumenLain({ dokLain, idPeraturan, lastPath, step }) {
  const history = useHistory();
  const [currentPage, setCurrentPage] = useState(1);
  const [sizePage, setSizePage] = useState(50);
  const addDokumenLainnya = () =>
    history.push(
      `/process-knowledge/mapping/${idPeraturan}/${step}/edit/doklainnya/show`
    );
  const deleteDialog = (idDokLainnya) => {
    swal({
      title: "Hapus",
      text: "Apakah Anda yakin menghapus data ini ?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((ret) => {
      if (ret === true) {
        deleteDokLainMapping(idDokLainnya).then(({ data }) => {
          if (data.deleted === true) {
            swal("Berhasil", "Data berhasil dihapus", "success").then(() => {
              history.push("/dashboard");
              history.replace(
                `/process-knowledge/mapping/${idPeraturan}/${step}/edit`
              );
            });
          } else {
            swal("Gagal", "Data gagal dihapus", "error").then(() => {
              history.push("/dashboard");
              history.replace(
                `/process-knowledge/mapping/${idPeraturan}/${step}/edit`
              );
            });
          }
        });
      }
    });
  };
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "no_regulasi",
    pageNumber: currentPage,
    pageSize: sizePage,
  };
  const columns = [
    {
        dataField: "any",
        text: "No",
        sort: true,
        // formatter: columnFormatters.IdColumnFormatter,
        sortCaret: sortCaret,
        headerSortingClasses,
        formatter: (cell, row, rowIndex) => {
          let rowNumber = (currentPage - 1) * sizePage + (rowIndex + 1);
          return <span>{rowNumber}</span>;
        },
      },
    {
      dataField: "download",
      text: "File Upload",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.FileColumnFormatterProcessKnowledgeMappingPeraturanDokumenLainnya,
      headerSortingClasses,
    },
    {
      dataField: "keterangan",
      text: "Keterangan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterProcessKnowledgeMappingPeraturanDokumenLainnya,
      formatExtraData: {
        deleteDialog: deleteDialog,
        // applyProposal: applyProposal,
        // openAddPerTerkait: openAddPerTerkait
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      hidden: lastPath === "edit" ? false : true,
      style: {
        minWidth: "100px",
      },
    },
  ];
  //const { SearchBar } = Search;
  const defaultSorted = [{ dataField: "id_km_dok_lainnya", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "100", value: 100 },
    { text: "200", value: 200 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: dokLain.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
    onPageChange: (page, sizePerPage) => {
        setCurrentPage(page);
      },
      onSizePerPageChange: (page, sizePerPage) => {
        setSizePage(page);
        setCurrentPage(sizePerPage);
      },
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;

  return (
    <>
      <>
        <div>
          <br />
          <br />
          <h3>Dokumen Lainnya</h3>
        </div>

        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_km_dok_lainnya"
                  data={dokLain}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      {lastPath === "edit" ? (
                        <div className="row">
                          <div className="col-lg-6 col-xl-6 mb-3">
                            <SearchBar
                              {...props.searchProps}
                              style={{ width: "500px" }}
                            />
                            <br />
                          </div>
                          <div className="col-lg-6 col-xl-6 mb-3">
                            <button
                              type="button"
                              className="btn btn-primary"
                              style={{
                                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                                float: "right",
                              }}
                              onClick={addDokumenLainnya}
                            >
                              <span className="svg-icon menu-icon">
                                <SVG
                                  src={toAbsoluteUrl(
                                    "/media/svg/icons/Code/Plus.svg"
                                  )}
                                />
                              </span>
                              Tambah
                            </button>
                          </div>
                        </div>
                      ) : null}
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
      <Route path="/process-knowledge/mapping/:id/:step/edit/doklainnya/show">
        {({ history, match }) => (
          <MappingPeraturanDokumenLainModal
            show={match != null}
            step={step}
            id={match && match.params.id}
            onHide={() => {
              history.push(`process-knowledge/mapping/${idPeraturan}/${step}/edit`);
            }}
            onRef={() => {
              history.push(`process-knowledge/mapping/${idPeraturan}/${step}/edit`);
            }}
          />
        )}
      </Route>
    </>
  );
}

export default MappingPeraturanDokumenLain;
