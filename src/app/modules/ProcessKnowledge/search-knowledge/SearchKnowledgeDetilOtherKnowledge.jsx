/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect, useState } from "react";
import ReactPlayer from "react-player";
import { useHistory } from "react-router-dom";
import { Card as Cd, Row } from "react-bootstrap";
import swal from "sweetalert";
import {
  getDasarHukumByIdKmPro,
  getFeedbackByIdKmPro,
  getKnowledgeProcessById,
  getOtherKnowledgeByIdKmPro,
  getPeraturanTerkaitByIdKmPro,
  getReferensiByIdKmPro,
  getSopByIdKmPro,
  getSuccessStoryByIdKmPro,
  getUploadTerkaitByIdKmPro,
  updateStatusKnowledgeProcess,
} from "../../../references/Api";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import MateriPokok from "./materi-pokok/MateriPokok";
import {
  getBacaanbyIdKM,
  getLevelAksesbyIdKM,
  getLihatPulabyIdKM,
  getTusibyIdKM,
  getUnitbyIdKM,
} from "../Api";
const { URL_DOWNLOAD, STREAM_URL_VIDEO, STREAM_URL_AUDIO } = window.ENV;

function SearchKnowledgeDetilOtherKnowledge({ id_km }) {
  const history = useHistory();
  const [content, setContent] = useState([]);
  const [draft, setDraft] = useState("");
  const [materi, setMateri] = useState();
  const [tusi, setTusi] = useState([]);
  const [unit, setUnit] = useState([]);
  const [file, setFile] = useState([]);
  const [referensi, setReferensi] = useState([]);
  const [lihatPula, setLihatPula] = useState([]);
  const [bacaan, setBacaan] = useState([]);
  const [peraturanTerkait, setPeraturanTerkait] = useState([]);
  const [hukum, setHukum] = useState([]);
  const [feedback, setFeedback] = useState([]);
  const [dataTemplate, setDataTemplate] = useState([]);
  const [levelDetil, setLevelDetil] = useState([]);
  // console.log(content)
  useEffect(() => {
    getKnowledgeProcessById(id_km).then(({ data }) => {
      setContent(data);
      // getSuccessStoryByIdKmPro(id_km).then((data_2) => {
      //   setMateri(data_2.data[0].successStoryTemp.materi);
      // });
      // switch (data.tipeKnowledge.template) {
      //   case "Success Story":
      //     return getSuccessStoryByIdKmPro(id_km).then((data_2) => {
      //       setMateri(data_2.data[0].successStoryTemp.materi);
      //     });
      //   case "SOP":
      //     return getSopByIdKmPro(id_km).then((data_2) => {
      //       setMateri(data_2.data[0] ? data_2.data[0].sopTemp.materi : null);
      //     })
      //   case "Other Knowledge":
      //     return getOtherKnowledgeByIdKmPro(id_km).then((data_2) => {
      //       setMateri(data_2.data[0] ? data_2.data[0].defaultTemp.materi : null)
      //     })
      //   default:
      //     break;
      // }
      getTusibyIdKM(id_km).then(({ data }) => {
        setTusi(data);
      });
      getUnitbyIdKM(id_km).then(({ data }) => {
        setUnit(data);
      });
      getUploadTerkaitByIdKmPro(id_km).then(({ data }) => {
        setFile(data);
      });
      getDasarHukumByIdKmPro(id_km).then((data) => {
        setHukum(data);
      });
      getReferensiByIdKmPro(id_km).then(({ data }) => {
        setReferensi(data);
      });
      getLihatPulabyIdKM(id_km).then(({ data }) => {
        setLihatPula(data);
      });
      getBacaanbyIdKM(id_km).then(({ data }) => {
        setBacaan(data);
      });
      getPeraturanTerkaitByIdKmPro(id_km).then(({ data }) => {
        setPeraturanTerkait(data);
      });
      getLevelAksesbyIdKM(id_km).then(({ data }) => {
        setLevelDetil(data);
      });
    });
  }, [id_km]);


  useEffect(() => {
    getKnowledgeProcessById(id_km).then(({ data }) => {
      switch (data.tipeKnowledge.template) {
        case "Success Story":
          return getSuccessStoryByIdKmPro(id_km).then((data_2) => {
            setMateri(data_2.data[0].successStoryTemp.materi);
            setDataTemplate(data_2.data[0].successStoryTemp);
          });
        case "SOP":
          return getSopByIdKmPro(id_km).then((data_2) => {
            setMateri(data_2.data[0] ? data_2.data[0].sopTemp.materi : null);
          });
        case "Other Knowledge":
          return getOtherKnowledgeByIdKmPro(id_km).then((data_2) => {
            setMateri(
              data_2.data[0] ? data_2.data[0].defaultTemp.materi : null
            );
          });
        default:
          break;
      }
    });
    getFeedbackByIdKmPro(id_km).then(({ data }) => {
      setFeedback(data);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const nama = "Angga ITo - 19/02/2022";

  const backAction = () => {
    history.push(`/process-knowledge/search-knowledge`);
  };

  const handleChange = () => {
    history.push(`/process-knowledge/update-review`);
  };

  // useEffect(()=> {
  //   console.log(id_km)
  //   console.log(tipe_km)
  // },[id_km, tipe_km])

  const applyReview = () => {
    // getUsulanById(id).then(({ data }) => {
    updateStatusKnowledgeProcess(id_km, "10").then(({ status }) => {
      if (status === 201 || status === 200) {
        swal("Berhasil", "Data Berhasil Disimpan", "success").then(() => {
          history.push("/dashboard");
          history.replace("/process-knowledge/update-review");
        });
      } else {
        swal("Gagal", "Data Gagal Disimpan", "error").then(() => {
          history.push("/dashboard");
          history.replace("/process-knowledge/update-review");
        });
      }
    });
    // });
  };

  const applyChange = () => {
    getKnowledgeProcessById(id_km).then(({ data }) => {
      swal("Masukkan Alasan Perubahan:", {
        content: "input",
        buttons: true,
      }).then((value) => {
        if (value) {
          if (data.jenis === "Tacit") {
            updateStatusKnowledgeProcess(id_km, "13", value).then(
              ({ status }) => {
                if (status === 201 || status === 200) {
                  swal("Berhasil", "Data Berhasil Disimpan", "success").then(
                    () => {
                      history.push("/dashboard");
                      history.replace("/process-knowledge/update-review");
                    }
                  );
                } else {
                  swal("Gagal", "Data Gagal Disimpan", "error").then(() => {
                    history.push("/dashboard");
                    history.replace("/process-knowledge/update-review");
                  });
                }
              }
            );
          } else {
            updateStatusKnowledgeProcess(id_km, "14", value).then(
              ({ status }) => {
                if (status === 201 || status === 200) {
                  swal("Berhasil", "Data Berhasil Disimpan", "success").then(
                    () => {
                      history.push("/dashboard");
                      history.replace("/process-knowledge/update-review");
                    }
                  );
                } else {
                  swal("Gagal", "Data Gagal Disimpan", "error").then(() => {
                    history.push("/dashboard");
                    history.replace("/process-knowledge/update-review");
                  });
                }
              }
            );
          }
        }
      });
    });
  };

  const showDetilLevel = (eselon, jabatan, index) => {
    return (
      <div className="d-flex align-items-center mb-10" key={index}>
        <div className="symbol symbol-40 symbol-light-info mr-5">
          <span className="symbol-label">
            <span className="svg-icon svg-icon-lg svg-icon-info">
              <SVG
                className="h-75 align-self-end"
                src={toAbsoluteUrl(
                  "/media/svg/icons/Communication/Shield-user.svg"
                )}
              ></SVG>
            </span>
          </span>
        </div>

        <div className="d-flex flex-column font-weight-bold">
          <a
            href="#"
            className="text-dark text-hover-primary mb-1 font-size-lg"
          >
            {jabatan + " " + eselon}
          </a>
        </div>
      </div>
    );
  };
  return (
    <>
      <Row>
        {/* Begin Content*/}
        <div
          className="col-lg-12 col-xxl-12 order-1 order-xxl-1"
          style={{ paddingTop: "20px" }}
        >
          <Cd>
            <Cd.Header style={{ backgroundColor: "#FFC91B" }}>
              <h3 className="card-label font-weight-bolder text-dark">
                Detil Knowledge
              </h3>
            </Cd.Header>
            <Cd.Body>
              <div className="d-flex flex-row">
                <div className="d-flex align-items-center mb-10 mr-10">
                  <div className="symbol symbol-40 symbol-light-dark mr-5">
                    <span className="symbol-label">
                      <span className="svg-icon svg-icon-lg svg-icon-dark">
                        <SVG
                          className="h-75 align-self-end"
                          src={toAbsoluteUrl(
                            "/media/svg/icons/Layout/Layout-4-blocks.svg"
                          )}
                        ></SVG>
                      </span>
                    </span>
                  </div>

                  <div className="d-flex flex-column font-weight-bold">
                    <a
                      href="#"
                      className="text-dark text-hover-primary mb-1 font-size-lg"
                    >
                      {content.judul}
                    </a>

                    {/* <span className="text-dark"> {content.judul}</span> */}
                  </div>
                </div>
                <div className="d-flex align-items-center mb-10 mr-10">
                  <div className="symbol symbol-40 symbol-light-dark mr-5">
                    <span className="symbol-label">
                      <span className="svg-icon svg-icon-lg svg-icon-dark">
                        <SVG
                          className="h-75 align-self-end"
                          src={toAbsoluteUrl(
                            "/media/svg/icons/General/User.svg"
                          )}
                        ></SVG>
                      </span>
                    </span>
                  </div>

                  <div className="d-flex flex-column font-weight-bold">
                    <a
                      href="#"
                      className="text-dark text-hover-primary mb-1 font-size-lg"
                    >
                      {content.nama_kontri ? content.nama_kontri : "-"}
                    </a>

                    {/* <span className="text-dark">
                      {content.nama_kontri ? content.nama_kontri : "-"}
                    </span> */}
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-8">
                  <h4 className="font-weight-bold mb-5">Media</h4>
                  <div
                    className="navi-separator mt-3"
                    style={{
                      marginTop: "3px",
                      marginBottom: "15px",
                      borderBottom: "1px solid #ebedf3",
                    }}
                  ></div>
                  {content.tipe_konten === "dokumen" ? (
                    "-"
                  ) : content.tipe_konten === "audio" ? (
                    <ReactPlayer
                      className="react-player"
                      controls={true}
                      width="480px"
                      height="40px"
                      url={STREAM_URL_AUDIO + content.media_upload}
                    />
                  ) : (
                    <ReactPlayer
                      className="react-player"
                      controls={true}
                      url={STREAM_URL_VIDEO + content.media_upload}
                    />
                  )}

                  <h4 className="font-weight-bold mb-5 mt-10">Reviewer</h4>
                  <div
                    className="navi-separator mt-3"
                    style={{
                      marginTop: "3px",
                      marginBottom: "15px",
                      borderBottom: "1px solid #ebedf3",
                    }}
                  ></div>
                  <div className="d-flex align-items-center mb-10">
                    <div className="symbol symbol-40 symbol-light-primary mr-5">
                      <span className="symbol-label">
                        <span className="svg-icon svg-icon-lg svg-icon-primary">
                          <SVG
                            className="h-75 align-self-end"
                            src={toAbsoluteUrl(
                              "/media/svg/icons/General/User.svg"
                            )}
                          ></SVG>
                        </span>
                      </span>
                    </div>

                    <div className="d-flex flex-column font-weight-bold">
                      <a
                        href="#"
                        className="text-dark text-hover-primary mb-1 font-size-lg"
                      >
                        Penjamin Kualitas Pengetahuan
                      </a>

                      <span className="text-muted">
                        {content.nama_kontri ? content.nama_kontri : "-"}
                      </span>
                    </div>
                  </div>

                  <div className="d-flex align-items-center mb-10">
                    <div className="symbol symbol-40 symbol-light-primary mr-5">
                      <span className="symbol-label">
                        <span className="svg-icon svg-icon-lg svg-icon-primary">
                          <SVG
                            className="h-75 align-self-end"
                            src={toAbsoluteUrl(
                              "/media/svg/icons/General/User.svg"
                            )}
                          ></SVG>
                        </span>
                      </span>
                    </div>

                    <div className="d-flex flex-column font-weight-bold">
                      <a
                        href="#"
                        className="text-dark-75 text-hover-primary mb-1 font-size-lg"
                      >
                        Knowledge Owner
                      </a>

                      <span className="text-muted">
                        {content.ko ? content.ko.nm_es4 : null}
                      </span>
                    </div>
                  </div>
                  <div className="d-flex align-items-center mb-10">
                    <div className="symbol symbol-40 symbol-light-primary mr-5">
                      <span className="symbol-label">
                        <span className="svg-icon svg-icon-lg svg-icon-primary">
                          <SVG
                            className="h-75 align-self-end"
                            src={toAbsoluteUrl(
                              "/media/svg/icons/General/User.svg"
                            )}
                          ></SVG>
                        </span>
                      </span>
                    </div>

                    <div className="d-flex flex-column font-weight-bold">
                      <a
                        href="#"
                        className="text-dark text-hover-primary mb-1 font-size-lg"
                      >
                        Subject Matter Expert
                      </a>

                      <span className="text-muted">
                        {" "}
                        {content.sme ? content.sme.nama : null}
                      </span>
                    </div>
                  </div>
                  <h4 className="font-weight-bold mb-5">Level Akses</h4>
                  <div
                    className="navi-separator mt-3"
                    style={{
                      marginTop: "3px",
                      marginBottom: "15px",
                      borderBottom: "1px solid #ebedf3",
                    }}
                  ></div>

                  {content.level_knowledge === "1"
                    ? levelDetil.map((data, index) => (
                        data.nm_es4
                          ? showDetilLevel(data.nm_es4, data.nm_jabatan, index)
                          : data.nm_es3
                          ? showDetilLevel(data.nm_es3, data.nm_jabatan, index)
                          : showDetilLevel(data.nm_es2, data.nm_jabatan, index)
                    ))
                    : "-"}
                  {/* <div className="d-flex align-items-center mb-10">
                    <div className="symbol symbol-40 symbol-light-info mr-5">
                      <span className="symbol-label">
                        <span className="svg-icon svg-icon-lg svg-icon-info">
                          <SVG
                            className="h-75 align-self-end"
                            src={toAbsoluteUrl(
                              "/media/svg/icons/Communication/Shield-user.svg"
                            )}
                          ></SVG>
                        </span>
                      </span>
                    </div>

                    <div className="d-flex flex-column font-weight-bold">
                      <a
                        href="#"
                        className="text-dark text-hover-primary mb-1 font-size-lg"
                      >
                        {content.level_knowledge === "1" ? "ada"
                        : '-'}
                      </a>
                    </div>
                  </div> */}
                  <h4 className="font-weight-bold mb-5">Tugas dan Fungsi</h4>
                  <div
                    className="navi-separator mt-3"
                    style={{
                      marginTop: "3px",
                      marginBottom: "15px",
                      borderBottom: "1px solid #ebedf3",
                    }}
                  ></div>
                  {tusi.length > 0
                    ? tusi.map((data, index) => (
                        <div
                          key={index}
                          className="d-flex align-items-center mb-10"
                        >
                          <div className="symbol symbol-40 symbol-light-warning mr-5">
                            <span className="symbol-label">
                              <span className="svg-icon svg-icon-lg svg-icon-warning">
                                <SVG
                                  className="h-75 align-self-end"
                                  src={toAbsoluteUrl(
                                    "/media/svg/icons/Communication/Clipboard-list.svg"
                                  )}
                                ></SVG>
                              </span>
                            </span>
                          </div>

                          <div className="d-flex flex-column font-weight-bold">
                            <a
                              href="#"
                              className="text-dark text-hover-primary mb-1 font-size-lg"
                            >
                              {data.tusi.nama}
                            </a>
                          </div>
                        </div>
                      ))
                    : "-"}
                  <h4 className="font-weight-bold mb-5">Unit Organisasi</h4>
                  <div
                    className="navi-separator mt-3"
                    style={{
                      marginTop: "3px",
                      marginBottom: "15px",
                      borderBottom: "1px solid #ebedf3",
                    }}
                  ></div>
                  {unit.length > 0
                    ? unit.map((data, index) => (
                        <div
                          key={index}
                          className="d-flex align-items-center mb-10"
                        >
                          <div className="symbol symbol-40 symbol-light-success mr-5">
                            <span className="symbol-label">
                              <span className="svg-icon svg-icon-lg svg-icon-success">
                                <SVG
                                  className="h-75 align-self-end"
                                  src={toAbsoluteUrl(
                                    "/media/svg/icons/Communication/Group.svg"
                                  )}
                                ></SVG>
                              </span>
                            </span>
                          </div>

                          <div className="d-flex flex-column font-weight-bold">
                            <a
                              href="#"
                              className="text-dark text-hover-primary mb-1 font-size-lg"
                            >
                              {data.nama}
                            </a>
                          </div>
                        </div>
                      ))
                    : "-"}
                  <h4 className="font-weight-bold mb-5">Materi</h4>
                  <div
                    className="navi-separator mt-3"
                    style={{
                      marginTop: "3px",
                      marginBottom: "15px",
                      borderBottom: "1px solid #ebedf3",
                    }}
                  ></div>
                  <MateriPokok draft={materi} setDraft={setDraft} />
                </div>

                {/* Begin Blue Box*/}
                <div className="col-lg-4">
                  <Cd
                    style={{
                      borderRadius: "20px",
                      backgroundColor: "#95bfeb",
                      marginRight: "20px",
                    }}
                  >
                    <Cd.Body>
                      <div className="d-flex align-items-center mb-10">
                        <div className="symbol symbol-40 symbol-light-warning mr-5">
                          <span className="symbol-label">
                            <span className="svg-icon svg-icon-lg svg-icon-warning">
                              <SVG
                                className="h-75 align-self-end"
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Files/Selected-file.svg"
                                )}
                              ></SVG>
                            </span>
                          </span>
                        </div>

                        <div className="d-flex flex-column font-weight-bold">
                          <a
                            href="#"
                            className="text-dark-75 text-hover-primary mb-1 font-size-lg"
                          >
                            Proses Bisnis
                          </a>

                          <span className="text-dark">
                            {content.bisnisProses
                              ? content.bisnisProses.nama
                              : null}
                          </span>
                        </div>
                      </div>
                      <div className="d-flex align-items-center mb-10">
                        <div className="symbol symbol-40 symbol-light-warning mr-5">
                          <span className="symbol-label">
                            <span className="svg-icon svg-icon-lg svg-icon-warning">
                              <SVG
                                className="h-75 align-self-end"
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Files/Selected-file.svg"
                                )}
                              ></SVG>
                            </span>
                          </span>
                        </div>

                        <div className="d-flex flex-column font-weight-bold">
                          <a
                            href="#"
                            className="text-dark text-hover-primary mb-1 font-size-lg"
                          >
                            Bisnis Sektor
                          </a>

                          <span className="text-dark">
                            {" "}
                            {content.bisnisSektor
                              ? content.bisnisSektor.nama
                              : null}
                          </span>
                        </div>
                      </div>
                      <div className="d-flex align-items-center mb-10">
                        <div className="symbol symbol-40 symbol-light-warning mr-5">
                          <span className="symbol-label">
                            <span className="svg-icon svg-icon-lg svg-icon-warning">
                              <SVG
                                className="h-75 align-self-end"
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Files/Selected-file.svg"
                                )}
                              ></SVG>
                            </span>
                          </span>
                        </div>

                        <div className="d-flex flex-column font-weight-bold">
                          <a
                            href="#"
                            className="text-dark text-hover-primary mb-1 font-size-lg"
                          >
                            Case Name
                          </a>

                          <span className="text-dark">
                            {" "}
                            {content.caseName ? content.caseName.nama : null}
                          </span>
                        </div>
                      </div>

                      <div className="d-flex align-items-center mb-10">
                        <div className="symbol symbol-40 symbol-light-warning mr-5">
                          <span className="symbol-label">
                            <span className="svg-icon svg-icon-lg  svg-icon-warning">
                              <SVG
                                className="h-75 align-self-end"
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Files/Selected-file.svg"
                                )}
                              ></SVG>
                            </span>
                          </span>
                        </div>

                        <div className="d-flex flex-column font-weight-bold">
                          <a
                            href="#"
                            className="text-dark text-hover-primary mb-1 font-size-lg"
                          >
                            Sub Case
                          </a>

                          <span className="text-dark">
                            {" "}
                            {content.subCase ? content.subCase.nama : null}
                          </span>
                        </div>
                      </div>
                      <div className="d-flex align-items-center mb-10">
                        <div className="symbol symbol-40 symbol-light-warning mr-5">
                          <span className="symbol-label">
                            <span className="svg-icon svg-icon-lg svg-icon-warning">
                              <SVG
                                className="h-75 align-self-end"
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Files/Selected-file.svg"
                                )}
                              ></SVG>
                            </span>
                          </span>
                        </div>

                        <div className="d-flex flex-column font-weight-bold">
                          <a
                            href="#"
                            className="text-dark text-hover-primary mb-1 font-size-lg"
                          >
                            Kategori
                          </a>

                          <span className="text-dark">
                            {content.kategori ? content.kategori : "-"}
                          </span>
                        </div>
                      </div>

                      <div className="d-flex align-items-center mb-10">
                        <div className="symbol symbol-40 symbol-light-warning mr-5">
                          <span className="symbol-label">
                            <span className="svg-icon svg-icon-lg svg-icon-warning">
                              <SVG
                                className="h-75 align-self-end"
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Files/Selected-file.svg"
                                )}
                              ></SVG>
                            </span>
                          </span>
                        </div>

                        <div className="d-flex flex-column font-weight-bold">
                          <a
                            href="#"
                            className="text-dark-75 text-hover-primary mb-1 font-size-lg"
                          >
                            Level
                          </a>

                          <span className="text-dark">
                            {content.level_knowledge
                              ? content.level_knowledge
                              : "-"}
                          </span>
                        </div>
                      </div>
                    </Cd.Body>
                  </Cd>
                </div>
                {/* End of Blue Box*/}
              </div>
            </Cd.Body>
          </Cd>
        </div>
        {/* End of Content*/}

        {/* Begin Peraturan Terkait*/}
        <div
          className="col-xxl-12 order-2 order-xxl-2"
          style={{ paddingTop: "20px" }}
        >
          <Cd>
            <Cd.Header style={{ backgroundColor: "#FFC91B" }}>
              <h3 className="card-label font-weight-bolder text-dark">
                Peraturan Terkait
              </h3>
            </Cd.Header>
            <Cd.Body>
              {peraturanTerkait.length > 0
                ? peraturanTerkait.map((data, index) => (
                    <div
                      key={index}
                      className="d-flex align-items-center mb-10"
                    >
                      <div className="mr-5">
                        <span className="symbol-label">
                          <span className="svg-icon svg-icon-xxl svg-icon-success">
                            <SVG
                              className="h-75 align-self-end"
                              src={toAbsoluteUrl(
                                "/media/svg/icons/Home/Book-open.svg"
                              )}
                            ></SVG>
                          </span>
                        </span>
                      </div>

                      <div className="d-flex flex-column font-weight-bold">
                        <a
                          href="#"
                          target="_blank"
                          rel="noopener noreferrer"
                          className="text-dark text-hover-primary mb-1 font-size-lg"
                        >
                          {data.no_regulasi}
                        </a>
                      </div>
                    </div>
                  ))
                : "-"}
            </Cd.Body>
          </Cd>
        </div>
        {/* End of Peraturan Terkait*/}

        {/* Begin File Lampiran*/}
        <div
          className="col-xxl-12 order-2 order-xxl-2"
          style={{ paddingTop: "20px" }}
        >
          <Cd>
            <Cd.Header style={{ backgroundColor: "#FFC91B" }}>
              <h3 className="card-label font-weight-bolder text-dark">
                File Lampiran
              </h3>
            </Cd.Header>
            <Cd.Body>
              {file.length > 0
                ? file.map((data, index) => (
                    <div
                      key={index}
                      className="d-flex align-items-center mb-10"
                    >
                      <div className="mr-5">
                        <span className="symbol-label">
                          <span className="svg-icon svg-icon-xxl svg-icon-success">
                            <SVG
                              className="h-75 align-self-end"
                              src={toAbsoluteUrl("/media/svg/files/pdf.svg")}
                            ></SVG>
                          </span>
                        </span>
                      </div>

                      <div className="d-flex flex-column font-weight-bold">
                        <a
                          href={URL_DOWNLOAD + data.link}
                          target="_blank"
                          rel="noopener noreferrer"
                          className="text-dark text-hover-primary mb-1 font-size-lg"
                        >
                          {data.link}
                        </a>
                      </div>
                    </div>
                  ))
                : "-"}
            </Cd.Body>
          </Cd>
        </div>
        {/* End of File Lampiran*/}

        {/* Begin Feedback*/}
        <div
          className="col-xxl-12 order-2 order-xxl-2"
          style={{ paddingTop: "20px" }}
        >
          <Cd>
            <Cd.Header style={{ backgroundColor: "#FFC91B" }}>
              <h3 className="card-label font-weight-bolder text-dark">
                Feedback
              </h3>
            </Cd.Header>
            <Cd.Body>
              {feedback.map((data, index) => (
                <Cd style={{ marginTop: "20px" }} key={index}>
                  <Cd.Body>
                    <>
                      <div className="d-flex align-items-center pb-4">
                        <span className="symbol-label">
                          <img
                            src={toAbsoluteUrl("/media/svg/avatars/user.png")}
                            alt=""
                            className="h-75 align-self-end"
                            style={{ height: "50px", width: "50px" }}
                          />
                        </span>
                        <div
                          className="d-flex flex-column flex-grow-1"
                          style={{ marginLeft: "10px" }}
                        >
                          <a
                            href="#"
                            className="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder"
                          >
                            {data.nama_feedback}
                          </a>
                          <span className="text-muted font-weight-bold">
                            {data.created_at}
                          </span>
                        </div>
                      </div>
                      <div>
                        <p className="text-dark-75 font-size-lg font-weight-normal">
                          {data.isi}
                        </p>
                        {data.child_feedback.length > 0 ? (
                          <div className="d-flex align-items-center">
                            <a
                              href="#"
                              className="btn btn-hover-text-primary btn-hover-icon-primary btn-sm btn-text-dark-50 bg-light-primary rounded font-weight-bolder font-size-sm p-2 mr-2"
                            >
                              <span className="svg-icon svg-icon-md svg-icon-primary pr-2">
                                <SVG
                                  className="h-10 align-self-end"
                                  src={toAbsoluteUrl(
                                    "/media/svg/icons/Communication/Chat6.svg"
                                  )}
                                ></SVG>
                              </span>
                              {data.child_feedback.length}
                            </a>
                          </div>
                        ) : null}

                        {data.child_feedback.length > 0
                          ? data.child_feedback.map((data, index) => (
                              <div key={index} className="d-flex py-5">
                                <span className="symbol-label">
                                  <img
                                    src={toAbsoluteUrl(
                                      "/media/svg/avatars/user.png"
                                    )}
                                    alt=""
                                    className="align-self-end"
                                    style={{ height: "40px", width: "40px" }}
                                  />
                                </span>

                                <div
                                  className="d-flex flex-column flex-row-fluid"
                                  style={{ marginLeft: "10px" }}
                                >
                                  <div className="d-flex align-items-center flex-wrap">
                                    <a
                                      href="#"
                                      className="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder pr-6"
                                    >
                                      {data.nama_feedback}
                                    </a>
                                    <span className="text-muted font-weight-normal flex-grow-1 font-size-sm">
                                      {data.created_at}
                                    </span>
                                  </div>
                                  <span className="text-dark-75 font-size-sm font-weight-normal pt-1">
                                    {data.isi}
                                  </span>
                                </div>
                              </div>
                            ))
                          : null}
                      </div>
                    </>
                  </Cd.Body>
                </Cd>
              ))}
            </Cd.Body>
            <Cd.Footer style={{ borderTop: "none" }}>
              <div className="col-lg-12" style={{ textAlign: "center" }}>
                <button
                  type="button"
                  className="btn btn-danger ml-2"
                  // onClick={saveButton}
                  onClick={() => applyChange()}
                  style={{
                    boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                  }}
                  // disabled={disabled}
                >
                  Ajukan Perubahan
                </button>
                <button
                  type="button"
                  className="btn btn-success ml-6"
                  // onClick={saveButton}
                  onClick={() => applyReview()}
                  style={{
                    boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                  }}
                  // disabled={disabled}
                >
                  Selesai Review
                </button>
                <button
                  type="button"
                  className="btn btn-primary ml-6"
                  // onClick={saveButton}
                  onClick={backAction}
                  style={{
                    boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                  }}
                  // disabled={disabled}
                >
                  Kembali
                </button>
              </div>
            </Cd.Footer>
          </Cd>
        </div>
        {/* End of Feedback*/}
      </Row>
    </>
  );
}

export default SearchKnowledgeDetilOtherKnowledge;
