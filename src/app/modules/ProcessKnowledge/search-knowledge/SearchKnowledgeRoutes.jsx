import React, { useEffect, useState } from "react";
import { getTipeKnowledgeById } from "../../../references/Api";
import SearchKnowledgeDetilOtherKnowledge from "./SearchKnowledgeDetilOtherKnowledge";
import SearchKnowledgeDetilSop from "./SearchKnowledgeDetilSop";
import SearchKnowledgeDetilSuccessStory from "./SearchKnowledgeDetilSuccessStory";

function SearchKnowledgeRoutes({
  history,
  match: {
    params: { id_km, tipe_km },
  },
}) {
  
  const [template, setTemplate] = useState("");

  useEffect(() => {
    getTipeKnowledgeById(tipe_km).then(({ data }) => {
      setTemplate(data.template);
    });
  }, [tipe_km]);

  const checkType = (template) => {
    switch (template) {
      case "Success Story":
        return <SearchKnowledgeDetilSuccessStory id_km={id_km} />;
      case "SOP":
        return <SearchKnowledgeDetilSop id_km={id_km} />;
      case "Other Knowledge":
        return <SearchKnowledgeDetilOtherKnowledge id_km={id_km} />;
      default:
        break;
    }
  };
  return <div>{checkType(template)}</div>;
}

export default SearchKnowledgeRoutes;
