import React,{useState} from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../_metronic/_partials/controls"; 
import SearchKnowledgeForm from "./SearchKnowledgeForm";
import SearchKnowledgeTable from "./SearchKnowledgeTable";
import swal from "sweetalert";
import { getKonten } from "../../../references/Api";


function SearchKnowledge() {

  const [isShow, setIsShow] = useState(false)
  const [content, setContent] = useState();

  const saveForm = values => {
    // console.log(values);
    // setIsShow(true)
    // Object.keys(values).length === 0 ? console.log('kosong') : console.log('ada')
    if(Object.keys(values).length === 0){
      swal("Gagal", "Harap Masukkan Minimal Satu Pilihan", "error");
    } else{
      // console.log(values)
      setIsShow(false)
      setContent([])
      getKonten(values).then(({data})=> {
        // setContent(data.content)
        setContent(data)
        setIsShow(true)
      })
    }
    
  };

  // console.log(content)

  return (
    <>
      <Card>
        <CardHeader
          title="Pencarian Knowledge"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          {/* <TambahKnowledgeTable /> */}
          <SearchKnowledgeForm
          saveForm={saveForm}
          // saveButton={saveButton}
           />
        </CardBody>
        <CardFooter style={{ borderTop: "none" }}>
        {isShow ? <SearchKnowledgeTable content={content}/> : null}
        </CardFooter>
      </Card>
    </>
  );
}

//#a6c8e6
export default SearchKnowledge;
