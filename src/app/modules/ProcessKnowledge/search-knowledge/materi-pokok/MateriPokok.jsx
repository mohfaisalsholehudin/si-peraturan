// import React, { useEffect, useState } from "react";
// import { CKEditor } from "@ckeditor/ckeditor5-react";
// import Editor from "ckeditor5-custom-build/build/ckeditor";
// // import "./decoupled.css"
// const { BACKEND_URL } = window.ENV;

// function MateriPokok(
//   { draft, setDraft
//   }
// ) {
//   // const [draft, setDraft] = useState("");
//   const [show, setShow] = useState(false);
//   const [data, setData] = useState([]);

//   return (
//     <>
//       {/* <div className="row mb-4"> */}
//         <div className="document-editor" style={{ width: "100%", maxHeight:'600px' }}>
//           <div className="document-editor__toolbar"></div>
//           <div className="document-editor__editable-container">
//           <CKEditor
//              disabled
//              onReady={editor => {
//                window.editor = editor;
//                setShow(true);

//                // Add these two lines to properly position the toolbar
//                const toolbarContainer = document.querySelector(
//                  ".document-editor__toolbar"
//                );
//                toolbarContainer.appendChild(editor.ui.view.toolbar.element);
//              }}
//              config={{
//                        removePlugins: ["Heading", "Link"],
//                        toolbar: [],
//                        isReadOnly: true,
//                      }}
//              editor={Editor}
//              data={draft ? draft : null}
//            />
//           </div>
//         </div>
//       {/* </div> */}
//     </>
//   );
// }

// export default MateriPokok;

import React from 'react';
import { CKEditor, CKEditorContext } from '@ckeditor/ckeditor5-react';
import Editor from "ckeditor5-custom-build/build/ckeditor";

function MateriPokok(
  { draft, setDraft
  }
){
        return (
          <>
                <CKEditor
                    onReady={ editor => {
                        // console.log( 'Editor is ready to use!', editor );

                        // Insert the toolbar before the editable area.
                        // editor.ui.getEditableElement().parentElement.insertBefore(
                        //     editor.ui.view.toolbar.element,
                        //     editor.ui.getEditableElement()
                        // );

                        // this.editor = editor;
                    } }
                    onError={ ( error, { willEditorRestart } ) => {
                        // If the editor is restarted, the toolbar element will be created once again.
                        // The `onReady` callback will be called again and the new toolbar will be added.
                        // This is why you need to remove the older toolbar.
                        if ( willEditorRestart ) {
                            this.editor.ui.view.toolbar.element.remove();
                        }
                    } }
                    // onChange={ ( event, editor ) => console.log( { event, editor } ) }
                    editor={ Editor }
                    data={draft ? draft : null}
                                 config={{
                       removePlugins: ["Heading", "Link"],
                       toolbar: [],
                       isReadOnly: true,
                     }}
                />
          </>
              
        );
    }

export default MateriPokok;