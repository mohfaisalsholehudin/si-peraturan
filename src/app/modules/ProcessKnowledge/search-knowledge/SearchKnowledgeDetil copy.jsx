/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { Card as Cd, Row } from "react-bootstrap";
import swal from "sweetalert";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../_metronic/_partials/controls";
import {
  getDasarHukumByIdKmPro,
  getKnowledgeProcessById,
  getOtherKnowledgeByIdKmPro,
  getSopByIdKmPro,
  getSuccessStoryByIdKmPro,
  getUploadTerkaitByIdKmPro,
  updateStatusKnowledgeProcess,
} from "../../../references/Api";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import MateriPokok from "./materi-pokok/MateriPokok";
import { getTusibyIdKM, getUnitbyIdKM } from "../Api";
const { URL_DOWNLOAD } = window.ENV;

function SearchKnowledgeDetil({
  match: {
    params: { id_km, tipe_km },
  },
}) {
  const history = useHistory();
  const [content, setContent] = useState([]);
  const [draft, setDraft] = useState("");
  const [materi, setMateri] = useState();
  const [tusi, setTusi] = useState([]);
  const [unit, setUnit] = useState([]);
  const [file, setFile] = useState([]);
  const [hukum, setHukum] = useState([]);

  useEffect(() => {
    getKnowledgeProcessById(id_km).then(({ data }) => {
      setContent(data);
      // getSuccessStoryByIdKmPro(id_km).then((data_2) => {
      //   setMateri(data_2.data[0].successStoryTemp.materi);
      // });
      // switch (data.tipeKnowledge.template) {
      //   case "Success Story":
      //     return getSuccessStoryByIdKmPro(id_km).then((data_2) => {
      //       setMateri(data_2.data[0].successStoryTemp.materi);
      //     });
      //   case "SOP":
      //     return getSopByIdKmPro(id_km).then((data_2) => {
      //       setMateri(data_2.data[0] ? data_2.data[0].sopTemp.materi : null);
      //     })
      //   case "Other Knowledge":
      //     return getOtherKnowledgeByIdKmPro(id_km).then((data_2) => {
      //       setMateri(data_2.data[0] ? data_2.data[0].defaultTemp.materi : null)
      //     })
      //   default:
      //     break;
      // }
      getTusibyIdKM(id_km).then(({ data }) => {
        setTusi(data);
      });
      getUnitbyIdKM(id_km).then(({ data }) => {
        setUnit(data);
      });
      getUploadTerkaitByIdKmPro(id_km).then(({ data }) => {
        setFile(data);
      });
      getDasarHukumByIdKmPro(id_km).then((data) => {
        setHukum(data);
      });
    });
  }, [id_km]);

  useEffect(() => {
    getKnowledgeProcessById(id_km).then(({ data }) => {
      switch (data.tipeKnowledge.template) {
        case "Success Story":
          return getSuccessStoryByIdKmPro(id_km).then((data_2) => {
            setMateri(data_2.data[0].successStoryTemp.materi);
          });
        case "SOP":
          return getSopByIdKmPro(id_km).then((data_2) => {
            setMateri(data_2.data[0] ? data_2.data[0].sopTemp.materi : null);
          });
        case "Other Knowledge":
          return getOtherKnowledgeByIdKmPro(id_km).then((data_2) => {
            setMateri(
              data_2.data[0] ? data_2.data[0].defaultTemp.materi : null
            );
          });
        default:
          break;
      }
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const nama = "Angga ITo - 19/02/2022";

  const backAction = () => {
    history.push(`/process-knowledge/search-knowledge`);
  };

  const handleChange = () => {
    history.push(`/process-knowledge/update-review`);
  };

  // useEffect(()=> {
  //   console.log(id_km)
  //   console.log(tipe_km)
  // },[id_km, tipe_km])

  const applyReview = () => {
    // getUsulanById(id).then(({ data }) => {
    updateStatusKnowledgeProcess(id_km, "10").then(({ status }) => {
      if (status === 201 || status === 200) {
        swal("Berhasil", "Data Berhasil Disimpan", "success").then(() => {
          history.push("/dashboard");
          history.replace("/process-knowledge/update-review");
        });
      } else {
        swal("Gagal", "Data Gagal Disimpan", "error").then(() => {
          history.push("/dashboard");
          history.replace("/process-knowledge/update-review");
        });
      }
    });
    // });
  };

  const applyChange = () => {
    getKnowledgeProcessById(id_km).then(({ data }) => {
      swal("Masukkan Alasan Perubahan:", {
        content: "input",
        buttons: true,
      }).then((value) => {
        if (value) {
          if (data.jenis === "Tacit") {
            updateStatusKnowledgeProcess(id_km, "13", value).then(
              ({ status }) => {
                if (status === 201 || status === 200) {
                  swal("Berhasil", "Data Berhasil Disimpan", "success").then(
                    () => {
                      history.push("/dashboard");
                      history.replace("/process-knowledge/update-review");
                    }
                  );
                } else {
                  swal("Gagal", "Data Gagal Disimpan", "error").then(() => {
                    history.push("/dashboard");
                    history.replace("/process-knowledge/update-review");
                  });
                }
              }
            );
          } else {
            updateStatusKnowledgeProcess(id_km, "14", value).then(
              ({ status }) => {
                if (status === 201 || status === 200) {
                  swal("Berhasil", "Data Berhasil Disimpan", "success").then(
                    () => {
                      history.push("/dashboard");
                      history.replace("/process-knowledge/update-review");
                    }
                  );
                } else {
                  swal("Gagal", "Data Gagal Disimpan", "error").then(() => {
                    history.push("/dashboard");
                    history.replace("/process-knowledge/update-review");
                  });
                }
              }
            );
          }
        }
      });
    });
  };
  return (
    <>
      <Card>
        <CardHeader
          title="Detil Knowledge"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody style={{ backgroundColor: "#F7F7F7" }}>
          <>
            <Row>
              {/* Begin Overview*/}
              <div className="col-lg-6 col-xxl-4 order-1 order-xxl-1" style={{ paddingTop: "20px" }}>
                <Cd>
                  <Cd.Header
                    style={{ color: "white", backgroundColor: "#273787" }}
                  >
                    <h5 className="card-label">Knowledge Overview</h5>
                  </Cd.Header>
                  <Cd.Body>
                    <div className="d-flex align-items-center mb-10">
                      <div className="symbol symbol-40 symbol-light-success mr-5">
                        <span className="symbol-label">
                          <span className="svg-icon svg-icon-lg svg-icon-success">
                            <SVG
                              className="h-75 align-self-end"
                              src={toAbsoluteUrl(
                                "/media/svg/icons/Communication/Group-chat.svg"
                              )}
                            ></SVG>
                          </span>
                        </span>
                      </div>

                      <div className="d-flex flex-column font-weight-bold">
                        <a
                          href="#"
                          className="text-dark text-hover-primary mb-1 font-size-lg"
                        >
                          Judul
                        </a>

                        <span className="text-muted"> {content.judul}</span>
                      </div>
                    </div>
                    <div className="d-flex align-items-center mb-10">
                      <div className="symbol symbol-40 symbol-light-primary mr-5">
                        <span className="symbol-label">
                          <span className="svg-icon svg-icon-lg svg-icon-primary">
                            <SVG
                              className="h-75 align-self-end"
                              src={toAbsoluteUrl(
                                "/media/svg/icons/Home/Library.svg"
                              )}
                            ></SVG>
                          </span>
                        </span>
                      </div>

                      <div className="d-flex flex-column font-weight-bold">
                        <a
                          href="#"
                          className="text-dark text-hover-primary mb-1 font-size-lg"
                        >
                          Kontributor
                        </a>

                        <span className="text-muted">
                          {content.nama_kontri ? content.nama_kontri : "-"}
                        </span>
                      </div>
                    </div>

                    <div className="d-flex align-items-center mb-10">
                      <div className="symbol symbol-40 symbol-light-warning mr-5">
                        <span className="symbol-label">
                          <span className="svg-icon svg-icon-lg svg-icon-warning">
                            <SVG
                              className="h-75 align-self-end"
                              src={toAbsoluteUrl(
                                "/media/svg/icons/Communication/Write.svg"
                              )}
                            ></SVG>
                          </span>
                        </span>
                      </div>

                      <div className="d-flex flex-column font-weight-bold">
                        <a
                          href="#"
                          className="text-dark-75 text-hover-primary mb-1 font-size-lg"
                        >
                          Proses Bisnis
                        </a>

                        <span className="text-muted">
                          {content.bisnisProses
                            ? content.bisnisProses.nama
                            : null}
                        </span>
                      </div>
                    </div>
                    <div className="d-flex align-items-center mb-10">
                      <div className="symbol symbol-40 symbol-light-success mr-5">
                        <span className="symbol-label">
                          <span className="svg-icon svg-icon-lg svg-icon-success">
                            <SVG
                              className="h-75 align-self-end"
                              src={toAbsoluteUrl(
                                "/media/svg/icons/Communication/Group-chat.svg"
                              )}
                            ></SVG>
                          </span>
                        </span>
                      </div>

                      <div className="d-flex flex-column font-weight-bold">
                        <a
                          href="#"
                          className="text-dark text-hover-primary mb-1 font-size-lg"
                        >
                          Bisnis Sektor
                        </a>

                        <span className="text-muted">
                          {" "}
                          {content.bisnisSektor
                            ? content.bisnisSektor.nama
                            : null}
                        </span>
                      </div>
                    </div>
                    <div className="d-flex align-items-center mb-10">
                      <div className="symbol symbol-40 symbol-light-danger mr-5">
                        <span className="symbol-label">
                          <span className="svg-icon svg-icon-lg svg-icon-danger">
                            <SVG
                              className="h-75 align-self-end"
                              src={toAbsoluteUrl(
                                "/media/svg/icons/General/Attachment2.svg"
                              )}
                            ></SVG>
                          </span>
                        </span>
                      </div>

                      <div className="d-flex flex-column font-weight-bold">
                        <a
                          href="#"
                          className="text-dark text-hover-primary mb-1 font-size-lg"
                        >
                          Case Name
                        </a>

                        <span className="text-muted">
                          {" "}
                          {content.caseName ? content.caseName.nama : null}
                        </span>
                      </div>
                    </div>

                    <div className="d-flex align-items-center mb-10">
                      <div className="symbol symbol-40 symbol-light-info mr-5">
                        <span className="symbol-label">
                          <span className="svg-icon svg-icon-lg  svg-icon-info">
                            <SVG
                              className="h-75 align-self-end"
                              src={toAbsoluteUrl(
                                "/media/svg/icons/Communication/Shield-user.svg"
                              )}
                            ></SVG>
                          </span>
                        </span>
                      </div>

                      <div className="d-flex flex-column font-weight-bold">
                        <a
                          href="#"
                          className="text-dark text-hover-primary mb-1 font-size-lg"
                        >
                          Sub Case
                        </a>

                        <span className="text-muted">
                          {" "}
                          {content.subCase ? content.subCase.nama : null}
                        </span>
                      </div>
                    </div>
                    <div className="d-flex align-items-center mb-10">
                      <div className="symbol symbol-40 symbol-light-primary mr-5">
                        <span className="symbol-label">
                          <span className="svg-icon svg-icon-lg svg-icon-primary">
                            <SVG
                              className="h-75 align-self-end"
                              src={toAbsoluteUrl(
                                "/media/svg/icons/Home/Library.svg"
                              )}
                            ></SVG>
                          </span>
                        </span>
                      </div>

                      <div className="d-flex flex-column font-weight-bold">
                        <a
                          href="#"
                          className="text-dark text-hover-primary mb-1 font-size-lg"
                        >
                          Kategori
                        </a>

                        <span className="text-muted">
                          {content.kategori ? content.kategori : "-"}
                        </span>
                      </div>
                    </div>

                    <div className="d-flex align-items-center mb-10">
                      <div className="symbol symbol-40 symbol-light-warning mr-5">
                        <span className="symbol-label">
                          <span className="svg-icon svg-icon-lg svg-icon-warning">
                            <SVG
                              className="h-75 align-self-end"
                              src={toAbsoluteUrl(
                                "/media/svg/icons/Communication/Write.svg"
                              )}
                            ></SVG>
                          </span>
                        </span>
                      </div>

                      <div className="d-flex flex-column font-weight-bold">
                        <a
                          href="#"
                          className="text-dark-75 text-hover-primary mb-1 font-size-lg"
                        >
                          Level
                        </a>

                        <span className="text-muted">
                          {content.level_knowledge
                            ? content.level_knowledge
                            : "-"}
                        </span>
                      </div>
                    </div>
                  </Cd.Body>
                </Cd>
              </div>
              {/* End of Overview*/}

              {/* Begin Materi */}
              <div className="col-xxl-8 order-2 order-xxl-1"   style={{ paddingTop: "20px" }}>
                <Cd>
                  <Cd.Header
                    style={{ color: "white", backgroundColor: "#273787" }}
                  >
                    <h5 className="card-label">Materi Pokok</h5>
                  </Cd.Header>
                  <Cd.Body>
                    {/* <MateriPokok draft={materi} setDraft={setDraft} /> */}
                    <div>
                      {/* {materi} */}
                      <p className="fs-5 fw-bold text-gray-600 justify">
                        {materi}
                      </p>
                    </div>
                  </Cd.Body>
                </Cd>
              </div>
              {/* End of Materi */}

              {/* Begin Reviewer */}
              <div
                className="col-lg-6 col-xxl-4 order-1 order-xxl-2"
                style={{ paddingTop: "20px" }}
              >
                <Cd>
                  <Cd.Header
                    style={{ color: "white", backgroundColor: "#273787" }}
                  >
                    <h5 className="card-label">Reviewer</h5>
                  </Cd.Header>
                  <Cd.Body>
                    <div className="d-flex align-items-center mb-10">
                      <div className="symbol symbol-40 symbol-light-primary mr-5">
                        <span className="symbol-label">
                          <span className="svg-icon svg-icon-lg svg-icon-primary">
                            <SVG
                              className="h-75 align-self-end"
                              src={toAbsoluteUrl(
                                "/media/svg/icons/General/User.svg"
                              )}
                            ></SVG>
                          </span>
                        </span>
                      </div>

                      <div className="d-flex flex-column font-weight-bold">
                        <a
                          href="#"
                          className="text-dark text-hover-primary mb-1 font-size-lg"
                        >
                          Penjamin Kualitas Pengetahuan
                        </a>

                        <span className="text-muted">
                          {content.nama_kontri ? content.nama_kontri : "-"}
                        </span>
                      </div>
                    </div>

                    <div className="d-flex align-items-center mb-10">
                      <div className="symbol symbol-40 symbol-light-primary mr-5">
                        <span className="symbol-label">
                          <span className="svg-icon svg-icon-lg svg-icon-primary">
                            <SVG
                              className="h-75 align-self-end"
                              src={toAbsoluteUrl(
                                "/media/svg/icons/General/User.svg"
                              )}
                            ></SVG>
                          </span>
                        </span>
                      </div>

                      <div className="d-flex flex-column font-weight-bold">
                        <a
                          href="#"
                          className="text-dark-75 text-hover-primary mb-1 font-size-lg"
                        >
                          Knowledge Owner
                        </a>

                        <span className="text-muted">
                          {content.ko ? content.ko.nm_es4 : null}
                        </span>
                      </div>
                    </div>
                    <div className="d-flex align-items-center mb-10">
                      <div className="symbol symbol-40 symbol-light-primary mr-5">
                        <span className="symbol-label">
                          <span className="svg-icon svg-icon-lg svg-icon-primary">
                            <SVG
                              className="h-75 align-self-end"
                              src={toAbsoluteUrl(
                                "/media/svg/icons/General/User.svg"
                              )}
                            ></SVG>
                          </span>
                        </span>
                      </div>

                      <div className="d-flex flex-column font-weight-bold">
                        <a
                          href="#"
                          className="text-dark text-hover-primary mb-1 font-size-lg"
                        >
                          Subject Matter Expert
                        </a>

                        <span className="text-muted">
                          {" "}
                          {content.sme ? content.sme.nama : null}
                        </span>
                      </div>
                    </div>
                  </Cd.Body>
                </Cd>
              </div>
              {/* End of Reviewer */}

              {/* Begin Level Akses */}
              <div
                className="col-lg-6 col-xxl-4 order-1 order-xxl-2"
                style={{ paddingTop: "20px" }}
              >
                <Cd>
                  <Cd.Header
                    style={{ color: "white", backgroundColor: "#273787" }}
                  >
                    <h5 className="card-label">Level Akses</h5>
                  </Cd.Header>
                  <Cd.Body>
                    <div className="d-flex align-items-center mb-10">
                      <div className="symbol symbol-40 symbol-light-info mr-5">
                        <span className="symbol-label">
                          <span className="svg-icon svg-icon-lg svg-icon-info">
                            <SVG
                              className="h-75 align-self-end"
                              src={toAbsoluteUrl(
                                "/media/svg/icons/Communication/Shield-user.svg"
                              )}
                            ></SVG>
                          </span>
                        </span>
                      </div>

                      <div className="d-flex flex-column font-weight-bold">
                        <a
                          href="#"
                          className="text-dark text-hover-primary mb-1 font-size-lg"
                        >
                          ?????????????????
                        </a>

                        {/* <span className="text-muted">
                          {content.nama_kontri ? content.nama_kontri : "-"}
                        </span> */}
                      </div>
                    </div>
                  </Cd.Body>
                </Cd>
              </div>
              {/* End of Level Akses */}

              {/* Begin Tugas Fungsi */}
              <div
                className="col-lg-12 col-xxl-4 order-1 order-xxl-2"
                style={{ paddingTop: "20px" }}
              >
                <Cd>
                  <Cd.Header
                    style={{ color: "white", backgroundColor: "#273787" }}
                  >
                    <h5 className="card-label">Tugas dan Fungsi</h5>
                  </Cd.Header>
                  <Cd.Body>
                    {tusi.length > 0
                      ? tusi.map((data, index) => (
                          <div
                            key={index}
                            className="d-flex align-items-center mb-10"
                          >
                            <div className="symbol symbol-40 symbol-light-warning mr-5">
                              <span className="symbol-label">
                                <span className="svg-icon svg-icon-lg svg-icon-warning">
                                  <SVG
                                    className="h-75 align-self-end"
                                    src={toAbsoluteUrl(
                                      "/media/svg/icons/Communication/Clipboard-list.svg"
                                    )}
                                  ></SVG>
                                </span>
                              </span>
                            </div>

                            <div className="d-flex flex-column font-weight-bold">
                              <a
                                href="#"
                                className="text-dark text-hover-primary mb-1 font-size-lg"
                              >
                                {data.tusi.nama}
                              </a>
                            </div>
                          </div>
                        ))
                      : "-"}
                  </Cd.Body>
                </Cd>
              </div>
              {/* End of Tugas Fungsi */}

              {/* Begin Unit Organisasi */}
              <div
                className="col-lg-6 col-xxl-4 order-1 order-xxl-2"
                style={{ paddingTop: "20px" }}
              >
                <Cd>
                  <Cd.Header
                    style={{ color: "white", backgroundColor: "#273787" }}
                  >
                    <h5 className="card-label">Unit Organisasi</h5>
                  </Cd.Header>
                  <Cd.Body>
                    {unit.length > 0
                      ? unit.map((data, index) => (
                          <div
                            key={index}
                            className="d-flex align-items-center mb-10"
                          >
                            <div className="symbol symbol-40 symbol-light-success mr-5">
                              <span className="symbol-label">
                                <span className="svg-icon svg-icon-lg svg-icon-success">
                                  <SVG
                                    className="h-75 align-self-end"
                                    src={toAbsoluteUrl(
                                      "/media/svg/icons/Communication/Group.svg"
                                    )}
                                  ></SVG>
                                </span>
                              </span>
                            </div>

                            <div className="d-flex flex-column font-weight-bold">
                              <a
                                href="#"
                                className="text-dark text-hover-primary mb-1 font-size-lg"
                              >
                                {data.nama}
                              </a>
                            </div>
                          </div>
                        ))
                      : "-"}
                  </Cd.Body>
                </Cd>
              </div>
              {/* End of Unit Organisasi */}

              {/* Begin Dasar Hukum */}
              <div
                className="col-lg-6 col-xxl-4 order-1 order-xxl-2"
                style={{ paddingTop: "20px" }}
              >
                <Cd>
                  <Cd.Header
                    style={{ color: "white", backgroundColor: "#273787" }}
                  >
                    <h5 className="card-label">Dasar Hukum</h5>
                  </Cd.Header>
                  <Cd.Body>
                    {hukum.length > 0
                      ? hukum.map((data, index) => (
                          <div
                            key={index}
                            className="d-flex align-items-center mb-10"
                          >
                            <div className="mr-5">
                              <span className="symbol-label">
                                <span className="svg-icon svg-icon-xxl svg-icon-success">
                                  <SVG
                                    className="h-75 align-self-end"
                                    src={toAbsoluteUrl(
                                      "/media/svg/files/law.svg"
                                    )}
                                  ></SVG>
                                </span>
                              </span>
                            </div>

                            <div className="d-flex flex-column font-weight-bold">
                              <a
                                href="#"
                                className="text-dark text-hover-primary mb-1 font-size-lg"
                              >
                                {data.judul}
                              </a>
                            </div>
                          </div>
                        ))
                      : "-"}
                  </Cd.Body>
                </Cd>
              </div>
              {/* End of Dasar Hukum */}

              {/* Begin File Lampiran */}
              <div
                className="col-lg-12 col-xxl-4 order-1 order-xxl-2"
                style={{ paddingTop: "20px" }}
              >
                <Cd>
                  <Cd.Header
                    style={{ color: "white", backgroundColor: "#273787" }}
                  >
                    <h5 className="card-label">File Lampiran</h5>
                  </Cd.Header>
                  <Cd.Body>
                    {file.length > 0
                      ? file.map((data, index) => (
                          <div
                            key={index}
                            className="d-flex align-items-center mb-10"
                          >
                            <div className="mr-5">
                              <span className="symbol-label">
                                <span className="svg-icon svg-icon-xxl svg-icon-success">
                                  <SVG
                                    className="h-75 align-self-end"
                                    src={toAbsoluteUrl(
                                      "/media/svg/files/pdf.svg"
                                    )}
                                  ></SVG>
                                </span>
                              </span>
                            </div>

                            <div className="d-flex flex-column font-weight-bold">
                              <a
                                href={URL_DOWNLOAD + data.link}
                                target="_blank"
                                rel="noopener noreferrer"
                                className="text-dark text-hover-primary mb-1 font-size-lg"
                              >
                                {data.link}
                              </a>
                            </div>
                          </div>
                        ))
                      : "-"}
                  </Cd.Body>
                </Cd>
              </div>
              {/* End of File Lampiran */}

              {/* Begin Feedback */}
              <div
                className="col-lg-12 col-xxl-12 order-1 order-xxl-2"
                style={{ paddingTop: "20px" }}
              >
                <Cd>
                  <Cd.Header
                    style={{ color: "white", backgroundColor: "#273787" }}
                  >
                    <h5 className="card-label">Feedback</h5>
                  </Cd.Header>
                  <Cd.Body>??????????????????????</Cd.Body>
                </Cd>
              </div>
              {/* End of Feedback */}
            </Row>
          </>
        </CardBody>
        <CardFooter style={{ borderTop: "none", backgroundColor: "#F7F7F7" }}>
          <div
            className="col-lg-12"
            style={{ textAlign: "center", backgroundColor: "#F7F7F7" }}
          >
            <button
              type="button"
              className="btn btn-danger ml-2"
              // onClick={saveButton}
              onClick={() => applyChange()}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
              }}
              // disabled={disabled}
            >
              {/* <i className="fas fa-search"></i> */}
              Ajukan Perubahan
            </button>
            <button
              type="button"
              className="btn btn-success ml-6"
              // onClick={saveButton}
              onClick={() => applyReview()}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
              }}
              // disabled={disabled}
            >
              {/* <i className="fas fa-search"></i> */}
              Selesai Review
            </button>
            <button
              type="button"
              className="btn btn-primary ml-6"
              // onClick={saveButton}
              onClick={backAction}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
              }}
              // disabled={disabled}
            >
              {/* <i className="fas fa-search"></i> */}
              Kembali
            </button>
          </div>
        </CardFooter>
      </Card>
    </>
  );
}

export default SearchKnowledgeDetil;
