import React, {useEffect} from "react";
import { useHistory } from "react-router-dom";
import { Col, Row } from "react-bootstrap";
import swal from "sweetalert";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../_metronic/_partials/controls";
import { getKnowledgeProcessById, updateStatusKnowledgeProcess } from "../../../references/Api";

function SearchKnowledgeDetil({
  match: {
    params: { id_km, tipe_km }
  }
}) {
  const history = useHistory();

  const judulKnowledge = "Judul Knowledge 1";
  const kontributor = "Erna Alisa";
  const kategori = "Sulit";
  const level = "1";

  const probis = "Proses Bisnis 1";
  const sektor = "Bisnis Sektor 1";
  const casename = "Case Name";
  const subcase = "Sub Case";

  const materi =
    "I distinguish three main text objectives.First, your objective could be merely to inform people.A second be to persuade people. I distinguish three main text objectives.First, your objective could be merely to inform people.A second be to persuade people. I distinguish three main text objectives.First, your objective could be merely to inform people.A second be to persuade people. I distinguish three main text objectives.First, your objective could be merely to inform people.A second be to persuade people. I distinguish three main text objectives.First, your objective could be merely to inform people.A second be to persuade people. I distinguish three main text objectives.First, your objective could be merely to inform people.A second be to persuade people.";

  const pkp = "Seno";
  const ko = "Kasi PP2";
  const sme = "Angga";

  const levelAkses = ["Kasi Seksi PP2", "Kasubdit Subdit PP1", "Direktur TIK"];
  const tusi = ["Tusi 1", "Tusi 2", "Tusi 3"];
  const unitOrg = ["Seluruh Kanwil", "KPP A", "KPP B"];
  const dasarHukum = ["kep12312/123", "kep12312/124"];
  const fileLampiran = ["fileSOP.pdf", "Lampiran SOP.pdf"];

  const feedback =
    "Ini SOP bagus sekali ini SOP bagus sekali ini SOP bagus sekali ini SOP bagus sekali ini SOP bagus sekali";
  const nama = "Angga ITo - 19/02/2022";

  const backAction = () => {
    history.push(
        `/process-knowledge/search-knowledge`
      );
  }

  const handleChange = () => {
    history.push(
        `/process-knowledge/update-review`
      );
  }

  // useEffect(()=> {
  //   console.log(id_km)
  //   console.log(tipe_km)
  // },[id_km, tipe_km])

  const applyReview = () => {
    // getUsulanById(id).then(({ data }) => {
      updateStatusKnowledgeProcess(id_km, '10').then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data Berhasil Disimpan", "success").then(() => {
            history.push("/dashboard");
            history.replace("/process-knowledge/update-review");
          });
        } else {
          swal("Gagal", "Data Gagal Disimpan", "error").then(() => {
            history.push("/dashboard");
            history.replace("/process-knowledge/update-review");
          });
        }
      });
    // });
  }

  const applyChange = () => {
    getKnowledgeProcessById(id_km).then(({data})=> {
      swal("Masukkan Alasan Perubahan:", {
        content: "input",
        buttons: true
      })
      .then((value) => {
        if(value){
          if(data.jenis === "Tacit"){
            updateStatusKnowledgeProcess(id_km, '13', value).then(({ status }) => {
              if (status === 201 || status === 200) {
                swal("Berhasil", "Data Berhasil Disimpan", "success").then(() => {
                  history.push("/dashboard");
                  history.replace("/process-knowledge/update-review");
                });
              } else {
                swal("Gagal", "Data Gagal Disimpan", "error").then(() => {
                  history.push("/dashboard");
                  history.replace("/process-knowledge/update-review");
                });
              }
            });
          } else {
            updateStatusKnowledgeProcess(id_km, '14', value).then(({ status }) => {
              if (status === 201 || status === 200) {
                swal("Berhasil", "Data Berhasil Disimpan", "success").then(() => {
                  history.push("/dashboard");
                  history.replace("/process-knowledge/update-review");
                });
              } else {
                swal("Gagal", "Data Gagal Disimpan", "error").then(() => {
                  history.push("/dashboard");
                  history.replace("/process-knowledge/update-review");
                });
              }
            });
          }

        }


      });
    })
  }
  return (
    <>
      <Card>
        <CardHeader
          title="Detil Knowledge"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <>
            <Row>
              <Col md={6}>
                <h3> {judulKnowledge}</h3>
              </Col>
            </Row>
            <Row>
              <Col md={6}>
                <a href="#" className="text-primary">
                  {" "}
                  Daftar Review
                </a>
              </Col>
            </Row>
            <Row className="mt-2">
              <Col md={6}>Kontributor : {kontributor}</Col>
              <Col md={6}>
                Kategori : {kategori} , Level : {level}
              </Col>
            </Row>
            <Row className="mt-3">
              <Col md={6}>
                <h5>Proses Bisnis : {probis}</h5>
              </Col>
              <Col md={6}>
                <h5>Case Name : {casename}</h5>
              </Col>
            </Row>
            <Row className="mt-2">
              <Col md={6}>
                <h5>Bisnis Sektor : {sektor}</h5>
              </Col>
              <Col md={6}>
                <h5>Sub Case : {subcase}</h5>
              </Col>
            </Row>
            <Row className="mt-3">
              <Col md={12}>
                <p className="mb-7 mt-3">{materi}</p>
              </Col>
            </Row>
            <Row className="mt-3">
              <Col md={6}>
                Reviewer :
                <br />
                Penjamin Kualitas Pengetahuan : {pkp}
                <br />
                Knowledge Owner : {ko}
                <br />
                Subject Matter Expert : {sme}
              </Col>
              <Col md={6}>
                Level Akses :
                <br />
                {levelAkses.map((data, index) => (
                  <div key={index}>
                    {data}
                    <br />
                  </div>
                ))}
              </Col>
            </Row>
            <Row className="mt-3">
              <Col md={6}>
                <label style={{ fontWeight: "500", fontSize: "18px" }}>
                  Tugas dan Fungsi :{" "}
                </label>
                {tusi.map((data, index) => (
                  <div key={index}>
                    <label>{data}</label>
                    <br />
                  </div>
                ))}
              </Col>
              <Col md={6}>
                <label style={{ fontWeight: "500", fontSize: "18px" }}>
                  Unit Organisasi :{" "}
                </label>
                {unitOrg.map((data, index) => (
                  <div key={index}>
                    <label>{data}</label>
                    <br />
                  </div>
                ))}
              </Col>
            </Row>
            <Row className="mt-3">
              <Col md={6}>
                <label style={{ fontWeight: "500", fontSize: "18px" }}>
                  Dasar Hukum :{" "}
                </label>
                {dasarHukum.map((data, index) => (
                  <div key={index}>
                    <a href="#">{data}</a>
                    <br />
                  </div>
                ))}
              </Col>
              <Col md={6}>
                <label style={{ fontWeight: "500", fontSize: "18px" }}>
                  File Lampiran :{" "}
                </label>
                {fileLampiran.map((data, index) => (
                  <div key={index}>
                    <a href="#">{data}</a>
                    <br />
                  </div>
                ))}
              </Col>
            </Row>
            <Row className="mt-3">
              <Col md={12}>
                <label style={{ fontWeight: "500", fontSize: "18px" }}>
                  Feedback :{" "}
                </label>{" "}
                <br />
                <label>{feedback}</label>
              </Col>
            </Row>
            <Row className="mt-3">
              <Col md={12}>
                <label style={{ color: "orange" }}>{nama}</label>
              </Col>
            </Row>
          </>
        </CardBody>
        <CardFooter style={{ borderTop: "none" }}>
          <div className="col-lg-12" style={{ textAlign: "center" }}>
            <button
              type="button"
              className="btn btn-danger ml-2"
              // onClick={saveButton}
              onClick={()=> applyChange()}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
              // disabled={disabled}
            >
              {/* <i className="fas fa-search"></i> */}
              Ajukan Perubahan
            </button>
            <button
              type="button"
              className="btn btn-success ml-6"
              // onClick={saveButton}
              onClick={()=> applyReview()}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
              // disabled={disabled}
            >
              {/* <i className="fas fa-search"></i> */}
              Selesai Review
            </button>
            <button
              type="button"
              className="btn btn-primary ml-6"
              // onClick={saveButton}
              onClick={backAction}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
              // disabled={disabled}
            >
              {/* <i className="fas fa-search"></i> */}
              kembali
            </button>
          </div>
        </CardFooter>
      </Card>
    </>
  );
}

export default SearchKnowledgeDetil;
