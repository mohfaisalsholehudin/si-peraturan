/* eslint-disable jsx-a11y/role-supports-aria-props */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useEffect } from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
import DocumentEditorUpdateKonten from "../../../helpers/editor/DocumentEditorUpdateKonten";
import { getMatrix } from "../../../references/Api";


function MatrixKnowledge() {
  const [content, setContent] = useState('')

  useEffect(()=> {
    getMatrix().then(({data})=> {
      setContent(data)
    })
  },[])

  return (
    <>
      <Card>
        <CardHeader
          title="Matrix Knowledge"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          {/* {content} */}
          <DocumentEditorUpdateKonten content={content} setDraft={setContent} isReadOnly={true} />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default MatrixKnowledge;
