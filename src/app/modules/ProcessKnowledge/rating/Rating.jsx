import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls"; 
import RatingTable from "./RatingTable";

function Rating() {

  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Rating"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <RatingTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default Rating;
