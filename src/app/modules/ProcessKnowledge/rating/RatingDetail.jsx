import React, { useEffect, useState } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import { Field, Formik, Form } from "formik";
import { Textarea} from "../../../helpers";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";

/* Helpers */
import { Pagination } from "../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../../../helpers/column-formatters";
import { getRatingRata } from "../Api";

function RatingDetail({
  content
}) {
  const [ratingRata, setRatingRata] = useState([[]])

  useEffect(() => {
    getRatingRata().then(({data}) => {
      data.map((dt) => {
        if(content[0]){
          return content[0].id_km_pro === dt[0] ? 
        setRatingRata(dt[1])
        : null
        }
      })
    })
  }, [content[0]])

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "nilai_rating",
      text: "Nilai Rating",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "nip_rating",
      text: "NIP Pegawai",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    }
  ];

  const initialFilter = {
    sortOrder: "desc", // asc||desc
    sortField: "id_rating",
    pageNumber: 1,
    pageSize: 50
  };
  const defaultSorted = [{ dataField: "id_rating", order: "desc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_rating"
                  data={content}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
      <br></br>
      <Formik>
        {({ }) => {
          return (
            <>
              <Form className="form form-label-right">
                {/* Field Rating Rata-Rata */}
                <div className="form-group row">
                  <Field
                    value={ratingRata}
                    component={Textarea}
                    placeholder="Rating Rata-Rata"
                    label="Rating Rata-Rata"
                    disabled
                  />
                </div>
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default RatingDetail;
