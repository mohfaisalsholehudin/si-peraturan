/* Library */
import React, { useEffect, useState } from "react";
import { useSubheader } from "../../../../_metronic/layout";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../_metronic/_partials/controls";
import { getRatingbyIdKM } from "../Api"
import RatingDetail from "./RatingDetail";

function RatingMain({
  history,
  match: {
    params: { id_km_pro  }
  }
}) {
  const [content, setContent] = useState([]);
  const subheader = useSubheader();
  const [title, setTitle] = useState("");

  useEffect(()=> {
    let _title = "Detail Rating";
      setTitle(_title);
      subheader.setTitle(_title);
      getRatingbyIdKM(id_km_pro).then(({ data }) => {
        setContent(data)
      })
  },[id_km_pro, subheader])

  const backAction = () => {
    history.push(
      `/process-knowledge/rating`
    );
  };

  function tombolKembali(){
    return (
      <div className="col-lg-12" style={{ textAlign: "center" }}>
        <button
          type="button"
          onClick={backAction}
          className="btn btn-light"
          style={{
            boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
          }}
        >
          <i className="fa fa-arrow-left"></i>
          Kembali
        </button>
      </div>
    )     
  }
  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="mt-5">
            <RatingDetail
            content={content}
            id_km_pro = {id_km_pro}
            />
          </div>
        </>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        {tombolKembali()}
      </CardFooter>
    </Card>
  );
}

export default RatingMain;
