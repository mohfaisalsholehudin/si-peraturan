import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
import {
  getUsulanPerubahanById,
  updateStatusUsulanPerubahan,
} from "../../../references/Api";
import ReviewPengajuanUsulanApprove from "./ReviewPengajuanUsulanApprove";

function ReviewPengajuanUsulanOpen({ id, show, onHide }) {
  const history = useHistory();
  const [content, setContent] = useState();
  const [isShow, setIsShow] = useState(true);
  const [isApprove, setIsApprove] = useState(false);



  useEffect(() => {
    if (id) {
      getUsulanPerubahanById(id).then(({ data }) => {
        setContent(data);
        // console.log(data)
      });
    }
  }, [id]);

  const handleBack = () => {
    history.push(`/process-knowledge/review-pengajuan`);
  };

  const acceptAction = (val) => {
    updateStatusUsulanPerubahan(id, val.catatan,"Terima Pengajuan Template").then(
      ({ status }) => {
        if (status === 201 || status === 200) {
          // updateWaktuPublish(id_usulan_template, getCurrentDate());
          swal("Berhasil", "Usulan berhasil disetujui", "success").then(() => {
            history.push("/dashboard");
            history.replace("/process-knowledge/review-pengajuan");
          });
        } else {
          swal("Gagal", "Usulan gagal disetujui", "error").then(() => {
            history.push("/dashboard");
            history.replace("/process-knowledge/review-pengajuan");
          });
        }
      }
    );
    // updateStatusUsulan(id, 6).then(({status})=> {
    //   if (status === 201 || status === 200) {
    //     swal("Berhasil", "Usulan berhasil disetujui", "success").then(
    //       () => {
    //         history.push("/dashboard");
    //         history.replace("/evaluation/research");
    //       }
    //     );
    //   } else {
    //     swal("Gagal", "Data gagal disimpan", "error").then(() => {
    //       history.push("/dashboard");
    //       history.replace("/evaluation/research");
    //     });
    //   }
    // })
  };

  const handleApprove =()=> {
    setIsApprove(true);
    setIsShow(false)
  }
  const handleCancel = () => {
    setIsApprove(false);
    setIsShow(true);
  };

  return (
    <Modal
      size="md"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Detil Usulan Perubahan
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Judul</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${content ? content.judul_usulan : null}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Jenis Usulan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${content ? content.jenis_usulan : null}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Tipe</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${content ? content.tipe : null}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Isi</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${content ? content.isi : null}`}
            </span>
          </div>
        </div>
        {isApprove ? (
          <ReviewPengajuanUsulanApprove
            handleCancel={handleCancel}
            approve={acceptAction}
          />
        ) : (
          ""
        )}
      </Modal.Body>
      {isShow ? (
        <Modal.Footer style={{ borderTop: "none" }}>
          <div className="col-lg-12" style={{ textAlign: "center" }}>
            <button
              type="button"
              onClick={handleBack}
              // onClick={rejectAction}
              // onClick={handleReject}
              className="btn btn-light"
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
              }}
            >
              <i className="flaticon2-cancel icon-nm"></i>
              Cancel
            </button>
            {`  `}
            <button
              type="submit"
              className="btn btn-success ml-2"
              // onClick={saveForm}
              // onClick={acceptAction}
              onClick={handleApprove}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
              }}
            >
              <i className="fas fa-check"></i>
              Setuju
            </button>
          </div>
        </Modal.Footer>
      ) : (
        ""
      )}
    </Modal>
  );
}

export default ReviewPengajuanUsulanOpen;
