import React, { useEffect, useState } from "react";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import Select from "react-select";
import ReactPlayer from 'react-player'
import {
  Input,
  Textarea,
  Select as Sel,
  CustomAudio,
} from "../../../../helpers";
import {
  getBisnisSektor,
  getCaseNameByIdProbis,
  getProbisByStatus,
  getSubcaseByIdCasename,
} from "../../../../references/Api";
const { SLICE_URL, DOWNLOAD_URL, URL_DOWNLOAD, STREAM_URL_VIDEO, STREAM_URL_AUDIO } = window.ENV;


function ReviewUpdateOtherKnowledgeForm({ content, btnRef, saveForm, media, path }) {
  const [ValidateSchema, setValidateSchema] = useState();
  const [probis, setProbis] = useState([]);
  const [valProbis, setValProbis] = useState();
  const [casename, setCasename] = useState([]);
  const [valCasename, setValCasename] = useState();
  const [subcase, setSubcase] = useState([]);
  const [valSubcase, setValSubcase] = useState();
  const [sektor, setSektor] = useState([]);
  const [valSektor, setValSektor] = useState();

  useEffect(() => {
    if (media === "dokumen") {
      setValidateSchema(
        Yup.object().shape({
          tipe_knowledge: Yup.string().required("Tipe Knowledge is required"),
          id_probis: Yup.number().required("Proses Bisnis is required"),
          // id_sektor: Yup.number().required("Bisnis Sektor is required"),
          id_csname: Yup.number().required("Case Name is required"),
          // id_subcase: Yup.number().required("Sub Case is required"),
          judul: Yup.string()
            .min(2, "Minimum 2 symbols")
            .required("Judul is required"),
        })
      );
    } else {
      setValidateSchema(
        Yup.object().shape({
          tipe_knowledge: Yup.string().required("Tipe Knowledge is required"),
          id_probis: Yup.number().required("Proses Bisnis is required"),
          // id_sektor: Yup.number().required("Bisnis Sektor is required"),
          id_csname: Yup.number().required("Case Name is required"),
          // id_subcase: Yup.number().required("Sub Case is required"),
          judul: Yup.string()
            .min(2, "Minimum 2 symbols")
            .required("Judul is required"),
        })
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [media]);

  useEffect(() => {
    getProbisByStatus(1).then(({ data }) => {
      data.map((data) => {
        return setProbis((probis) => [
          ...probis,
          {
            label: data.nama,
            value: data.id_probis,
          },
        ]);
      });
      getBisnisSektor().then(({ data }) => {
        data.map((data) => {
          return setSektor((sektor) => [
            ...sektor,
            {
              label: data.nama,
              value: data.id_sektor,
            },
          ]);
        });
      });
      // })
    });
  }, []);

  useEffect(() => {
    if (content.id_sektor) {
      sektor
        .filter((data) => data.value === content.id_sektor)
        .map((data) => {
          setValSektor(data.label);
        });
    }
    // else {
    // }
    if (content.id_probis) {
      probis
        .filter((data) => data.value === content.id_probis)
        .map((data) => {
          setValProbis(data.label);
        });
      getCaseNameByIdProbis(content.id_probis).then(({ data }) => {
        data.map((data) => {
          return data.caseName.status === 1
            ? setCasename((casename) => [
                ...casename,
                {
                  label: data.caseName.nama,
                  value: data.caseName.id_case_name,
                },
              ])
            : null;
        });
      });
      getSubcaseByIdCasename(content.id_csname).then(({ data }) => {
        data.map((data) => {
          return data.subCase.status === 1
            ? setSubcase((subcase) => [
                ...subcase,
                {
                  label: data.subCase.nama,
                  value: data.subCase.id_subcase,
                },
              ])
            : null;
        });
      });
    }
    // else {
    // }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sektor, content.id_sektor]);
  useEffect(() => {
    if (content.id_csname) {
      casename
        .filter((data) => data.value === content.id_csname)
        .map((data) => {
          setValCasename(data.label);
        });
    } else {
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [casename]);

  useEffect(() => {
    if (content.id_subcase) {
      subcase
        .filter((data) => data.value === content.id_subcase)
        .map((data) => {
          setValSubcase(data.label);
        });
    } else {
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [subcase]);

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={content}
        validationSchema={ValidateSchema}
        onSubmit={(values) => {
          // console.log(values);
          saveForm(values);
        }}
      >
        {({ handleSubmit, setFieldValue, isValid, values, isSubmitting }) => {
          const handleChangeProbis = (val) => {
            setFieldValue("id_probis", val.value);
            setValProbis(val.label);
            setCasename([]);
            getCaseNameByIdProbis(val.value).then(({ data }) => {
              data.map((data) => {
                return data.caseName.status === 1
                  ? setCasename((casename) => [
                      ...casename,
                      {
                        label: data.caseName.nama,
                        value: data.caseName.id_case_name,
                      },
                    ])
                  : null;
              });
            });
          };
          const handleChangeCasename = (val) => {
            setFieldValue("id_csname", val.value);
            setValCasename(val.label);
            setSubcase([]);
            getSubcaseByIdCasename(val.value).then(({ data }) => {
              data.map((data) => {
                return data.subCase.status === 1
                  ? setSubcase((subcase) => [
                      ...subcase,
                      {
                        label: data.subCase.nama,
                        value: data.subCase.id_subcase,
                      },
                    ])
                  : null;
              });
            });
          };
          const handleChangeSubcase = (val) => {
            setFieldValue("id_subcase", val.value);
            setValSubcase(val.label);
          };
          const handleChangeSektor = (val) => {
            setFieldValue("id_sektor", val.value);
            setValSektor(val.label);
          };
          return (
            <>
              <Form className="form form-label-right">
                <div className="form-group row">
                  <Field
                    name="tipe_knowledge"
                    component={Input}
                    placeholder="Tipe Knowledge"
                    label="Tipe Knowledge"
                    disabled
                    solid={"true"}
                  />
                </div>
                {/* FIELD UPLOAD FILE */}
                {media !== "dokumen" ? (
                  values.tipe_knowledge ? (
                    <div className="form-group row">
                      <label className="col-xl-3 col-lg-3 col-form-label">
                        Upload Media
                      </label>
                      <div className="col-lg-9 col-xl-6">
                        <a
                          href={
                            content.file_upload
                              ? DOWNLOAD_URL +
                                content.file_upload.slice(SLICE_URL)
                              : null
                          }
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                          {content.file_upload
                            ? content.file_upload.slice(SLICE_URL)
                            : null}
                          {/* .slice(SLICE_URL) */}
                        </a>
                        {content.tipe_konten === "video" ? (
                          <ReactPlayer
                            className='react-player'
                            controls={true}
                            url={STREAM_URL_VIDEO + content.file_upload}
                          />
                        ) : 
                          content.tipe_konten === "audio" ? (
                          <ReactPlayer
                            className='react-player'
                            controls={true}
                            width="480px"
                            height="40px"
                            url={STREAM_URL_AUDIO + content.file_upload}
                          />
                          ) : null }
                      </div>
                    </div>
                  ) : null
                ) : null}
                {/* Field Bisnis Sektor */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Bisnis Sektor
                  </label>
                  <div className="col-lg-9 col-xl-6">
                  {path === 'view' ? (
                     <Select
                     options={sektor}
                     onChange={(value) => handleChangeSektor(value)}
                     value={sektor.filter((data) => data.label === valSektor)}
                     isDisabled
                   />
                   ) : (
                    <Select
                    options={sektor}
                    onChange={(value) => handleChangeSektor(value)}
                    value={sektor.filter((data) => data.label === valSektor)}
                  />
                   )}
                  </div>
                </div>
                {/* Field Proses Bisnis */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Proses Bisnis
                  </label>
                  <div className="col-lg-9 col-xl-6">
                  {path === 'view' ? (
                      <Select
                      options={probis}
                      onChange={(value) => handleChangeProbis(value)}
                      value={probis.filter((data) => data.label === valProbis)}
                      isDisabled
                    />
                    ) : (
                      <Select
                      options={probis}
                      onChange={(value) => handleChangeProbis(value)}
                      value={probis.filter((data) => data.label === valProbis)}
                    />
                    )}
                  </div>
                </div>
                {/* Field Case Name */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Case Name
                  </label>
                  <div className="col-lg-9 col-xl-6">
                  {path === 'view' ? (
                      <Select
                      options={casename}
                      onChange={(value) => handleChangeCasename(value)}
                      value={casename.filter(
                        (data) => data.label === valCasename
                      )}
                      isDisabled
                    />
                    ) : (
                      <Select
                      options={casename}
                      onChange={(value) => handleChangeCasename(value)}
                      value={casename.filter(
                        (data) => data.label === valCasename
                      )}
                    />
                    )}
                  </div>
                </div>
                {/* FIELD SUBCASE */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Subcase
                  </label>
                  <div className="col-lg-9 col-xl-6">
                  {path === 'view' ? (
                      <Select
                      options={subcase}
                      onChange={(value) => handleChangeSubcase(value)}
                      value={subcase.filter(
                        (data) => data.label === valSubcase
                      )}
                      isDisabled
                    />
                    ) : (
                      <Select
                      options={subcase}
                      onChange={(value) => handleChangeSubcase(value)}
                      value={subcase.filter(
                        (data) => data.label === valSubcase
                      )}
                    />
                    )}
                  </div>
                </div>
                 {/* Field Catatan PKP */}
                 <div className="form-group row">
                  <Field
                    name="catatan_pkp"
                    component={Textarea}
                    placeholder="Catatan PKP"
                    label="Catatan PKP"
                    disabled
                  />
                </div>
                {/* Field Judul */}
                <div className="form-group row">
                {path === 'view' ? (
                    <Field
                    name="judul"
                    component={Textarea}
                    placeholder="Judul"
                    label="Judul"
                    disabled
                  />
                  ) : (
                    <Field
                    name="judul"
                    component={Textarea}
                    placeholder="Judul"
                    label="Judul"
                  />
                  )}
                </div>
                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default ReviewUpdateOtherKnowledgeForm;
