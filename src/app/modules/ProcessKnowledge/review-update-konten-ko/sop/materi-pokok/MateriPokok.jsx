import React, { useEffect, useState } from "react";
import swal from "sweetalert";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../../../_metronic/_partials/controls";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import Editor from "ckeditor5-custom-build/build/ckeditor";
// import "./decoupled.css";

const { BACKEND_URL } = window.ENV;

function MateriPokok(
  { draft, setDraft
    // history,
    // match: {
    //   params: { id, id_draft, name }
    // }
  }
) {
  // const [draft, setDraft] = useState("");
  const [show, setShow] = useState(false);
  const [data, setData] = useState([]);

  useEffect(() => {
    // if (id_draft) {
    //   getDraft(id_draft).then(({ data }) => {
    //     setContent({
    //       body_draft: data.body_draft,
    //       jns_draft: data.jns_draft,
    //       file_upload: data.fileupload,
    //       lampiran: data.filelampiranr
    //     });
    //   });
    // }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const saveDraft = values => {
  };

  return (
    <>
      <div className="form-group row mb-4" style={{ marginBottom: "0px" }}>
        <div className="col-lg-9 col-xl-6">
          <h5 style={{ fontWeight: "600" }}>
            Materi Pokok
          </h5>
        </div>
      </div>
      <div className="row mb-4">
        <div className="document-editor" style={{ width: "100%" }}>
          <div className="document-editor__toolbar"></div>
          <div className="document-editor__editable-container">
            <CKEditor
              onReady={editor => {
                window.editor = editor;
                setShow(true);

                // Add these two lines to properly position the toolbar
                const toolbarContainer = document.querySelector(
                  ".document-editor__toolbar"
                );
                toolbarContainer.appendChild(editor.ui.view.toolbar.element);
              }}
              config={{
                simpleUpload: {
                  // The URL that the images are uploaded to.
                  uploadUrl: `${BACKEND_URL}/api/uploadckeditor`,

                  // Enable the XMLHttpRequest.withCredentials property.
                  withCredentials: false,

                  // Headers sent along with the XMLHttpRequest to the upload server.
                  headers: {
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Credentials": "true"
                  }
                }
              }}
              onChange={(event, editor) => {
                setDraft(editor.getData());
                setShow(true);
              }}
              editor={Editor}
              // data={content.body_draft ? content.body_draft : null}
              data={draft ? draft : null}
            />
          </div>
        </div>
      </div>
    </>
  );
}

export default MateriPokok;
