import React from 'react'
import MateriPokok from "./materi-pokok/MateriPokok";

function ReviewUpdateSopFormTwo({
    content,
    handleChangeEvaluation,
    setShow,
    btnRef,
    saveForm,
    isEdit,
    loading,
    media,
    draft,
    setDraft
  }) {
    return (
        <>
          <MateriPokok
          draft={draft}
          setDraft={setDraft}
           />
          <button
            type="submit"
            style={{ display: "none" }}
            ref={btnRef}
            onClick={() => saveForm()}
          ></button>
        </>
      );
}

export default ReviewUpdateSopFormTwo