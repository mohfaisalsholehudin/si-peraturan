import React, { useEffect, useState } from "react";
import { Formik, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import swal from "sweetalert";
import KategoriPengetahuanTable from "../kategori-pengetahuan/KategoriPengetahuanTable";
import Select from "react-select";
import { Select as Sel } from "../../../../../../helpers";
import { getKnowledgebyId, updateKnowledge, updateStatusPKP } from "../../../../Api";
import { useHistory } from "react-router-dom";

function KategoriPengetahuanDetailForm({
  content,
  jenis,
  btnRef,
  btnPublish,
  media,
  step,
  id_km_pro,
  showButton,
  hideButton,
  showForm,
  hideForm,
  lastPath
}) {
    const history = useHistory();
    const [isShow, setIsShow] = useState(false);
    const [knowledge, setKnowledge] = useState([]);
    const [isShowPengetahuan, setIsShowPengetahuan] = useState(false);
    const [table, setTable] = useState(false);
    const [isShowForm, setIsShowForm] = useState(true);
    const [valKategori, setValKategori] = useState([]);
    const [valLevel, setValLevel] = useState([]);
    
    useEffect(() => {
      getKnowledgebyId(id_km_pro).then(({data}) => {
        setKnowledge({
              tipe_konten: data.tipe_konten,
              id_probis: data.id_probis,
              id_sektor: data.id_sektor,
              id_csname: data.id_csname,
              id_subcase: data.id_subcase,
              id_tipe_km: data.id_tipe_km,
              id_km_pro: data.id_km_pro,
              judul: data.judul,
              media_upload: data.media_upload,
              catatan_pkp: data.catatan_pkp,
              catatan_ko: data.catatan_ko,
              kategori: data.kategori,
              level_knowledge: data.level_knowledge,
          });
      });
    })
  
    const initialValues = {
      id_km_pro: id_km_pro,
      kategori: knowledge.kategori,
      level_knowledge: knowledge.level_knowledge
    }
  
    const ValidateSchema = Yup.object().shape({
      kategori: Yup.string()
      .required("Kategori Pengetahuan is required"),
      level_knowledge: Yup.string()
      .required("Level Akses is required")
    });
  
    useEffect(() => {
      knowledge.level_knowledge != 1 ? setTable(false) : setTable(true);
      isShowPengetahuan ? showButton() : hideButton();
      isShowForm ? showForm() : hideForm();
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [knowledge, isShowPengetahuan, isShowForm]);
  
    const dataLevel = [
      {
        label: "1",
        value: "1"
      },
      {
        label: "2",
        value: "2"
      },
      {
        label: "3",
        value: "3"
      },
      {
        label: "4",
        value: "4"
      },
      { 
        label: "5",
        value: "5"
      }
    ];
  
    const dataPengetahuan = [
      {
        label: "Mudah",
        value: "mudah"
      },
      {
        label: "Sulit",
        value: "sulit"
      }
    ];
  
    const change = () => {
      setTable(false);
      setIsShow(false);
    };
  
    const changePengetahuan = () => {
      setIsShowPengetahuan(false);
      hideButton();
    };
  
    const changeForm = () => {
      setIsShowForm(true)
    }
  
    const saveSimpan = (values) => {
      swal({
        title: "Setuju",
        text: "Apakah Anda yakin menyetujui usulan ini ?",
        icon: "warning",
        buttons: true
      }).then(ret => {
        if (ret == true) {
      updateKnowledge(
        id_km_pro,
        "",
        content.catatan_ko,
        content.catatan_pkp,
        content.catatan_tolak,
        content.id_csname,
        "",
        content.id_probis,
        content.id_sektor,
        "",
        content.id_subcase,
        content.id_tipe_km,
        content.jenis,
        content.jml_view,
        content.judul,
        values.kategori,
        "",
        "",
        values.level_knowledge,
        content.media_upload,
        content.nama,
        "",
        "",
        "",
        content.nip_pkp,
        "",
        "",
        "",
        content.tipe_konten
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
          updateStatusPKP(content.id_km_pro, 2, 7, values.kategori, values.level_knowledge).then(({status}) => {
              if (status === 201 || status === 200) {
                swal("Berhasil", "Usulan berhasil disimpan", "success").then(() => {
                  history.push("/dashboard");
                  history.replace(`/process-knowledge/review-sme`);
            })
        } else {
          swal("Gagal", "Data Gagal Disimpan", "error").then(() => {
            history.push("/dashboard");
            history.replace(`/process-knowledge/review-sme/${id_km_pro}/success-story/${jenis}/5/review`);
          });
        }
      });
      }
    })
  }
      })
    }
  
    return (
      <>
        <>
          <Formik
            enableReinitialize={true}
            initialValues={initialValues}
            validationSchema={ValidateSchema}
            onSubmit={values => {
              console.log(values);
              saveSimpan(values);
            }}
          >
            {({ handleSubmit, setFieldValue, values, errors }) => {
              const handleChangeLevel = val => {
                // console.log(val.value);
                setFieldValue("level_knowledge", val.value);
                setValLevel(val.label);
                val.value === "1" ? setIsShow(true) : change();
              };
  
              const handleChangePengetahuan = val => {
                setFieldValue("kategori", val.value);
                setValKategori(val.label);
                val.value === "sulit"
                  ? setIsShowPengetahuan(true)
                  : changePengetahuan();
              };
  
              const handleChangeForm = val => {
                val.value !== "sulit"
                ? changeForm()
                : setIsShowForm(false)
              }
              return (
                <>
                  <Form className="form form-label-right">
                    {/* FIELD Kategori Pengetahuan */}
                    <div className="form-group row">
                        <Sel 
                          name="kategori" 
                          label="Kategori Pengetahuan"
                          disabled
                          >
                            <option key ={knowledge.kategori} value={knowledge.kategori}>
                              {knowledge.kategori}
                            </option>
                        </Sel>
                    </div>
                    {/* FIELD Level Akses */}
                      <div className="form-group row">
                        <Sel 
                            name="level_knowledge" 
                            label="Level Akses"
                            disabled
                            >
                            <option key ={knowledge.level_knowledge} value={knowledge.level_knowledge}>
                              {knowledge.level_knowledge}
                            </option>
                        </Sel>
                      <button
                        type="submit"
                        style={{ display: "none" }}
                        ref={btnRef}
                        onSubmit={() => handleSubmit()}
                      ></button>
                    </div>
                  </Form>
                </>
              );
            }}
          </Formik>
        </>
        {table ? <KategoriPengetahuanTable mdeia={media} step={step} jenis={jenis} id_km_pro={id_km_pro} lastPath={lastPath}/> : null}
      </>
    );
  }

export default KategoriPengetahuanDetailForm;
