import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { Textarea, Select as Sel } from "../../../../../helpers";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
import { useSubheader } from "../../../../../../_metronic/layout";
import { saveReferensi, getReferensiByIdRefrensi } from "../../../../../references/Api";

function ReferensiModal({ id_refrensi, show, onHide, after, id_detil, id_km_pro, id_tipe_km, step }) {
  const history = useHistory();
  const [content, setContent] = useState();
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");

  useEffect(() => {
    let _title = id_refrensi ? "Edit Referensi" : "Tambah Referensi";

    setTitle(_title);
    suhbeader.setTitle(_title);
  }, [id_refrensi, suhbeader]);

  // const initialValues = {
  //   pokok_pengaturan: "",
  //   jenis: ""
  // };
  useEffect(() => {
    if (id_refrensi) {
      getReferensiByIdRefrensi(id_refrensi).then(({ data }) => {
        setContent(data)
        // console.log(data)
        // setContent({
        //   judul: data.judul,
        //   link: data.link
        // });
      });
    } else {
      setContent({
        judul: "",
        link: ""
      });
    }
  }, [id_refrensi]);

  const validationSchema = Yup.object().shape({
    judul: Yup.string()
      .min(2, "Minimum 2 Characters")
      .required("Judul is required"),
    link: Yup.string()
      .min(2, "Minimum 2 Characters")
      .required("Link is required")
  });

  const saveFormReferensi = values => {
    // console.log(values)
    saveReferensi(
      id_km_pro,
      values.judul,
      values.link
    ).then(({ status }) => {
      if (status === 201 || status === 200) {
        swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
          history.push(`/dashboard`);
          // onHide();
          history.replace(
            `/process-knowledge/ko-update-review/${id_tipe_km}/${id_km_pro}/${step}/detil`
          );
        });
      }
    });
  };

  return (
    <Modal
      size="md"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
         {title}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <>
          <Formik
            enableReinitialize={true}
            initialValues={content}
            validationSchema={validationSchema}
            onSubmit={values => {
              // console.log(values);
              saveFormReferensi(values);
            }}
          >
            {({ handleSubmit }) => {
              return (
                <Form className="form form-label-right">
                  <div
                    className="form-group row"
                    style={{ marginBottom: "0px" }}
                  >
                    <div className="col-lg-9 col-xl-6">
                      <h5 className="mt-6" style={{ fontWeight: "600" }}>
                      Judul
                      </h5>
                    </div>
                  </div>
                  <div className="form-group row">
                    <Field
                      name="judul"
                      component={Textarea}
                      placeholder="Judul"
                      custom="custom"
                    />
                  </div>
                  <div
                    className="form-group row"
                    style={{ marginBottom: "0px" }}
                  >
                    <div className="col-lg-9 col-xl-6">
                      <h5 className="mt-6" style={{ fontWeight: "600" }}>
                      Link
                      </h5>
                    </div>
                  </div>
                  <div className="form-group row">
                    <Field
                      name="link"
                      component={Textarea}
                      placeholder="Link"
                      custom="custom"
                    />
                  </div>
                  <div className="col-lg-12" style={{ textAlign: "center" }}>
                    <button
                      type="button"
                      onClick={onHide}
                      className="btn btn-light"
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                      }}
                    >
                      <i className="flaticon2-cancel icon-nm"></i>
                      Batal
                    </button>
                    {`  `}
                    <button
                      type="submit"
                      onSubmit={() => handleSubmit()}
                      className="btn btn-success ml-2"
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                      }}
                    >
                      <i className="fas fa-save"></i>
                      Simpan
                    </button>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </>
      </Modal.Body>
    </Modal>
  );
}

export default ReferensiModal;
