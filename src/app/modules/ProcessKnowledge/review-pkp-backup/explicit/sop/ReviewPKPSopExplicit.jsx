/* Library */
import React, { useEffect, useState, useRef } from "react";
import swal from "sweetalert";

/* Helper */
import { useSubheader } from "../../../../../../_metronic/layout";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../../_metronic/_partials/controls";
import DasarHukumTable from "./dasar-hukum/DasarHukumTable";
import TugasFungsiTable from "./tugas-fungsi/TugasFungsiTable";
import UnitOrganisasiTable from "./unit-organisasi/UnitOrganisasiTable";
import ReviewPKPSopForm from "./ReviewPKPSopForm";
import ReviewPKPSopFormTwo from "./ReviewPKPSopFormTwo";
import UploadFileTable from "./upload-file/UploadFileTable";
import KategoriPengetahuanForm from "./kategori-pengetahuan/KategoriPengetahuanForm";

function ReviewPKPSopExplicit({
  history,
  match: {
    params: { id, media, step, jenis }
  }
}) {
  const initValues = {
    id: 3,
    tipe_knowledge: "SOP",
    tipe_konten: media,
    file: "C:UsersWahyuVideosShe Called me Wagyu-san lol.mp4",
    proses_bisnis: "b",
    bisnis_sektor: "a",
    casename: "a",
    subcase: "",
    judul: "SOP Business Process 2",
  };

  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [content, setContent] = useState();
  const [isShow, setIsShow] = useState(false);
  const [isShowForm, setIsShowForm] = useState(false)

  useEffect(() => {
    let _title = "Review Knowledge Explicit";

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps

    if (id) {
      //   getDetilJenisById(id).then(({ data })=> {
      //     setContent(data)
      //   })
    }
  }, [id, suhbeader]);
  const btnRef = useRef();

  // const saveButton = () => {
  //   if (btnRef && btnRef.current) {
  //     btnRef.current.click();
  //     // setIsComplete(true);
  //     // disabled ? setIsComplete(false) : setIsComplete(true);
  //   }
  // };

  // function levelAkses(){
  //   if(level_akses === "1") {
  //     return (
  //       <>
  //           <KategoriPengetahuanForm
  //           content={initValues}
  //           btnRef={btnRef}
  //           saveForm={saveForm}
  //           media={media}
  //         />
  //         <KategoriPengetahuanTable mdeia={media} step={step} />
  //       </>
  //     )
  //   } else {
  //       return (
  //         <>
  //             <KategoriPengetahuanForm
  //             content={initValues}
  //             btnRef={btnRef}
  //             saveForm={saveForm}
  //             media={media}
  //           />
  //         </>
  //       )
  //     }
  // }
  const hideButton = () => {
    setIsShow(false);
  };
  const showButton = () => {
    setIsShow(true);
  };
  const hideForm = () => {
    setIsShowForm(false);
  }
  const showForm = () => {
    setIsShowForm(true);
  }
  const backAction = () => {
    switch (step) {
      case "2":
        history.push(
          `/process-knowledge/review-knowledge/sop/${jenis}/${media}/1/review`
        );
        break;
      case "3":
        history.push(
          `/process-knowledge/review-knowledge/sop/${jenis}/${media}/2/review`
        );
        break;
      case "4":
        history.push(
          `/process-knowledge/review-knowledge/sop/${jenis}/${media}/3/review`
        );
        break;
      case "5":
        history.push(
          `/process-knowledge/review-knowledge/sop/${jenis}/${media}/4/review`
        );
        break;
      default:
        break;
    }
  };
  const saveForm = values => {
    // if (!id) {
    // } else {
    // }
    switch (step) {
      case "1":
        history.push(
          `/process-knowledge/review-knowledge/sop/${jenis}/${media}/2/review`
        );
        break;
      case "2":
        history.push(
          `/process-knowledge/review-knowledge/sop/${jenis}/${media}/3/review`
        );
        break;
      case "3":
        history.push(
          `/process-knowledge/review-knowledge/sop/${jenis}/${media}/4/review`
        );
        break;
      case "4":
        history.push(
          `/process-knowledge/review-knowledge/sop/${jenis}/${media}/5/review`
        );
        break;
      case "5":
        history.push(
          `/process-knowledge/review-knowledge/`
        );
        break;
      default:
        break;
    }
  };
  const checkStep = val => {
    switch (val) {
      case "1":
        return (
          <ReviewPKPSopForm
            // content={content || initValues}
            content={initValues}
            btnRef={btnRef}
            saveForm={saveForm}
            media={media}
          />
        );
      case "2":
        return (
          <ReviewPKPSopFormTwo
            content={initValues}
            btnRef={btnRef}
            saveForm={saveForm}
            media={media}
          />
        );
      case "3":
        return (
        <>
          <UploadFileTable media={media} step={step} />
          <br></br>
          <DasarHukumTable media={media} step={step} />
        </>
        );
      case "4":
        return (
          <>
            <TugasFungsiTable media={media} step={step} />
            <br></br>
            <UnitOrganisasiTable media={media} step={step} />
          </>
        );
      case "5":
        return (
          <>
            <KategoriPengetahuanForm
              content={initValues}
              btnRef={btnRef}
              saveForm={saveForm}
              media={media}
              step={step}
              hideButton={hideButton}
              showButton={showButton}
              showForm={showForm}
              hideForm={hideForm}
            />
          </>
        );
      default:
        break;
    }
  };
  const setujuDialog = id => {
    swal({
      title: "Setuju",
      text: "Apakah Anda yakin menyetujui usulan ini ?",
      icon: "warning",
      buttons: true
    }).then(ret => {
      if (ret == true) {
        // acceptAction(penegasan.status);
        history.push(`/process-knowledge/review-knowledge/`);
      }
    });
  };

  const tolakDialog = id => {
    swal({
      title: "Setuju",
      text: "Apakah Anda yakin menolak usulan ini ?",
      icon: "warning",
      buttons: true
    }).then(ret => {
      if (ret == true) {
        // acceptAction(penegasan.status);
        history.push(`/process-knowledge/review-knowledge/sop/${jenis}/${media}/tolak`);
      }
    });
  };

  const disposisiDialog = id => {
    swal({
      title: "Setuju",
      text: "Apakah Anda yakin disposisi usulan ini ?",
      icon: "warning",
      buttons: true
    }).then(ret => {
      if (ret == true) {
        // acceptAction(penegasan.status);
        history.push(`/process-knowledge/review-knowledge/sop/${jenis}/${media}/disposisi`);
      }
    });
  };

  function tombolProses() {
    if (step === "5") {
      return isShow ? (
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          <button
            type="button"
            onClick={backAction}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
          {`  `}
          <button
            type="submit"
            className="btn btn-info ml-2"
            onClick={disposisiDialog}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
            // disabled={disabled}
          >
            <i className="fas fa-share"></i>
            Disposisi
          </button>
        </div>
      ) : (
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          <button
            type="button"
            onClick={backAction}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
          {`  `}
          <button
            type="submit"
            className="btn btn-success ml-2"
            onClick={setujuDialog}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
            // disabled={disabled}
          >
            <i className="fas fa-check"></i>
            Simpan
          </button>
          {`  `}
          <button
            type="submit"
            className="btn btn-danger"
            onClick={tolakDialog}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
            // disabled={disabled}
          >
            <i className="flaticon2-cancel icon-nm"></i>
            Tolak
          </button>
        </div>
      );
    } else {
      if (step !== "1") {
        return (
          <div className="col-lg-12" style={{ textAlign: "center" }}>
            <button
              type="button"
              onClick={backAction}
              className="btn btn-light"
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
            >
              <i className="fa fa-arrow-left"></i>
              Kembali
            </button>
            <button
              type="submit"
              className="btn btn-success ml-2"
              onClick={saveForm}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
              // disabled={disabled}
            >
              Selanjutnya {`  `}
              <i className="fa fa-arrow-right"></i>
            </button>
          </div>
        );
      } else {
        return (
          <div className="col-lg-12" style={{ textAlign: "center" }}>
            <button
              type="submit"
              className="btn btn-success ml-2"
              onClick={saveForm}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
              // disabled={disabled}
            >
              Selanjutnya {`  `}
              <i className="fa fa-arrow-right"></i>
            </button>
          </div>
        );
      }
    }
  }
  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="mt-5">{checkStep(step)}</div>
        </>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        {/* <div className="col-lg-12" style={{ textAlign: "right" }}>
          {step !== "1" ? (
            <button
              type="button"
              onClick={backAction}
              className="btn btn-light"
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
            >
              <i className="fa fa-arrow-left"></i>
              Kembali
            </button>
          ) : null}
          {`  `} */}
        {tombolProses()}
        {/* {step === "7" ? (
            <button
              type="submit"
              className="btn btn-success ml-2"
              onClick={saveForm}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
              // disabled={disabled}
            >
              <i className="fas fa-save"></i>
              Simpan
            </button>
          ) : (
            <button
              type="submit"
              className="btn btn-success ml-2"
              onClick={saveForm}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
              // disabled={disabled}
            >
              Selanjutnya {`  `}
              <i className="fa fa-arrow-right"></i>
            </button>
          )} */}
        {/* </div> */}
      </CardFooter>
    </Card>
  );
}

export default ReviewPKPSopExplicit;
