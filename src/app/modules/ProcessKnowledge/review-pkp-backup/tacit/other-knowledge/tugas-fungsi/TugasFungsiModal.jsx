import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { Textarea, Select as Sel } from "../../../../../../helpers";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
import { useSubheader } from "../../../../../../../_metronic/layout";

function TugasFungsiModal({ id, show, onHide, after, id_detil, id_lhr }) {
  const history = useHistory();
  const [content, setContent] = useState();
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");

  useEffect(() => {
    let _title = id ? "Edit Tugas Fungsi" : "Tambah Tugas Fungsi";

    setTitle(_title);
    suhbeader.setTitle(_title);
  }, [id, suhbeader]);

  // const initialValues = {
  //   pokok_pengaturan: "",
  //   jenis: ""
  // };

  const validationSchema = Yup.object().shape({
    nama: Yup.string()
      .min(2, "Minimum 2 Characters")
      .required("Nama File is required"),
  });

  const savePengaturan = values => {
  };

  return (
    <Modal
      size="lg"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          {title}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <>
          <Formik
            enableReinitialize={true}
            initialValues={content}
            validationSchema={validationSchema}
            onSubmit={values => {
              // console.log(values);
              savePengaturan(values);
            }}
          >
            {({ handleSubmit }) => {
              return (
                <Form className="form form-label-right">
                  <div
                    className="form-group row"
                    style={{ marginBottom: "0px" }}
                  >
                    <div className="col-lg-9 col-xl-6">
                    </div>
                  </div>
                  <div className="form-group row">
                  <Sel name="tugas_fungsi" label="Tugas dan Fungsi">
                    <option>Pilih Tugas dan Fungsi</option>
                    <option value="a">Tugas dan Fungsi 1</option>
                    <option value="b">Tugas dan Fungsi 2</option>
                    <option value="c">Tugas dan Fungsi 3</option>
                  </Sel>
                  </div>
                  <div className="col-lg-12" style={{ textAlign: "center" }}>
                    <button
                      type="button"
                      onClick={onHide}
                      className="btn btn-light"
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                      }}
                    >
                      <i className="flaticon2-cancel icon-nm"></i>
                      Batal
                    </button>
                    {`  `}
                    <button
                      type="submit"
                      onSubmit={() => handleSubmit()}
                      className="btn btn-success ml-2"
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                      }}
                    >
                      <i className="fas fa-check"></i>
                      Kirim
                    </button>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </>
      </Modal.Body>
    </Modal>
  );
}

export default TugasFungsiModal;
