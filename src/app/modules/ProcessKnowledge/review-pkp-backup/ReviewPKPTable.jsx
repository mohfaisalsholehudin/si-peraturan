/* Library */
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import swal from "sweetalert";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";
import SVG from "react-inlinesvg";

/* Helpers */
import { Pagination } from "../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses
} from "../../../../_metronic/_helpers";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import * as columnFormatters from "../../../helpers/column-formatters";

/* Component */
// import ProposalOpen from "./ProposalOpen";
// import ProposalReject from "./ProposalReject";

/* Utility */

function ReviewPKPTable() {
  const history = useHistory();
//   const [content, setContent] = useState([]);
const content = [
  {id: 1, judul: "Knowledge", tipe_knowledge: "Success Story", jenis: "tacit", status:"Review PKP"},
  {id: 2, judul: "Business Process 2", tipe_knowledge: "Other Knowledge", jenis: "tacit", status:"Review PKP"},
  {id: 3, judul: "Business Process 3", tipe_knowledge: "SOP", jenis: "explicit", status:"Review PKP"}
]

const review = (id, jenis) => {
  if(content.jenis = "tacit"){
    history.push(`/process-knowledge/review-knowledge/${jenis}/${id}/review`);
  } else {
    if(content.jenis = "explicit"){
      history.push(`/process-knowledge/review-knowledge/${jenis}/${id}/review`);
    }
  }
}

  // const review = id => history.push(`/process-knowledge/review-knowledge/${id}/tacit/review`);
  const deleteAction = id => {
    //     swal({
    //       title: "Apakah Anda Yakin?",
    //       text: "Klik OK untuk melanjutkan",
    //       icon: "warning",
    //       buttons: true,
    //       dangerMode: true
    //     }).then(willDelete => {
    //       if (willDelete) {
    //         deleteDetilJenis(id).then(({ status }) => {
    //           if (status === 200) {
    //             swal("Berhasil", "Data berhasil dihapus", "success").then(() => {
    //               history.push("/dashboard");
    //               history.replace("/admin/detil-jenis");
    //             });
    //           } else {
    //             swal("Gagal", "Data gagal disimpan", "error").then(() => {
    //               history.push("/dashboard");
    //               history.replace("/admin/detil-jenis");
    //             });
    //           }
    //         });
    //       }
    //       // else {
    //       //   swal("Your imaginary file is safe!");
    //       // }
    //     });
  };
  const apply = () => {
      
    
  }

  //   useEffect(()=> {
  //     getDetilJenis().then(({ data }) => {
  //       setContent(data)
  //     })
  //   },[])

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "judul",
      text: "Judul",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "tipe_knowledge",
      text: "Tipe Knowledge",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "jenis",
      text: "Jenis",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      formatter: columnFormatters.StatusColumnFormatterProcessKnowledgeAdded,
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterProcessKnowledgeAdded,
      formatExtraData: {
        reviewProposal: review,
        openDeleteDialog: deleteAction,
        applyProposal: apply
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "nip",
    pageNumber: 1,
    pageSize: 50
  };
  const defaultSorted = [{ dataField: "nip", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id"
                  data={content}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
    </>
  );
}

export default ReviewPKPTable;
