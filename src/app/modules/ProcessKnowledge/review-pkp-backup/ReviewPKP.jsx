import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls"; 
import ReviewPKPTable from "./ReviewPKPTable";

function ReviewPKP() {

  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Review PKP"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <ReviewPKPTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default ReviewPKP;
