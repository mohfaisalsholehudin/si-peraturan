import React, { useState, useEffect } from "react";
import { Field, Formik, Form } from "formik";
import Select from "react-select";
import * as Yup from "yup";
import { Textarea, Select as Sel } from "../../../helpers";

function ReviewPKPTolakForm({ content, btnRef, saveForm }) {

  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    alasan_tolak: Yup.string().required("Alasan Tolak is requried")
  });

  useEffect(() => {
    // getMasterJenis().then(({ data }) => {
    //   data.map(data => {
    //     return setMaster(master => [
    //       ...master,
    //       {
    //         label: data.nama,
    //         value: data.id
    //       }
    //     ]);
    //   });
    // });
  }, []);
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={content}
        validationSchema={ProposalEditSchema}
        onSubmit={values => {
          saveForm(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
          handleBlur,
          handleChange,
          errors,
          touched,
          values,
          isValid
        }) => {
          return (
            <>
              <Form className="form form-label-right">
                {/* Field Catatan Tolak */}
                <div className="form-group row">
                <Field
                    name="alasan_tolak"
                    component={Textarea}
                    placeholder="Alasan Tolak"
                    label="Alasan Tolak"
                />
                </div>

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default ReviewPKPTolakForm;
