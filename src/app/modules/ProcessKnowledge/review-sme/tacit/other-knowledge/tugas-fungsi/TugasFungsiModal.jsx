import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import { Formik, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import Select from "react-select";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
import { useSubheader } from "../../../../../../../_metronic/layout";
import {getTusibyId, getTusibyStatus, saveTusiKM, updateTusiKM} from "../../../../Api";

function TugasFungsiModal({ id_tusi_kn, id_km_pro, show, onHide, jenis, step }) {

  const history = useHistory();
  const [content, setContent] = useState();
  const suhbeader = useSubheader();
  const [tusi, setTusi] = useState([]);
  const [valTusi, setValTusi] = useState([]);
  const [title, setTitle] = useState("");

  const initialValues = {
    id_km_pro: id_km_pro,
    id_tusi_km: ""
  };

  useEffect(() => {
    let _title = id_tusi_kn ? "Edit Tugas dan Fungsi" : "Tambah Tugas dan Fungsi";

    setTitle(_title);
    suhbeader.setTitle(_title);

    if (id_tusi_kn) {
      getTusibyId(id_tusi_kn).then(({ data }) => {
        setContent({
          id_tusi_kn: data.id_tusi_kn,
          id_km_pro: data.id_km_pro,
          id_tusi_km: data.id_tusi_km
        });
      });
    }
  }, [id_tusi_kn, suhbeader]);
 
  const validationSchema = Yup.object().shape({
    id_tusi_km: Yup.string()
      .required("Tugas dan Fungsi is required")
  });

  const saveForm = values => {
      saveTusiKM(
        values.id_km_pro,
        values.id_tusi_km,
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Tugas dan Fungsi berhasil disimpan", "success").then(
            () => {
              history.push('/dashboard');
              history.replace(`/process-knowledge/review-sme/${id_km_pro}/other-knowledge/${jenis}/${step}/review`);
            }
          );
        } else {
          swal("Gagal", "Tugas dan Fungsi gagal disimpan", "error").then(() => {
            history.push('/dashboard');
            history.replace(`/process-knowledge/review-sme/${id_km_pro}/other-knowledge/${jenis}/${step}/review/tugas-fungsi/open`);
          });
        }
      }
      )
  };

  useEffect(() => {
    getTusibyStatus(1).then(({ data }) => {
      data.map(data => {
        return setTusi(unit => [
          ...unit,
          {
            label: data.nama,
            value: data.id_tusi
          }
        ]);
      });
    });
  }, []);

  const renderError = (message) => <p style={{ color: "red" }}>{message}</p>;

  return (
    <Modal
      size="lg"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
         {title}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <>
          <Formik
            enableReinitialize={true}
            initialValues={content || initialValues}
            validationSchema={validationSchema}
            onSubmit={values => {
              saveForm(values);
            }}
          >
            {({ handleSubmit, setFieldValue }) => {
              const handleChangeTusi = val => {
                setFieldValue("id_tusi_km", val.value);
                setValTusi(val.label);
              };
              return (
                <Form className="form form-label-right">
                  <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Tugas dan Fungsi
                    </label>
                    <div className="col-lg-9 col-xl-6">
                      <Select
                          options={tusi}
                          onChange={value => handleChangeTusi(value)}
                          value={tusi.filter(data => data.label === valTusi)}
                        />
                      <ErrorMessage name="id_tusi_km" render={renderError} />
                  </div>
                  </div>
                  <div className="col-lg-12" style={{ textAlign: "center" }}>
                    <button
                      type="button"
                      onClick={onHide}
                      className="btn btn-light"
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                      }}
                    >
                      <i className="flaticon2-cancel icon-nm"></i>
                      Batal
                    </button>
                    {`  `}
                    <button
                      type="submit"
                      onSubmit={() => handleSubmit()}
                      className="btn btn-success ml-2"
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                      }}
                    >
                      <i className="fas fa-check"></i>
                      Kirim
                    </button>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </>
      </Modal.Body>
    </Modal>
  );
}

export default TugasFungsiModal;
