/* eslint-disable react-hooks/exhaustive-deps */
/* Library */
import React, { useEffect, useState, useRef } from "react";
import swal from "sweetalert";

/* Helper */
import { useSubheader } from "../../../../../../_metronic/layout";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../../_metronic/_partials/controls";
import PeraturanTerkaitTable from "./peraturan-terkait/PeraturanTerkaitTable";
import LihatPulaTable from "./lihat-pula/LihatPulaTable";
import BacaanLanjutanTable from "./bacaaan-lanjutan/BacaanLanjutanTable";
import ReferensiTable from "./referensi/ReferensiTable";
import TugasFungsiTable from "./tugas-fungsi/TugasFungsiTable";
import UnitOrganisasiTable from "./unit-organisasi/UnitOrganisasiTable";
import ReviewSMESuccessStoryForm from "./ReviewSMESuccessStoryForm";
import ReviewSMESuccessStoryFormTwo from "./ReviewSMESuccessStoryFormTwo";
import UploadFileTable from "./upload-file/UploadFileTable";
import KategoriPengetahuanForm from "./kategori-pengetahuan/KategoriPengetahuanForm";
import { getSuccessStorybyIdKM, updateKnowledge, getKnowledgebyId, updateSuccessStory, updateSuccessStoryMateri} from "../../../Api"
import { useSelector } from "react-redux";

function ReviewSMESuccessStoryTacit({
  history,
  match: {
    params: { id_tipe_km, id_km_pro, media, step, jenis, lastPath  }
  }
}) {
  
  const initValues = {
    tipe_konten: media,
    id_probis: "",
    id_sektor: "",
    id_csname: "",
    id_subcase: "",
    id_tipe_km: id_tipe_km,
    id_km_pro: id_km_pro,
    judul: "",
    pendahuluan: "",
    daftar_isi: "",
    definisi: "",
    tagging: "",
    kategori: "",
    level_knowledge: "",
    file: ""
  };

  const getCurrentDate = () => {
    const day = new Date();
    const YYYY = day.getFullYear();
      let MM = day.getMonth()+1;
      let DD = day.getDate();
      let HH = day.getHours() ;
      let mm = day.getMinutes()
      let ss = day.getSeconds();
      if(DD<10)
      {
          DD=`0${DD}`;
      }
      if(MM<10)
      {
          MM=`0${MM}`;
      }

      if(HH<10)
      {
          HH=`0${HH}`;
      }
      if(mm<10)
      {
          mm=`0${mm}`;
      }
      if(ss<10)
      {
          ss=`0${ss}`;
      }
    return YYYY + "-" + MM + "-" + DD + " " + HH + ":" + mm + ":" + ss
  }

  // Subheader
  const subheader = useSubheader();
  const { user } = useSelector((state) => state.auth);
  const [title, setTitle] = useState("");
  const [content, setContent] = useState();
  const [idTemp, setIdTemp] = useState();
  const [isShow, setIsShow] = useState(false);
  const [isShowForm, setIsShowForm] = useState(false)
  const [draft, setDraft] = useState("");
  const [materi, setMateri] = useState();
  
  const checkIdTemp = () => {
    getSuccessStorybyIdKM(id_km_pro).then(({ data }) => {
      if (data.length > 0) {
        setIdTemp(data[0].successStoryTemp.id_ss_temp);
      }
    });
  };

  useEffect(() => {
    let _title = "Review Knowledge Tacit";
    setTitle(_title);
    subheader.setTitle(_title);
    getKnowledgebyId(id_km_pro).then(({data}) => {
        getSuccessStorybyIdKM(id_km_pro).then(data_2 => {
          setContent({
              tipe_konten: data.tipe_konten,
              id_probis: data.id_probis,
              id_sektor: data.id_sektor,
              id_csname: data.id_csname,
              id_subcase: data.id_subcase,
              id_tipe_km: data.id_tipe_km,
              id_km_pro: data.id_km_pro,
              judul: data.judul,
              media_upload: data.media_upload,
              catatan_pkp: data.catatan_pkp,
              catatan_ko: data.catatan_ko,
              kategori: data.kategori,
              level_knowledge: data.level_knowledge,
              wkt_pkp: data.wkt_pkp,
              wkt_ko: data.wkt_ko,
              wkt_sme: data.wkt_sme,
              wkt_publish: data.wkt_publish,
              nip_pkp: data.nip_pkp,
              nip_ko: data.nip_ko,
              reviewer_pkp: data.reviewer_pkp,
              reviewer_ko: data.reviewer_ko,
              pendahuluan: data_2.data[0].successStoryTemp.pendahuluan,
              daftar_isi: data_2.data[0].successStoryTemp.daftar_isi,
              definisi: data_2.data[0].successStoryTemp.definisi,
              tagging: data_2.data[0].successStoryTemp.tagging
          });
          setMateri(data_2.data[0].successStoryTemp.materi);
          })
          checkIdTemp();
      });
    }, [checkIdTemp, id_km_pro, id_tipe_km, media, subheader])
    
  const btnRef = useRef();
  const btnPublish = useRef();

  const saveButton = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
    }
  };

  const publishButton = () => {
    if (btnPublish && btnPublish.current) {
      btnPublish.current.click();
    }
  };

  const hideButton = () => {
    setIsShow(false);
  };
  const showButton = () => {
    setIsShow(true);
  };
  const hideForm = () => {
    setIsShowForm(false);
  }
  const showForm = () => {
    setIsShowForm(true);
  }

  const backAction = () => {
    switch (step) {
      case "2":
        history.push(
          `/process-knowledge/review-sme/${id_km_pro}/success-story/${jenis}/1/review`
        );
        break;
      case "3":
        history.push(
          `/process-knowledge/review-sme/${id_km_pro}/success-story/${jenis}/2/review`
        );
        break;
      case "4":
        history.push(
          `/process-knowledge/review-sme/${id_km_pro}/success-story/${jenis}/3/review`
        );
        break;
      case "5":
        history.push(
          `/process-knowledge/review-sme/${id_km_pro}/success-story/${jenis}/4/review`
        );
        break;
      default:
        break;
    }
  };
  
  function saveSurat (values) {
      updateSuccessStory(
        idTemp,
        values.daftar_isi,
        values.definisi,
        content.id_km_pro,
        content.id_tipe_km,
        materi,
        values.pendahuluan,
        values.tagging
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
          switch (step) {
            case "1":
              history.push(
                `/process-knowledge/review-sme/${id_km_pro}/success-story/${jenis}/2/review`
              );
              break;
            case "2":
              history.push(
                `/process-knowledge/review-sme/${id_km_pro}/success-story/${jenis}/3/review`
              );
              break;
            case "3":
              history.push(
                `/process-knowledge/review-sme/${id_km_pro}/success-story/${jenis}/4/review`
              );
              break;
            case "4":
              history.push(
                `/process-knowledge/review-sme/${id_km_pro}/success-story/${jenis}/5/review`
              );
              break;
            case "5":
              history.push(`/process-knowledge/review-sme/`);
              break;
            default:
              break;
          }
        }
      });
      updateKnowledge(
        values.id_km_pro,
        values.id_csname,
        values.id_ko,
        values.id_probis,
        values.id_sektor,
        values.id_sme,
        values.id_subcase,
        values.id_tipe_km,
        values.judul,
        content.nip_pkp,
        content.nip_ko,
        user.nip18,
        content.wkt_ko,
        content.wkt_pkp,
        content.wkt_publish,
        getCurrentDate(),
        content.reviewer_pkp,
        content.reviewer_ko,
        user.namaPegawai
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            switch (step) {
            case "1":
              history.push(
                `/process-knowledge/review-sme/${id_km_pro}/success-story/${jenis}/2/review`
              );
              break;
            case "2":
              history.push(
                `/process-knowledge/review-sme/${id_km_pro}/success-story/${jenis}/3/review`
              );
              break;
            case "3":
              history.push(
                `/process-knowledge/review-sme/${id_km_pro}/success-story/${jenis}/4/review`
              );
              break;
            case "4":
              history.push(
                `/process-knowledge/review-sme/${id_km_pro}/success-story/${jenis}/5/review`
              );
              break;
            case "5":
              history.push(`/process-knowledge/review-sme/`);
              break;
            default:
              break;
          }
        }
      })
  }

  const saveFormTwo = () => {
    updateSuccessStoryMateri(idTemp, draft ? draft : materi).then(({ status }) => {
      if (status === 201 || status === 200) {
        switch (step) {
          case "1":
            history.push(
              `/process-knowledge/review-sme/${id_km_pro}/success-story/${jenis}/2/review`
            );
            break;
          case "2":
            history.push(
              `/process-knowledge/review-sme/${id_km_pro}/success-story/${jenis}/3/review`
            );
            break;
          case "3":
            history.push(
              `/process-knowledge/review-sme/${id_km_pro}/success-story/${jenis}/4/review`
            );
            break;
          case "4":
            history.push(
              `/process-knowledge/review-sme/${id_km_pro}/success-story/${jenis}/5/review`
            );
            break;
          case "5":
            history.push(`/process-knowledge/review-sme/`);
            break;
          default:
            break;
        }
      }
    })
}

  const nextStep = () => {
    switch (step) {
      case "3":
        history.push(
          `/process-knowledge/review-sme/${id_km_pro}/success-story/${jenis}/4/review`
        );
        break;
      case "4":
        history.push(
          `/process-knowledge/review-sme/${id_km_pro}/success-story/${jenis}/5/review`
        );
        break;
      case "5":
        history.push(`/process-knowledge/review-sme/`);
        break;
      default:
        break;
    }
  }
  
  const checkStep = val => {
    switch (val) {
      case "1":
        return (
          <ReviewSMESuccessStoryForm
            content={content || initValues}
            btnRef={btnRef}
            saveForm={saveSurat}
            media={media}
          />
        );
      case "2":
        return (
          <ReviewSMESuccessStoryFormTwo
            content={content || initValues}
            btnRef={btnRef}
            saveFormTwo={saveFormTwo}
            media={media}
            draft={materi}
            setDraft={setDraft}
          />
        );
      case "3":
        return (
          <>
            <ReferensiTable media={media} step={step} id_km_pro={id_km_pro} jenis={jenis}/>
            <br></br>
            <LihatPulaTable media={media} step={step} id_km_pro={id_km_pro} jenis={jenis} lastPath={lastPath}/>
            <br></br>
            <BacaanLanjutanTable media={media} step={step} id_km_pro={id_km_pro} jenis={jenis} lastPath={lastPath}/>
            <br></br>
            <PeraturanTerkaitTable media={media} step={step} id_km_pro={id_km_pro} jenis={jenis}/>
            <br></br>
            <UploadFileTable media={media} step={step} id_km_pro={id_km_pro} jenis={jenis}/>
          </>
        );
      case "4":
        return (
          <>
            <TugasFungsiTable media={media} step={step} id_km_pro={id_km_pro} jenis={jenis}/>
            <br></br>
            <UnitOrganisasiTable media={media} step={step} id_km_pro={id_km_pro} jenis={jenis}/>
          </>
        );
      case "5":
        return (
          <>
            <KategoriPengetahuanForm
              content={content}
              btnRef={btnRef}
              btnPublish={btnPublish}
              media={media}
              step={step}
              id_km_pro={id_km_pro}
              jenis={jenis}
              hideButton={hideButton}
              showButton={showButton}
              showForm={showForm}
              hideForm={hideForm}
            />
          </>
        );
      default:
        break;
    }
  };

  const tolakDialog = id => {
    swal({
      title: "Tolak",
      text: "Apakah Anda yakin menolak usulan ini ?",
      icon: "warning",
      buttons: true
    }).then(ret => {
      if (ret == true) {
        history.push(`/process-knowledge/review-sme/${id_km_pro}/success-story/${jenis}/tolak`);
      }
    });
  };

  function tombolProses() {
    if (step === "5") {
      return (
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          <button
            type="button"
            onClick={backAction}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
          {`  `}
          <button
            type="button"
            className="btn btn-success ml-2"
            onClick={saveButton}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fas fa-save"></i>
            Simpan Draft
          </button>
          {`  `}
          <button
            type="submit"
            className="btn btn-primary ml-2"
            onClick={publishButton}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fas fa-check"></i>
            Simpan Review
          </button>
          {`  `}
          <button
            type="submit"
            className="btn btn-danger ml-2"
            onClick={tolakDialog}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="flaticon2-cancel icon-nm"></i>
            Tolak
          </button>
        </div>
      );
    } else {
      if (step === "1") {
        return (
          <div className="col-lg-12" style={{ textAlign: "center" }}>
            <button
              type="submit"
              className="btn btn-success ml-2"
              onClick={saveButton}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
            >
              Selanjutnya {`  `}
              <i className="fa fa-arrow-right"></i>
            </button>
          </div>
        );
      } else if (step === "2"){
        return (
          <div className="col-lg-12" style={{ textAlign: "center" }}>
            <button
              type="button"
              onClick={backAction}
              className="btn btn-light"
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
            >
              <i className="fa fa-arrow-left"></i>
              Kembali
            </button>
            <button
              type="submit"
              className="btn btn-success ml-2"
              onClick={saveButton}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
            >
              Selanjutnya {`  `}
              <i className="fa fa-arrow-right"></i>
            </button>
          </div>
        );
        }
        else if (step === "3"){
          return (
            <div className="col-lg-12" style={{ textAlign: "center" }}>
              <button
                type="button"
                onClick={backAction}
                className="btn btn-light"
                style={{
                  boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                }}
              >
                <i className="fa fa-arrow-left"></i>
                Kembali
              </button>
              <button
                type="submit"
                className="btn btn-success ml-2"
                onClick={nextStep}
                style={{
                  boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                }}
              >
                Selanjutnya {`  `}
                <i className="fa fa-arrow-right"></i>
              </button>
            </div>
          );
        }
      else if (step === "4"){
        return (
          <div className="col-lg-12" style={{ textAlign: "center" }}>
            <button
              type="button"
              onClick={backAction}
              className="btn btn-light"
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
            >
              <i className="fa fa-arrow-left"></i>
              Kembali
            </button>
            <button
              type="submit"
              className="btn btn-success ml-2"
              onClick={nextStep}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
            >
              Selanjutnya {`  `}
              <i className="fa fa-arrow-right"></i>
            </button>
          </div>
        );
      }
    }
  }

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="mt-5">{checkStep(step)}</div>
        </>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        {tombolProses()}
      </CardFooter>
    </Card>
  );
}

export default ReviewSMESuccessStoryTacit;
