/* Library */
import React, { useEffect, useState, useRef } from "react";
import swal from "sweetalert";

/* Helper */
import { useSubheader } from "../../../../../../_metronic/layout";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../../_metronic/_partials/controls";
import { getKnowledgebyId, tolakPKP, updateKnowledge, updateKnowledgeTolak } from "../../../Api";
import ReviewSMESuccessStoryTolakForm from "./ReviewSMESuccessStoryTolakForm";

function ReviewSMESuccessStoryTolak({
  history,
  match: {
    params: { id_km_pro, id_ss_temp, media, jenis }
  }
}) {

  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [knowledge, setKnowledge] = useState([])

  useEffect(() => {
    let _title = "Catatan Tolak" ;

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps

    if (id_km_pro) {
      //   getDetilJenisById(id).then(({ data })=> {
      //     setContent(data)
      //   })
    }
  }, [id_km_pro, suhbeader]);

  useEffect(() => {
    getKnowledgebyId(id_km_pro).then(({data}) => {
      setKnowledge({
        id_km_pro: data.id_km_pro,
        id_tipe_km: data.id_tipe_km,
        id_ko: data.id_ko,
        id_sme: data.id_sme,
        id_probis: data.id_probis,
        id_sektor: data.id_sektor,
        id_csname: data.id_csname,
        id_subcase: data.id_subcase,
        })
    })
  }, [])

  const btnRef = useRef();
  const saveButton = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
      // setIsComplete(true);
      // disabled ? setIsComplete(false) : setIsComplete(true);
    }
  };

  const backAction = () => {
    history.push(`/process-knowledge/review-sme/${id_km_pro}/success-story/${jenis}/5/review`);
  };

  const saveForm = values => {
    // console.log(values)
    swal({
      title: "Setuju",
      text: "Apakah Anda yakin menolak usulan ini ?",
      icon: "warning",
      buttons: true
    }).then(ret => {
      if (ret == true) {
        updateKnowledgeTolak(
          knowledge.id_km_pro,
          values.catatan_tolak,
          knowledge.id_csname,
          knowledge.id_ko,
          knowledge.id_probis,
          knowledge.id_sektor,
          knowledge.id_sme,
          knowledge.id_subcase,
          knowledge.id_tipe_km
    ).then(({ status }) => {
      if (status === 201 || status === 200) {
          tolakPKP(knowledge.id_km_pro, 12, values.catatan_tolak).then(({status}) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Usulan berhasil ditolak", "success").then(() => {
                history.push("/dashboard");
                history.replace(`/process-knowledge/review-sme`);
          })
      } else {
        swal("Gagal", "Data Gagal Disimpan", "error").then(() => {
          history.push("/dashboard");
          history.replace(`/process-knowledge/review-sme/${id_km_pro}/success-story/${jenis}/5/review`);
        });
      }
    });
    }
  })
}
    })
  }

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="mt-5">
            <ReviewSMESuccessStoryTolakForm
              btnRef={btnRef}
              saveForm={saveForm}
              content={knowledge}
            />
          </div>
        </>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        {/* <DetilJenisEditFooter backAction={backAction} btnRef={btnRef} /> */}
        <div className="col-lg-12" style={{ textAlign: "right" }}>
          <button
            type="button"
            onClick={backAction}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
          {`  `}
          <button
            type="submit"
            className="btn btn-success ml-2"
            onClick={saveButton}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
            // disabled={disabled}
          >
            <i className="fas fa-save"></i>
            Simpan
          </button>
        </div>
      </CardFooter>
    </Card>
  );
}

export default ReviewSMESuccessStoryTolak;