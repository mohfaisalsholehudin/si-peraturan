/* Library */
import React, { useEffect, useState, useRef } from "react";
import swal from "sweetalert";

/* Helper */
import { useSubheader } from "../../../../../../_metronic/layout";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../../_metronic/_partials/controls";
import { getKnowledgebyId, tolakPKP, updateKnowledge } from "../../../Api";
import ReviewSMETolakForm from "./ReviewSMETolakForm";

function ReviewSMETolakSop({
  history,
  match: {
    params: { id_km_pro, id_ss_temp, media, jenis }
  }
}) {

  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [knowledge, setKnowledge] = useState([])

  useEffect(() => {
    let _title = "Catatan Tolak" ;

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps

    if (id_km_pro) {
      //   getDetilJenisById(id).then(({ data })=> {
      //     setContent(data)
      //   })
    }
  }, [id_km_pro, suhbeader]);

  useEffect(() => {
    getKnowledgebyId(id_km_pro).then(({data}) => {
      console.log(data)
      setKnowledge({
        id_km_pro: data.id_km_pro,
        nama: data.nama,
        id_tipe_km: data.id_tipe_km,
        jenis: data.jenis,
        id_status_km: data.id_status_km,
        kategori: data.kategori,
        level_knowledge: data.level_knowledge,
        catatan_tolak: data.catatan_tolak,
        catatan_pkp: data.catatan_pkp,
        catatan_ko: data.catatan_ko,
        id_ko: data.id_ko,
        id_sme: data.id_sme,
        nip_sme: data.nip_sme,
        nip_ko: data.nip_ko,
        nip_pkp: data.nip_pkp,
        nip_kontri: data.nip_kontri,
        nama_kontri: data.nama_kontri,
        nm_unit_kontri: data.nm_unit_kontri,
        ket_review: data.ket_review,
        relevan: data.relevan,
        cat_review: data.cat_review,
        jml_view: data.jml_view,
        kodekantor_kontri: data.kodekantor_kontri,
        id_probis: data.id_probis,
        id_sektor: data.id_sektor,
        id_csname: data.id_csname,
        id_subcase: data.id_subcase,
        tipe_konten: data.tipe_konten,
        media_upload: data.media_upload,
        judul: data.judul
        })
    })
  }, [])

  const btnRef = useRef();
  const saveButton = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
      // setIsComplete(true);
      // disabled ? setIsComplete(false) : setIsComplete(true);
    }
  };

  const backAction = () => {
    history.push(`/process-knowledge/review-sme/${id_km_pro}/sop/${jenis}/5/review`);
  };

  const saveForm = values => {
    // console.log(values)
    swal({
      title: "Setuju",
      text: "Apakah Anda yakin menolak usulan ini ?",
      icon: "warning",
      buttons: true
    }).then(ret => {
      if (ret == true) {
        updateKnowledge(
          knowledge.id_km_pro,
          "",
          knowledge.catatan_ko,
          knowledge.catatan_pkp,
          values.catatan_tolak,
          knowledge.id_csname,
          "",
          knowledge.id_probis,
          knowledge.id_sektor,
          "",
          knowledge.id_subcase,
          knowledge.id_tipe_km,
          knowledge.jenis,
          knowledge.jml_view,
          knowledge.judul,
          knowledge.kategori,
          "",
          "",
          "",
          knowledge.media_upload,
          knowledge.nama,
          "",
          "",
          "",
          knowledge.nip_pkp,
          "",
          "",
          "",
          knowledge.tipe_konten
    ).then(({ status }) => {
      if (status === 201 || status === 200) {
          tolakPKP(knowledge.id_km_pro, 12, values.catatan_tolak).then(({status}) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Usulan berhasil ditolak", "success").then(() => {
                history.push("/dashboard");
                history.replace(`/process-knowledge/review-sme`);
          })
      } else {
        swal("Gagal", "Data Gagal Disimpan", "error").then(() => {
          history.push("/dashboard");
          history.replace(`/process-knowledge/review-sme/${id_km_pro}/sop/${jenis}/5/review`);
        });
      }
    });
    }
  })
}
    })
  }

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="mt-5">
            <ReviewSMETolakForm
              btnRef={btnRef}
              saveForm={saveForm}
              content={knowledge}
            />
          </div>
        </>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        {/* <DetilJenisEditFooter backAction={backAction} btnRef={btnRef} /> */}
        <div className="col-lg-12" style={{ textAlign: "right" }}>
          <button
            type="button"
            onClick={backAction}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
          {`  `}
          <button
            type="submit"
            className="btn btn-success ml-2"
            onClick={saveButton}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
            // disabled={disabled}
          >
            <i className="fas fa-save"></i>
            Simpan
          </button>
        </div>
      </CardFooter>
    </Card>
  );
}

export default ReviewSMETolakSop;