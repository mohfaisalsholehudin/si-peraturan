import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { Textarea, Select as Sel } from "../../../../../../helpers";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
import { useSubheader } from "../../../../../../../_metronic/layout";


function BacaanLanjutanModal({ id, show, onHide}) {
  const history = useHistory();
  const [content, setContent] = useState();
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");

  useEffect(() => {
    let _title = id ? "Edit Bacaan Lanjutan" : "Tambah Bacaan Lanjutan";

    setTitle(_title);
    suhbeader.setTitle(_title);
  }, [id, suhbeader]);

  // const initialValues = {
  //   pokok_pengaturan: "",
  //   jenis: ""
  // };

  const validationSchema = Yup.object().shape({
    nama: Yup.string()
      .min(2, "Minimum 2 Characters")
      .required("Nama is required"),
    link: Yup.string()
      .min(2, "Minimum 2 Characters")
      .required("Link is required"),
  });

  const savePengaturan = values => {
  };

  return (
    <Modal
      size="md"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          {title}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <>
          <Formik
            enableReinitialize={true}
            initialValues={content}
            validationSchema={validationSchema}
            onSubmit={values => {
              // console.log(values);
              savePengaturan(values);
            }}
          >
            {({ handleSubmit }) => {
              return (
                <Form className="form form-label-right">
                  <div
                    className="form-group row"
                    style={{ marginBottom: "0px" }}
                  >
                    <div className="col-lg-9 col-xl-6">
                      <h5 className="mt-4" style={{ fontWeight: "600" }}>
                      Nama
                      </h5>
                    </div>
                  </div>
                  <div className="form-group row">
                    <Field
                      name="nama"
                      component={Textarea}
                      placeholder="Nama"
                      custom="custom"
                    />
                  </div>
                  <div
                    className="form-group row"
                    style={{ marginBottom: "0px" }}
                  >
                    <div className="col-lg-9 col-xl-6">
                      <h5 className="mt-6" style={{ fontWeight: "600" }}>
                      Link
                      </h5>
                    </div>
                  </div>
                  <div className="form-group row">
                    <Field
                      name="link"
                      component={Textarea}
                      placeholder="Link"
                      custom="custom"
                    />
                  </div>
                  <div className="col-lg-12" style={{ textAlign: "center" }}>
                    <button
                      type="button"
                      onClick={onHide}
                      className="btn btn-light"
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                      }}
                    >
                      <i className="flaticon2-cancel icon-nm"></i>
                      Batal
                    </button>
                    {`  `}
                    <button
                      type="submit"
                      onSubmit={() => handleSubmit()}
                      className="btn btn-success ml-2"
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                      }}
                    >
                      <i className="fas fa-check"></i>
                      Kirim
                    </button>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </>
      </Modal.Body>
    </Modal>
  );
}

export default BacaanLanjutanModal;
