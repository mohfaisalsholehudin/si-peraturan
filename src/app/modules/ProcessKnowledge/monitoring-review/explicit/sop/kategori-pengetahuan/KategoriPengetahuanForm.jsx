import React, { useEffect, useState } from "react";
import { Formik, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import swal from "sweetalert";
import KategoriPengetahuanTable from "../kategori-pengetahuan/KategoriPengetahuanTable";
import Select from "react-select";
import { updateKnowledge, updateStatusPKP } from "../../../../Api";
import { useHistory } from "react-router-dom";

function KategoriPengetahuanForm({
  content,
  jenis,
  btnRef,
  btnPublish,
  media,
  step,
  id_km_pro,
  showButton,
  hideButton,
  showForm,
  hideForm,
  lastPath
}) {
  const history = useHistory();
  const [isShow, setIsShow] = useState(false);
  const [isShowPengetahuan, setIsShowPengetahuan] = useState(false);
  const [table, setTable] = useState(false);
  const [isShowForm, setIsShowForm] = useState(true);
  const [valKategori, setValKategori] = useState([]);
  const [valLevel, setValLevel] = useState([]);
  
  const initialValues = {
    id_km_pro: id_km_pro,
    kategori: "",
    level_knowledge: ""
  }

  const ValidateSchema = Yup.object().shape({
    kategori: Yup.string()
    .required("Kategori Pengetahuan is required"),
    level_knowledge: Yup.string()
    .required("Level Akses is required")
  });

  useEffect(() => {
    isShow ? setTable(true) : setTable(false);
    isShowPengetahuan ? showButton() : hideButton();
    isShowForm ? showForm() : hideForm();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isShow, isShowPengetahuan, isShowForm]);

  const dataLevel = [
    {
      label: "1",
      value: "1"
    },
    {
      label: "2",
      value: "2"
    },
    {
      label: "3",
      value: "3"
    },
    {
      label: "4",
      value: "4"
    },
    { 
      label: "5",
      value: "5"
    }
  ];

  const dataPengetahuan = [
    {
      label: "Mudah",
      value: "mudah"
    },
    {
      label: "Sulit",
      value: "Sulit"
    }
  ];

  const change = () => {
    setTable(false);
    setIsShow(false);
  };

  const changePengetahuan = () => {
    setIsShowPengetahuan(false);
    hideButton();
  };

  const changeForm = () => {
    setIsShowForm(true)
  }

  const renderError = (message) => <p style={{ color: "red" }}>{message}</p>;

  const saveSimpan = (values) => {
    swal({
      title: "Setuju",
      text: "Apakah Anda Yakin Menyimpan Draft Usulan Ini ?",
      icon: "warning",
      buttons: true
    }).then(ret => {
      if (ret == true) {
    updateKnowledge(
      id_km_pro,
      "",
      content.catatan_ko,
      content.catatan_pkp,
      content.catatan_tolak,
      content.id_csname,
      "",
      content.id_probis,
      content.id_sektor,
      "",
      content.id_subcase,
      content.id_tipe_km,
      content.jenis,
      content.jml_view,
      content.judul,
      values.kategori,
      "",
      "",
      values.level_knowledge,
      content.media_upload,
      content.nama,
      "",
      "",
      "",
      content.nip_pkp,
      "",
      "",
      "",
      content.tipe_konten
    ).then(({ status }) => {
      if (status === 201 || status === 200) {
              swal("Berhasil", "Draft Usulan Berhasil Disimpan", "success").then(() => {
                history.push("/dashboard");
                history.replace(`/process-knowledge/review-pkp`);
          })
      } else {
        swal("Gagal", "Data Gagal Disimpan", "error").then(() => {
          history.push("/dashboard");
          history.replace(`/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/5/review`);
        });
    }
  })
  }
    })
  }

  const savePublish = (values) => {
    if(values.kategori && values.level_knowledge){
    swal({
      title: "Setuju",
      text: "Apakah Anda Ingin Menyetujui Usulan Ini ?",
      icon: "warning",
      buttons: true
    }).then(ret => {
      if (ret == true) {
    updateKnowledge(
      id_km_pro,
      "",
      content.catatan_ko,
      content.catatan_pkp,
      content.catatan_tolak,
      content.id_csname,
      "",
      content.id_probis,
      content.id_sektor,
      "",
      content.id_subcase,
      content.id_tipe_km,
      content.jenis,
      content.jml_view,
      content.judul,
      values.kategori,
      "",
      "",
      values.level_knowledge,
      content.media_upload,
      content.nama,
      "",
      "",
      "",
      content.nip_pkp,
      "",
      "",
      "",
      content.tipe_konten
    ).then(({ status }) => {
      if (status === 201 || status === 200) {
        updateStatusPKP(content.id_km_pro, 3, 8, values.kategori, values.level_knowledge).then(({status}) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Usulan Berhasil Disetujui", "success").then(() => {
                history.push("/dashboard");
                history.replace(`/process-knowledge/review-pkp`);
          })
      } else {
        swal("Gagal", "Data Gagal Disimpan", "error").then(() => {
          history.push("/dashboard");
          history.replace(`/process-knowledge/review-pkp/${id_km_pro}/sop/${jenis}/5/review`);
        });
      }
    });
    }
  })
}
    })
  }
}

  return (
    <>
      <>
        <Formik
          enableReinitialize={true}
          initialValues={initialValues}
          validationSchema={ValidateSchema}
          onSubmit={values => {
            console.log(values);
            savePublish(values);
          }}
        >
          {({ handleSubmit, setFieldValue, values, errors }) => {
            const handleChangeLevel = val => {
              // console.log(val.value);
              setFieldValue("level_knowledge", val.value);
              setValLevel(val.label);
              val.value === "1" ? setIsShow(true) : change();
            };

            const handleChangePengetahuan = val => {
              setFieldValue("kategori", val.value);
              setValKategori(val.label);
              val.value === "Sulit"
                ? setIsShowPengetahuan(true)
                : changePengetahuan();
            };

            const handleChangeForm = val => {
              val.value !== "Sulit"
              ? changeForm()
              : setIsShowForm(false)
            }
            return (
              <>
                <Form className="form form-label-right">
                  {/* FIELD Kategori Pengetahuan */}
                  <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Kategori Pengetahuan
                    </label>
                    <div className="col-lg-9 col-xl-6">
                      <Select
                        options={dataPengetahuan}
                        onChange={value => {handleChangePengetahuan(value); handleChangeForm(value)}}
                        value={dataPengetahuan.filter(data => data.label === valKategori)}
                      />
                      <ErrorMessage name="kategori" render={renderError} />
                    </div>
                  </div>
                  {/* FIELD Level Akses */}
                  { isShowForm ? (
                    <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Level Akses
                    </label>
                    <div className="col-lg-9 col-xl-6">
                      <Select
                        options={dataLevel}
                        onChange={value => handleChangeLevel(value)}
                        value={dataLevel.filter(data => data.label === valLevel)}
                      />
                      <ErrorMessage name="level_knowledge" render={renderError} />
                    </div>
                    <button
                      type="submit"
                      style={{ display: "none" }}
                      ref={btnPublish}
                      onSubmit={() => handleSubmit()}
                    ></button>
                    <button
                      type="button"
                      style={{ display: "none" }}
                      ref={btnRef}
                      onClick={() => saveSimpan(values)}
                    ></button>
                  </div>
                  ) : null}
                </Form>
              </>
            );
          }}
        </Formik>
      </>
      {table ? <KategoriPengetahuanTable mdeia={media} step={step} jenis={jenis} id_km_pro={id_km_pro} lastPath={lastPath}/> : null}
    </>
  );
}

export default KategoriPengetahuanForm;
