import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls"; 
import MonitoringReviewTable from "./MonitoringReviewTable";

function MonitoringReview() {

  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Monitoring Review"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <MonitoringReviewTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default MonitoringReview;
