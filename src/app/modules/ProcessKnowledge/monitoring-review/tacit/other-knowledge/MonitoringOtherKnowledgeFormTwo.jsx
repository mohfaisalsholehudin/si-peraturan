import React from "react";
import MateriPokok from "./materi-pokok/MateriPokok";

function MonitoringOtherKnowledgeFormTwo({
  draft,
  setDraft,
  lastPath
}) {
  return (
    <>
      <MateriPokok
      draft={draft}
      setDraft={setDraft}
      lastPath={lastPath}
       />
    </>
  );
}

export default MonitoringOtherKnowledgeFormTwo;
