/* Library */
import React, { useEffect, useState } from "react";
import swal from "sweetalert";

/* Helper */
import { useSubheader } from "../../../../../../_metronic/layout";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../../_metronic/_partials/controls";
import BacaanLanjutanTable from "./bacaaan-lanjutan/BacaanLanjutanTable";
import LihatPulaTable from "./lihat-pula/LihatPulaTable";
import PeraturanTerkaitTable from "./peraturan-terkait/PeraturanTerkaitTable";
import TugasFungsiTable from "./tugas-fungsi/TugasFungsiTable";
import UnitOrganisasiTable from "./unit-organisasi/UnitOrganisasiTable";
import MonitoringOtherKnowledgeFormTwo from "./MonitoringOtherKnowledgeFormTwo";
import UploadFileTable from "./upload-file/UploadFileTable";
import { getKnowledgebyId, getOtherKnowledgebyIdKM} from "../../../Api"
import MonitoringOtherKnowledgeForm from "./MonitoringOtherKnowledgeForm";
import KategoriPengetahuanDetailForm from "./kategori-pengetahuan/KategoriPengetahuanDetailForm";

function MonitoringReviewOtherKnowledgeTacit({
  history,
  match: {
    params: { id_tipe_km, id_km_pro, media, step, jenis  }
  }
}) {

  let thePath = document.URL;
  const initValues = {
    tipe_konten: media,
    id_probis: "",
    id_sektor: "",
    id_csname: "",
    id_subcase: "",
    id_tipe_km: id_tipe_km,
    id_km_pro: id_km_pro,
    judul: "",
    file: ""
  };

  // Subheader
  const subheader = useSubheader();
  const [title, setTitle] = useState("");
  const [content, setContent] = useState();
  const [idTemp, setIdTemp] = useState();
  const [isShow, setIsShow] = useState(false);
  const [isShowForm, setIsShowForm] = useState(false)
  const [draft, setDraft] = useState("");
  const [materi, setMateri] = useState();
  const lastPath = thePath.substring(thePath.lastIndexOf("/") + 1);
  
  const checkIdTemp = () => {
    getOtherKnowledgebyIdKM(id_km_pro).then(({ data }) => {
      if (data.length > 0) {
        setIdTemp(data[0].defaultTemp.id_default_temp);
      }
    });
  };

  useEffect(() => {
      let _title = "Detail Review Tacit";
      setTitle(_title);
      subheader.setTitle(_title);
    getKnowledgebyId(id_km_pro).then(({data}) => {
      getOtherKnowledgebyIdKM(id_km_pro).then(data_2 => {
          setContent({
              tipe_konten: data.tipe_konten,
              id_probis: data.id_probis,
              id_sektor: data.id_sektor,
              id_csname: data.id_csname,
              id_subcase: data.id_subcase,
              id_tipe_km: data.id_tipe_km,
              id_km_pro: data.id_km_pro,
              judul: data.judul,
              media_upload: data.media_upload,
              kategori: data.kategori,
              level_knowledge: data.level_knowledge
          });
          setMateri(data_2.data[0].defaultTemp.materi);
          })
          checkIdTemp();
      });
    }, [id_km_pro, id_tipe_km, media, subheader])

  const hideButton = () => {
    setIsShow(false);
  };
  const showButton = () => {
    setIsShow(true);
  };
  const hideForm = () => {
    setIsShowForm(false);
  }
  const showForm = () => {
    setIsShowForm(true);
  }

  const backAction = () => {
      switch (step) {
        case "2":
          history.push(
            `/process-knowledge/monitoring-review/${id_km_pro}/other-knowledge/${jenis}/1/detail`
          );
          break;
        case "3":
          history.push(
            `/process-knowledge/monitoring-review/${id_km_pro}/other-knowledge/${jenis}/2/detail`
          );
          break;
        case "4":
          history.push(
            `/process-knowledge/monitoring-review/${id_km_pro}/other-knowledge/${jenis}/3/detail`
          );
          break;
        case "5":
          history.push(
            `/process-knowledge/monitoring-review/${id_km_pro}/other-knowledge/${jenis}/4/detail`
          );
          break;
        default:
          break;
      }
  }

  const nextStep = () => {
      switch (step) {
        case "1":
          history.push(
            `/process-knowledge/monitoring-review/${id_km_pro}/other-knowledge/${jenis}/2/detail`
          );
          break;
        case "2":
          history.push(
            `/process-knowledge/monitoring-review/${id_km_pro}/other-knowledge/${jenis}/3/detail`
          );
          break;
        case "3":
          history.push(
            `/process-knowledge/monitoring-review/${id_km_pro}/other-knowledge/${jenis}/4/detail`
          );
          break;
        case "4":
          history.push(
            `/process-knowledge/monitoring-review/${id_km_pro}/other-knowledge/${jenis}/5/detail`
          );
          break;
        default:
          break;
      }
  };
  
  const checkStep = val => {
    switch (val) {
      case "1":
          return (
          <MonitoringOtherKnowledgeForm
            content={content || initValues}
            media={media}
          />
          )
      case "2":
        return (
          <MonitoringOtherKnowledgeFormTwo
            content={content || initValues}
            media={media}
            draft={materi}
            setDraft={setDraft}
            lastPath={lastPath}
          />
        );
      case "3":
        return (
          <>
            <LihatPulaTable media={media} step={step} id_km_pro={id_km_pro} jenis={jenis} lastPath={lastPath}/>
            <br></br>
            <BacaanLanjutanTable media={media} step={step} id_km_pro={id_km_pro} jenis={jenis} lastPath={lastPath}/>
            <br></br>
            <PeraturanTerkaitTable media={media} step={step} id_km_pro={id_km_pro} jenis={jenis} lastPath={lastPath}/>
            <br></br>
            <UploadFileTable media={media} step={step} id_km_pro={id_km_pro} jenis={jenis} lastPath={lastPath}/>
          </>
        );
      case "4":
        return (
          <>
            <TugasFungsiTable media={media} step={step} id_km_pro={id_km_pro} jenis={jenis} lastPath={lastPath}/>
            <br></br>
            <UnitOrganisasiTable media={media} step={step} id_km_pro={id_km_pro} jenis={jenis} lastPath={lastPath}/>
          </>
        );
      case "5":
        return (
          <>
            <KategoriPengetahuanDetailForm
              content={content}
              media={media}
              step={step}
              id_km_pro={id_km_pro}
              jenis={jenis}
              hideButton={hideButton}
              showButton={showButton}
              showForm={showForm}
              hideForm={hideForm}
              lastPath={lastPath}
            />
          </>
        );
        default:
          break;
    }
  };

  const homeDialog = id => {
    swal({
      title: "Kembali",
      text: "Apakah Anda Ingin Kembali Ke Halaman Monitoring ?",
      icon: "warning",
      buttons: true
    }).then(ret => {
      if (ret == true) {
        history.push(`/process-knowledge/monitoring-review`);
      }
    });
  }

  function tombolDetail(){
    if (step === "5") {
      return (
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          <button
            type="button"
            onClick={backAction}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
          {`  `}
          <button
            type="button"
            className="btn btn-primary ml-2"
            onClick={homeDialog}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fas fa-home"></i>
            Kembali ke Halaman Utama
          </button>
        </div>
      );
    } else {
      if (step === "1") {
        return (
          <div className="col-lg-12" style={{ textAlign: "center" }}>
            <button
              type="submit"
              className="btn btn-success ml-2"
              onClick={nextStep}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
            >
              Selanjutnya {`  `}
              <i className="fa fa-arrow-right"></i>
            </button>
          </div>
        );
      } else if (step === "2"){
        return (
          <div className="col-lg-12" style={{ textAlign: "center" }}>
            <button
              type="button"
              onClick={backAction}
              className="btn btn-light"
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
            >
              <i className="fa fa-arrow-left"></i>
              Kembali
            </button>
            <button
              type="submit"
              className="btn btn-success ml-2"
              onClick={nextStep}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
            >
              Selanjutnya {`  `}
              <i className="fa fa-arrow-right"></i>
            </button>
          </div>
        );
        }
        else if (step === "3"){
          return (
            <div className="col-lg-12" style={{ textAlign: "center" }}>
              <button
                type="button"
                onClick={backAction}
                className="btn btn-light"
                style={{
                  boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                }}
              >
                <i className="fa fa-arrow-left"></i>
                Kembali
              </button>
              <button
                type="submit"
                className="btn btn-success ml-2"
                onClick={nextStep}
                style={{
                  boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                }}
              >
                Selanjutnya {`  `}
                <i className="fa fa-arrow-right"></i>
              </button>
            </div>
          );
        }
      else if (step === "4"){
        return (
          <div className="col-lg-12" style={{ textAlign: "center" }}>
            <button
              type="button"
              onClick={backAction}
              className="btn btn-light"
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
            >
              <i className="fa fa-arrow-left"></i>
              Kembali
            </button>
            <button
              type="submit"
              className="btn btn-success ml-2"
              onClick={nextStep}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
            >
              Selanjutnya {`  `}
              <i className="fa fa-arrow-right"></i>
            </button>
          </div>
        );
      }
    }
  }

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="mt-5">{checkStep(step)}</div>
        </>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        {tombolDetail()}
      </CardFooter>
    </Card>
  );
}

export default MonitoringReviewOtherKnowledgeTacit;
