import React, { useEffect, useState } from "react";
import { getTipeKnowledgeById } from "../../../references/Api";
import TambahKnowledgeOtherKnowledge from "./other-knowledge/TambahKnowledgeOtherKnowledge";
import TambahKnowledgeSop from "./sop/TambahKnowledgeSop";
import TambahKnowledgeSuccessStory from "./success-story/TambahKnowledgeSuccessStory";

function TambahKnowledgeRoutes({
  history,
  match: {
    params: { id, step, id_km_pro, id_tipe_km }
  }
}) {
  const [template, setTemplate] = useState("");
  const [tipeKm, setTipeKm] = useState("");
  let thePath = document.URL;
  const lastPath = thePath.substring(thePath.lastIndexOf("/") + 1);

  useEffect(() => {
    getTipeKnowledgeById(id_tipe_km).then(({ data }) => {
      setTemplate(data.template);
      setTipeKm(data.nama);
    });
  }, [id_tipe_km]);

  const checkType = template => {
    switch (template) {
      case "Success Story":
        return (
          <TambahKnowledgeSuccessStory
            id={id}
            // media={media}
            id_km_pro={id_km_pro}
            id_tipe_km={id_tipe_km}
            step={step}
            tipe_knowledge={tipeKm}
            path={lastPath}
          />
        );
      case "SOP":
        return (
          <TambahKnowledgeSop
            id={id}
            // media={media}
            id_km_pro={id_km_pro}
            id_tipe_km={id_tipe_km}
            step={step}
            tipe_knowledge={tipeKm}
            path={lastPath}
          />
        );
      case "Other Knowledge":
        return (
          <TambahKnowledgeOtherKnowledge
            id={id}
            // media={media}
            id_km_pro={id_km_pro}
            id_tipe_km={id_tipe_km}
            step={step}
            tipe_knowledge={tipeKm}
            path={lastPath}
          />
        );
      default:
        break;
    }
  };
  return <div>{checkType(template)}</div>;
}

export default TambahKnowledgeRoutes;
