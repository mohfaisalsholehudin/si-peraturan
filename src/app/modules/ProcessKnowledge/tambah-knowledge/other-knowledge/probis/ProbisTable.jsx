import React, { useState, useEffect } from "react";
import swal from "sweetalert";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { Route, useHistory } from "react-router-dom";

//Helpers
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../../../_metronic/_helpers";
import { Pagination } from "../../../../../helpers/pagination/Pagination";
import * as columnFormatters from "../../../../../helpers/column-formatters";
import {
  deleteMappingProbis,
  getMappingProbisByIdKnowledge,
} from "../../../../../references/Api";
import ProbisModal from "./ProbisModal";

function ProbisTable({ id_km_pro, id_tipe_km, step, path }) {
  const history = useHistory();
  const [content, setContent] = useState([]);
  const [idMap, setIdMap] = useState(false)

  useEffect(() => {
    getMappingProbisByIdKnowledge(id_km_pro).then(({ data }) => {
     setContent(data);
     data.length > 0 ? setIdMap(true) : setIdMap(false);
    });
  }, [id_km_pro]);

  const add = () => {
    history.push(
      `/process-knowledge/add-knowledge/${id_tipe_km}/${id_km_pro}/${step}/add/probis/open`
    );
  };

  const deleteAction = (id_map_knowledge) => {
    swal({
      title: "Apakah Anda Yakin?",
      text: "Klik OK untuk melanjutkan",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        deleteMappingProbis(id_map_knowledge).then(({ status }) => {
          if (status === 200) {
            swal("Berhasil", "Data berhasil dihapus", "success").then(() => {
              history.push("/dashboard");
              history.replace(
                `/process-knowledge/add-knowledge/${id_tipe_km}/${id_km_pro}/${step}/add`
              );
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push("/dashboard");
              history.replace(
                `/process-knowledge/add-knowledge/${id_tipe_km}/${id_km_pro}/${step}/add`
              );
            });
          }
        });
      }
      // else {
      //   swal("Your imaginary file is safe!");
      // }
    });
  };

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
      // classes: "text-center pr-0",
      // headerClasses: "text-center pr-3",
      style: {
        minWidth: "2px",
      },
    },
    {
      dataField: "nama_probis",
      text: "Nama Probis",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
      style: {
        minWidth: "250px",
      },
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
        columnFormatters.ActionsColumnFormatterProcessKnowledgeSuccessStoryMappingProbis,
      formatExtraData: {
        openDeleteDialog: deleteAction,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px",
      },
      hidden: path === "view" ? true : false,
    },
  ];

  const emptyDataMessage = () => {
    return <div className="text-center">No Data to Display</div>;
  };
  const { SearchBar } = Search;
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "id_map_knowledge",
    pageNumber: 1,
    pageSize: 50,
  };
  const defaultSorted = [{ dataField: "id_map_knowledge", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "75", value: 75 },
    { text: "100", value: 100 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
  };

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_map_knowledge"
                  data={content}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-12 col-xl-12 mb-3 mt-3">
                          <h4> Proses Bisnis </h4>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        {path === "view" ? null : (
                          <div
                            className="col-lg-6 col-xl-6 mb-3"
                            style={{ textAlign: "right" }}
                          >
                            <button
                              type="button"
                              className="btn btn-primary ml-3"
                              style={{
                                float: "right",
                              }}
                              onClick={add}
                            >
                              <i className="fa fa-plus"></i>
                              Tambah
                            </button>
                          </div>
                        )}
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
      {/*Route to Open Details*/}
      <Route path="/process-knowledge/add-knowledge/:id_tipe_km/:id_km_pro/:step/add/probis/open">
        {({ history, match }) => (
          <ProbisModal
            show={match != null}
            idMap={idMap}
            id={match && match.params.id}
            id_km_pro={id_km_pro}
            id_tipe_km={id_tipe_km}
            step={step}
            after={false}
            onHide={() => {
              history.push(
                `/process-knowledge/add-knowledge/${id_tipe_km}/${id_km_pro}/${step}/add`
              );
            }}
            onRef={() => {
              history.push(
                `/process-knowledge/add-knowledge/${id_tipe_km}/${id_km_pro}/${step}/add`
              );
            }}
          />
        )}
      </Route>
    </>
  );
}

export default ProbisTable;
