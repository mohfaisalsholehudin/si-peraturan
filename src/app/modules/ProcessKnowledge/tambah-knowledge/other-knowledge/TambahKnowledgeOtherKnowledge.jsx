/* Library */
import React, { useEffect, useState, useRef } from "react";
import { Modal, Spinner } from "react-bootstrap";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";

/* Helper */
import { useSubheader } from "../../../../../_metronic/layout";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../../_metronic/_partials/controls";
import TambahKnowledgeOtherKnowledgeForm from "./TambahKnowledgeOtherKnowledgeForm";
import TambahKnowledgeOtherKnowledgeFormTwo from "./TambahKnowledgeOtherKnowledgeFormTwo";
import PeraturanTerkaitTable from "./peraturan-terkait/PeraturanTerkaitTable";
import UploadFileTable from "./upload-file/UploadFileTable";
import {
  getKnowledgeProcessById,
  getOtherKnowledgeByIdKmPro,
  saveOtherKnowledgeMateri,
  updateKnowledgeProcess,
  updateKnowledgeProcessMedia,
  updateOtherKnowledgeMateri,
  uploadKmi,
} from "../../../../references/Api";
import UnitOrganisasiTable from "./unit-organisasi/UnitOrganisasiTable";
import ProbisTable from "./probis/ProbisTable";
import CasenameTable from "./casename/CasenameTable";
import SubcaseTable from "./subcase/SubcaseTable";
import BisnisSektorTable from "./bisnis-sektor/BisnisSektorTable";

function TambahKnowledgeOtherKnowledge({
  id,
  step,
  id_km_pro,
  id_tipe_km,
  tipe_knowledge,
  path,
  // history,
  // match: {
  //   params: { id, media, step }
  // }
}) {
  // Subheader
  const suhbeader = useSubheader();
  const history = useHistory();
  const { user } = useSelector((state) => state.auth);

  const [title, setTitle] = useState("");
  const [loading, setLoading] = useState(false);
  const [isDisabled, setIsDisabled] = useState(false);
  const [content, setContent] = useState();
  const [draft, setDraft] = useState("");
  const [idTemp, setIdTemp] = useState();
  const [materi, setMateri] = useState();
  const [media, setMedia] = useState("");

  const initValues = {
    tipe_knowledge: tipe_knowledge,
    tipe_konten: media,
    id_probis: "",
    id_sektor: "",
    id_csname: "",
    id_subcase: "",
    id_tipe_km: id_tipe_km,
    id_km_pro: id_km_pro,
    judul: "",
    file: "",
    link_djpforum:""
  };
  useEffect(() => {
    if (path === "view") {
      let _title = "Detail Knowledge";
      setTitle(_title);
      suhbeader.setTitle(_title);
    } else {
      // let _title = "Review Knowledge";
      let _title = id_km_pro ? "Edit KM Tacit" : "Tambah KM Tacit";
      setTitle(_title);
      suhbeader.setTitle(_title);
    }
    // let _title = id ? "Edit KM Tacit" : "Tambah KM Tacit";

    // setTitle(_title);
    // suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps

    if (id_km_pro) {
      getKnowledgeProcessById(id_km_pro).then(({ data }) => {
        setMedia(data.tipe_konten);
        if (data.judul) {
          getOtherKnowledgeByIdKmPro(id_km_pro).then((data_2) => {
            // console.log(data_2.data[0].successStoryTemp);
            setContent({
              tipe_knowledge: data.tipeKnowledge.nama,
              tipe_konten: data.tipe_konten,
              id_probis: data.id_probis,
              id_sektor: data.id_sektor,
              id_csname: data.id_csname,
              id_subcase: data.id_subcase,
              id_tipe_km: data.id_tipe_km,
              id_km_pro: data.id_km_pro,
              judul: data.judul,
              file_upload: data.media_upload,
              catatan_tolak: data.catatan_tolak,
              link_djpforum: data.link_djpforum
            });
            setMateri(
              data_2.data[0] ? data_2.data[0].defaultTemp.materi : null
            );
          });
          checkIdTemp();
        } else {
          checkIdTemp();
        }
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id_km_pro, id_tipe_km, media, suhbeader]);

  const checkIdTemp = () => {
    getOtherKnowledgeByIdKmPro(id_km_pro).then(({ data }) => {
      if (data.length > 0) {
        setIdTemp(data[0].defaultTemp.id_default_temp);
      }
    });
  };
  const btnRef = useRef();

  const saveButton = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
    }
  };

  const backAction = () => {
    switch (step) {
      case "2":
        history.push(
          `/process-knowledge/add-knowledge/${id_tipe_km}/${id_km_pro}/1/${path}`
        );
        break;
      case "3":
        history.push(
          `/process-knowledge/add-knowledge/${id_tipe_km}/${id_km_pro}/2/${path}`
        );
        break;

      default:
        break;
    }
  };
  const saveForm = (values) => {
    switch (step) {
      case "1":
        saveFormOne(values);
        break;
      case "2":
        saveFormTwo();
        break;
      case "3":
        history.push(`/process-knowledge/add-knowledge`);
        break;

      default:
        break;
    }
  };
  const saveFormOne = (values) => {
    if (!id) {
      if (values.file) {
        if (values.file.name) {
          enableLoading();
          const formData = new FormData();
          formData.append("file", values.file);
          uploadKmi(formData).then(({ data }) => {
            disableLoading();
            updateKnowledgeProcessMedia(
              values.id_km_pro,
              values.id_tipe_km,
              values.tipe_konten,
              values.id_sektor,
              values.id_probis,
              values.id_csname,
              values.id_subcase,
              values.judul,
              data.message,
              user.nip18,
              values.link_djpforum
            ).then(({ status }) => {
              if (status === 201 || status === 200) {
                switch (step) {
                  case "1":
                    history.push(
                      `/process-knowledge/add-knowledge/${id_tipe_km}/${id_km_pro}/2/${path}`
                    );
                    break;
                  case "2":
                    history.push(
                      `/process-knowledge/add-knowledge/${id_tipe_km}/${id_km_pro}/3/${path}`
                    );
                    break;
                  case "3":
                    history.push(`/process-knowledge/add-knowledge`);
                    break;

                  default:
                    break;
                }
              } else {
                swal("Gagal", "Data gagal disimpan", "error").then(() => {
                  history.push(`/process-knowledge/add-knowledge/${path}`);
                });
              }
            });
          });
        } else {
          updateKnowledgeProcessMedia(
            values.id_km_pro,
            values.id_tipe_km,
            values.tipe_konten,
            values.id_sektor,
            values.id_probis,
            values.id_csname,
            values.id_subcase,
            values.judul,
            values.file_upload,
            user.nip18,
            values.link_djpforum
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              switch (step) {
                case "1":
                  history.push(
                    `/process-knowledge/add-knowledge/${id_tipe_km}/${id_km_pro}/2/${path}`
                  );
                  break;
                case "2":
                  history.push(
                    `/process-knowledge/add-knowledge/${id_tipe_km}/${id_km_pro}/3/${path}`
                  );
                  break;
                case "3":
                  history.push(`/process-knowledge/add-knowledge`);
                  break;

                default:
                  break;
              }
            }
          });
        }
      } else {
        updateKnowledgeProcess(
          values.id_km_pro,
          values.id_tipe_km,
          values.tipe_konten,
          values.id_sektor,
          values.id_probis,
          values.id_csname,
          values.id_subcase,
          values.judul,
          user.nip18,
          values.link_djpforum
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            switch (step) {
              case "1":
                history.push(
                  `/process-knowledge/add-knowledge/${id_tipe_km}/${id_km_pro}/2/${path}`
                );
                break;
              case "2":
                history.push(
                  `/process-knowledge/add-knowledge/${id_tipe_km}/${id_km_pro}/3/${path}`
                );
                break;
              case "3":
                history.push(`/process-knowledge/add-knowledge`);
                break;

              default:
                break;
            }
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push(`/process-knowledge/add-knowledge/${path}`);
            });
          }
        });
      }
    }
  };

  const saveFormTwo = () => {
    if (idTemp) {
      updateOtherKnowledgeMateri(idTemp, draft ? draft : materi).then(
        ({ status }) => {
          if (status === 201 || status === 200) {
            switch (step) {
              case "1":
                history.push(
                  `/process-knowledge/add-knowledge/${id_tipe_km}/${id_km_pro}/2/${path}`
                );
                break;
              case "2":
                history.push(
                  `/process-knowledge/add-knowledge/${id_tipe_km}/${id_km_pro}/3/${path}`
                );
                break;
              case "3":
                history.push(`/process-knowledge/add-knowledge`);
                break;
              default:
                break;
            }
          }
        }
      );
    } else {
      saveOtherKnowledgeMateri(id_km_pro, id_tipe_km, draft).then(
        ({ status }) => {
          if (status === 201 || status === 200) {
            switch (step) {
              case "1":
                history.push(
                  `/process-knowledge/add-knowledge/${id_tipe_km}/${id_km_pro}/2/${path}`
                );
                break;
              case "2":
                history.push(
                  `/process-knowledge/add-knowledge/${id_tipe_km}/${id_km_pro}/3/${path}`
                );
                break;
              case "3":
                history.push(`/process-knowledge/add-knowledge`);
                break;
              default:
                break;
            }
          }
        }
      );
    }
  };
  const checkStep = (val) => {
    switch (val) {
      case "1":
        return (
          <TambahKnowledgeOtherKnowledgeForm
            content={content || initValues}
            btnRef={btnRef}
            saveForm={saveForm}
            media={media}
            path={path}
          />
        );
      case "2":
        return (
          <TambahKnowledgeOtherKnowledgeFormTwo
            content={initValues}
            btnRef={btnRef}
            saveForm={saveForm}
            draft={materi}
            setDraft={setDraft}
            path={path}
          />
        );
      case "3":
        return (
          <>
            {/* <PranalaLuarTable media={media} step={step} />
            <ReferensiTable media={media} step={step} />
            <LihatPulaTable media={media} step={step} />
            <BacaanLanjutanTable media={media} step={step} />*/}
            <BisnisSektorTable
              step={step}
              id_km_pro={id_km_pro}
              id_tipe_km={id_tipe_km}
              path={path}
            />
            <ProbisTable
              step={step}
              id_km_pro={id_km_pro}
              id_tipe_km={id_tipe_km}
              path={path}
            />
            <CasenameTable
              step={step}
              id_km_pro={id_km_pro}
              id_tipe_km={id_tipe_km}
              path={path}
            />
            <SubcaseTable
              step={step}
              id_km_pro={id_km_pro}
              id_tipe_km={id_tipe_km}
              path={path}
            />
            <PeraturanTerkaitTable
              step={step}
              id_km_pro={id_km_pro}
              id_tipe_km={id_tipe_km}
              path={path}
            />
            <UploadFileTable
              step={step}
              id_km_pro={id_km_pro}
              id_tipe_km={id_tipe_km}
              path={path}
            />
            <UnitOrganisasiTable
              step={step}
              id_km_pro={id_km_pro}
              id_tipe_km={id_tipe_km}
              path={path}
            />
          </>
        );

      default:
        break;
    }
  };

  const enableLoading = () => {
    setLoading(true);
    setIsDisabled(true);
  };

  const disableLoading = () => {
    setLoading(false);
    setIsDisabled(false);
  };
  const homeDialog = (id) => {
    swal({
      title: "Kembali",
      text: "Apakah Anda Ingin Kembali Ke Halaman Tambah Knowledge ?",
      icon: "warning",
      buttons: true,
    }).then((ret) => {
      if (ret === true) {
        history.push(`/process-knowledge/add-knowledge/`);
      }
    });
  };

  return (
    <>
      <Card>
        <CardHeader
          title={title}
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <>
            <div className="mt-5">{checkStep(step)}</div>
          </>
        </CardBody>
        <CardFooter style={{ borderTop: "none" }}>
          <div className="col-lg-12" style={{ textAlign: "right" }}>
            {step !== "1" ? (
              <button
                type="button"
                onClick={backAction}
                className="btn btn-light"
                style={{
                  boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                }}
              >
                <i className="fa fa-arrow-left"></i>
                Kembali
              </button>
            ) : null}
            {`  `}

            {step === "3" ? (
              path === "view" ? (
                <button
                  type="button"
                  className="btn btn-primary ml-2"
                  onClick={homeDialog}
                  style={{
                    boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                  }}
                  // disabled={disabled}
                >
                  <i className="fas fa-home"></i>
                  Kembali ke Halaman Utama
                </button>
              ) : (
                <button
                  type="submit"
                  className="btn btn-success ml-2"
                  onClick={saveForm}
                  style={{
                    boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                  }}
                >
                  <i className="fas fa-save"></i>
                  Simpan
                </button>
              )
            ) : loading ? (
              <button
                type="submit"
                className="btn btn-success spinner spinner-white spinner-left ml-2"
                onClick={saveButton}
                style={{
                  boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                }}
                disabled={isDisabled}
              >
                <span>Selanjutnya</span>
              </button>
            ) : (
              <button
                type="submit"
                onClick={saveButton}
                className="btn btn-success ml-2"
                disabled={isDisabled}
                style={{
                  boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                }}
              >
                <i className="fa fa-arrow-right"></i>
                Selanjutnya
              </button>
            )}
          </div>
        </CardFooter>
      </Card>
      <Modal
        show={loading}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Body style={{ textAlign: "center" }}>
          <h1>
            <Spinner
              animation="border"
              variant="primary"
              role="status"
              style={{ width: "50px", height: "50px" }}
            >
              <span className="sr-only">Loading...</span>
            </Spinner>
          </h1>
          <h1>Sedang Upload File. . .</h1>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default TambahKnowledgeOtherKnowledge;
