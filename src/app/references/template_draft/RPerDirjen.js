export const RPerDirjen =  
{
    "counters": {
        "u_column": 45,
        "u_row": 26,
        "u_content_text": 38
    },
    "body": {
        "rows": [
            {
                "cells": [
                    1
                ],
                "columns": [
                    {
                        "contents": [
                            {
                                "type": "text",
                                "values": {
                                    "containerPadding": "10px",
                                    "textAlign": "left",
                                    "lineHeight": "140%",
                                    "linkStyle": {
                                        "inherit": true,
                                        "linkColor": "#0000ee",
                                        "linkHoverColor": "#0000ee",
                                        "linkUnderline": true,
                                        "linkHoverUnderline": true
                                    },
                                    "hideDesktop": false,
                                    "_meta": {
                                        "htmlID": "u_content_text_1",
                                        "htmlClassNames": "u_content_text"
                                    },
                                    "selectable": true,
                                    "draggable": true,
                                    "duplicatable": true,
                                    "deletable": true,
                                    "hideable": true,
                                    "text": "<p style=\"font-size: 14px; line-height: 140%; text-align: center;\"><span style=\"font-size: 14px; line-height: 19.6px; font-family: 'book antiqua', palatino;\">KEMENTERIAN KEUANGAN REPUBLIK INDONESIA</span><br /><span style=\"font-size: 14px; line-height: 19.6px; font-family: 'book antiqua', palatino;\">DIREKTORAT JENDERAL PAJAK</span></p>"
                                }
                            }
                        ],
                        "values": {
                            "backgroundColor": "",
                            "padding": "0px",
                            "border": {},
                            "borderRadius": "0px",
                            "_meta": {
                                "htmlID": "u_column_2",
                                "htmlClassNames": "u_column"
                            }
                        }
                    }
                ],
                "values": {
                    "displayCondition": null,
                    "columns": false,
                    "backgroundColor": "",
                    "columnsBackgroundColor": "",
                    "backgroundImage": {
                        "url": "",
                        "fullWidth": true,
                        "repeat": false,
                        "center": true,
                        "cover": false
                    },
                    "padding": "0px",
                    "hideDesktop": false,
                    "_meta": {
                        "htmlID": "u_row_2",
                        "htmlClassNames": "u_row"
                    },
                    "selectable": true,
                    "draggable": true,
                    "duplicatable": true,
                    "deletable": true,
                    "hideable": true
                }
            },
            {
                "cells": [
                    1
                ],
                "columns": [
                    {
                        "contents": [
                            {
                                "type": "text",
                                "values": {
                                    "containerPadding": "10px",
                                    "textAlign": "left",
                                    "lineHeight": "140%",
                                    "linkStyle": {
                                        "inherit": true,
                                        "linkColor": "#0000ee",
                                        "linkHoverColor": "#0000ee",
                                        "linkUnderline": true,
                                        "linkHoverUnderline": true
                                    },
                                    "hideDesktop": false,
                                    "_meta": {
                                        "htmlID": "u_content_text_3",
                                        "htmlClassNames": "u_content_text"
                                    },
                                    "selectable": true,
                                    "draggable": true,
                                    "duplicatable": true,
                                    "deletable": true,
                                    "hideable": true,
                                    "text": "<p style=\"font-size: 14px; line-height: 140%; text-align: center;\"><span style=\"font-size: 14px; line-height: 19.6px; font-family: 'book antiqua', palatino;\">PERATURAN DIREKTUR JENDERAL PAJAK&nbsp;</span></p>\n<p style=\"font-size: 14px; line-height: 140%; text-align: center;\"><span style=\"font-size: 14px; line-height: 19.6px; font-family: 'book antiqua', palatino;\">NOMOR &hellip;&hellip;&hellip;&hellip;.</span><br /><span style=\"font-size: 14px; line-height: 19.6px; font-family: 'book antiqua', palatino;\">TENTANG</span></p>\n<p style=\"font-size: 14px; line-height: 140%; text-align: center;\"><span style=\"font-size: 14px; line-height: 19.6px; font-family: 'book antiqua', palatino;\">&hellip;&hellip;&hellip;&hellip;&hellip;</span></p>"
                                }
                            }
                        ],
                        "values": {
                            "backgroundColor": "",
                            "padding": "0px",
                            "border": {},
                            "_meta": {
                                "htmlID": "u_column_1",
                                "htmlClassNames": "u_column"
                            }
                        }
                    }
                ],
                "values": {
                    "displayCondition": null,
                    "columns": false,
                    "backgroundColor": "",
                    "columnsBackgroundColor": "",
                    "backgroundImage": {
                        "url": "",
                        "fullWidth": true,
                        "repeat": false,
                        "center": true,
                        "cover": false
                    },
                    "padding": "0px",
                    "hideDesktop": false,
                    "_meta": {
                        "htmlID": "u_row_1",
                        "htmlClassNames": "u_row"
                    },
                    "selectable": true,
                    "draggable": true,
                    "duplicatable": true,
                    "deletable": true,
                    "hideable": true
                }
            },
            {
                "cells": [
                    1
                ],
                "columns": [
                    {
                        "contents": [
                            {
                                "type": "text",
                                "values": {
                                    "containerPadding": "10px 10px 20px",
                                    "textAlign": "left",
                                    "lineHeight": "140%",
                                    "linkStyle": {
                                        "inherit": true,
                                        "linkColor": "#0000ee",
                                        "linkHoverColor": "#0000ee",
                                        "linkUnderline": true,
                                        "linkHoverUnderline": true
                                    },
                                    "hideDesktop": false,
                                    "_meta": {
                                        "htmlID": "u_content_text_4",
                                        "htmlClassNames": "u_content_text"
                                    },
                                    "selectable": true,
                                    "draggable": true,
                                    "duplicatable": true,
                                    "deletable": true,
                                    "hideable": true,
                                    "text": "<p style=\"font-size: 14px; line-height: 140%; text-align: center;\"><span style=\"font-size: 14px; line-height: 19.6px; font-family: 'book antiqua', palatino;\">DIREKTUR JENDERAL PAJAK ,&nbsp;</span></p>"
                                }
                            }
                        ],
                        "values": {
                            "backgroundColor": "",
                            "padding": "0px",
                            "border": {},
                            "borderRadius": "0px",
                            "_meta": {
                                "htmlID": "u_column_4",
                                "htmlClassNames": "u_column"
                            }
                        }
                    }
                ],
                "values": {
                    "displayCondition": null,
                    "columns": false,
                    "backgroundColor": "",
                    "columnsBackgroundColor": "",
                    "backgroundImage": {
                        "url": "",
                        "fullWidth": true,
                        "repeat": false,
                        "center": true,
                        "cover": false
                    },
                    "padding": "0px",
                    "hideDesktop": false,
                    "_meta": {
                        "htmlID": "u_row_4",
                        "htmlClassNames": "u_row"
                    },
                    "selectable": true,
                    "draggable": true,
                    "duplicatable": true,
                    "deletable": true,
                    "hideable": true
                }
            },
            {
                "cells": [
                    18.47,
                    81.53
                ],
                "columns": [
                    {
                        "contents": [
                            {
                                "type": "text",
                                "values": {
                                    "containerPadding": "13px 0px 10px 10px",
                                    "textAlign": "left",
                                    "lineHeight": "140%",
                                    "linkStyle": {
                                        "inherit": true,
                                        "linkColor": "#0000ee",
                                        "linkHoverColor": "#0000ee",
                                        "linkUnderline": true,
                                        "linkHoverUnderline": true
                                    },
                                    "hideDesktop": false,
                                    "_meta": {
                                        "htmlID": "u_content_text_12",
                                        "htmlClassNames": "u_content_text"
                                    },
                                    "selectable": true,
                                    "draggable": true,
                                    "duplicatable": true,
                                    "deletable": true,
                                    "hideable": true,
                                    "text": "<p style=\"font-size: 14px; line-height: 140%;\"><span style=\"font-size: 14px; line-height: 19.6px; font-family: 'book antiqua', palatino;\">Menimbang&nbsp; &nbsp;:</span></p>"
                                }
                            }
                        ],
                        "values": {
                            "backgroundColor": "",
                            "padding": "0px",
                            "border": {},
                            "borderRadius": "0px",
                            "_meta": {
                                "htmlID": "u_column_13",
                                "htmlClassNames": "u_column"
                            }
                        }
                    },
                    {
                        "contents": [
                            {
                                "type": "text",
                                "values": {
                                    "containerPadding": "0px 10px 10px 0px",
                                    "textAlign": "left",
                                    "lineHeight": "140%",
                                    "linkStyle": {
                                        "inherit": true,
                                        "linkColor": "#0000ee",
                                        "linkHoverColor": "#0000ee",
                                        "linkUnderline": true,
                                        "linkHoverUnderline": true
                                    },
                                    "hideDesktop": false,
                                    "_meta": {
                                        "htmlID": "u_content_text_14",
                                        "htmlClassNames": "u_content_text"
                                    },
                                    "selectable": true,
                                    "draggable": true,
                                    "duplicatable": true,
                                    "deletable": true,
                                    "hideable": true,
                                    "text": "<ol style=\"list-style-type: lower-alpha;\">\n<li style=\"font-size: 14px; line-height: 19.6px;\"><span style=\"font-family: 'book antiqua', palatino; font-size: 14px; line-height: 19.6px;\">lorem ipsum</span></li>\n<li style=\"font-size: 14px; line-height: 19.6px;\"><span style=\"font-family: 'book antiqua', palatino; font-size: 14px; line-height: 19.6px;\">lorem ipsum dolor</span></li>\n</ol>"
                                }
                            }
                        ],
                        "values": {
                            "backgroundColor": "",
                            "padding": "0px",
                            "border": {},
                            "borderRadius": "0px",
                            "_meta": {
                                "htmlID": "u_column_14",
                                "htmlClassNames": "u_column"
                            }
                        }
                    }
                ],
                "values": {
                    "displayCondition": null,
                    "columns": false,
                    "backgroundColor": "",
                    "columnsBackgroundColor": "",
                    "backgroundImage": {
                        "url": "",
                        "fullWidth": true,
                        "repeat": false,
                        "center": true,
                        "cover": false
                    },
                    "padding": "0px",
                    "hideDesktop": false,
                    "_meta": {
                        "htmlID": "u_row_8",
                        "htmlClassNames": "u_row"
                    },
                    "selectable": true,
                    "draggable": true,
                    "duplicatable": true,
                    "deletable": true,
                    "hideable": true
                }
            },
            {
                "cells": [
                    18.51,
                    81.49
                ],
                "columns": [
                    {
                        "contents": [
                            {
                                "type": "text",
                                "values": {
                                    "containerPadding": "10px 0px 10px 10px",
                                    "textAlign": "left",
                                    "lineHeight": "140%",
                                    "linkStyle": {
                                        "inherit": true,
                                        "linkColor": "#0000ee",
                                        "linkHoverColor": "#0000ee",
                                        "linkUnderline": true,
                                        "linkHoverUnderline": true
                                    },
                                    "hideDesktop": false,
                                    "_meta": {
                                        "htmlID": "u_content_text_13",
                                        "htmlClassNames": "u_content_text"
                                    },
                                    "selectable": true,
                                    "draggable": true,
                                    "duplicatable": true,
                                    "deletable": true,
                                    "hideable": true,
                                    "text": "<p style=\"font-size: 14px; line-height: 140%;\"><span style=\"font-size: 14px; line-height: 19.6px; font-family: 'book antiqua', palatino;\">Mengingat&nbsp; &nbsp; &nbsp;:</span></p>"
                                }
                            }
                        ],
                        "values": {
                            "backgroundColor": "",
                            "padding": "0px",
                            "border": {},
                            "borderRadius": "0px",
                            "_meta": {
                                "htmlID": "u_column_15",
                                "htmlClassNames": "u_column"
                            }
                        }
                    },
                    {
                        "contents": [
                            {
                                "type": "text",
                                "values": {
                                    "containerPadding": "0px 10px 8px 0px",
                                    "textAlign": "left",
                                    "lineHeight": "140%",
                                    "linkStyle": {
                                        "inherit": true,
                                        "linkColor": "#0000ee",
                                        "linkHoverColor": "#0000ee",
                                        "linkUnderline": true,
                                        "linkHoverUnderline": true,
                                        "body": false
                                    },
                                    "hideDesktop": false,
                                    "_meta": {
                                        "htmlID": "u_content_text_15",
                                        "htmlClassNames": "u_content_text"
                                    },
                                    "selectable": true,
                                    "draggable": true,
                                    "duplicatable": true,
                                    "deletable": true,
                                    "hideable": true,
                                    "text": "<ol>\n<li style=\"font-size: 14px; line-height: 19.6px;\"><span style=\"font-family: 'book antiqua', palatino; font-size: 14px; line-height: 19.6px;\">lorem ipsum dolor</span></li>\n<li style=\"font-size: 14px; line-height: 19.6px;\"><span style=\"font-family: 'book antiqua', palatino; font-size: 14px; line-height: 19.6px;\">lorem ipsum dolor</span></li>\n<li style=\"font-size: 14px; line-height: 19.6px;\"><span style=\"font-family: 'book antiqua', palatino; font-size: 14px; line-height: 19.6px;\">ldkal lkaldka lkalda</span></li>\n</ol>"
                                }
                            }
                        ],
                        "values": {
                            "backgroundColor": "",
                            "padding": "0px",
                            "border": {},
                            "borderRadius": "0px",
                            "_meta": {
                                "htmlID": "u_column_16",
                                "htmlClassNames": "u_column"
                            }
                        }
                    }
                ],
                "values": {
                    "displayCondition": null,
                    "columns": false,
                    "backgroundColor": "",
                    "columnsBackgroundColor": "",
                    "backgroundImage": {
                        "url": "",
                        "fullWidth": true,
                        "repeat": false,
                        "center": true,
                        "cover": false
                    },
                    "padding": "0px",
                    "hideDesktop": false,
                    "_meta": {
                        "htmlID": "u_row_9",
                        "htmlClassNames": "u_row"
                    },
                    "selectable": true,
                    "draggable": true,
                    "duplicatable": true,
                    "deletable": true,
                    "hideable": true
                }
            },
            {
                "cells": [
                    1
                ],
                "columns": [
                    {
                        "contents": [
                            {
                                "type": "text",
                                "values": {
                                    "containerPadding": "10px",
                                    "textAlign": "left",
                                    "lineHeight": "140%",
                                    "linkStyle": {
                                        "inherit": true,
                                        "linkColor": "#0000ee",
                                        "linkHoverColor": "#0000ee",
                                        "linkUnderline": true,
                                        "linkHoverUnderline": true
                                    },
                                    "hideDesktop": false,
                                    "_meta": {
                                        "htmlID": "u_content_text_16",
                                        "htmlClassNames": "u_content_text"
                                    },
                                    "selectable": true,
                                    "draggable": true,
                                    "duplicatable": true,
                                    "deletable": true,
                                    "hideable": true,
                                    "text": "<p style=\"font-size: 14px; line-height: 140%; text-align: center;\"><span style=\"font-size: 14px; line-height: 19.6px; font-family: 'book antiqua', palatino;\"><span style=\"line-height: 19.6px; font-size: 14px;\">MEMUTUSKAN</span>:</span></p>"
                                }
                            }
                        ],
                        "values": {
                            "backgroundColor": "",
                            "padding": "0px",
                            "border": {},
                            "borderRadius": "0px",
                            "_meta": {
                                "htmlID": "u_column_17",
                                "htmlClassNames": "u_column"
                            }
                        }
                    }
                ],
                "values": {
                    "displayCondition": null,
                    "columns": false,
                    "backgroundColor": "",
                    "columnsBackgroundColor": "",
                    "backgroundImage": {
                        "url": "",
                        "fullWidth": true,
                        "repeat": false,
                        "center": true,
                        "cover": false
                    },
                    "padding": "0px",
                    "hideDesktop": false,
                    "_meta": {
                        "htmlID": "u_row_10",
                        "htmlClassNames": "u_row"
                    },
                    "selectable": true,
                    "draggable": true,
                    "duplicatable": true,
                    "deletable": true,
                    "hideable": true
                }
            },
            {
                "cells": [
                    21.93,
                    78.07
                ],
                "columns": [
                    {
                        "contents": [
                            {
                                "type": "text",
                                "values": {
                                    "containerPadding": "10px",
                                    "textAlign": "left",
                                    "lineHeight": "140%",
                                    "linkStyle": {
                                        "inherit": true,
                                        "linkColor": "#0000ee",
                                        "linkHoverColor": "#0000ee",
                                        "linkUnderline": true,
                                        "linkHoverUnderline": true
                                    },
                                    "hideDesktop": false,
                                    "_meta": {
                                        "htmlID": "u_content_text_17",
                                        "htmlClassNames": "u_content_text"
                                    },
                                    "selectable": true,
                                    "draggable": true,
                                    "duplicatable": true,
                                    "deletable": true,
                                    "hideable": true,
                                    "text": "<p style=\"font-size: 14px; line-height: 140%;\"><span style=\"font-size: 14px; line-height: 19.6px; font-family: 'book antiqua', palatino;\"><span style=\"line-height: 19.6px; font-size: 14px;\">Menetapkan&nbsp; </span>:</span></p>"
                                }
                            }
                        ],
                        "values": {
                            "backgroundColor": "",
                            "padding": "0px",
                            "border": {},
                            "borderRadius": "0px",
                            "_meta": {
                                "htmlID": "u_column_18",
                                "htmlClassNames": "u_column"
                            }
                        }
                    },
                    {
                        "contents": [
                            {
                                "type": "text",
                                "values": {
                                    "containerPadding": "10px",
                                    "textAlign": "left",
                                    "lineHeight": "140%",
                                    "linkStyle": {
                                        "inherit": true,
                                        "linkColor": "#0000ee",
                                        "linkHoverColor": "#0000ee",
                                        "linkUnderline": true,
                                        "linkHoverUnderline": true
                                    },
                                    "hideDesktop": false,
                                    "_meta": {
                                        "htmlID": "u_content_text_18",
                                        "htmlClassNames": "u_content_text"
                                    },
                                    "selectable": true,
                                    "draggable": true,
                                    "duplicatable": true,
                                    "deletable": true,
                                    "hideable": true,
                                    "text": "<p style=\"font-size: 14px; line-height: 140%;\"><span style=\"font-size: 14px; line-height: 19.6px; font-family: 'book antiqua', palatino;\">PERATURAN DIREKTUR JENDERAL PAJAK TENTANG &hellip;&hellip;&hellip;&hellip;.. .</span></p>"
                                }
                            }
                        ],
                        "values": {
                            "backgroundColor": "",
                            "padding": "0px",
                            "border": {},
                            "borderRadius": "0px",
                            "_meta": {
                                "htmlID": "u_column_19",
                                "htmlClassNames": "u_column"
                            }
                        }
                    }
                ],
                "values": {
                    "displayCondition": null,
                    "columns": false,
                    "backgroundColor": "",
                    "columnsBackgroundColor": "",
                    "backgroundImage": {
                        "url": "",
                        "fullWidth": true,
                        "repeat": false,
                        "center": true,
                        "cover": false
                    },
                    "padding": "0px",
                    "hideDesktop": false,
                    "_meta": {
                        "htmlID": "u_row_11",
                        "htmlClassNames": "u_row"
                    },
                    "selectable": true,
                    "draggable": true,
                    "duplicatable": true,
                    "deletable": true,
                    "hideable": true
                }
            },
            {
                "cells": [
                    18.26,
                    69.8,
                    11.94
                ],
                "columns": [
                    {
                        "contents": [],
                        "values": {
                            "backgroundColor": "",
                            "padding": "0px",
                            "border": {},
                            "borderRadius": "0px",
                            "_meta": {
                                "htmlID": "u_column_21",
                                "htmlClassNames": "u_column"
                            }
                        }
                    },
                    {
                        "contents": [
                            {
                                "type": "text",
                                "values": {
                                    "containerPadding": "10px 10px 3px",
                                    "textAlign": "left",
                                    "lineHeight": "140%",
                                    "linkStyle": {
                                        "inherit": true,
                                        "linkColor": "#0000ee",
                                        "linkHoverColor": "#0000ee",
                                        "linkUnderline": true,
                                        "linkHoverUnderline": true
                                    },
                                    "hideDesktop": false,
                                    "_meta": {
                                        "htmlID": "u_content_text_34",
                                        "htmlClassNames": "u_content_text"
                                    },
                                    "selectable": true,
                                    "draggable": true,
                                    "duplicatable": true,
                                    "deletable": true,
                                    "hideable": true,
                                    "text": "<p style=\"font-size: 14px; line-height: 140%; text-align: center;\"><span style=\"font-family: 'book antiqua', palatino; font-size: 14px; line-height: 19.6px;\">Pasal 1</span></p>"
                                }
                            },
                            {
                                "type": "text",
                                "values": {
                                    "containerPadding": "0px 10px 0px 22px",
                                    "textAlign": "left",
                                    "lineHeight": "140%",
                                    "linkStyle": {
                                        "inherit": true,
                                        "linkColor": "#0000ee",
                                        "linkHoverColor": "#0000ee",
                                        "linkUnderline": true,
                                        "linkHoverUnderline": true
                                    },
                                    "hideDesktop": false,
                                    "_meta": {
                                        "htmlID": "u_content_text_20",
                                        "htmlClassNames": "u_content_text"
                                    },
                                    "selectable": true,
                                    "draggable": true,
                                    "duplicatable": true,
                                    "deletable": true,
                                    "hideable": true,
                                    "text": "<p style=\"font-size: 14px; line-height: 140%;\"><span style=\"font-family: 'book antiqua', palatino; font-size: 14px; line-height: 19.6px;\">Dalam Peraturan Direktur Jenderal ini, yang dimaksud dengan:</span></p>"
                                }
                            },
                            {
                                "type": "text",
                                "values": {
                                    "containerPadding": "0px 8px 0px 0px",
                                    "textAlign": "left",
                                    "lineHeight": "140%",
                                    "linkStyle": {
                                        "inherit": true,
                                        "linkColor": "#0000ee",
                                        "linkHoverColor": "#0000ee",
                                        "linkUnderline": true,
                                        "linkHoverUnderline": true
                                    },
                                    "hideDesktop": false,
                                    "_meta": {
                                        "htmlID": "u_content_text_27",
                                        "htmlClassNames": "u_content_text"
                                    },
                                    "selectable": true,
                                    "draggable": true,
                                    "duplicatable": true,
                                    "deletable": true,
                                    "hideable": true,
                                    "text": "<ol>\n<li style=\"font-size: 14px; line-height: 19.6px;\"><span style=\"font-family: 'book antiqua', palatino; font-size: 14px; line-height: 19.6px;\">lorem ipsum</span></li>\n<li style=\"font-size: 14px; line-height: 19.6px;\"><span style=\"font-family: 'book antiqua', palatino; font-size: 14px; line-height: 19.6px;\">dfwed seff sf sef es asdad&nbsp;</span></li>\n<li style=\"font-size: 14px; line-height: 19.6px;\"><span style=\"font-family: 'book antiqua', palatino; font-size: 14px; line-height: 19.6px;\">fesfse sef s e fs se s fsef sefse&nbsp;</span></li>\n</ol>"
                                }
                            },
                            {
                                "type": "text",
                                "values": {
                                    "containerPadding": "10px 10px 3px",
                                    "textAlign": "left",
                                    "lineHeight": "140%",
                                    "linkStyle": {
                                        "inherit": true,
                                        "linkColor": "#0000ee",
                                        "linkHoverColor": "#0000ee",
                                        "linkUnderline": true,
                                        "linkHoverUnderline": true
                                    },
                                    "hideDesktop": false,
                                    "_meta": {
                                        "htmlID": "u_content_text_36",
                                        "htmlClassNames": "u_content_text"
                                    },
                                    "selectable": true,
                                    "draggable": true,
                                    "duplicatable": true,
                                    "deletable": true,
                                    "hideable": true,
                                    "text": "<p style=\"font-size: 14px; line-height: 140%; text-align: center;\"><span style=\"font-family: 'book antiqua', palatino; font-size: 14px; line-height: 19.6px;\">Pasal 2</span></p>"
                                }
                            },
                            {
                                "type": "text",
                                "values": {
                                    "containerPadding": "0px 10px 0px 22px",
                                    "textAlign": "left",
                                    "lineHeight": "140%",
                                    "linkStyle": {
                                        "inherit": true,
                                        "linkColor": "#0000ee",
                                        "linkHoverColor": "#0000ee",
                                        "linkUnderline": true,
                                        "linkHoverUnderline": true
                                    },
                                    "hideDesktop": false,
                                    "_meta": {
                                        "htmlID": "u_content_text_37",
                                        "htmlClassNames": "u_content_text"
                                    },
                                    "selectable": true,
                                    "draggable": true,
                                    "duplicatable": true,
                                    "deletable": true,
                                    "hideable": true,
                                    "text": "<p style=\"font-size: 14px; line-height: 140%;\"><span style=\"font-family: 'book antiqua', palatino; font-size: 14px; line-height: 19.6px;\">Dalam Peraturan Direktur Jenderal ini, yang dimaksud dengan:</span></p>"
                                }
                            },
                            {
                                "type": "text",
                                "values": {
                                    "containerPadding": "0px",
                                    "textAlign": "left",
                                    "lineHeight": "130%",
                                    "linkStyle": {
                                        "inherit": true,
                                        "linkColor": "#0000ee",
                                        "linkHoverColor": "#0000ee",
                                        "linkUnderline": true,
                                        "linkHoverUnderline": true,
                                        "body": false
                                    },
                                    "hideDesktop": false,
                                    "_meta": {
                                        "htmlID": "u_content_text_26",
                                        "htmlClassNames": "u_content_text"
                                    },
                                    "selectable": true,
                                    "draggable": true,
                                    "duplicatable": true,
                                    "deletable": true,
                                    "hideable": true,
                                    "text": "<ol>\n<li style=\"font-size: 14px; line-height: 18.2px;\"><span style=\"font-family: 'book antiqua', palatino; font-size: 14px; line-height: 18.2px;\">dawdaw dawdawd</span></li>\n<li dir=\"ltr\" style=\"font-size: 14px; line-height: 18.2px;\"><span style=\"font-family: 'book antiqua', palatino; font-size: 14px; line-height: 18.2px;\">dawdawcszcsz s&nbsp; awd aw wa aw&nbsp;</span></li>\n<li dir=\"ltr\" style=\"font-size: 14px; line-height: 18.2px;\"><span style=\"font-family: 'book antiqua', palatino; font-size: 14px; line-height: 18.2px;\">dawdawdaw</span></li>\n<li style=\"font-size: 14px; line-height: 18.2px;\"><span style=\"font-family: 'book antiqua', palatino; font-size: 14px; line-height: 18.2px;\">awdawdawd wad adawaw awda a d aw d&nbsp; aw a a&nbsp;</span></li>\n</ol>"
                                }
                            }
                        ],
                        "values": {
                            "backgroundColor": "",
                            "padding": "0px",
                            "border": {},
                            "borderRadius": "0px",
                            "_meta": {
                                "htmlID": "u_column_22",
                                "htmlClassNames": "u_column"
                            }
                        }
                    },
                    {
                        "contents": [],
                        "values": {
                            "backgroundColor": "",
                            "padding": "0px",
                            "border": {},
                            "borderRadius": "0px",
                            "_meta": {
                                "htmlID": "u_column_23",
                                "htmlClassNames": "u_column"
                            }
                        }
                    }
                ],
                "values": {
                    "displayCondition": null,
                    "columns": false,
                    "backgroundColor": "",
                    "columnsBackgroundColor": "",
                    "backgroundImage": {
                        "url": "",
                        "fullWidth": true,
                        "repeat": false,
                        "center": true,
                        "cover": false
                    },
                    "padding": "0px",
                    "hideDesktop": false,
                    "_meta": {
                        "htmlID": "u_row_13",
                        "htmlClassNames": "u_row"
                    },
                    "selectable": true,
                    "draggable": true,
                    "duplicatable": true,
                    "deletable": true,
                    "hideable": true
                }
            },
            {
                "cells": [
                    26.79,
                    73.21
                ],
                "columns": [
                    {
                        "contents": [
                            {
                                "type": "text",
                                "values": {
                                    "containerPadding": "10px",
                                    "textAlign": "left",
                                    "lineHeight": "140%",
                                    "linkStyle": {
                                        "inherit": true,
                                        "linkColor": "#0000ee",
                                        "linkHoverColor": "#0000ee",
                                        "linkUnderline": true,
                                        "linkHoverUnderline": true
                                    },
                                    "hideDesktop": false,
                                    "_meta": {
                                        "htmlID": "u_content_text_33",
                                        "htmlClassNames": "u_content_text"
                                    },
                                    "selectable": true,
                                    "draggable": true,
                                    "duplicatable": true,
                                    "deletable": true,
                                    "hideable": true,
                                    "text": "<p style=\"font-size: 14px; line-height: 140%;\"><span style=\"color: #ecf0f1; font-size: 14px; line-height: 19.6px;\">aaaaaa</span></p>"
                                }
                            }
                        ],
                        "values": {
                            "backgroundColor": "",
                            "padding": "0px",
                            "border": {},
                            "borderRadius": "0px",
                            "_meta": {
                                "htmlID": "u_column_33",
                                "htmlClassNames": "u_column"
                            }
                        }
                    },
                    {
                        "contents": [
                            {
                                "type": "text",
                                "values": {
                                    "containerPadding": "10px",
                                    "textAlign": "left",
                                    "lineHeight": "140%",
                                    "linkStyle": {
                                        "inherit": true,
                                        "linkColor": "#0000ee",
                                        "linkHoverColor": "#0000ee",
                                        "linkUnderline": true,
                                        "linkHoverUnderline": true
                                    },
                                    "hideDesktop": false,
                                    "_meta": {
                                        "htmlID": "u_content_text_28",
                                        "htmlClassNames": "u_content_text"
                                    },
                                    "selectable": true,
                                    "draggable": true,
                                    "duplicatable": true,
                                    "deletable": true,
                                    "hideable": true,
                                    "text": "<p style=\"font-size: 14px; line-height: 140%; text-align: center;\"><span style=\"font-size: 14px; line-height: 19.6px; font-family: 'book antiqua', palatino;\">Pasal XXX</span></p>\n<p style=\"font-size: 14px; line-height: 140%; text-align: right;\"><span style=\"font-size: 14px; line-height: 19.6px; font-family: 'book antiqua', palatino;\">Peraturan Direktur Jenderal ini mulai berlaku pada &hellip;&hellip;&hellip;&hellip;.</span></p>"
                                }
                            }
                        ],
                        "values": {
                            "backgroundColor": "",
                            "padding": "0px",
                            "border": {},
                            "borderRadius": "0px",
                            "_meta": {
                                "htmlID": "u_column_34",
                                "htmlClassNames": "u_column"
                            }
                        }
                    }
                ],
                "values": {
                    "displayCondition": null,
                    "columns": false,
                    "backgroundColor": "",
                    "columnsBackgroundColor": "",
                    "backgroundImage": {
                        "url": "",
                        "fullWidth": true,
                        "repeat": false,
                        "center": true,
                        "cover": false
                    },
                    "padding": "0px",
                    "hideDesktop": false,
                    "_meta": {
                        "htmlID": "u_row_19",
                        "htmlClassNames": "u_row"
                    },
                    "selectable": true,
                    "draggable": true,
                    "duplicatable": true,
                    "deletable": true,
                    "hideable": true
                }
            },
            {
                "cells": [
                    33.33,
                    25.33,
                    41.34
                ],
                "columns": [
                    {
                        "contents": [],
                        "values": {
                            "backgroundColor": "",
                            "padding": "0px",
                            "border": {},
                            "borderRadius": "0px",
                            "_meta": {
                                "htmlID": "u_column_36",
                                "htmlClassNames": "u_column"
                            }
                        }
                    },
                    {
                        "contents": [],
                        "values": {
                            "backgroundColor": "",
                            "padding": "0px",
                            "border": {},
                            "borderRadius": "0px",
                            "_meta": {
                                "htmlID": "u_column_37",
                                "htmlClassNames": "u_column"
                            }
                        }
                    },
                    {
                        "contents": [
                            {
                                "type": "text",
                                "values": {
                                    "containerPadding": "10px",
                                    "textAlign": "left",
                                    "lineHeight": "140%",
                                    "linkStyle": {
                                        "inherit": true,
                                        "linkColor": "#0000ee",
                                        "linkHoverColor": "#0000ee",
                                        "linkUnderline": true,
                                        "linkHoverUnderline": true
                                    },
                                    "hideDesktop": false,
                                    "_meta": {
                                        "htmlID": "u_content_text_32",
                                        "htmlClassNames": "u_content_text"
                                    },
                                    "selectable": true,
                                    "draggable": true,
                                    "duplicatable": true,
                                    "deletable": true,
                                    "hideable": true,
                                    "text": "<p style=\"font-size: 14px; line-height: 140%;\"><span style=\"font-size: 14px; line-height: 19.6px; font-family: 'book antiqua', palatino;\">Ditetapkan di Jakarta</span></p>\n<p style=\"font-size: 14px; line-height: 140%;\"><span style=\"font-size: 14px; line-height: 19.6px; font-family: 'book antiqua', palatino;\">pada tanggal &hellip;&hellip;&hellip;..</span></p>\n<p style=\"font-size: 14px; line-height: 140%;\">&nbsp;</p>\n<p style=\"font-size: 14px; line-height: 140%;\"><span style=\"font-size: 14px; line-height: 19.6px; font-family: 'book antiqua', palatino;\">DIREKTUR JENDERAL PAJAK,</span></p>\n<p style=\"font-size: 14px; line-height: 140%;\">&nbsp;</p>\n<p style=\"font-size: 14px; line-height: 140%;\">&nbsp;</p>\n<p style=\"font-size: 14px; line-height: 140%;\">&nbsp;</p>\n<p style=\"font-size: 14px; line-height: 140%;\"><span style=\"font-size: 14px; line-height: 19.6px; font-family: 'book antiqua', palatino;\">Nama Direktur</span></p>"
                                }
                            }
                        ],
                        "values": {
                            "backgroundColor": "",
                            "padding": "0px",
                            "border": {},
                            "borderRadius": "0px",
                            "_meta": {
                                "htmlID": "u_column_38",
                                "htmlClassNames": "u_column"
                            }
                        }
                    }
                ],
                "values": {
                    "displayCondition": null,
                    "columns": false,
                    "backgroundColor": "",
                    "columnsBackgroundColor": "",
                    "backgroundImage": {
                        "url": "",
                        "fullWidth": true,
                        "repeat": false,
                        "center": true,
                        "cover": false
                    },
                    "padding": "0px",
                    "hideDesktop": false,
                    "_meta": {
                        "htmlID": "u_row_21",
                        "htmlClassNames": "u_row"
                    },
                    "selectable": true,
                    "draggable": true,
                    "duplicatable": true,
                    "deletable": true,
                    "hideable": true
                }
            }
        ],
        "values": {
            "textColor": "#000000",
            "backgroundColor": "",
            "backgroundImage": {
                "url": "",
                "fullWidth": true,
                "repeat": false,
                "center": true,
                "cover": false
            },
            "contentWidth": "550px",
            "contentAlign": "center",
            "fontFamily": {
                "label": "Book Antiqua",
                "value": "book antiqua,palatino"
            },
            "preheaderText": "",
            "linkStyle": {
                "body": true,
                "linkColor": "#0000ee",
                "linkHoverColor": "#0000ee",
                "linkUnderline": true,
                "linkHoverUnderline": true
            },
            "_meta": {
                "htmlID": "u_body",
                "htmlClassNames": "u_body"
            }
        }
    },
    "schemaVersion": 6
}