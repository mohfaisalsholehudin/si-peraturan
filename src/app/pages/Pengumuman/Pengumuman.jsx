/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import PerfectScrollbar from "react-perfect-scrollbar";
import { getPengumuman } from "../../references/Api";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../_metronic/_partials/controls";
import { useSubheader } from "../../../_metronic/layout";

function Pengumuman() {
  const history = useHistory();
  const [content, setContent] = useState([]);
  const [isMore, setIsMore] = useState(false);
  let thePath = document.URL;
  const lastPath = thePath.substring(thePath.lastIndexOf("/") + 1);
  const subheader = useSubheader();

  const perfectScrollbarOptions = {
    wheelSpeed: 2,
    wheelPropagation: false,
  };
  useEffect(() => {
    getPengumuman().then(({ data }) => {
      setContent(data);
      data.length > 5 && setIsMore(true);
    });
  }, []);

  useEffect(() => {
    if (lastPath === "announcement") {
      subheader.setTitle("Pengumuman");
      subheader.setBreadcrumbs([
        {
          pathname: "/announcement",
          title: "Pengumuman",
        },
      ]);
    }
  }, []);

  const handleClick = (id) => {
    history.push(`/announcement/${id}/detail`);
  };

  const showPengumuman = () => {
    return content.map((data, index) => {
      const date = new Date(data.updated_at);
      const created =
        String(date.getDate()).padStart(2, "0") +
        "/" +
        String(date.getMonth() + 1).padStart(2, "0") +
        "/" +
        date.getFullYear() +
        ", " +
        String(date.getHours()).padStart(2, "0") +
        ":" +
        String(date.getMinutes()).padStart(2, "0") +
        ":" +
        String(date.getSeconds()).padStart(2, "0");
      return (
        <div className="mb-10" key={index}>
          <div className="d-flex align-items-center">
            <i
              className="flaticon2-digital-marketing text-primary mr-4"
              style={{ color: "#1D428A", fontSize: "2rem" }}
            ></i>
            <div className="d-flex flex-column flex-grow-1">
              <a
                onClick={() => handleClick(data.id_pengumuman)}
                className="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1"
              >
                {data.judul}
              </a>
              <span className="text-muted font-weight-bold">{created}</span>
            </div>
          </div>
          <p className="text-dark-50 m-0 pt-5 font-weight-normal">{data.isi}</p>
        </div>
      );
    });
  };
  const showDashboard = () => {
    if (content.length > 5) {
      return content.slice(0, 5).map((data, index) => {
        const date = new Date(data.updated_at);
        const created =
          String(date.getDate()).padStart(2, "0") +
          "/" +
          String(date.getMonth() + 1).padStart(2, "0") +
          "/" +
          date.getFullYear() +
          ", " +
          String(date.getHours()).padStart(2, "0") +
          ":" +
          String(date.getMinutes()).padStart(2, "0") +
          ":" +
          String(date.getSeconds()).padStart(2, "0");
        return (
          <div className="mb-10" key={index}>
            <div className="d-flex align-items-center">
              <i
                className="flaticon2-digital-marketing text-primary mr-4"
                style={{ color: "#1D428A", fontSize: "2rem" }}
              ></i>
              <div className="d-flex flex-column flex-grow-1">
                <a
                  onClick={() => handleClick(data.id_pengumuman)}
                  className="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1"
                >
                  {data.judul}
                </a>
                <span className="text-muted font-weight-bold">{created}</span>
              </div>
            </div>
            <p className="text-dark-50 m-0 pt-5 font-weight-normal">
              {data.isi}
            </p>
          </div>
        );
      });
    } else {
      return content.map((data, index) => {
        const date = new Date(data.updated_at);
        const created =
          String(date.getDate()).padStart(2, "0") +
          "/" +
          String(date.getMonth() + 1).padStart(2, "0") +
          "/" +
          date.getFullYear() +
          ", " +
          String(date.getHours()).padStart(2, "0") +
          ":" +
          String(date.getMinutes()).padStart(2, "0") +
          ":" +
          String(date.getSeconds()).padStart(2, "0");
        return (
          <div className="mb-10" key={index}>
            <div className="d-flex align-items-center">
              <i
                className="flaticon2-digital-marketing text-primary mr-4"
                style={{ color: "#1D428A", fontSize: "2rem" }}
              ></i>
              <div className="d-flex flex-column flex-grow-1">
                <a
                  onClick={() => handleClick(data.id_pengumuman)}
                  className="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1"
                >
                  {data.judul}
                </a>
                <span className="text-muted font-weight-bold">{created}</span>
              </div>
            </div>
            <p className="text-dark-50 m-0 pt-5 font-weight-normal">
              {data.isi}
            </p>
          </div>
        );
      });
    }
  };

  return (
    <>
      {/* {showPengumuman()} */}
      {lastPath === "dashboard" ? (
        showDashboard()
      ) : (
        //  showPengumuman()
        <>
          <Card>
            <CardHeader
              title="Pengumuman"
              style={{ backgroundColor: "#FFC91B" }}
            ></CardHeader>
            <CardBody>
              <PerfectScrollbar
                options={perfectScrollbarOptions}
                className="navi navi-hover scroll my-4"
                style={{ maxHeight: "500px", position: "relative" }}
              >
                {showPengumuman()}
              </PerfectScrollbar>
            </CardBody>
          </Card>
        </>
      )}
      {lastPath === "dashboard"
        ? isMore && (
            <div className="mb-6">
              <div className="d-flex align-items-center flex-grow-1">
                <div className="d-flex flex-wrap align-items-center justify-content-between w-100">
                  <span
                    className="btn btn-sm btn-light-primary btn-inline font-weight-bold py-4"
                    onClick={() => history.push("/announcement")}
                  >
                    Lihat Lainnya . . .
                  </span>
                </div>
              </div>
            </div>
          )
        : null}
    </>
  );
}

export default Pengumuman;
