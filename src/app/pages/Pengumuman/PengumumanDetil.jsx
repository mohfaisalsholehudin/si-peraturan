import React, { useEffect, useState } from "react";
import { useSubheader } from "../../../_metronic/layout";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../_metronic/_partials/controls";
import { getPengumumanById } from "../../references/Api";

function PengumumanDetil({
  history,
  match: {
    params: { id },
  },
}) {
  const [content, setContent] = useState([]);
  let thePath = document.URL;
  const lastPath = thePath.substring(thePath.lastIndexOf("/") + 1);
  const subheader = useSubheader();

  useEffect(() => {
    getPengumumanById(id).then(({ data }) => {
      setContent(data);
    });
    if (lastPath === "detail") {
      subheader.setTitle("Detail Pengumuman");
      subheader.setBreadcrumbs([
        {
          pathname: `/announcement`,
          title: "Pengumuman",
        },
        {
          pathname: `/announcement/${id}/detail`,
          title: "Detail Pengumuman",
        },
      ]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const backAction = () => {
    history.push(`/announcement`)
  }
  return (
    <>
      <Card>
        <CardHeader
          title="Detail Pengumuman"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <div className="row">
            <div className="col-lg-12">
              <h4 className="font-weight-bold mb-5">{content.judul}</h4>
              <div
                className="navi-separator mt-3"
                style={{
                  marginTop: "3px",
                  marginBottom: "15px",
                  borderBottom: "1px solid #ebedf3",
                }}
              ></div>
              <div className="d-flex align-items-center mb-10">
                <div className="d-flex flex-column font-weight-bold">
                  <span className="text-dark mb-1 font-size-lg">
                    {content.isi ? content.isi : "-"}
                  </span>
                </div>
              </div>
            </div>
          </div>
        </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
      <div className="col-lg-12" style={{ textAlign: "right" }}>
        <button
          type="button"
          onClick={backAction}
          className="btn btn-light"
          style={{
            boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
          }}
        >
          <i className="fa fa-arrow-left"></i>
          Kembali
        </button>
      </div>
      </CardFooter>
      </Card>
    </>
  );
}

export default PengumumanDetil;
