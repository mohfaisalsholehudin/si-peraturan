/**
 * High level router.
 *
 * Note: It's recommended to compose related routes in internal router
 * components (e.g: `src/app/modules/Auth/pages/AuthPage`, `src/app/BasePage`).
 */

import React from "react";
import { Redirect, Switch, Route, useLocation } from "react-router-dom";
import { shallowEqual, useSelector } from "react-redux";
import { Layout } from "../_metronic/layout";
import BasePage from "./BasePage";
// import { Logout, AuthPage } from "./modules/Auth";
import { Logout } from "./modules/Auth";
import ErrorsPage from "./modules/ErrorsExamples/ErrorsPage";
import AuthPage from "./modules/Auth/pages/AuthPage"


export function useQuery() {
  return new URLSearchParams(useLocation().search);
}


export function Routes() {

  const { isAuthorized } = useSelector(
    ({ auth }) => ({
      isAuthorized: auth.user && auth.role != null,
    }),
    shallowEqual
    );

    const {SSO_URL, LOCAL_URL} = window.ENV;
    const query = useQuery();
    const code = query.get("code");
    const checkCode = () => {
        if(code){
            return <AuthPage code={code} />
        }
        else{
          return window.location.href = `${SSO_URL}/oauth/authorize?client_id=kmi&response_type=code&redirect_url=${LOCAL_URL}/auth/login`
        }
  
    }

  return (
    <Switch>
      {!isAuthorized ? (
        /*Render auth page when user at `/auth` and not authorized.*/
        <Route>
          <AuthPage />
          {/* {checkCode()} */}
        </Route>
      ) : (
        /*Otherwise redirect to root page (`/`)*/
        <Redirect from="/auth" to="/" />
      )}

      <Route path="/error" component={ErrorsPage} />
      <Route path="/logout" component={Logout} />

      {!isAuthorized ? (
        /*Redirect to `/auth` when user is not authorized*/
        <Redirect to="/auth/login" />
      ) : (
        <Layout>
          <BasePage />
        </Layout> 
      )}
    </Switch>
  );
}
