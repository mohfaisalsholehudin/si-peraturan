// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";

export function ActionsColumnFormatterComposeProposal(
  cellContent,
  row,
  rowIndex,
  {
    openEditDialog,
    openDeleteDialog,
    showProposal,
    applyProposal,
    showReject,
    openProcess,
    showDetil,
  }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Eselon 4":
        return (
          <>
            {/* <a 
              title="Show Details"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showProposal(row.id_penyusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                <SVG src={toAbsoluteUrl("/media/svg/icons/Text/Menu.svg")} />
              </span>
            </a>
            <> </> */}
            <a
              title="Open Data"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showDetil(row.id_penyusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      case "Proses Penyusunan":
        return (
          <>
            {/* <a
              title="Show Details"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showProposal(row.id_penyusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                <SVG src={toAbsoluteUrl("/media/svg/icons/Text/Menu.svg")} />
              </span>
            </a>
            <> </> */}
            <a
              title="Open Data"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showDetil(row.id_penyusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      case "Penelitian Cosign Unit/KL Es4":
        return (
          <>
            {/* <a
              title="Show Details"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showProposal(row.id_penyusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                <SVG src={toAbsoluteUrl("/media/svg/icons/Text/Menu.svg")} />
              </span>
            </a>
            <> </> */}
            <a
              title="Open Data"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showDetil(row.id_penyusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      case "Cosign Sahli":
        return (
          <>
            <a
              title="Open Data"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showDetil(row.id_penyusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      case "Penelitian Cosign Sahli":
        return (
          <>
            <a
              title="Open Data"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showDetil(row.id_penyusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      case "Cosign Dirjen":
        return (
          <>
            <a
              title="Open Data"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showDetil(row.id_penyusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      case "Penelitian Dirjen Eselon 4":
        return (
          <>
            <a
              title="Open Data"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showDetil(row.id_penyusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      case "Penelitian Dirjen Eselon 3":
        return (
          <>
            <a
              title="Open Data"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showDetil(row.id_penyusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                    src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                  /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      case "Penetapan":
        return (
          <>
            <a
              title="Open Data"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showDetil(row.id_penyusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      case "Terima":
        return (
          <>
            <a
              title="Open Data"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showDetil(row.id_penyusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                    src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                  /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );

      case "Draft":
        return (
          <>
            {/* <a
              title="Edit Proposal"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openEditDialog(row.id_penyusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                />
              </span>
            </a>
            <> </> */}

            <a
              title="Hapus Penyusunan"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() => openDeleteDialog(row.id_penyusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
            <> </>
            <a
              title="Open Detils"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openProcess(row.id_penyusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                {/* <SVG src={toAbsoluteUrl("/media/svg/icons/Text/Menu.svg")} /> */}
                <i className="fas fa-bars text-primary"></i>
              </span>
            </a>
          </>
        );

      case "Tolak":
        return (
          <>
            <a
              title="Edit Penyusunan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openProcess(row.id_penyusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                {/* <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                /> */}
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            {/* <> </>
            <a
              title="Delete Proposal"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showReject(row.id_penyusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                />
              </span>
            </a>
            <> </>
            <a
              title="Show Details"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => applyProposal(row.id_penyusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                <SVG src={toAbsoluteUrl("/media/svg/icons/Text/Menu.svg")} />
              </span>
            </a> */}
          </>
        );

      default:
        break;
    }
  };

  return <>{checkStatus(row.status)}</>;
}

export function ActionsColumnFormatterComposeDrafting(
  cellContent,
  row,
  rowIndex,
  {
    openEditDialog,
    openDeleteDialog,
    showProposal,
    applyProposal,
    rejectProposal,
    openProcess,
    showReject,
    showDraft,
    openEditTolakDialog,
  }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Eselon 4":
        return (
          <>
            <a
              title="Show Data"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showDraft(row.id_draftperaturan, row.jns_draft)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
            <> </>
            <a
              title="Tolak"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => rejectProposal()}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Close.svg")}
                /> */}
                <i className="far fa-window-close text-danger"></i>
              </span>
            </a>
            <> </>
            <a
              title="Apply Data"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => applyProposal()}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")}
                /> */}
                <i className="fas fa-check text-success"></i>
              </span>
            </a>
          </>
        );

      case "Draft":
        return (
          <>
            <a
              title="Edit Data"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() =>
                openEditDialog(row.id_draftperaturan, row.jns_draft)
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                {/* <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                /> */}
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>
            <a
              title="Apply Drafting"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => applyProposal()}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")}
                /> */}
                <i className="fas fa-check text-success"></i>
              </span>
            </a>
          </>
        );

      case "Tolak":
        return (
          <>
            <a
              title="Edit Data"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() =>
                openEditTolakDialog(row.id_draftperaturan, row.jns_draft)
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                {/* <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                /> */}
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            {/* <> </>
            <a
              title="Apply Drafting"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => applyProposal()}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")}
                />
              </span>
            </a> */}
          </>
        );

      default:
        break;
    }
  };

  return <>{checkStatus(row.status)}</>;
}

export function ActionsColumnFormatterComposeProcess(
  cellContent,
  row,
  rowIndex,
  { openProcess }
) {
  // console.log(row.status === 'Cosign Sahli' || row.status=== 'Penelitian Cosign Dirjen' ? 'Betul' : 'salah')
  return (
    <>
      {" "}
      {row.status ? (
        row.status === "Cosign Sahli" ||
        row.status === "Penelitian Dirjen Eselon 4" ||
        row.status === "Penelitian Dirjen Eselon 3" ||
        row.status === "Cosign Dirjen" ||
        row.status === "Penelitian Cosign Sahli" ||
        row.status === "Penetapan" ? (
          <a
            title="Open Detils"
            className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
            onClick={() => openProcess(row.id_detil)}
          >
            <span className="svg-icon svg-icon-md svg-icon-primary">
              {/* <SVG src={toAbsoluteUrl("/media/svg/icons/Code/Plus.svg")} /> */}
              <i className="fas fa-plus-circle text-primary"></i>
            </span>
          </a>
        ) : null
      ) : (
        <a
          title="Open Detils"
          className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
          onClick={() => openProcess(row.id_detil)}
        >
          <span className="svg-icon svg-icon-md svg-icon-primary">
            {/* <SVG src={toAbsoluteUrl("/media/svg/icons/Code/Plus.svg")} /> */}
            <i className="fas fa-plus-circle text-primary"></i>
          </span>
        </a>
      )}
      {/* <a
        title="Open Detils"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => openProcess(row.id_detil)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          <SVG src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")} />
        </span>
      </a> */}
    </>
  );
}

export function ActionsColumnFormatterComposeDetilSurat(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog }
) {
  return (
    <>
      <a
        title="Edit Data"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => openEditDialog(row.id_surat)}
      >
        <span className="svg-icon svg-icon-md svg-icon-primary">
          {/* <SVG
            src={toAbsoluteUrl("/media/svg/icons/Communication/Write.svg")}
          /> */}
          <i className="fas fa-edit text-primary"></i>
        </span>
      </a>
      <> </>
      <a
        title="Delete Data"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => openDeleteDialog(row.id_surat)}
      >
        <span className="svg-icon svg-icon-md svg-icon-danger">
          {/* <SVG src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")} /> */}
          <i className="fas fa-trash text-danger"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterComposeDetilLhr(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog }
) {
  return (
    <>
      <a
        title="Edit Data"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => openEditDialog(row.id_lhr)}
      >
        <span className="svg-icon svg-icon-md svg-icon-primary">
          {/* <SVG src={toAbsoluteUrl("/media/svg/icons/Code/Plus.svg")} /> */}
          <i className="fas fa-plus-circle text-primary"></i>
        </span>
      </a>
      <> </>
      <a
        title="Delete Data"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => openDeleteDialog(row.id_lhr)}
      >
        <span className="svg-icon svg-icon-md svg-icon-danger">
          {/* <SVG src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")} /> */}
          <i className="fas fa-trash text-danger"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterComposeDetilPerter(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog }
) {
  return (
    <>
      {" "}
      <a
        title="Edit Data"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => openEditDialog(row.id_peraturan)}
      >
        <span className="svg-icon svg-icon-md svg-icon-primary">
          {/* <SVG
            src={toAbsoluteUrl("/media/svg/icons/Communication/Write.svg")}
          /> */}
          <i className="fas fa-edit text-primary"></i>
        </span>
      </a>
      <a
        title="Delete Data"
        className="btn btn-icon btn-light btn-hover-primary btn-sm"
        onClick={() => openDeleteDialog(row.id_peraturan)}
      >
        <span className="svg-icon svg-icon-md svg-icon-danger">
          {/* <SVG src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")} /> */}
          <i className="fas fa-trash text-danger"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterComposeDetilLhrPokok(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog }
) {
  return (
    <>
      {" "}
      <a
        title="Edit Data"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => openEditDialog(row.id_lhr_detil)}
      >
        <span className="svg-icon svg-icon-md svg-icon-primary">
          {/* <SVG
            src={toAbsoluteUrl("/media/svg/icons/Communication/Write.svg")}
          /> */}
          <i className="fas fa-edit text-primary"></i>
        </span>
      </a>
      <a
        title="Delete Data "
        className="btn btn-icon btn-light btn-hover-primary btn-sm"
        onClick={() => openDeleteDialog(row.id_lhr_detil)}
      >
        <span className="svg-icon svg-icon-md svg-icon-danger">
          {/* <SVG src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")} /> */}
          <i className="fas fa-trash text-danger"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterComposeDetilLhrAfter(
  cellContent,
  row,
  rowIndex,
  { openProcess }
) {
  return (
    <>
      {" "}
      <a
        title="Edit Data"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => openProcess(row.id_perter)}
      >
        <span className="svg-icon svg-icon-md svg-icon-primary">
          {/* <SVG
            src={toAbsoluteUrl("/media/svg/icons/Communication/Write.svg")}
          /> */}
          <i className="fas fa-edit text-primary"></i>
        </span>
      </a>
      <a
        title="Delete Data"
        className="btn btn-icon btn-light btn-hover-primary btn-sm"
        onClick={() => openProcess(row.id_perter)}
      >
        <span className="svg-icon svg-icon-md svg-icon-danger">
          {/* <SVG src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")} /> */}
          <i className="fas fa-trash text-danger"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterComposeResearch(
  cellContent,
  row,
  rowIndex,
  { openDialog }
) {
  return (
    <>
      {" "}
      <a
        title="Open Detils"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => openDialog(row.id_penyusunan)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          {/* <SVG src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")} /> */}
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
    </>
  );
}
export function ActionsColumnFormatterComposeResearchProcess(
  cellContent,
  row,
  rowIndex,
  { openProcess }
) {
  return (
    <>
      {" "}
      <a
        title="Open Detils"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => openProcess(row.id_detil)}
      >
        <span className="svg-icon svg-icon-md svg-icon-primary">
          {/* <SVG src={toAbsoluteUrl("/media/svg/icons/Code/Plus.svg")} />
           */}
          <i className="fas fa-plus-circle text-primary"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterComposeResearchProcessSurat(
  cellContent,
  row,
  rowIndex,
  { showDialog }
) {
  return (
    <>
      <a
        title="Open Surat"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => showDialog(row.id_surat)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          {/* <SVG src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")} /> */}
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterComposeResearchProcessLhr(
  cellContent,
  row,
  rowIndex,
  { showDialog }
) {
  return (
    <>
      <a
        title="Open LHR"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => showDialog(row.id_lhr)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          {/* <SVG src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")} /> */}
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterComposeResearchProcessPerter(
  cellContent,
  row,
  rowIndex,
  { showDialog }
) {
  return (
    <>
      {" "}
      <a
        title="Open Peraturan"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => showDialog(row.id_peraturan)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          {/* <SVG src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")} /> */}
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterComposeProcessModule(
  cellContent,
  row,
  rowIndex,
  { openDialog }
) {
  return (
    <>
      {" "}
      {row.status === "Penelitian Dirjen Eselon 4" ||
        row.status === "Penelitian Dirjen Eselon 3" ||
        row.status === "Penelitian Cosign Sahli" ||
        row.status === "Penelitian Cosign Unit/KL Es4" ? (
          <a
        title="Open Detils"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => openDialog(row.id_penyusunan)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          {/* <SVG src={toAbsoluteUrl("/media/svg/icons/Code/Plus.svg")} /> */}
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
        ) : (
          <a
          title="Open Detils"
          className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
          onClick={() => openDialog(row.id_penyusunan)}
        >
          <span className="svg-icon svg-icon-md svg-icon-primary">
            {/* <SVG src={toAbsoluteUrl("/media/svg/icons/Code/Plus.svg")} /> */}
            <i className="fas fa-plus-circle text-primary"></i>
          </span>
        </a>
        )}
     
      {/* <> </>
      <a
        title="Open Detils"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        // onClick={() => openProcess(row.id_perter)}
      >
        <span className="svg-icon svg-icon-md svg-icon-danger">
          <SVG src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")} />
        </span>
      </a> */}
    </>
  );
}

export function ActionsColumnFormatterComposeProcessCosignDjp(
  cellContent,
  row,
  rowIndex,
  { editAction, deleteAction }
) {
  return (
    <>
      <a
        title="Edit Cosign"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => editAction(row.id_cosign_unit)}
      >
        <span className="svg-icon svg-icon-md svg-icon-primary">
          {/* <SVG
            src={toAbsoluteUrl("/media/svg/icons/Communication/Write.svg")}
          /> */}
          <i className="fas fa-edit text-primary"></i>
        </span>
      </a>
      <> </>

      <a
        title="Hapus Cosign"
        className="btn btn-icon btn-light btn-hover-danger btn-sm"
        onClick={() => deleteAction(row.id_cosign_unit)}
      >
        <span className="svg-icon svg-icon-md svg-icon-danger">
          {/* <SVG src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")} /> */}
          <i className="fas fa-trash text-danger"></i>
        </span>
      </a>
    </>
  );
}
export function ActionsColumnFormatterComposeProcessCosignDjpJustView(
  cellContent,
  row,
  rowIndex,
  { showDetil }
) {
  return (
    <>
      <a
        title="Show Detil"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => showDetil(row.id_cosign_unit)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          {/* <SVG src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")} /> */}
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
    </>
  );
}
export function ActionsColumnFormatterComposeProcessCosignKementerian(
  cellContent,
  row,
  rowIndex,
  { editAction, deleteAction }
) {
  return (
    <>
      <a
        title="Edit Cosign"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => editAction(row.id_cosign_kl)}
      >
        <span className="svg-icon svg-icon-md svg-icon-primary">
          {/* <SVG
            src={toAbsoluteUrl("/media/svg/icons/Communication/Write.svg")}
          /> */}
          <i className="fas fa-edit text-primary"></i>
        </span>
      </a>
      <> </>

      <a
        title="Hapus Cosign"
        className="btn btn-icon btn-light btn-hover-danger btn-sm"
        onClick={() => deleteAction(row.id_cosign_kl)}
      >
        <span className="svg-icon svg-icon-md svg-icon-danger">
          {/* <SVG src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")} /> */}
          <i className="fas fa-trash text-danger"></i>
        </span>
      </a>
    </>
  );
}
export function ActionsColumnFormatterComposeProcessCosignKementerianJustView(
  cellContent,
  row,
  rowIndex,
  { showDetil }
) {
  return (
    <>
      <a
        title="Show Detil"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => showDetil(row.id_cosign_kl)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          {/* <SVG src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")} /> */}
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
    </>
  );
}
export function ActionsColumnFormatterComposeProcessTindakLanjutDirektoratKajianAksi(
  cellContent,
  row,
  rowIndex,
  { openProcess, showStatus }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Proses Penyusunan":
        return (
          <>
            <a
              title="Apply Tindak Lanjut"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openProcess()}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")}
                /> */}
                <i className="fas fa-check text-success"></i>
              </span>
            </a>
          </>
        );
      case "Penelitian Cosign Unit/KL Es4":
        return (
          <>
            <a
              title="Show Status"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showStatus()}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      case "Penelitian Cosign Dirjen":
        return (
          <>
            <a
              title="Show Status"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showStatus()}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      case "Cosign Sahli":
        return (
          <>
            <a
              title="Show Status"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showStatus()}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      case "Penelitian Cosign Sahli":
        return (
          <>
            <a
              title="Show Status"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showStatus()}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      case "Cosign Dirjen":
        return (
          <>
            <a
              title="Show Status"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showStatus()}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      case "Penetapan":
        return (
          <>
            <a
              title="Show Status"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showStatus()}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
        case "Terima":
          return (
            <>
              <a
                title="Show Status"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() => showStatus()}
              >
                <span className="svg-icon svg-icon-md svg-icon-dark">
                  <i className="fas fa-eye text-dark"></i>
                </span>
              </a>
            </>
          );
      default:
        break;
    }
  };
  return <>{checkStatus(row.status)}</>;
}
export function ActionsColumnFormatterComposeMonitoringTindakLanjutDirektoratKajianAksi(
  cellContent,
  row,
  rowIndex,
  { openProcess, showStatus }
) {
 
  return (
    <>
      <a
        title="Show Status"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => showStatus()}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          {/* <SVG
            src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
          /> */}
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterComposeProcessTindakLanjutSahliKajianAksi(
  cellContent,
  row,
  rowIndex,
  { openProcess, showStatus }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Cosign Sahli":
        return (
          <>
            <a
              title="Apply Tindak Lanjut"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openProcess()}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")}
                /> */}
                <i className="fas fa-check text-success"></i>
              </span>
            </a>
          </>
        );
      case "Penelitian Cosign Unit/KL Es4":
        return (
          <>
            <a
              title="Show Status"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showStatus()}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      case "Penelitian Cosign Dirjen":
        return (
          <>
            <a
              title="Show Status"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showStatus()}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      case "Proses Penyusunan":
        return (
          <>
            <a
              title="Show Status"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showStatus()}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      case "Penelitian Cosign Sahli":
        return (
          <>
            <a
              title="Show Status"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showStatus()}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      case "Cosign Dirjen":
        return (
          <>
            <a
              title="Show Status"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showStatus()}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      case "Penetapan":
        return (
          <>
            <a
              title="Show Status"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showStatus()}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
        case "Terima":
          return (
            <>
              <a
                title="Show Status"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() => showStatus()}
              >
                <span className="svg-icon svg-icon-md svg-icon-dark">
                  <i className="fas fa-eye text-dark"></i>
                </span>
              </a>
            </>
          );
      default:
        break;
    }
  };
  return (
    <>
      <a
        title="Show Status"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => showStatus()}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          {/* <SVG
            src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
          /> */}
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
    </>
  );
}
export function ActionsColumnFormatterComposeMonitoringTindakLanjutSahliKajianAksi(
  cellContent,
  row,
  rowIndex,
  { openProcess, showStatus }
) {
 
  return (
    <>
      <a
        title="Show Status"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => showStatus()}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          {/* <SVG
            src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
          /> */}
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterComposeProcessTindakLanjutDirektoratKajian(
  cellContent,
  row,
  rowIndex,
  { openProcess }
) {
  return (
    <a
      title="Open Kajian"
      className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
      onClick={() => openProcess()}
    >
      <span className="svg-icon svg-icon-lg svg-icon-success">
        {/* <SVG src={toAbsoluteUrl("/media/svg/icons/Files/Selected-file.svg")} /> */}
        <i className="fas fa-file-alt text-success"></i>
      </span>
    </a>
  );
}

export function ActionsColumnFormatterComposeProcessTindakLanjutDirektoratUnit(
  cellContent,
  row,
  rowIndex,
  { openProcess }
) {
  return (
    <>
      {" "}
      <a
        title="Open Tindak Lanjut"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => openProcess(row.id_cosign_unit)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
    </>
  );
}
export function ActionsColumnFormatterComposeProcessTindakLanjutDirektoratKementerian(
  cellContent,
  row,
  rowIndex,
  { openProcess }
) {
  return (
    <>
      {" "}
      <a
        title="Open Tindak Lanjut"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => openProcess(row.id_cosign_kl)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
    </>
  );
}
export function ActionsColumnFormatterComposeProcessTindakLanjutDirektoratJawaban(
  cellContent,
  row,
  rowIndex,
  { openProcess }
) {
  return (
    <>
      {" "}
      <a
        title="Open Tindak Lanjut"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => openProcess(row.id_cosign_unit)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterComposeProcessNdPermintaan(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog }
) {
  return (
    <>
      <a
        title="Edit Data"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => openEditDialog(row.id_minta_jawab)}
      >
        <span className="svg-icon svg-icon-md svg-icon-primary">
          {/* <SVG
            src={toAbsoluteUrl("/media/svg/icons/Communication/Write.svg")}
          /> */}
          <i className="fas fa-edit text-primary"></i>
        </span>
      </a>
      <> </>

      <a
        title="Hapus Data"
        className="btn btn-icon btn-light btn-hover-danger btn-sm mx-3"
        onClick={() => openDeleteDialog(row.id_minta_jawab)}
      >
        <span className="svg-icon svg-icon-md svg-icon-danger">
          {/* <SVG src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")} /> */}
          <i className="fas fa-trash text-danger"></i>
        </span>
      </a>
    </>
  );
}
export function ActionsColumnFormatterComposeProcessNdPermintaanJustView(
  cellContent,
  row,
  rowIndex,
  { showDetil }
) {
  return (
    <>
      <a
        title="Show Detil"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => showDetil(row.id_minta_jawab)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          {/* <SVG src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")} /> */}
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterComposeProcessCosignSahli(
  cellContent,
  row,
  rowIndex,
  { editAction, deleteAction }
) {
  return (
    <>
      {" "}
      <a
        title="Edit Cosign"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => editAction(row.id_cosign_sahli)}
      >
        <span className="svg-icon svg-icon-md svg-icon-primary">
          {/* <SVG
            src={toAbsoluteUrl("/media/svg/icons/Communication/Write.svg")}
          /> */}
          <i className="fas fa-edit text-primary"></i>
        </span>
      </a>
      <> </>
      <a
        title="Delete"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => deleteAction(row.id_cosign_sahli)}
      >
        <span className="svg-icon svg-icon-md svg-icon-primary">
          {/* <SVG
            src={toAbsoluteUrl("/media/svg/icons/Communication/Write.svg")}
          /> */}
          <i className="fas fa-trash text-danger"></i>
        </span>
      </a>
    </>
  );
}
export function ActionsColumnFormatterComposeProcessCosignSahliJustView(
  cellContent,
  row,
  rowIndex,
  { showDetil }
) {
  return (
    <>
      <a
        title="Show Detil"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => showDetil(row.id_cosign_sahli)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          {/* <SVG src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")} /> */}
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterComposeProcessTindakLanjutSahli(
  cellContent,
  row,
  rowIndex,
  { openAction }
) {
  return (
    <>
      {" "}
      <a
        title="Open Tindak Lanjut"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        // onClick={() => openAction(row.id_surat)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          {/* <SVG src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")} /> */}
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterComposeProcessTindakLanjutSahliJawaban(
  cellContent,
  row,
  rowIndex,
  { openAction }
) {
  return (
    <>
      {" "}
      <a
        title="Open Tindak Lanjut"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        // onClick={() => openAction(row.id_surat)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          {/* <SVG src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")} /> */}
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
    </>
  );
}
export function ActionsColumnFormatterComposeProcessTindakLanjutSahliKajian(
  cellContent,
  row,
  rowIndex,
  { openProcess }
) {
  return (
    <>
      {" "}
      <a
        title="Open Tindak Lanjut"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        // onClick={() => openProcess(row.id_detil)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          {/* <SVG src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")} /> */}
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
      <> </>
      <a
        title="Edit Tindak Lanjut"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        // onClick={() => openProcess(row.id_detil)}
      >
        <span className="svg-icon svg-icon-md svg-icon-primary">
          {/* <SVG
            src={toAbsoluteUrl("/media/svg/icons/Communication/Write.svg")}
          /> */}
          <i className="fas fa-edit text-primary"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterComposeProcessPengajuanRancanganKajian(
  cellContent,
  row,
  rowIndex,
  {
    openEditDialog,
    openDeleteDialog,
    showProposal,
    applyProposal,
    showReject,
    openProcess,
  }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Eselon 4":
        return (
          <>
            <a
              title="Show Details"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showProposal(row.id_penyusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                {/* <SVG src={toAbsoluteUrl("/media/svg/icons/Text/Menu.svg")} /> */}
                <i className="fas fa-bars text-success"></i>
              </span>
            </a>
            <> </>
            <a
              title="Show Status"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showProposal(row.id_penyusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );

      case "Siap Diajukan":
        return (
          <>
            {/* <a
              title="Edit Proposal"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openEditDialog(row.id_penyusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                />
              </span>
            </a>
            <> </>

            <a
              title="Hapus Proposal"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() => openDeleteDialog(row.id)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                />
              </span>
            </a>
            <> </> */}
            <a
              title="Open Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              // onClick={() => openProcess(row.id_penyusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                />
              </span>
            </a>
          </>
        );

      case "Tolak":
        return (
          <>
            {/* <a
              title="Edit Proposal"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openEditDialog(row.id_penysusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                />
              </span>
            </a>
            <> </>
            <a
              title="Delete Proposal"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showReject(row.id_penyusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                />
              </span>
            </a>
            <> </> */}
            <a
              title="Open Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              // onClick={() => openProcess(row.id_penyusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
            <> </>
            <a
              title="Edit Kajian"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              // onClick={() => openProcess(row.id_penyusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                {/* <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                /> */}
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
          </>
        );

      default:
        break;
    }
  };

  return <>{checkStatus(row.status)}</>;
}

// export function ActionsColumnFormatterComposeProcessInputLhrBefore(
//   cellContent,
//   row,
//   rowIndex,
//   { openProcess }
// ) {
//   return (
//     <>
//       {" "}
//       <a
//         title="Open Detils"
//         className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
//         // onClick={() => openProcess(row.id_perter)}
//       >
//         <span className="svg-icon svg-icon-md svg-icon-primary">
//           <SVG
//             src={toAbsoluteUrl("/media/svg/icons/Communication/Write.svg")}
//           />
//         </span>
//       </a>
//       <a
//         title="Open Detils"
//         className="btn btn-icon btn-light btn-hover-primary btn-sm"
//         // onClick={() => openProcess(row.id_perter)}
//       >
//         <span className="svg-icon svg-icon-md svg-icon-danger">
//           <SVG src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")} />
//         </span>
//       </a>
//     </>
//   );
// }

// export function ActionsColumnFormatterComposeProcessInputLhrAfter(
//   cellContent,
//   row,
//   rowIndex,
//   { openProcess }
// ) {
//   return (
//     <>
//       {" "}
//       <a
//         title="Open Detils"
//         className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
//         // onClick={() => openProcess(row.id_perter)}
//       >
//         <span className="svg-icon svg-icon-md svg-icon-primary">
//           <SVG
//             src={toAbsoluteUrl("/media/svg/icons/Communication/Write.svg")}
//           />
//         </span>
//       </a>
//       <a
//         title="Open Detils"
//         className="btn btn-icon btn-light btn-hover-primary btn-sm"
//         // onClick={() => openProcess(row.id_perter)}
//       >
//         <span className="svg-icon svg-icon-md svg-icon-danger">
//           <SVG src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")} />
//         </span>
//       </a>
//     </>
//   );
// }

export function ActionsColumnFormatterComposeReProcess(
  cellContent,
  row,
  rowIndex,
  { openDialog }
) {
  return (
    <>
      {" "}
      <a
        title="Open Detils"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => openDialog(row.id_penyusunan, row.status)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          {/* <SVG src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")} /> */}
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
    </>
  );
}
export function ActionsColumnFormatterComposeBkf(
  cellContent,
  row,
  rowIndex,
  {
    openEditDialog,
    openDeleteDialog,
    showProposal,
    applyProposal,
    showReject,
    openProcess,
  }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Eselon 4":
        return (
          <>
            <a
              title="Show Details"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showProposal(row.id_penyusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                {/* <SVG src={toAbsoluteUrl("/media/svg/icons/Text/Menu.svg")} /> */}
                <i className="fas fa-bars text-success"></i>
              </span>
            </a>
            <> </>
            <a
              title="Show Status"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showProposal(row.id_penyusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );

      case "Draft":
        return (
          <>
            <a
              title="Edit Data"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openEditDialog(row.id_penyusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                {/* <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                /> */}
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>

            <a
              title="Hapus Data"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() => openDeleteDialog(row.id)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
            <> </>
            <a
              title="Open Details"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openProcess(row.id_penyusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                {/* <SVG src={toAbsoluteUrl("/media/svg/icons/Text/Menu.svg")} /> */}
                <i className="fas fa-bars text-success"></i>
              </span>
            </a>
          </>
        );

      case "Tolak":
        return (
          <>
            <a
              title="Edit Data"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openEditDialog(row.id_penysusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                {/* <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                /> */}
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>
            <a
              title="Delete Data"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showReject(row.id_penyusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
            <> </>
            <a
              title="Show Details"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => applyProposal(row.id_penyusunan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                {/* <SVG src={toAbsoluteUrl("/media/svg/icons/Text/Menu.svg")} /> */}
                <i className="fas fa-bars text-success"></i>
              </span>
            </a>
          </>
        );

      default:
        break;
    }
  };

  return <>{checkStatus(row.status)}</>;
}

export function ActionsColumnFormatterComposeProcessInputLhr(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog, showDetil }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "draft":
        return (
          <>
            <a
              title="Show Detil"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showDetil(row.id_lhr)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      case "penyusunan":
        return (
          <>
            <a
              title="Edit Data"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openEditDialog(row.id_lhr)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                {/* <SVG src={toAbsoluteUrl("/media/svg/icons/Code/Plus.svg")} /> */}
                <i className="fas fa-plus-circle text-primary"></i>
              </span>
            </a>
            <> </>
            <a
              title="Delete Data"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openDeleteDialog(row.id_lhr)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
          </>
        );

      default:
        break;
    }
  };

  return <>{checkStatus(row.status)}</>;
}
export function ActionsColumnFormatterComposeProcessInputLhrJustView(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog, showDetil }
) {
  return (
    <>
      <a
        title="Show Detil"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => showDetil(row.id_lhr)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          {/* <SVG src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")} /> */}
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterComposeReProcessInputLhr(
  cellContent,
  row,
  rowIndex,
  { showDetil }
) {
  return (
    <>
      <a
        title="Show Detil"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => showDetil(row.id_lhr)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          {/* <SVG src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")} /> */}
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterComposeProcessSuratUndangan(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog, showDetil }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "draft":
        return (
          <>
            <a
              title="Show Detil"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showDetil(row.id_surat)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      case "penyusunan":
        return (
          <>
            <a
              title="Edit Data"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openEditDialog(row.id_surat)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                {/* <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                /> */}
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>
            <a
              title="Delete Data"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openDeleteDialog(row.id_surat)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
          </>
        );

      default:
        break;
    }
  };

  return <>{checkStatus(row.status)}</>;
}
export function ActionsColumnFormatterComposeProcessSuratUndanganJustView(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog, showDetil }
) {
  return (
    <>
      <a
        title="Show Detil"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => showDetil(row.id_surat)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          {/* <SVG src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")} /> */}
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
    </>
  );
}
export function ActionsColumnFormatterComposeReProcessSuratUndangan(
  cellContent,
  row,
  rowIndex,
  { showDetil }
) {
  return (
    <>
      <a
        title="Show Detil"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => showDetil(row.id_surat)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          {/* <SVG src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")} /> */}
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterComposeProcessPeraturanTerkait(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog, showDetil }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "draft":
        return (
          <>
            <a
              title="Show Detil"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showDetil(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      case "penyusunan":
        return (
          <>
            <a
              title="Edit Data"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openEditDialog(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                {/* <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                /> */}
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>
            <a
              title="Delete Data"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openDeleteDialog(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
          </>
        );

      default:
        break;
    }
  };

  return <>{checkStatus(row.status)}</>;
}
export function ActionsColumnFormatterComposeProcessPeraturanTerkaitJustView(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog, showDetil }
) {
  return (
    <>
      <a
        title="Show Detil"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => showDetil(row.id_peraturan)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          {/* <SVG src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")} /> */}
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterComposeReProcessPeraturanTerkait(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog, showDetil }
) {
  return (
    <>
      <a
        title="Show Detil"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => showDetil(row.id_peraturan)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          {/* <SVG src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")} /> */}
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterComposeReProcessNdPermintaan(
  cellContent,
  row,
  rowIndex,
  { showDetil }
) {
  return (
    <>
      <a
        title="Show Detil"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => showDetil(row.id_minta_jawab)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          {/* <SVG src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")} /> */}
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterComposeReProcessCosignUnit(
  cellContent,
  row,
  rowIndex,
  { showDetil }
) {
  return (
    <>
      <a
        title="Show Detil"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => showDetil(row.id_cosign_unit)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          {/* <SVG src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")} /> */}
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterComposeReProcessCosignKementerian(
  cellContent,
  row,
  rowIndex,
  { showDetil }
) {
  return (
    <>
      <a
        title="Show Detil"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => showDetil(row.id_cosign_kl)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          {/* <SVG src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")} /> */}
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterComposeReProcessTindakLanjutDirektoratKajianAksi(
  cellContent,
  row,
  rowIndex,
  { openProcess, showStatus, rejectProposal }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Penelitian Cosign Unit/KL Es4":
        return (
          <>
            <a
              title="Tolak"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => rejectProposal()}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Close.svg")}
                /> */}
                <i className="far fa-window-close text-danger"></i>
              </span>
            </a>
            <> </>
            <a
              title="Apply Tindak Lanjut"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openProcess()}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")}
                /> */}
                <i className="fas fa-check text-success"></i>
              </span>
            </a>
          </>
        );
      case "Cosign Sahli":
        return (
          <>
            <a
              title="Show Status"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showStatus()}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      default:
        break;
    }
  };
  return <>{checkStatus(row.status)}</>;
}
export function ActionsColumnFormatterComposeReProcessTindakLanjutDirektoratKajianAksiEs3(
  cellContent,
  row,
  rowIndex,
  { openProcess, showStatus, rejectProposal }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Penelitian Cosign Unit/KL Es4":
        return <></>;
      case "Cosign Sahli":
        return <></>;
      default:
        break;
    }
  };
  return <>{checkStatus(row.status)}</>;
}

export function ActionsColumnFormatterComposeReProcessTindakLanjutSahliKajianAksi(
  cellContent,
  row,
  rowIndex,
  { openProcess, showStatus, rejectProposal }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Penelitian Cosign Sahli":
        return (
          <>
            {" "}
            <a
              title="Tolak"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => rejectProposal()}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Close.svg")}
                /> */}
                <i className="far fa-window-close text-danger"></i>
              </span>
            </a>
            <> </>
            <a
              title="Apply Tindak Lanjut"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openProcess()}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")}
                /> */}
                <i className="fas fa-check text-success"></i>
              </span>
            </a>
          </>
        );
      case "Cosign Dirjen":
        return (
          <>
            <a
              title="Show Status"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showStatus()}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      default:
        break;
    }
  };
  return <>{checkStatus(row.status)}</>;
}
export function ActionsColumnFormatterComposeReProcessTindakLanjutSahliKajianAksiEs3(
  cellContent,
  row,
  rowIndex,
  { openProcess, showStatus, rejectProposal }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Penelitian Cosign Sahli":
        return <></>;
      case "Cosign Dirjen":
        return <></>;
      default:
        break;
    }
  };
  return <>{checkStatus(row.status)}</>;
}

export function ActionsColumnFormatterComposeProcessPengajuanRancanganKajianAksi(
  cellContent,
  row,
  rowIndex,
  { openProcess, showStatus, rejectProposal }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Cosign Dirjen":
        return (
          <>
            <a
              title="Apply Tindak Lanjut"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openProcess()}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")}
                /> */}
                <i className="fas fa-check text-success"></i>
              </span>
            </a>
          </>
        );
      case "Penelitian Cosign Unit/KL Es4":
        return (
          <>
            <a
              title="Show Status"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showStatus()}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      case "Penelitian Dirjen Eselon 4":
        return (
          <>
            {" "}
            <a
              title="Tolak"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => rejectProposal()}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Close.svg")}
                /> */}
                <i className="far fa-window-close text-danger"></i>
              </span>
            </a>
            <> </>
            <a
              title="Apply Tindak Lanjut"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openProcess()}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")}
                /> */}
                <i className="fas fa-check text-success"></i>
              </span>
            </a>
          </>
        );
      case "Penelitian Dirjen Eselon 3":
        return (
          <>
            {" "}
            <a
              title="Tolak"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => rejectProposal()}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                {/* <SVG
                    src={toAbsoluteUrl("/media/svg/icons/Navigation/Close.svg")}
                  /> */}
                <i className="far fa-window-close text-danger"></i>
              </span>
            </a>
            <> </>
            <a
              title="Apply Tindak Lanjut"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openProcess()}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                {/* <SVG
                    src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")}
                  /> */}
                <i className="fas fa-check text-success"></i>
              </span>
            </a>
          </>
        );
      case "Proses Penyusunan":
        return (
          <>
            <a
              title="Show Status"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showStatus()}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      case "Penelitian Cosign Sahli":
        return (
          <>
            <a
              title="Show Status"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showStatus()}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      case "Cosign Sahli":
        return (
          <>
            <a
              title="Apply"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openProcess()}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")}
                /> */}
                <i className="fas fa-check text-success"></i>
              </span>
            </a>
          </>
        );
      case "Penetapan":
        return (
          <>
            <a
              title="Show Status"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showStatus()}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
        case "Terima":
          return (
            <>
              <a
                title="Show Status"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() => showStatus()}
              >
                <span className="svg-icon svg-icon-md svg-icon-dark">
                  {/* <SVG
                    src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                  /> */}
                  <i className="fas fa-eye text-dark"></i>
                </span>
              </a>
            </>
          );
      default:
        break;
    }
  };
  return <>{checkStatus(row.status)}</>;
}


export function ActionsColumnFormatterComposeMonitoringModule(
  cellContent,
  row,
  rowIndex,
  { openDialog }
) {
  return (
    <>
      <a
        title="Open Detils"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => openDialog(row.id_penyusunan)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterComposeMonitoringDetil(
  cellContent,
  row,
  rowIndex,
  { openProcess }
) {
  return (
    <>
      <a
        title="Open Detils"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => openProcess(row.id_detil)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
    </>
  );
}