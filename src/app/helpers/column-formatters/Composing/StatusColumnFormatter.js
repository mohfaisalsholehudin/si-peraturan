// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
import React from "react";

export function StatusColumnFormatterComposeProposal(cellContent, row) {
  const CustomerStatusCssClasses = [
    "warning",
    "success",
    "danger",
    "primary",
    "primary",
    "primary",
    "primary",
    "primary",
    "primary",
    "primary",
    "primary",
    "primary",
    "",
  ];
  const CustomerStatusTitles = [
    "Eselon 4",
    "Draft",
    "Tolak",
    "Proses Penyusunan",
    "Penelitian Cosign Unit/KL Es4",
    "Cosign Sahli",
    "Penelitian Cosign Sahli",
    "Cosign Dirjen",
    "Penelitian Dirjen Eselon 4",
    "Penelitian Dirjen Eselon 3",
    "Penetapan",
    "Terima",
    "",
  ];
  let status = "";

  switch (row.status) {
    case "Eselon 4":
      status = 0;
      break;

    case "Draft":
      status = 1;
      break;

    case "Tolak":
      status = 2;
      break;
    case "Proses Penyusunan":
      status = 3;
      break;
    case "Penelitian Cosign Unit/KL Es4":
      status = 4;
      break;
    case "Cosign Sahli":
      status = 5;
      break;
    case "Penelitian Cosign Sahli":
      status = 6;
      break;
    case "Cosign Dirjen":
      status = 7;
      break;
    case "Penelitian Dirjen Eselon 4":
      status = 8;
      break;
    case "Penelitian Dirjen Eselon 3":
      status = 9;
      break;
    case "Penetapan":
      status = 10;
      break;
    case "Terima":
      status = 11;
      break;
    default:
      status = 12;
  }
  const getLabelCssClasses = () => {
    // if (row.status === "Terima") {
    //   return "";
    // } else {
      return `label label-lg label-light-${CustomerStatusCssClasses[status]} label-inline`;
    // }
  };
  return (
    <span className={getLabelCssClasses()}>{CustomerStatusTitles[status]}</span>
  );
}

export function StatusColumnFormatterComposeDrafting(cellContent, row) {
  const CustomerStatusCssClasses = ["warning", "success", "danger", ""];
  const CustomerStatusTitles = ["Eselon 4", "Draft", "Tolak", ""];
  let status = "";

  switch (row.status) {
    case "Eselon 4":
      status = 0;
      break;

    case "Draft":
      status = 1;
      break;

    case "Tolak":
      status = 2;
      break;

    default:
      status = 3;
  }
  const getLabelCssClasses = () => {
    if (row.status === "Terima") {
      return "";
    } else {
      return `label label-lg label-light-${CustomerStatusCssClasses[status]} label-inline`;
    }
  };
  return (
    <span className={getLabelCssClasses()}>{CustomerStatusTitles[status]}</span>
  );
}

export function StatusColumnFormatterComposeDetilSurat(cellContent, row) {
  const CustomerStatusCssClasses = ["success", "primary", "danger", ""];
  const CustomerStatusTitles = ["Proses Drafting", "Proses Penyusunan", ""];
  let status = "";

  switch (row.status) {
    case "draft":
      status = 0;
      break;

    case "penyusunan":
      status = 1;
      break;

    default:
      status = 2;
  }
  const getLabelCssClasses = () => {
    if (row.status === "Terima") {
      return "";
    } else {
      return `label label-lg label-light-${CustomerStatusCssClasses[status]} label-inline`;
    }
  };
  return (
    <span className={getLabelCssClasses()}>{CustomerStatusTitles[status]}</span>
  );
}
export function StatusColumnFormatterComposeDetilLhr(cellContent, row) {
  const CustomerStatusCssClasses = ["success", "primary", "danger", ""];
  const CustomerStatusTitles = ["Proses Drafting", "Proses Penyusunan", ""];
  let status = "";

  switch (row.status) {
    case "draft":
      status = 0;
      break;

    case "penyusunan":
      status = 1;
      break;

    default:
      status = 2;
  }
  const getLabelCssClasses = () => {
    if (row.status === "Terima") {
      return "";
    } else {
      return `label label-lg label-light-${CustomerStatusCssClasses[status]} label-inline`;
    }
  };
  return (
    <span className={getLabelCssClasses()}>{CustomerStatusTitles[status]}</span>
  );
}
export function StatusColumnFormatterComposeDetilPeraturanTerkait(
  cellContent,
  row
) {
  const CustomerStatusCssClasses = ["success", "primary", "danger", ""];
  const CustomerStatusTitles = ["Proses Drafting", "Proses Penyusunan", ""];
  let status = "";

  switch (row.status) {
    case "draft":
      status = 0;
      break;

    case "penyusunan":
      status = 1;
      break;

    default:
      status = 2;
  }
  const getLabelCssClasses = () => {
    if (row.status === "Terima") {
      return "";
    } else {
      return `label label-lg label-light-${CustomerStatusCssClasses[status]} label-inline`;
    }
  };
  return (
    <span className={getLabelCssClasses()}>{CustomerStatusTitles[status]}</span>
  );
}

export function StatusColumnFormatterComposeProcessCosignDjp(cellContent, row) {
  const CustomerStatusCssClasses = ["warning", "success", "danger", ""];
  const CustomerStatusTitles = ["Eselon 4", "Draft", "Tolak", ""];
  let status = "";

  switch (row.status) {
    case "Eselon 4":
      status = 0;
      break;

    case "Draft":
      status = 1;
      break;

    case "Tolak":
      status = 2;
      break;

    default:
      status = 3;
  }
  const getLabelCssClasses = () => {
    if (row.status === "Terima") {
      return "";
    } else {
      return `label label-lg label-light-${CustomerStatusCssClasses[status]} label-inline`;
    }
  };
  return (
    <span className={getLabelCssClasses()}>{CustomerStatusTitles[status]}</span>
  );
}
export function StatusColumnFormatterComposeProcessTindakLanjutDirektoratKajian(
  cellContent,
  row
) {
  const CustomerStatusCssClasses = [
    "warning",
    "success",
    "danger",
    "primary",
    "primary",
    "primary",
    "primary",
    "primary",
    "primary",
    "primary",
    "primary",
    "primary",
    "",
  ];
  const CustomerStatusTitles = [
    "Eselon 4",
    "Draft",
    "Tolak",
    "Proses Penyusunan",
    "Penelitian Cosign Unit/KL Es4",
    "Cosign Sahli",
    "Penelitian Cosign Sahli",
    "Cosign Dirjen",
    "Penelitian Dirjen Eselon 4",
    "Penelitian Dirjen Eselon 3",
    "Penetapan",
    "Terima",
    "",
  ];
  let status = "";

  switch (row.status) {
    case "Eselon 4":
      status = 0;
      break;

    case "Draft":
      status = 1;
      break;

    case "Tolak":
      status = 2;
      break;

    case "Proses Penyusunan":
      status = 3;
      break;
    case "Penelitian Cosign Unit/KL Es4":
      status = 4;
      break;
    case "Cosign Sahli":
      status = 5;
      break;
    case "Penelitian Cosign Sahli":
      status = 6;
      break;
    case "Cosign Dirjen":
      status = 7;
      break;
    case "Penelitian Dirjen Eselon 4":
      status = 8;
      break;
    case "Penelitian Dirjen Eselon 3":
      status = 9;
      break;
    case "Penetapan":
      status = 10;
      break;
    case "Terima":
        status = 11;
        break;

    default:
      status = 12;
  }
  const getLabelCssClasses = () => {
    // if (row.status === "Terima") {
    //   return "";
    // } else {
      return `label label-lg label-light-${CustomerStatusCssClasses[status]} label-inline`;
    // }
  };
  return (
    <span className={getLabelCssClasses()}>{CustomerStatusTitles[status]}</span>
  );
}

export function StatusColumnFormatterComposeProcessTindakLanjutDirektoratJawaban(
  cellContent,
  row
) {
  const CustomerStatusCssClasses = ["primary", "success", ""];
  const CustomerStatusTitles = ["Sistem", "Manual", ""];
  let status = "";

  switch (row.tipe_jawaban) {
    case "Sistem":
      status = 0;
      break;

    case "Manual":
      status = 1;
      break;

    default:
      status = 3;
  }
  const getLabelCssClasses = () => {
    return `label label-lg label-light-${CustomerStatusCssClasses[status]} label-inline`;
  };
  return (
    <span className={getLabelCssClasses()}>{CustomerStatusTitles[status]}</span>
  );
}

export function StatusColumnFormatterComposeProcessTindakLanjutSahliKajian(
  cellContent,
  row
) {
  const CustomerStatusCssClasses = ["warning", "success", "danger", ""];
  const CustomerStatusTitles = ["Eselon 4", "Draft", "Tolak", ""];
  let status = "";

  switch (row.status) {
    case "Eselon 4":
      status = 0;
      break;

    case "Draft":
      status = 1;
      break;

    case "Tolak":
      status = 2;
      break;

    default:
      status = 3;
  }
  const getLabelCssClasses = () => {
    if (row.status === "Terima") {
      return "";
    } else {
      return `label label-lg label-light-${CustomerStatusCssClasses[status]} label-inline`;
    }
  };
  return (
    <span className={getLabelCssClasses()}>{CustomerStatusTitles[status]}</span>
  );
}

export function StatusColumnFormatterComposeProcessPengajuanRancanganKajian(
  cellContent,
  row
) {
  const CustomerStatusCssClasses = ["warning", "primary", "danger", ""];
  const CustomerStatusTitles = ["Eselon 4", "Siap Diajukan", "Tolak", ""];
  let status = "";

  switch (row.status) {
    case "Eselon 4":
      status = 0;
      break;

    case "Siap Diajukan":
      status = 1;
      break;

    case "Tolak":
      status = 2;
      break;

    default:
      status = 3;
  }
  const getLabelCssClasses = () => {
    if (row.status === "Terima") {
      return "";
    } else {
      return `label label-lg label-light-${CustomerStatusCssClasses[status]} label-inline`;
    }
  };
  return (
    <span className={getLabelCssClasses()}>{CustomerStatusTitles[status]}</span>
  );
}
export function StatusColumnFormatterComposeBkf(cellContent, row) {
  const CustomerStatusCssClasses = ["warning", "success", "danger", ""];
  const CustomerStatusTitles = ["Eselon 4", "Draft", "Tolak", ""];
  let status = "";

  switch (row.status) {
    case "Eselon 4":
      status = 0;
      break;

    case "Draft":
      status = 1;
      break;

    case "Tolak":
      status = 2;
      break;

    default:
      status = 3;
  }
  const getLabelCssClasses = () => {
    if (row.status === "Terima") {
      return "";
    } else {
      return `label label-lg label-light-${CustomerStatusCssClasses[status]} label-inline`;
    }
  };
  return (
    <span className={getLabelCssClasses()}>{CustomerStatusTitles[status]}</span>
  );
}
