// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";

export function ProposalActionsColumnFormatter(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog, showProposal, applyProposal, showReject }
) {

  const checkStatus = (status) => {
    switch (status) {
      case "Eselon 4":
        return (
          <>
            <a
              title="Show Status"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showProposal(row.id_usulan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                />
              </span>
            </a>
          </>
        );
        break;

      case "Draft":
        return (
          <>
            <a
              title="Edit Proposal"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openEditDialog(row.id_usulan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                />
              </span>
            </a>
            <> </>

            <a
              title="Hapus Proposal"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() => openDeleteDialog(row.id)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                />
              </span>
            </a>
            <> </>
            <a
              title="Ajukan Proposal"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => applyProposal(row.id_usulan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")}
                />
              </span>
            </a>
          </>
        );
        break;

      case "Tolak":
        return (
          <>
            <a
              title="Show Reject"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showReject(row.id_usulan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                />
              </span>
            </a>
            <> </>
            <a
              title="Edit Proposal"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openEditDialog(row.id_usulan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                />
              </span>
            </a>
            <> </>
          </>
        );
        break;

      default:
        break;
    }
  };

  return <>{checkStatus(row.status)}</>;
}
