// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import { lowerCase } from "lodash";

export function ActionsColumnFormatterProcessKnowledgeSuccessStoryRefrensi(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog }
) {
  return (
    <>
      {/* <a
              title="Edit Refrensi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openEditDialog(row.id_refrensi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </> */}

      <a
        title="Hapus Refrensi"
        className="btn btn-icon btn-light btn-hover-danger btn-sm"
        onClick={() => openDeleteDialog(row.id_refrensi)}
      >
        <span className="svg-icon svg-icon-md svg-icon-danger">
          {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
          <i className="fas fa-trash text-danger"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterProcessKnowledgeSuccessStoryPranalaLuar(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog }
) {
  return (
    <>
      <a
        title="Edit Refrensi"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => openEditDialog(row.id_refrensi)}
      >
        <span className="svg-icon svg-icon-md svg-icon-primary">
          {/* <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                /> */}
          <i className="fas fa-edit text-primary"></i>
        </span>
      </a>
      <> </>

      <a
        title="Hapus Refrensi"
        className="btn btn-icon btn-light btn-hover-danger btn-sm"
        onClick={() => openDeleteDialog(row.id_refrensi)}
      >
        <span className="svg-icon svg-icon-md svg-icon-danger">
          {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
          <i className="fas fa-trash text-danger"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterProcessKnowledgeSuccessStoryPeraturanTerkait(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog }
) {
  return (
    <>
      <a
        title="Hapus Peraturan Terkait"
        className="btn btn-icon btn-light btn-hover-danger btn-sm"
        onClick={() => openDeleteDialog(row.id_peraturan_terkait)}
      >
        <span className="svg-icon svg-icon-md svg-icon-danger">
          {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
          <i className="fas fa-trash text-danger"></i>
        </span>
      </a>
    </>
  );
}

// export function ActionsColumnFormatterProcessKnowledgeSuccessStoryLevel(
//   cellContent,
//   row,
//   rowIndex,
//   { openEditDialog, openDeleteDialog }
// ) {
//   return (
//     <>
//       <a
//         title="Edit Level"
//         className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
//         onClick={() => openEditDialog(row.id_level_detil)}
//       >
//         <span className="svg-icon svg-icon-md svg-icon-primary">
//           {/* <SVG
//                   src={toAbsoluteUrl(
//                     "/media/svg/icons/Communication/Write.svg"
//                   )}
//                 /> */}
//           <i className="fas fa-edit text-primary"></i>
//         </span>
//       </a>
//       <> </>

//       <a
//         title="Hapus Referensi"
//         className="btn btn-icon btn-light btn-hover-danger btn-sm"
//         onClick={() => openDeleteDialog(row.id_level_detil)}
//       >
//         <span className="svg-icon svg-icon-md svg-icon-danger">
//           {/* <SVG
//                   src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
//                 /> */}
//           <i className="fas fa-trash text-danger"></i>
//         </span>
//       </a>
//     </>
//   );
// }

export function ActionsColumnFormatterProcessKnowledgeSuccessStoryUploadLampiran(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog }
) {
  return (
    <>
      {/* <a
              title="Edit Lampiran"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openEditDialog(row.id_upd_terkait)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </> */}

      <a
        title="Hapus Lampiran"
        className="btn btn-icon btn-light btn-hover-danger btn-sm"
        onClick={() => openDeleteDialog(row.id_upd_terkait)}
      >
        <span className="svg-icon svg-icon-md svg-icon-danger">
          {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
          <i className="fas fa-trash text-danger"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterProcessKnowledgeSuccessStoryTugasFungsi(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog }
) {
  return (
    <>
      <a
        title="Hapus Tugas dan Fungsi"
        className="btn btn-icon btn-light btn-hover-danger btn-sm"
        onClick={() => openDeleteDialog(row.id_tusi_kn)}
      >
        <span className="svg-icon svg-icon-md svg-icon-danger">
          {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
          <i className="fas fa-trash text-danger"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterProcessKnowledgeSuccessStoryUnitOrganisasi(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog }
) {
  return (
    <>
      <a
              title="Edit Unit Organisasi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openEditDialog(row.id_unit_org)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                {/* <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                /> */}
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>

            <a
              title="Hapus Unit Organisasi"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() => openDeleteDialog(row.id_unit_org)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
    </>
  );
}

export function ActionsColumnFormatterProcessKnowledgeSuccessStoryReferensi(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog }
) {
  return (
    <>
      <a
        title="Edit Referensi"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => openEditDialog(row.id_refrensi)}
      >
        <span className="svg-icon svg-icon-md svg-icon-primary">
          {/* <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                /> */}
          <i className="fas fa-edit text-primary"></i>
        </span>
      </a>
      <> </>

      <a
        title="Hapus Referensi"
        className="btn btn-icon btn-light btn-hover-danger btn-sm"
        onClick={() => openDeleteDialog(row.id_refrensi)}
      >
        <span className="svg-icon svg-icon-md svg-icon-danger">
          {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
          <i className="fas fa-trash text-danger"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterProcessKnowledgeAdded(
  cellContent,
  row,
  rowIndex,
  {
    openEditDialog,
    openDeleteDialog,
    showProposal,
    applyProposal,
    reviewProposal,
    showReview,
  }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Review PKP Tacit":
        return (
          <>
            <a
              title="Review Knowledge"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() =>
                showReview(
                  row.id_km_pro,
                  row.id_tipe_km
                )
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );

      case "Review PKP Explicit":
        return (
          <>
            <a
              title="Review Knowledge"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() =>
                showReview(
                  row.id_km_pro,
                  row.id_tipe_km
                )
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );

      case "Review KO Tacit":
        return (
          <>
            <a
              title="Review Knowledge"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() =>
                showReview(
                  row.id_km_pro,
                  row.id_tipe_km
                )
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
        case "Review KO Explicit":
          return (
            <>
              <a
                title="Review Knowledge"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() =>
                  showReview(
                    row.id_km_pro,
                    row.id_tipe_km
                  )
                }
              >
                <span className="svg-icon svg-icon-md svg-icon-dark">
                  <i className="fas fa-eye text-dark"></i>
                </span>
              </a>
            </>
          );

      case "Review SME Tacit":
        return (
          <>
            <a
              title="Review Knowledge"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() =>
                showReview(
                  row.id_km_pro,
                  row.id_tipe_km
                )
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );

      case "Draft Knowledge":
        return (
          <>
            <a
              title="Edit Knowledge"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() =>
                openEditDialog(
                  row.id_km_pro,
                  row.id_tipe_km
                )
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                {/* <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                /> */}
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>

            <a
              title="Hapus Knowledge"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() =>
                openDeleteDialog(
                  row.id_km_pro
                  // row.knowledgeProses.id_tipe_km
                )
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
            <> </>
            <a
              title="Ajukan Knowledge"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() =>
                applyProposal(
                  row.id_km_pro,
                  row.jenis
                )
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")}
                /> */}
                <i className="fas fa-check text-success"></i>
              </span>
            </a>
          </>
        );

      case "Tolak":
        return (
          <>
            <a
              title="Show Data"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showReview(
                row.id_km_pro,
                row.id_tipe_km
                )}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
            {/* <> </>
            <a
              title="Edit Perencanaan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openEditDialog(row.id_perencanaan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </> */}
          </>
        );
        case "Publish Knowledge":
        return (
          <>
            <a
              title="Review Knowledge"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() =>
                showReview(
                  row.id_km_pro,
                  row.id_tipe_km
                )
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
        case "Telah Review Knowledge":
          return (
            <>
              <a
                title="Review Knowledge"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() =>
                  showReview(
                    row.id_km_pro,
                    row.id_tipe_km
                  )
                }
              >
                <span className="svg-icon svg-icon-md svg-icon-dark">
                  <i className="fas fa-eye text-dark"></i>
                </span>
              </a>
            </>
          );
          case "Siap Publish PKP Tacit":
            return (
              <>
                <a
                  title="Review Knowledge"
                  className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                  onClick={() =>
                    showReview(
                      row.id_km_pro,
                      row.id_tipe_km
                    )
                  }
                >
                  <span className="svg-icon svg-icon-md svg-icon-dark">
                    <i className="fas fa-eye text-dark"></i>
                  </span>
                </a>
              </>
            );
            case "Siap Publish PKP Explicit":
              return (
                <>
                  <a
                    title="Review Knowledge"
                    className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                    onClick={() =>
                      showReview(
                        row.id_km_pro,
                        row.id_tipe_km
                      )
                    }
                  >
                    <span className="svg-icon svg-icon-md svg-icon-dark">
                      <i className="fas fa-eye text-dark"></i>
                    </span>
                  </a>
                </>
              );

      default:
        break;
    }
  };

  return <>{checkStatus(row.statusKm.nama)}</>;
}

export function ActionsColumnFormatterProcessKnowledgeSimpleAdded(
  cellContent,
  row,
  rowIndex,
  {
    openEditDialog,
    openDeleteDialog,
    showProposal,
    publishKnowledge,
    reviewProposal,
    showReview,
  }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Review PKP Tacit":
        return (
          <>
            <a
              title="Review Knowledge"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() =>
                showReview(
                  row.id_km_pro,
                  row.id_tipe_km
                )
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );

      case "Review PKP Explicit":
        return (
          <>
            <a
              title="Review Knowledge"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() =>
                showReview(
                  row.id_km_pro,
                  row.id_tipe_km
                )
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );

      case "Review KO Tacit":
        return (
          <>
            <a
              title="Review Knowledge"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() =>
                showReview(
                  row.id_km_pro,
                  row.id_tipe_km
                )
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
        case "Review KO Explicit":
          return (
            <>
              <a
                title="Review Knowledge"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() =>
                  showReview(
                    row.id_km_pro,
                    row.id_tipe_km
                  )
                }
              >
                <span className="svg-icon svg-icon-md svg-icon-dark">
                  <i className="fas fa-eye text-dark"></i>
                </span>
              </a>
            </>
          );

      case "Review SME Tacit":
        return (
          <>
            <a
              title="Review Knowledge"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() =>
                showReview(
                  row.id_km_pro,
                  row.id_tipe_km
                )
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );

      case "Draft Knowledge":
        return (
          <>
            <a
              title="Edit Knowledge"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() =>
                openEditDialog(
                  row.id_km_pro,
                  row.id_tipe_km
                )
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                {/* <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                /> */}
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>

            <a
              title="Hapus Knowledge"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() =>
                openDeleteDialog(
                  row.id_km_pro
                  // row.knowledgeProses.id_tipe_km
                )
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
            <> </>
            <a
              title="Publish Knowledge"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() =>
                publishKnowledge(
                  row.id_km_pro
                )
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")}
                /> */}
                <i className="fab fa-telegram-plane text-success"></i>
              </span>
            </a>
          </>
        );

      case "Tolak":
        return (
          <>
            <a
              title="Show Data"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showReview(
                row.id_km_pro,
                row.id_tipe_km
                )}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
            {/* <> </>
            <a
              title="Edit Perencanaan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openEditDialog(row.id_perencanaan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </> */}
          </>
        );
        case "Publish Knowledge":
        return (
          <>
            <a
              title="Review Knowledge"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() =>
                showReview(
                  row.id_km_pro,
                  row.id_tipe_km
                )
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
        case "Telah Review Knowledge":
          return (
            <>
              <a
                title="Review Knowledge"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() =>
                  showReview(
                    row.id_km_pro,
                    row.id_tipe_km
                  )
                }
              >
                <span className="svg-icon svg-icon-md svg-icon-dark">
                  <i className="fas fa-eye text-dark"></i>
                </span>
              </a>
            </>
          );
          case "Siap Publish PKP Tacit":
            return (
              <>
                <a
                  title="Review Knowledge"
                  className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                  onClick={() =>
                    showReview(
                      row.id_km_pro,
                      row.id_tipe_km
                    )
                  }
                >
                  <span className="svg-icon svg-icon-md svg-icon-dark">
                    <i className="fas fa-eye text-dark"></i>
                  </span>
                </a>
              </>
            );
            case "Siap Publish PKP Explicit":
              return (
                <>
                  <a
                    title="Review Knowledge"
                    className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                    onClick={() =>
                      showReview(
                        row.id_km_pro,
                        row.id_tipe_km
                      )
                    }
                  >
                    <span className="svg-icon svg-icon-md svg-icon-dark">
                      <i className="fas fa-eye text-dark"></i>
                    </span>
                  </a>
                </>
              );

      default:
        break;
    }
  };

  return <>{checkStatus(row.statusKm.nama)}</>;
}

export function ActionsColumnFormatterSearchKnowledge(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDetilDialog }
) {
  return (
    <>
      {/* <a
              title="Edit Refrensi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openEditDialog(row.id_refrensi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </> */}

      <a
        title="Open Detil"
        className="btn btn-icon btn-light btn-hover-dark btn-sm"
        onClick={() => openDetilDialog(row.id, row.tipeKnowledge)}
      >
        <span className="svg-icon svg-icon-md svg-icon-dark">
          {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
          <i className="fas fa-eye text-dark"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterReviewUpdate(
  cellContent,
  row,
  rowIndex,
  {
    openEditDialog,
    openDeleteDialog,
    showProposal,
    applyProposal,
    reviewProposal,
    detilProposal,
    ajukanKnowledge,
    showReject,
  }
) {
  const checkStatus = (status) => {
    switch (status) {
     

      case "Update Review PKP Tacit":
        return (
          <>
            <a
              title="Review Knowledge"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() =>
                reviewProposal(
                  row.id_tipe_km,
                  row.id_km_pro,
                )
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
            src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
          /> */}
                <i className="fas fa-file-signature text-primary"></i>
              </span>
            </a>
          </>
        );

        case "Update Review PKP Explicit":
          return (
            <>
              <a
                title="Review Knowledge"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() =>
                  reviewProposal(
                  row.id_tipe_km,
                  row.id_km_pro,
                  )
                }
              >
                <span className="svg-icon svg-icon-md svg-icon-dark">
                  {/* <SVG
              src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
            /> */}
                  <i className="fas fa-file-signature text-primary"></i>
                </span>
              </a>
            </>
          );
          case "Update Review SME Tacit":
            return (
              <>
                <a
                  title="Review Knowledge"
                  className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                  onClick={() =>
                    detilProposal(
                    row.id_tipe_km,
                    row.id_km_pro,
                    )
                  }
                >
                  <span className="svg-icon svg-icon-md svg-icon-dark">
                    <i className="fas fa-eye text-dark"></i>
                  </span>
                </a>
              </>
            );
            case "Update Review KO Explicit":
              return (
                <>
                  <a
                    title="Review Knowledge"
                    className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                    onClick={() =>
                      detilProposal(
                      row.id_tipe_km,
                      row.id_km_pro,
                      )
                    }
                  >
                    <span className="svg-icon svg-icon-md svg-icon-dark">
                      <i className="fas fa-eye text-dark"></i>
                    </span>
                  </a>
                </>
              );
              
            case "Siap Publish Review PKP":
              return (
                <>
                  <a
                    title="Review Knowledge"
                    className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                    onClick={() =>
                      detilProposal(
                      row.id_tipe_km,
                      row.id_km_pro,
                      )
                    }
                  >
                    <span className="svg-icon svg-icon-md svg-icon-dark">
                      <i className="fas fa-eye text-dark"></i>
                    </span>
                  </a>
                  <> </>   
                  <a
                    title="Publish Knowledge"
                    className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                    onClick={() =>
                      ajukanKnowledge(
                      row.id_tipe_km,
                      row.id_km_pro,
                      )
                    }
                  >
                    <span className="svg-icon svg-icon-md svg-icon-success">
                      <i className="fas fa-check text-success"></i>
                    </span>
                  </a>
                </>
              );
              case "Update Review KO Tacit":
                return (
                  <>
                    <a
                      title="Review Knowledge"
                      className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                      onClick={() =>
                        detilProposal(
                        row.id_tipe_km,
                        row.id_km_pro,
                        )
                      }
                    >
                      <span className="svg-icon svg-icon-md svg-icon-dark">
                        <i className="fas fa-eye text-dark"></i>
                      </span>
                    </a>
                  </>
                );
      default:
        break;
    }
  };

  return <>{checkStatus(row.statusKm.nama)}</>;
}

export function ActionsColumnFormatterProcessKnowledgePKP(
  cellContent,
  row,
  rowIndex,
  { detailProposal, openDeleteDialog, showProposal, applyProposal, reviewProposal, showReject }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Review PKP Tacit":
        return (
          <>
            <a
              title="Review Knowledge"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => reviewProposal(
                row.id_km_pro, 
                "tacit",
                row.tipeKnowledge.template)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-file-signature text-primary"></i>
              </span>
            </a>
            {/* <a
              title="Ajukan Knowledge"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => applyProposal(row.id_km_pro)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark"> */}
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                {/* <i className="fas fa-check text-success"></i>
              </span>
            </a> */}
          </>
        );
      
        case "Review PKP Explicit":
          return (
            <>
              <a
                title="Review Knowledge"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() => reviewProposal(
                row.id_km_pro, 
                "explicit",
                row.tipeKnowledge.template)}
            >
                <span className="svg-icon svg-icon-md svg-icon-dark">
                  {/* <SVG
                    src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                  /> */}
                  <i className="fas fa-file-signature text-primary"></i>
                </span>
              </a>
              {/* <a
                title="Ajukan Knowledge"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() => applyProposal(row.id_km_pro)}
              >
                <span className="svg-icon svg-icon-md svg-icon-dark"> */}
                  {/* <SVG
                    src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                  /> */}
                  {/* <i className="fas fa-check text-success"></i>
                </span>
              </a> */}
            </>
          );

            case "Siap Publish PKP Tacit":
              return (
                <>
                  <a
                    title="Detail Knowledge"
                    className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                    onClick={() => detailProposal(
                      row.id_km_pro, 
                      "tacit",
                      row.tipeKnowledge.template)}
                  >
                    <span className="svg-icon svg-icon-md svg-icon-dark">
                      {/* <SVG
                        src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                      /> */}
                      <i className="fas fa-eye text-dark"></i>
                    </span>
                  </a>
                  <a
                    title="Publish Knowledge"
                    className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                    onClick={() => applyProposal(row.id_km_pro)}
                  >
                    <span className="svg-icon svg-icon-md svg-icon-dark">
                      {/* <SVG
                        src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                      /> */}
                      <i className="fas fa-check text-success"></i>
                    </span>
                  </a>
                </>
              );

              case "Siap Publish PKP Explicit":
              return (
                <>
                  <a
                    title="Detail Knowledge"
                    className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                    onClick={() => detailProposal(
                      row.id_km_pro, 
                      "explicit",
                      row.tipeKnowledge.template)}
                  >
                    <span className="svg-icon svg-icon-md svg-icon-dark">
                      {/* <SVG
                        src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                      /> */}
                      <i className="fas fa-eye text-dark"></i>
                    </span>
                  </a>
                  <a
                    title="Publish Knowledge"
                    className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                    onClick={() => applyProposal(row.id_km_pro)}
                  >
                    <span className="svg-icon svg-icon-md svg-icon-dark">
                      {/* <SVG
                        src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                      /> */}
                      <i className="fas fa-check text-success"></i>
                    </span>
                  </a>
                </>
              );

              default:
                  break;
    }
    
  };

  return <>{checkStatus(row.statusKm.nama)}</>;
}

export function ActionsColumnFormatterProcessKnowledgeKO(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog, showProposal, applyProposal, reviewProposal, showReject }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Review KO Tacit":
        return (
          <>
            <a
              title="Review Knowledge"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => reviewProposal(
                row.id_km_pro, 
                "tacit",
                row.tipeKnowledge.template)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-file-signature text-primary"></i>
              </span>
            </a>
            {/* <a
              title="Ajukan Knowledge"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => applyProposal(row.id_km_pro)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark"> */}
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                {/* <i className="fas fa-check text-success"></i>
              </span>
            </a> */}
          </>
        );
      
        case "Review KO Explicit":
          return (
            <>
              <a
                title="Review Knowledge"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() => reviewProposal(
                row.id_km_pro, 
                "explicit",
                row.tipeKnowledge.template)}
            >
                <span className="svg-icon svg-icon-md svg-icon-dark">
                  {/* <SVG
                    src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                  /> */}
                  <i className="fas fa-file-signature text-primary"></i>
                </span>
              </a>
              {/* <a
                title="Ajukan Knowledge"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() => applyProposal(row.id_km_pro)}
              >
                <span className="svg-icon svg-icon-md svg-icon-dark"> */}
                  {/* <SVG
                    src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                  /> */}
                  {/* <i className="fas fa-check text-success"></i>
                </span>
              </a> */}
            </>
          );
      default:
        break;
    }
    
  };

  return <>{checkStatus(row.statusKm.nama)}</>;
}

export function ActionsColumnFormatterProcessKnowledgeSME(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog, showProposal, applyProposal, reviewProposal, showReject }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Review SME Tacit":
        return (
          <>
            <a
              title="Review Knowledge"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => reviewProposal(
                row.id_km_pro, 
                "tacit",
                row.tipeKnowledge.template)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                <i className="fas fa-file-signature text-primary"></i>
              </span>
            </a>
            {/* <a
              title="Ajukan Knowledge"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => applyProposal(row.id_km_pro)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark"> */}
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                /> */}
                {/* <i className="fas fa-check text-success"></i>
              </span>
            </a> */}
          </>
        );
      
      default:
        break;
    }
    
  };

  return <>{checkStatus(row.statusKm.nama)}</>;
}

export function ActionsColumnFormatterProcessKnowledgeMonitoring(
  cellContent,
  row,
  rowIndex,
  { detailProposal }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Review PKP Tacit":
        return (
          <>
            <a
                title="Detail Knowledge"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() => detailProposal(
                  row.id_km_pro, 
                  "tacit",
                  row.tipeKnowledge.template)}
                >
                <span className="svg-icon svg-icon-md svg-icon-dark">
                  {/* <SVG
                    src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                  /> */}
                  <i className="fas fa-eye text-dark"></i>
                </span>
            </a>
          </>
        );
      
        case "Review PKP Explicit":
          return (
            <>
              <a
                title="Detail Knowledge"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() => detailProposal(
                  row.id_km_pro, 
                  "explicit",
                  row.tipeKnowledge.template)}
                >
                <span className="svg-icon svg-icon-md svg-icon-dark">
                  {/* <SVG
                    src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                  /> */}
                  <i className="fas fa-eye text-dark"></i>
                </span>
            </a>
            </>
          );
          case "Review KO Tacit":
            return (
              <>
                <a
                title="Detail Knowledge"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() => detailProposal(
                  row.id_km_pro, 
                  "tacit",
                  row.tipeKnowledge.template)}
                >
                <span className="svg-icon svg-icon-md svg-icon-dark">
                  {/* <SVG
                    src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                  /> */}
                  <i className="fas fa-eye text-dark"></i>
                </span>
            </a>
              </>
            );
          
            case "Review KO Explicit":
              return (
                <>
                  <a
                title="Detail Knowledge"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() => detailProposal(
                  row.id_km_pro, 
                  "explicit",
                  row.tipeKnowledge.template)}
                >
                <span className="svg-icon svg-icon-md svg-icon-dark">
                  {/* <SVG
                    src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                  /> */}
                  <i className="fas fa-eye text-dark"></i>
                </span>
            </a>

                </>
              );

            case "Review SME Tacit":
            return (
              <>
                <a
                title="Detail Knowledge"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() => detailProposal(
                  row.id_km_pro, 
                  "tacit",
                  row.tipeKnowledge.template)}
                >
                <span className="svg-icon svg-icon-md svg-icon-dark">
                  {/* <SVG
                    src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                  /> */}
                  <i className="fas fa-eye text-dark"></i>
                </span>
            </a>
              </>
            );

            case "Siap Publish PKP Tacit":
              return (
                <>
                  <a
                    title="Detail Knowledge"
                    className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                    onClick={() => detailProposal(
                      row.id_km_pro, 
                      "tacit",
                      row.tipeKnowledge.template)}
                  >
                    <span className="svg-icon svg-icon-md svg-icon-dark">
                      {/* <SVG
                        src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                      /> */}
                      <i className="fas fa-eye text-dark"></i>
                    </span>
                  </a>
                </>
              );

              case "Siap Publish PKP Explicit":
              return (
                <>
                  <a
                    title="Detail Knowledge"
                    className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                    onClick={() => detailProposal(
                      row.id_km_pro, 
                      "explicit",
                      row.tipeKnowledge.template)}
                  >
                    <span className="svg-icon svg-icon-md svg-icon-dark">
                      {/* <SVG
                        src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                      /> */}
                      <i className="fas fa-eye text-dark"></i>
                    </span>
                  </a>
                </>
              );

              case "Publish Knowledge":
              return (
                <>
                  <a
                    title="Detail Knowledge"
                    className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                    onClick={() => detailProposal(
                      row.id_km_pro, 
                      lowerCase(row.jenis),
                      row.tipeKnowledge.template)}
                  >
                    <span className="svg-icon svg-icon-md svg-icon-dark">
                      {/* <SVG
                        src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                      /> */}
                      <i className="fas fa-eye text-dark"></i>
                    </span>
                  </a>
                </>
              );

              default:
                  break;
    }
    
  };

  return <>{checkStatus(row.statusKm.nama)}</>;
}

export function AddKnowledgeReferensiColumnFormatter(cellContent, row) {
  return (
    <a href={row.link} target="_blank" rel="noopener noreferrer">{row.link}</a>
  );
}

export function ActionsColumnFormatterReviewUpdateKo(
  cellContent,
  row,
  rowIndex,
  {
    reviewProposal,
    detilProposal
  }
) {
  const checkStatus = (status) => {
    switch (status) {

      case "Update Review PKP Tacit":
        return (
          <>
            <a
              title="Review Knowledge"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() =>
                detilProposal(
                  row.id_tipe_km,
                  row.id_km_pro,
                )
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );

        case "Update Review PKP Explicit":
          return (
            <>
              <a
                title="Review Knowledge"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() =>
                  detilProposal(
                  row.id_tipe_km,
                  row.id_km_pro,
                  )
                }
              >
                <span className="svg-icon svg-icon-md svg-icon-dark">
                  <i className="fas fa-eye text-dark"></i>
                </span>
              </a>
            </>
          );
          case "Update Review KO Tacit":
            return (
              <>
                <a
                  title="Review Knowledge"
                  className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                  onClick={() =>
                    reviewProposal(
                    row.id_tipe_km,
                    row.id_km_pro,
                    )
                  }
                >
                  <span className="svg-icon svg-icon-md svg-icon-dark">
                    <i className="fas fa-file-signature text-success"></i>
                  </span>
                </a>
              </>
            );
            case "Update Review KO Explicit":
              return (
                <>
                  <a
                    title="Review Knowledge"
                    className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                    onClick={() =>
                      reviewProposal(
                      row.id_tipe_km,
                      row.id_km_pro,
                      )
                    }
                  >
                    <span className="svg-icon svg-icon-md svg-icon-dark">
                      <i className="fas fa-file-signature text-success"></i>
                    </span>
                  </a>
                </>
              );
              case "Update Review SME Tacit":
                return (
                  <>
                    <a
                      title="Detil Knowledge"
                      className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                      onClick={() =>
                        detilProposal(
                        row.id_tipe_km,
                        row.id_km_pro,
                        )
                      }
                    >
                      <span className="svg-icon svg-icon-md svg-icon-dark">
                        <i className="fas fa-eye text-dark"></i>
                      </span>
                    </a>
                  </>
                );
      default:
        break;
    }
  };

  return <>{checkStatus(row.statusKm.nama)}</>;
}

export function ActionsColumnFormatterProcessKnowledgeMonitoringReview(
  cellContent,
  row,
  rowIndex,
  { detailProposal }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Telah Review Knowledge":
        return (
          <>
            <a
                title="Detail Review"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() => detailProposal(
                  row.id_km_pro, 
                  lowerCase(row.tipeKnowledge.jenis),
                  row.tipeKnowledge.template)}
                >
                <span className="svg-icon svg-icon-md svg-icon-dark">
                  {/* <SVG
                    src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                  /> */}
                  <i className="fas fa-eye text-dark"></i>
                </span>
            </a>
          </>
        );
      
        case "Siap Review Knowledge":
          return (
            <>
              <a
                title="Detail Review"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() => detailProposal(
                  row.id_km_pro, 
                  lowerCase(row.tipeKnowledge.jenis),
                  row.tipeKnowledge.template)}
                >
                <span className="svg-icon svg-icon-md svg-icon-dark">
                  {/* <SVG
                    src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                  /> */}
                  <i className="fas fa-eye text-dark"></i>
                </span>
            </a>
            </>
          );
          case "Update Review PKP Tacit":
            return (
              <>
                <a
                title="Detail Review"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() => detailProposal(
                  row.id_km_pro, 
                  "tacit",
                  row.tipeKnowledge.template)}
                >
                <span className="svg-icon svg-icon-md svg-icon-dark">
                  {/* <SVG
                    src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                  /> */}
                  <i className="fas fa-eye text-dark"></i>
                </span>
            </a>
              </>
            );
          
            case "Update Review PKP Explicit":
              return (
                <>
                  <a
                title="Detail Review"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() => detailProposal(
                  row.id_km_pro, 
                  "explicit",
                  row.tipeKnowledge.template)}
                >
                <span className="svg-icon svg-icon-md svg-icon-dark">
                  {/* <SVG
                    src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                  /> */}
                  <i className="fas fa-eye text-dark"></i>
                </span>
            </a>
                </>
              );

            case "Update Review KO Tacit":
            return (
              <>
                <a
                title="Detail Review"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() => detailProposal(
                  row.id_km_pro, 
                  "tacit",
                  row.tipeKnowledge.template)}
                >
                <span className="svg-icon svg-icon-md svg-icon-dark">
                  {/* <SVG
                    src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                  /> */}
                  <i className="fas fa-eye text-dark"></i>
                </span>
            </a>
              </>
            );

            case "Update Review KO Explicit":
              return (
                <>
                  <a
                    title="Detail Review"
                    className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                    onClick={() => detailProposal(
                      row.id_km_pro, 
                      "explicit",
                      row.tipeKnowledge.template)}
                  >
                    <span className="svg-icon svg-icon-md svg-icon-dark">
                      {/* <SVG
                        src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                      /> */}
                      <i className="fas fa-eye text-dark"></i>
                    </span>
                  </a>
                </>
              );

              case "Update Review SME Tacit":
              return (
                <>
                  <a
                    title="Detail Review"
                    className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                    onClick={() => detailProposal(
                      row.id_km_pro, 
                      "tacit",
                      row.tipeKnowledge.template)}
                  >
                    <span className="svg-icon svg-icon-md svg-icon-dark">
                      {/* <SVG
                        src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                      /> */}
                      <i className="fas fa-eye text-dark"></i>
                    </span>
                  </a>
                </>
              );
            
              case "Siap Publish Review PKP":
              return (
                <>
                  <a
                    title="Detail Review"
                    className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                    onClick={() => detailProposal(
                      row.id_km_pro, 
                      lowerCase(row.tipeKnowledge.jenis),
                      row.tipeKnowledge.template)}
                  >
                    <span className="svg-icon svg-icon-md svg-icon-dark">
                      {/* <SVG
                        src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                      /> */}
                      <i className="fas fa-eye text-dark"></i>
                    </span>
                  </a>
                </>
              );

              default:
                  break;
    }
  };

  return <>{checkStatus(row.statusKm.nama)}</>;
}

export function ActionsColumnFormatterProcessKnowledgeFeedback(
  cellContent,
  row,
  rowIndex,
  { editProposal, openDeleteDialog }
) {
  return (
    <>
      <a
              title="Edit Feedback"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => editProposal(row.id_feedback)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                {/* <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                /> */}
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>
    </>
  );
}

export function AddKnowledgeUploadLampiranColumnFormatter(cellContent, row) {
  const { URL_DOWNLOAD } = window.ENV;
  return (
    <a href={URL_DOWNLOAD + row.link} target="_blank" rel="noopener noreferrer">{row.link}</a>
  );
}

export function ActionsColumnFormatterProcessKnowledgeSuccessStoryLevel(
  cellContent,
  row,
  rowIndex,
  { openDeleteDialog }
) {
  return (
    <>
            <a
              title="Hapus Level"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() => openDeleteDialog(row.id_level_detil)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
    </>
  );
}

export function ActionsColumnFormatterProcessKnowledgeSopDasarHukum(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog }
) {
  return (
    <>
      {/* <a
              title="Edit Dasar Hukum"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openEditDialog(row.id_dasar_hukum)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </> */}

            <a
              title="Hapus Dasar Hukum"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() => openDeleteDialog(row.id_dasar_hukum)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
    </>
  );
}

export function ActionsColumnFormatterReviewUpdateSme(
  cellContent,
  row,
  rowIndex,
  {
    reviewProposal,
    detilProposal
  }
) {
  const checkStatus = (status) => {
    switch (status) {

      case "Update Review PKP Tacit":
        return (
          <>
            <a
              title="Review Knowledge"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() =>
                detilProposal(
                  row.id_tipe_km,
                  row.id_km_pro,
                )
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );

        case "Update Review PKP Explicit":
          return (
            <>
              <a
                title="Review Knowledge"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() =>
                  detilProposal(
                  row.id_tipe_km,
                  row.id_km_pro,
                  )
                }
              >
                <span className="svg-icon svg-icon-md svg-icon-dark">
                  <i className="fas fa-eye text-dark"></i>
                </span>
              </a>
            </>
          );
          case "Update Review KO Tacit":
            return (
              <>
                <a
                  title="Review Knowledge"
                  className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                  onClick={() =>
                    reviewProposal(
                    row.id_tipe_km,
                    row.id_km_pro,
                    )
                  }
                >
                  <span className="svg-icon svg-icon-md svg-icon-dark">
                    <i className="fas fa-file-signature text-success"></i>
                  </span>
                </a>
              </>
            );
            case "Update Review KO Explicit":
              return (
                <>
                  <a
                    title="Review Knowledge"
                    className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                    onClick={() =>
                      reviewProposal(
                      row.id_tipe_km,
                      row.id_km_pro,
                      )
                    }
                  >
                    <span className="svg-icon svg-icon-md svg-icon-dark">
                      <i className="fas fa-file-signature text-success"></i>
                    </span>
                  </a>
                </>
              );
              case "Update Review SME Tacit":
                return (
                  <>
                    <a
                      title="Review Knowledge"
                      className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                      onClick={() =>
                        reviewProposal(
                        row.id_tipe_km,
                        row.id_km_pro,
                        )
                      }
                    >
                      <span className="svg-icon svg-icon-md svg-icon-dark">
                        <i className="fas fa-file-signature text-success"></i>
                      </span>
                    </a>
                  </>
                );
      default:
        break;
    }
  };

  return <>{checkStatus(row.statusKm.nama)}</>;
}

export function ActionsColumnFormatterProcessKnowledgeLihatPula(
  cellContent,
  row,
  rowIndex,
  { openDeleteDialog }
) {
  return (
    <>
            <a
              title="Hapus Lihat Pula"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() => openDeleteDialog(row.id_lihatpula)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
    </>
  );
}

export function ActionsColumnFormatterProcessKnowledgeBacaanLanjutan(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog }
) {
  return (
    <>
      {/* <a
              title="Edit Bacaan Lanjutan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openEditDialog(row.id_bacaan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </> */}

            <a
              title="Hapus Bacaan Lanjutan"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() => openDeleteDialog(row.id_bacaan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
    </>
  );
}

export function ActionsColumnFormatterProcessKnowledgeRating(
  cellContent,
  row,
  rowIndex,
  { detailProposal }
) {
  const checkStatus = (status) => {
    switch (status) {
              case "Publish Knowledge":
              return (
                <>
                  <a
                title="Rating Knowledge"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() => detailProposal(
                  row.id_km_pro
                  )}
                >
                <span className="svg-icon svg-icon-md svg-icon-dark">
                  {/* <SVG
                    src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                  /> */}
                  <i className="far fa-star"></i>
                </span>
                  </a>
                </>
              );

            case "Telah Review Knowledge":
              return (
                <>
                  <a
                title="Rating Knowledge"
                className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
                onClick={() => detailProposal(
                  row.id_km_pro
                  )}
                >
                <span className="svg-icon svg-icon-md svg-icon-dark">
                  {/* <SVG
                    src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                  /> */}
                  <i className="far fa-star"></i>
                </span>
                  </a>
                </>
              );
              
              default:
                  break;
    }
    
  };

  return <>{checkStatus(row.statusKm.nama)}</>;
}


export function ActionsColumnFormatterProcessKnowledgeSuccessStoryMappingProbis(
  cellContent,
  row,
  rowIndex,
  { openDeleteDialog }
) {
  return (
    <>

      <a
        title="Hapus Probis"
        className="btn btn-icon btn-light btn-hover-danger btn-sm"
        onClick={() => openDeleteDialog(row.id_map_knowledge)}
      >
        <span className="svg-icon svg-icon-md svg-icon-danger">
          {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
          <i className="fas fa-trash text-danger"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterProcessKnowledgeSuccessStoryMappingCasename(
  cellContent,
  row,
  rowIndex,
  { openDeleteDialog }
) {
  return (
    <>

      <a
        title="Hapus Casename"
        className="btn btn-icon btn-light btn-hover-danger btn-sm"
        onClick={() => openDeleteDialog(row.id_mapping_casename)}
      >
        <span className="svg-icon svg-icon-md svg-icon-danger">
          {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
          <i className="fas fa-trash text-danger"></i>
        </span>
      </a>
    </>
  );
}
export function ActionsColumnFormatterProcessKnowledgeSuccessStoryMappingSubcase(
  cellContent,
  row,
  rowIndex,
  { openDeleteDialog }
) {
  return (
    <>

      <a
        title="Hapus Subcase"
        className="btn btn-icon btn-light btn-hover-danger btn-sm"
        onClick={() => openDeleteDialog(row.id_map_subcase)}
      >
        <span className="svg-icon svg-icon-md svg-icon-danger">
          {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
          <i className="fas fa-trash text-danger"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterProcessKnowledgeSuccessStoryMappingSektor(
  cellContent,
  row,
  rowIndex,
  { openDeleteDialog }
) {
  return (
    <>

      <a
        title="Hapus Sektor"
        className="btn btn-icon btn-light btn-hover-danger btn-sm"
        onClick={() => openDeleteDialog(row.id_map_sektor)}
      >
        <span className="svg-icon svg-icon-md svg-icon-danger">
          {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
          <i className="fas fa-trash text-danger"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterProcessKnowledgeMappingPeraturan(
  cellContent,
  row,
  rowIndex,
  {
    detailPeraturan,
    showHistory,
    editPeraturan
  }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Terima":
        return (
          <>
            {/* Show history proses Pengajuan */}
            {/* <a
              title="Status Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-warning">
                <i className="fas fa-stopwatch text-warning"></i>
              </span>
            </a>
            <a
              title="Detail Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailPeraturan(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a> */}
            <a
              title="Edit Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => editPeraturan(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
          </>
        );

      default:
        break;
    }
  };

  return <>{checkStatus(row.status)}</>;
}

export function ActionsColumnFormatterProcessKnowledgeMappingPeraturanTerkait(
  cellContent,
  row,
  rowIndex,
  { deleteDialog }
) {
  return (
    <>
      <a
        title="Hapus Peraturan"
        className="btn btn-icon btn-light btn-hover-danger btn-sm"
        onClick={() => deleteDialog(row.id_per_terkait)}
      >
        <span className="svg-icon svg-icon-md svg-icon-danger">
          <i className="fas fa-trash text-danger"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterProcessKnowledgeMappingPeraturanDokumenLainnya(
  cellContent,
  row,
  rowIndex,
  { deleteDialog }
) {
  return (
    <>
      <a
        title="Hapus Peraturan"
        className="btn btn-icon btn-light btn-hover-danger btn-sm"
        onClick={() => deleteDialog(row.id_km_dok_lainnya)}
      >
        <span className="svg-icon svg-icon-md svg-icon-danger">
          <i className="fas fa-trash text-danger"></i>
        </span>
      </a>
    </>
  );
}

export function ActionsColumnFormatterProcessKnowledgeUsulanTemplate(
  cellContent,
  row,
  rowIndex,
  {
    openEditDialog,
    openDeleteDialog,
    showProposal,
    publishKnowledge,
    reviewProposal,
    showReview,
  }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Review Pengajuan Template":
        return (
          <>
            <a
              title="Review Knowledge"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() =>
                showReview(
                  row.id_usulan_template,
                )
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );

      case "Draft Pengajuan Template":
        return (
          <>
            <a
              title="Edit Usulan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() =>
                openEditDialog(
                  row.id_usulan_template,
                )
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                {/* <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                /> */}
                <i className="fas fa-edit text-primary"></i>
              </span>
            </a>
            <> </>

            <a
              title="Hapus Usulan"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() =>
                openDeleteDialog(
                  row.id_usulan_template
                  // row.knowledgeProses.id_tipe_km
                )
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                /> */}
                <i className="fas fa-trash text-danger"></i>
              </span>
            </a>
            <> </>
            <a
              title="Publish Usulan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() =>
                publishKnowledge(
                  row.id_usulan_template
                )
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                {/* <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")}
                /> */}
                <i className="fab fa-telegram-plane text-success"></i>
              </span>
            </a>
          </>
        );

        case "Terima Pengajuan Template":
        return (
          <>
            <a
              title="Review Knowledge"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() =>
                showReview(
                  row.id_usulan_template,
                )
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );
      default:
        break;
    }
  };

  return <>{checkStatus(row.status)}</>;
}

export function ActionsColumnFormatterProcessKnowledgeReviewUsulanTemplate(
  cellContent,
  row,
  rowIndex,
  {
    openEditDialog,
    openDeleteDialog,
    showProposal,
    publishKnowledge,
    reviewProposal,
    showReview,
  }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Review Pengajuan Template":
        return (
          <>
            <a
              title="Review Knowledge"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() =>
                showReview(
                  row.id_usulan_template,
                )
              }
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <i className="fas fa-eye text-dark"></i>
              </span>
            </a>
          </>
        );

      default:
        break;
    }
  };

  return <>{checkStatus(row.status)}</>;
}