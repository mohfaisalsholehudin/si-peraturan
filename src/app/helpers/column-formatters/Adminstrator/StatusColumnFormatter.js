// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
import React from "react";

export function StatusColumnFormatterAdminJenisPajak(cellContent, row) {
  const CustomerStatusCssClasses = ["success", "warning", ""];
  const CustomerStatusTitles = ["AKTIF", "PASIF", ""];
  let status = "";

  switch (row.status) {
    case "AKTIF":
      status = 0;
      break;

    case "PASIF":
      status = 1;
      break;
      
    default:
      status = 2;
  }
  const getLabelCssClasses = () => {
    if(row.status === "Terima"){
      return '';
    } else {
      return `label label-lg label-light-${CustomerStatusCssClasses[status]} label-inline`;
    }
    
  };
  return (
    <span className={getLabelCssClasses()}>{CustomerStatusTitles[status]}</span>
  );
}

export function StatusColumnFormatterAdminJenisPeraturan(cellContent, row) {
  const CustomerStatusCssClasses = ["success", "warning", ""];
  const CustomerStatusTitles = ["AKTIF", "PASIF", ""];
  let status = "";

  switch (row.status) {
    case "AKTIF":
      status = 0;
      break;

    case "PASIF":
      status = 1;
      break;
      
    default:
      status = 2;
  }
  const getLabelCssClasses = () => {
    if(row.status === "Terima"){
      return '';
    } else {
      return `label label-lg label-light-${CustomerStatusCssClasses[status]} label-inline`;
    }
    
  };
  return (
    <span className={getLabelCssClasses()}>{CustomerStatusTitles[status]}</span>
  );
}
export function StatusColumnFormatterAdminSettingProbis(cellContent, row) {
  const CustomerStatusCssClasses = ["danger", "success", ""];
  const CustomerStatusTitles = ["TIDAK AKTIF", "AKTIF", ""];
  let status = "";

  switch (row.status) {
    case 0:
      status = 0;
      break;

    case 1:
      status = 1;
      break;
      
    default:
      status = 2;
  }
  const getLabelCssClasses = () => {
    if(row.status === "Terima"){
      return '';
    } else {
      return `label label-lg label-light-${CustomerStatusCssClasses[status]} label-inline`;
    }
    
  };
  return (
    <span className={getLabelCssClasses()}>{CustomerStatusTitles[status]}</span>
  );
}
export function StatusColumnFormatterAdminSettingSubcase(cellContent, row) {
  const CustomerStatusCssClasses = ["danger", "success", ""];
  const CustomerStatusTitles = ["TIDAK AKTIF", "AKTIF", ""];
  let status = "";

  switch (row.subCase.status) {
    case 0:
      status = 0;
      break;

    case 1:
      status = 1;
      break;
      
    default:
      status = 2;
  }
  const getLabelCssClasses = () => {
    if(row.status === "Terima"){
      return '';
    } else {
      return `label label-lg label-light-${CustomerStatusCssClasses[status]} label-inline`;
    }
    
  };
  return (
    <span className={getLabelCssClasses()}>{CustomerStatusTitles[status]}</span>
  );
}
export function StatusColumnFormatterAdminSettingProbisCaseName(cellContent, row) {
  const CustomerStatusCssClasses = ["danger", "success", ""];
  const CustomerStatusTitles = ["TIDAK AKTIF", "AKTIF", ""];
  let status = "";

  switch (row.caseName.status) {
    case 0:
      status = 0;
      break;

    case 1:
      status = 1;
      break;
      
    default:
      status = 2;
  }
  const getLabelCssClasses = () => {
    if(row.status === "Terima"){
      return '';
    } else {
      return `label label-lg label-light-${CustomerStatusCssClasses[status]} label-inline`;
    }
    
  };
  return (
    <span className={getLabelCssClasses()}>{CustomerStatusTitles[status]}</span>
  );
}
export function StatusColumnFormatterAdminSettingSektorBisnis(cellContent, row) {
  const CustomerStatusCssClasses = ["danger", "success", ""];
  const CustomerStatusTitles = [" TIDAK AKTIF", "AKTIF", ""];
  let status = "";

  switch (row.status) {
    case 0:
      status = 0;
      break;

    case 1:
      status = 1;
      break;
      
    default:
      status = 2;
  }
  const getLabelCssClasses = () => {
    if(row.status === "Terima"){
      return '';
    } else {
      return `label label-lg label-light-${CustomerStatusCssClasses[status]} label-inline`;
    }
    
  };
  return (
    <span className={getLabelCssClasses()}>{CustomerStatusTitles[status]}</span>
  );
}

export function StatusColumnFormatterAdminPengumuman(cellContent, row) {
  const CustomerStatusCssClasses = [
    "success",
    "primary",
    "danger",
    ""
  ];
  const CustomerStatusTitles = [
    "Draft",
    "Publish Aktif",
    "Publish Non Aktif",
    ""
    ];

  let status = "";

  switch (row.status) {
    case "Draft":
      status = 0;
      break;

    case "Publish Aktif":
      status = 1;
      break;

      case "Publish Non Aktif":
      status = 2;
      break;

    default:
      status = 3;
      break;
  }
  const getLabelCssClasses = () => {
    if (row.status === "") {
      return "";
    } else {
      return `label label-lg label-light-${CustomerStatusCssClasses[status]} label-inline`;
    }
  };
  return (
    <span className={getLabelCssClasses()}>{CustomerStatusTitles[status]}</span>
  );
}