// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */

import React from "react";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
const { SLICE_URL,SLICE_ZIP, DOWNLOAD_URL } = window.ENV;

export function FileColumnFormatterAdminSettingProbis(
  cellContent,
  row,
  rowIndex,
  { openCaseName }
) {
  return (
    <a
      title="Open Case Name"
      className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
      onClick={() => openCaseName(row.id_probis)}
    >
      <span className="svg-icon svg-icon-lg svg-icon-success">
        {/* <SVG src={toAbsoluteUrl("/media/svg/icons/Files/Selected-file.svg")} /> */}
        <i className="fas fa-file-alt text-success"></i>
      </span>
    </a>
  );
}

export function FileColumnFormatterAdminSettingCasename(
  cellContent,
  row,
  rowIndex,
  { openSubcase }
) {
  return (
    <a
      title="Open Case Name"
      className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
      onClick={() => openSubcase(row.caseName.id_case_name)}
    >
      <span className="svg-icon svg-icon-lg svg-icon-success">
        {/* <SVG src={toAbsoluteUrl("/media/svg/icons/Files/Selected-file.svg")} /> */}
        <i className="fas fa-file-alt text-success"></i>
      </span>
    </a>
  );
}
