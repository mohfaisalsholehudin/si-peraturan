// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";

export function DateFormatterKowner(cellContent, row, rowIndex) {
  const date = new Date(row.created);
  const tanggal = String(date.getDate()).padStart(2, "0");
  var bulan = String(date.getMonth() + 1).padStart(2, "0");
  const tahun = date.getFullYear();

  const tampilTanggal = tanggal + "-" + bulan + "-" + tahun;

  return (
    <>
      <div>{tampilTanggal}</div>
    </>
  );
}

export function DateFormatterProbis(cellContent, row, rowIndex) {


  // const tampilTanggal =
  //   tanggal + "-" + bulan + "-" + tahun + " - " + jam + ":" + menit;

  const tampilTanggal = (data) => {
    const date = new Date(data);
    const tanggal = String(date.getDate()).padStart(2, "0");
    var bulan = String(date.getMonth() + 1).padStart(2, "0");
    const tahun = date.getFullYear();
    const jam = date.getHours();
    const menit = (date.getMinutes() < 10 ? "0" : "") + date.getMinutes();
    return tanggal + "-" + bulan + "-" + tahun + " - " + jam + ":" + menit;

  }

  return (
    <>
      <div> {row.updated === null
          ? tampilTanggal(row.created)
          : tampilTanggal(row.updated)}</div>
    </>
  );
}

export function DateFormatterCaseName(cellContent, row, rowIndex) {
  //  const tampilTanggal = tanggal + "-" + bulan + "-" + tahun + " - " + jam + ":" + menit;

  const tampilTanggal = (data) => {
    const date = new Date(data);
    const tanggal = String(date.getDate()).padStart(2, "0");
    var bulan = String(date.getMonth() + 1).padStart(2, "0");
    const tahun = date.getFullYear();
    const jam = date.getHours();
    const menit = (date.getMinutes() < 10 ? "0" : "") + date.getMinutes();
    return tanggal + "-" + bulan + "-" + tahun + " - " + jam + ":" + menit;
  };

  return (
    <>
      <div>
        {row.caseName.updated === null
          ? tampilTanggal(row.caseName.created)
          : tampilTanggal(row.caseName.updated)}
      </div>
    </>
  );
}
export function DateFormatterSubcase(cellContent, row, rowIndex) {
  //  const tampilTanggal = tanggal + "-" + bulan + "-" + tahun + " - " + jam + ":" + menit;

  const tampilTanggal = (data) => {
    const date = new Date(data);
    const tanggal = String(date.getDate()).padStart(2, "0");
    var bulan = String(date.getMonth() + 1).padStart(2, "0");
    const tahun = date.getFullYear();
    const jam = date.getHours();
    const menit = (date.getMinutes() < 10 ? "0" : "") + date.getMinutes();
    return tanggal + "-" + bulan + "-" + tahun + " - " + jam + ":" + menit;
  };

  return (
    <>
      <div>
        {row.subCase.updated === null
          ? tampilTanggal(row.subCase.created)
          : tampilTanggal(row.subCase.updated)}
      </div>
    </>
  );
}

export function DateFormatterTambahKnowledge(cellContent, row, rowIndex) {


  // const tampilTanggal =
  //   tanggal + "-" + bulan + "-" + tahun + " - " + jam + ":" + menit;

  const tampilTanggal = (data) => {
    const date = new Date(data);
    const tanggal = String(date.getDate()).padStart(2, "0");
    var bulan = String(date.getMonth() + 1).padStart(2, "0");
    const tahun = date.getFullYear();
    const jam = date.getHours();
    const menit = (date.getMinutes() < 10 ? "0" : "") + date.getMinutes();
    return tanggal + "-" + bulan + "-" + tahun + " / " + jam + ":" + menit;

  }

  return (
    <>
      <div> {row.updated_at === null
          ? tampilTanggal(row.created_at)
          : tampilTanggal(row.updated_at)}</div>
    </>
  );
}

export function DateFormatterReviewPkp(cellContent, row, rowIndex) {


  // const tampilTanggal =
  //   tanggal + "-" + bulan + "-" + tahun + " - " + jam + ":" + menit;

  const tampilTanggal = (data) => {
    const date = new Date(data);
    const tanggal = String(date.getDate()).padStart(2, "0");
    var bulan = String(date.getMonth() + 1).padStart(2, "0");
    const tahun = date.getFullYear();
    const jam = date.getHours();
    const menit = (date.getMinutes() < 10 ? "0" : "") + date.getMinutes();
    return tanggal + "-" + bulan + "-" + tahun + " / " + jam + ":" + menit;

  }

  return (
    <>
      <div> {row.wkt_pkp === null ? "-" : tampilTanggal(row.wkt_pkp)}</div>
    </>
  );
}

export function DateFormatterReviewKo(cellContent, row, rowIndex) {


  // const tampilTanggal =
  //   tanggal + "-" + bulan + "-" + tahun + " - " + jam + ":" + menit;

  const tampilTanggal = (data) => {
    const date = new Date(data);
    const tanggal = String(date.getDate()).padStart(2, "0");
    var bulan = String(date.getMonth() + 1).padStart(2, "0");
    const tahun = date.getFullYear();
    const jam = date.getHours();
    const menit = (date.getMinutes() < 10 ? "0" : "") + date.getMinutes();
    return tanggal + "-" + bulan + "-" + tahun + " / " + jam + ":" + menit;

  }

  return (
    <>
      <div> {row.wkt_ko === null ? "-" :tampilTanggal(row.wkt_ko)}</div>
    </>
  );
}