import React from "react";
import {FieldFeedbackLabel} from "./FieldFeedbackLabel";

const getFieldCSSClasses = (touched, errors, solid) => {
  
  const classes = [""];
  if (touched && errors) {
    classes.push("is-invalid");
  }

  if (touched && !errors) {
    classes.push("is-valid");
  }

  if(solid === 'true') {
    classes.push('form-control-solid');
  }

  return classes.join(" ");
};

export function Radio({
  field, // { name, value, onChange, onBlur }
  form: { touched, errors, handleChange, values }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
  label,
  key,
  content,
  withFeedbackLabel = true,
  customFeedbackLabel,
  type = "radio",
  ...props
}) {
  return (
    <>
      {/* {label && <label className="col-xl-3 col-lg-3 col-form-label">{label}</label>} */}
      <label key={key} className='form-check-label' style={{marginLeft: '20px'}}>
      <input
        type={type}
        className={getFieldCSSClasses(touched[field.name], errors[field.name], props.solid)}
        checked={values[field.name]}
        onChange={handleChange}
        {...field}
        {...props}
      />
      <h5 style={{marginLeft:'10px'}}>{content}</h5>
      </label>
      {withFeedbackLabel && (
        <FieldFeedbackLabel
          error={errors[field.name]}
          touched={touched[field.name]}
          label={label}
          type={type}
          customFeedbackLabel={customFeedbackLabel}
        />
      )}
    </>
  );
}

export function RadioJabatan({
  field, // { name, value, onChange, onBlur }
  form: { touched, errors, handleChange, values, setFieldValue }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
  label,
  key,
  content,
  withFeedbackLabel = true,
  customFeedbackLabel,
  type = "radio",
  ...props
}) {

  // console.log(props)

  const handleClick = (val) => {
    // console.log(val)
    setFieldValue('tipeJabatan',val);
  }
  return (
    <>
      {/* {label && <label className="col-xl-3 col-lg-3 col-form-label">{label}</label>} */}
      {/* <label key={key} className='form-check-label' style={{marginLeft: '20px'}}> */}
      <div className="row" style={{marginLeft: '20px'}}>
      <input
        type={type}
        className={getFieldCSSClasses(touched[field.name], errors[field.name], props.solid)}
        checked={values[field.name]}
        onChange={handleChange}
        onClick={()=>handleClick(props.jabatan)}
        {...field}
        {...props}
      />
      <h4 style={{marginLeft:'10px'}}>{content}</h4>
      {/* </label> */}
      </div>
      {withFeedbackLabel && (
        <FieldFeedbackLabel
          error={errors[field.name]}
          touched={touched[field.name]}
          label={label}
          type={type}
          customFeedbackLabel={customFeedbackLabel}
        />
      )}
    </>
  );
}
