import React, { useState, useEffect } from "react";
import "./Style.css";
const { SLICE_UPLOAD } = window.ENV;

export function CustomAudio({
  field, // { name, value, onChange, onBlur }
  form: { touched, errors, values, setFieldValue, setFieldTouched }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
  label,
  withFeedbackLabel = true,
  customFeedbackLabel,
  type = "text",
  classes,
  ...props
}) {
  const [touch, setTouch] = useState(undefined);
  const [file, setFile] = useState(undefined);
  const fileUpload = React.createRef();

  //   const showFileUpload = () => {
  //     if (fileUpload) {
  //       fileUpload.current.click();
  //     }
  //   };

  const handleClick = () => {
    setTouch(true);
    setFieldTouched("file", true);
    if (!file) {
      if (values.file_upload) {
        setFieldValue(field.name, {
          file: values ? values.file_upload : null,
          size: 150000000,
          type: ["audio/mpeg", "video/mp4", "video/mpeg"],
        });
      }
    }
  };

  // useEffect(() => {
  //   if (values.file_upload) {
  //     setFieldValue(field.name, {
  //       file: values.file_upload,
  //       size: 150000000,
  //       type: "video/mp4"
  //       // type: ["audio/mpeg","video/mp4","video/mpeg"]
  //     });
  //   }
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, [values.file_upload]);
  useEffect(() => {
    if (!file) {
      if (values.file_upload) {
        setFieldValue(field.name, {
          file: values ? values.file_upload : null,
          size: 150000000,
          type: ["audio/mpeg", "video/mp4", "video/mpeg"],
        });
      }
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [file, values.file_upload]);

  const handleFileChange = (e) => {
    e.preventDefault();
    let reader = new FileReader();
    let file = e.target.files[0];
    if (file) {
      reader.onloadend = () => {
        setFile(file);
      };
      reader.readAsDataURL(file);
      setFieldValue(field.name, file);
    }
  };

  //   const checkCaption = (error, file) => {
  //     let co = null;
  //     if (error && file === null) {
  //       co = null;
  //     } else if (error === undefined && file.props.children !== "folder") {
  //       co = (
  //         <div className="valid-feedback" style={{ display: "block" }}>
  //           File was entered correct
  //         </div>
  //       );
  //     } else {
  //       co = (
  //         <div className="invalid-feedback" style={{ display: "block" }}>
  //           {error}
  //         </div>
  //       );
  //     }
  //     return co;
  //   };

  const checkFileUpload = (error, touched) => {
    let co = null;
    if (!error && touched) {
      co = "active";
    } else if (error && touched) {
      co = "invalid";
    } else {
      co = "null";
    }
    return co;
  };

  const checkCapt = (error, touched, file = undefined) => {
    let co = null;
    if (touched && error) {
      co = (
        <div className="invalid-feedback" style={{ display: "block" }}>
          {error}
        </div>
      );
    } else if (touched && !error && label) {
      co = (
        <>
          <div className="valid-feedback" style={{ display: "block" }}>
            {label} was entered correct
          </div>
          {file === undefined ? (
            ""
          ) : (
            <>
              {" "}
              <span
                className="form-text text-muted"
                style={{ display: "block" }}
              >
                File Tersimpan: {/* {file.slice(SLICE_UPLOAD)} */}
                {file}
                {/* <a href={file} target="_blank" rel="noopener noreferrer">
                  {file}
                </a> */}
              </span>
            </>
          )}
        </>
      );
      // } else if (touched && !error && this.props.label && file){
      //   co = (
      //     <div className="valid-feedback" style={{ display: "block" }}>
      //       {this.props.label} was entered correct
      //     </div>
      //   );
    } else {
      //   co = null;
      co = (
        <>
          <span className="form-text text-muted" style={{ display: "block" }}>
            MP3 and MP4 only, up to 150MB
          </span>
          {file === undefined ? (
            ""
          ) : (
            <>
              {" "}
              <span
                className="form-text text-muted"
                style={{ display: "block" }}
              >
                File Tersimpan: {/* {file.slice(SLICE_UPLOAD)} */}
                {file}
                {/* <a href={file} target="_blank" rel="noopener noreferrer">
                  {file}
                </a> */}
              </span>
            </>
          )}
        </>
      );
    }
    return co;
  };

  return (
    <>
      <div
        className={`file-upload ${checkFileUpload(
          errors[field.name],
          touched[field.name]
        )}`}
        // className={`file-upload ${checkFileUpload(errors[field.name], touch)}`}
      >
        <div className="file-select">
          <div className="file-select-button">Choose File</div>
          <div className="file-select-name">
            {file
              ? file.name.length > 40
                ? file.name.slice(0, 15) + ".........." + file.name.slice(-10)
                : file.name
              : "No file chosen..."}
          </div>
          <input
            // className={classes.hidden}
            id={field.name}
            name={field.name}
            type="file"
            onChange={handleFileChange}
            ref={fileUpload}
            onBlur={handleClick}
            {...props}
          />
        </div>
      </div>
      {checkCapt(errors[field.name], touch, values.file_upload)}
    </>
  );
}
