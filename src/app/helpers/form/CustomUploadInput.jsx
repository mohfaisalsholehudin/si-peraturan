import React, { Component } from "react";
import { Icon } from "@material-ui/core";
import withStyles from "@material-ui/core/styles/withStyles";
import customFileInputStyle from "./CustomFileInputStyle";
import "./Style.css";
const { SLICE_URL, SLICE_FILE, BACKEND_URL } = window.ENV;

class CustomUploadInput extends Component {
  constructor(props) {
    super(props);
    this.fileUpload = React.createRef();
    this.showFileUpload = this.showFileUpload.bind(this);
    this.handleFileChange = this.handleFileChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
    // if(this.props.form.values.file_upload){
    //   console.log('masuk sini lho padahal')
    //   this.props.form.setFieldValue(this.props.field.name, {file: this.props.form.values.file_upload, size: 5000000, type: 'application/pdf'})
    // }
  }

  state = {
    file: undefined,
    touch: undefined
  };
  showFileUpload() {
    if (this.fileUpload) {
      this.fileUpload.current.click();
    }
  }

  handleClick() {
    this.setState({ touch: true });
    // this.props.form.setFieldValue(
    //   "alamat",
    //   this.props.form.values.alamat ? this.props.form.values.alamat : "alamat"
    // );
  }

  componentDidMount() {
    if (this.props.form.values.file_upload) {
      this.props.form.setFieldValue(this.props.field.name, {
        file: this.props.form.values.file_upload,
        size: 15000000,
        type: [
        // pdf
    "application/pdf",
    // xls
    "application/vnd.ms-excel",
    // xlsx
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    // doc
    "application/msword",
    // docx
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    // ppt
    "application/vnd.ms-powerpoint",
    // pptx
    "application/vnd.openxmlformats-officedocument.presentationml.presentation",
        ]
      });
    }
  }

  handleFileChange(e) {
    e.preventDefault();
    let reader = new FileReader();
    let file = e.target.files[0];
    if (file) {
      reader.onloadend = () => {
        this.setState({
          file: file
        });
      };
      reader.readAsDataURL(file);
      this.props.form.setFieldValue(this.props.field.name, file);
    }
  }

  showPreloadImage() {
    const { errorMessage, classes } = this.props;
    const { file, imagePreviewUrl } = this.state;

    let comp = null;

    if (errorMessage) {
      comp = <Icon style={{ fontSize: 36 }}>error_outline</Icon>;
    } else if (file) {
      comp = (
        <img className={classes.avatarThumb} src={imagePreviewUrl} alt="..." />
      );
    } else {
      comp = <Icon style={{ fontSize: 36 }}>folder</Icon>;
    }
    return comp;
  }

  //   componentDidMount() {
  //     // console.log(this.fileUpload.current);
  //   }
  checkCaption(error, file) {
    let co = null;

    if (error && file === null) {
      co = null;
    } else if (error === undefined && file.props.children !== "folder") {
      co = (
        <div className="valid-feedback" style={{ display: "block" }}>
          File was entered correct
        </div>
      );
    } else {
      co = (
        <div className="invalid-feedback" style={{ display: "block" }}>
          {error}
        </div>
      );
    }
    return co;
  }

  //   checkFileUpload(error) {
  //     let co = null;
  //     if (error === undefined && this.state.file === undefined) {
  //       co = null;
  //     } else if (error !== undefined && this.state.file !== undefined) {
  //       co = "invalid";
  //     } else {
  //       co = "active";
  //     }
  //     return co;
  //   }
  checkFileUpload(error, touched) {
    let co = null;
    if (!error && touched) {
      co = "active";
    } else if (error && touched) {
      co = "invalid";
    } else {
      co = "null";
    }
    return co;
  }

  checkCapt(error, touched, file = undefined) {
    let co = null;
    const temp = file ? file.slice(SLICE_FILE) : null;
    const url = BACKEND_URL + temp;

    if (touched && error) {
      co = (
        <div className="invalid-feedback" style={{ display: "block" }}>
          {error}
        </div>
      );
    } else if (touched && !error && this.props.label) {
      co = (
        <>
          <div className="valid-feedback" style={{ display: "block" }}>
            {this.props.label} was entered correct
          </div>
          {file === undefined ? (
            ""
          ) : (
            <>
              {" "}
              <span
                className="form-text text-muted"
                style={{ display: "block" }}
              >
                File Tersimpan:{" "}
                <a href={url} target="_blank" rel="noopener noreferrer">
                  {file.slice(SLICE_URL)}
                </a>
              </span>
            </>
          )}
        </>
      );
      // } else if (touched && !error && this.props.label && file){
      //   co = (
      //     <div className="valid-feedback" style={{ display: "block" }}>
      //       {this.props.label} was entered correct
      //     </div>
      //   );
    } else {
      //   co = null;
      co = (
        <>
          <span className="form-text text-muted" style={{ display: "block" }}>
            PDF, Word and Excel Only, up to 15MB
          </span>
          {file === undefined ? (
            ""
          ) : (
            <>
              {" "}
              <span
                className="form-text text-muted"
                style={{ display: "block" }}
              >
                File Tersimpan:{" "}
                <a href={url} target="_blank" rel="noopener noreferrer">
                  {file.slice(SLICE_URL)}
                </a>
              </span>
            </>
          )}
        </>
      );
    }
    return co;
  }

  render() {
    const {
      classes,
      form: { errors }
    } = this.props;
    const { name } = this.props.field;

    return (
      <>
        <div
          className={`file-upload ${this.checkFileUpload(
            errors[name],
            this.state.touch
          )}`}
        >
          <div className="file-select">
            <div className="file-select-button">Choose File</div>
            <div className="file-select-name">
              {this.state.file
                ? this.state.file.name.length > 40
                  ? this.state.file.name.slice(0, 15) +
                    ".........." +
                    this.state.file.name.slice(-10)
                  : this.state.file.name
                : "No file chosen..."}
            </div>
            <input
              className={classes.hidden}
              id={name}
              name={name}
              type="file"
              onChange={this.handleFileChange}
              ref={this.fileUpload}
              onBlur={this.handleClick}
              //   {...this.props.field}
              {...this.props}
              // onBlur={onBlur}
              //className="form-control"
            />
          </div>
        </div>
        {/* <input
            //   className={classes.hidden}
              id={name}
              name={name}
              type="file"
              onChange={this.handleFileChange}
              ref={this.fileUpload}
              onBlur={this.handleClick}
            //   {...this.props.field}
            //   {...this.props}
              // onBlur={onBlur}
              //className="form-control"
            /> */}

        {/* {this.checkCaption(errors[name], this.showPreloadImage())} */}
        {/* {this.checkCapt(errors[name], touched[name])} */}
        {this.checkCapt(
          errors[name],
          this.state.touch,
          this.props.form.values.file_upload
        )}
      </>
    );
  }
}

export default withStyles(customFileInputStyle)(CustomUploadInput);
