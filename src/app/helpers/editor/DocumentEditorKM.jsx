import React from "react";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import Editor from "ckeditor5-custom-build/build/ckeditor";
import "./decoupled.css";
const { BACKEND_URL } = window.ENV;

function DocumentEditorKM({content, setDraft, isReadOnly = false}) {

  return (
    <div className="row">
      <div className="document-editor">
        <div className="document-editor__toolbar"></div>
        <div className="document-editor__editable-container">
          <CKEditor
            onReady={(editor) => {
              window.editor = editor;
              // isReadOnly ? editor.isReadOnly= true : editor.isReadOnly= false;
              // isReadOnly ? editor.ui.view.toolbar.element.style.display = 'none' : editor.ui.view.toolbar.element.style.display = 'flex';
              // editor.view.toolbar.element.style.display = 'none'
              if(isReadOnly){
                editor.isReadOnly= true;
                editor.ui.view.toolbar.element.style.display = 'none';
              }

              // Add these two lines to properly position the toolbar
              const toolbarContainer = document.querySelector(
                ".document-editor__toolbar"
              );
              toolbarContainer.appendChild(editor.ui.view.toolbar.element);
              editor.editing.view.change((writer) => {
                writer.setStyle(
                  "width",
                  "100%",
                  editor.editing.view.document.getRoot()
                );
              });
            }}
            config={{
              simpleUpload: {
                // The URL that the images are uploaded to.
                uploadUrl: `${BACKEND_URL}/api/uploadckeditor`,

                // Enable the XMLHttpRequest.withCredentials property.
                withCredentials: false,

                // Headers sent along with the XMLHttpRequest to the upload server.
                headers: {
                  "Access-Control-Allow-Origin": "*",
                  "Access-Control-Allow-Credentials": "true"
                }
              },
            }}
            onChange={(event, editor) => {
              setDraft(editor.getData());
            }}
            editor={Editor}
            data={content ? content : null}
          />
        </div>
      </div>
    </div>
  );
}

export default DocumentEditorKM;
