import React from "react";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import Editor from "ckeditor5-custom-build/build/ckeditor";
import "./decoupled.css";
const { BACKEND_URL } = window.ENV;

function DocumentEditorUpdateKonten({ content, setDraft, isReadOnly = false }) {
  return (
          <CKEditor
            onReady={(editor) => {
              // window.editor = editor;
              // isReadOnly ? editor.isReadOnly= true : editor.isReadOnly= false;
              // isReadOnly ? editor.ui.view.toolbar.element.style.display = 'none' : editor.ui.view.toolbar.element.style.display = 'flex';
              // editor.view.toolbar.element.style.display = 'none'
              if (isReadOnly) {
                editor.isReadOnly = true;
                editor.ui.view.toolbar.element.style.display = "none";
              }
            }}
            config={{
              simpleUpload: {
                // The URL that the images are uploaded to.
                uploadUrl: `${BACKEND_URL}/api/uploadckeditor`,

                // Enable the XMLHttpRequest.withCredentials property.
                withCredentials: false,

                // Headers sent along with the XMLHttpRequest to the upload server.
                headers: {
                  "Access-Control-Allow-Origin": "*",
                  "Access-Control-Allow-Credentials": "true",
                },
                removePlugins: ["Heading", "Link"],
                toolbar: [],
                isReadOnly: true,
              },
            }}
            onChange={(event, editor) => {
              setDraft(editor.getData());
            }}
            editor={Editor}
            data={content ? content : null}
          />
  );
}

export default DocumentEditorUpdateKonten;
